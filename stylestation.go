package main

import (
	"encoding/json"
	"fmt"
	"github.com/deckarep/golang-set"
	"github.com/disintegration/imaging"
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin/binding"
	"github.com/gorilla/sessions"
	"github.com/jmcvetta/neoism"
	"github.com/jmcvetta/randutil"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/facebook"
	"github.com/markbates/goth/providers/gplus"
	"github.com/markbates/goth/providers/twitter"
	"github.com/ngerakines/ginpongo2"

	mandrill "github.com/keighl/mandrill"
	"image/jpeg"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"
)

// var DB gorm.DB

var r *regexp.Regexp

/*
	Regexps for extracting url sections.
	Regexps in Go are quite slow.
	TODO: Maybe switch to using a more efficient method
*/
var GENDER_REGEXP = regexp.MustCompile(`g:(\w+)`)
var STYLE_REGEXP = regexp.MustCompile(`s:(\w+)`)
var COLOUR_REGEXP = regexp.MustCompile(`c:(\w+)`)
var PAGE_REGEXP = regexp.MustCompile(`p:(\d+)`)
var BRAND_REGEXP = regexp.MustCompile(`b:(\w+)`)
var COLLECTION_REGEXP = regexp.MustCompile(`w:(\w+)`)
var FAVOURITED_REGEXP = regexp.MustCompile(`f:(\w+)`)

var ENTRIES_PER_PAGE int64
var THUMBNAIL_FILES = []string{"_t", "_m", "_l"}
var THUMBNAIL_SIZES = []int{300, 600, 900}
var ALL_GENDERS = []string{"Male", "Female"}
var GENDERS = mapset.NewSetFromSlice(ConvertToInterface(ALL_GENDERS))
var ALL_STYLES = []string{"Blazer", "Trousers", "Top", "Shorts", "Dress", "Skirt", "Accessories", "Kaftan", "Jumpsuit"}
var STYLES = mapset.NewSetFromSlice(ConvertToInterface(ALL_STYLES))
var ALL_COLOURS = []string{"Red", "Orange", "Blue", "Gold", "Silver", "Yellow", "Brown", "Black", "Purple", "Pink", "Green", "White"}
var COLOURS = mapset.NewSetFromSlice(ConvertToInterface(ALL_COLOURS))
var PROVDIERS = map[string]bool{"facebook": true, "twitter": true, "gplus": true}

const SESSION_NAME string = "MYHAIRSTYLES"

var db *neoism.Database
var store *sessions.CookieStore
var config Config
var mandrillClient *mandrill.Client

func init() {
	ENTRIES_PER_PAGE = 24
	mandrillClient = mandrill.ClientWithKey(config.MandrillKey)
	r, _ = regexp.Compile("bot|crawler|baiduspider|80legs|ia_archiver|voyager|curl|wget|yahoo! slurp|mediapartners-google")

}

// func RequireAuth(c *gin.Context) {
// 	userName :=
// 	c.Next()
// }
func SessionMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		currentSession, _ := store.Get(c.Request, SESSION_NAME)
		c.Set("session", currentSession)
		// currentSession.
		userId := currentSession.Values["userId"]
		displayName := currentSession.Values["displayName"]
		hasAccess := currentSession.Values["hasAccess"]
		c.Set("hasAccess", hasAccess)
		fmt.Printf("\nSession: %#v\n", currentSession.Values)
		c.Set("currentUser", userId)
		c.Set("displayName", displayName)
		c.Next()
		// currentSession.Save(c.Request, c.Writer)
	}

}
func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		hasAccess, _ := c.Get("hasAccess")
		if hasAccess == nil {
			c.Redirect(302, "/restricted")
		} else {
			c.Next()
		}
	}
}
func ResponseFormatMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.Request.Header.Get("Content-Type") == "application/json" {
			c.Set("format", "json")
		} else {
			c.Set("format", "")
		}
		c.Next()
	}
}
func SetUpAuthProviders() {

	goth.UseProviders(
		twitter.New(config.TwitterKey, config.TwitterSecret, fmt.Sprintf("%s/callback?provider=twitter", config.Hostname)),
		facebook.New(config.FacebookClientID, config.FacebookClientSecret, fmt.Sprintf("%s/callback?provider=facebook", config.Hostname)),
		gplus.New(config.GoogleClientID, config.GoogleClientSecret, fmt.Sprintf("%s/callback?provider=gplus", config.Hostname)),
	)
}

func main() {
	/* Load Config from config file */
	config = LoadConfig("conf.gcfg")
	/* Initialize cookie backed session store */
	store = sessions.NewCookieStore([]byte(config.CookieString))

	/* Connect to Database */
	dbLink := fmt.Sprintf("http://%s:%s@%s:%s/db/data", config.DatabaseUser, config.DatabasePassword, config.DatabaseHost, config.DatabasePort)
	fmt.Printf("\n\n DBLink: %#v \n\n", dbLink)
	db, _ = neoism.Connect(dbLink)

	SetUpAuthProviders()
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.Use(ginpongo2.Pongo2())
	// r.Use(static.Serve("/static"))
	// r.LoadHTMLTemplates("templates/*")
	// r.LoadHTMLGlob("templates/*")

	/* Use Session Middleware and Response Format Middleware */
	r.Use(SessionMiddleWare())
	r.Use(ResponseFormatMiddleWare())

	/* SET UP ROUTES */
	r.GET("/", ShowHomePage)
	r.GET("/restricted", ShowPPPage)
	r.POST("/access", AllowAccess)
	r.GET("/explore", Search)
	r.GET("/new", GetUploadPage)

	r.GET("/about", AboutPage)
	r.GET("/contact", ContactPage)
	r.POST("/contact", SendContact)
	r.GET("/terms", Terms)
	/* Start Login/Register Routes*/
	r.GET("/login", OAuthLogin)
	r.GET("/logout", Logout)
	r.GET("/register", ShowLoginPage)
	r.POST("/search", Search)
	r.GET("/callback", OAuthCallback)
	/* End Login/Register Routes */
	r.GET("/email", SendEmail)
	r.GET("/me", GetOwnProfile)
	r.PUT("/me", UpdateOwnProfile)
	r.DELETE("/me", DeleteProfile)

	r.GET("profile/:ref/collections", GetCollectionsForOwner)
	r.GET("/profile/:ref", GetUserProfile)

	r.GET("/styles/:id", GetMultiple)
	r.GET("/a/:ref", GetAnkara)
	r.GET("/ankara/:ref", StaticStyle)
	r.POST("/ankara", PostAnkara)
	r.PUT("/a/:ref", UpdateAnkara)

	r.POST("/collections", CreateCollection)
	r.POST("/collections/:collection/:entry", AddItemToUserCollection)
	r.GET("/collections", SearchCollection)

	r.DELETE("/a/:ref", DeleteAnkara)
	r.POST("/favourite/:ref", PostFavourite)
	r.DELETE("/favourite/:ref", DeleteFavourite)
	r.GET("/dashboard/:ref", GetUserDashboard)
	// r.GET("/patch/profile", PatchProfile)
	r.GET("/related/:ref", GetRelated)
	r.GET("edit/:ref", GetAnkara)
	// r.GET("/about", AboutPages)
	r.POST("/imageprocessor/entry", ProcessImage)

	//Listen on port configured in conf.gcfg
	http.ListenAndServe(config.Port, r)

}
func ShowPPPage(c *gin.Context) {
	/* Show this page when user does not have access
	Used for preventing unauthorized people from accessing the page
	*/
	hasAccess, _ := c.Get("hasAccess")
	if hasAccess == nil {
		c.Set("template", "templates/access.html")
	} else {
		c.Redirect(302, "/")
	}
}
func AllowAccess(c *gin.Context) {
	/* This function allows us to give access to a user if they've provided the
	correct code */
	allowedCodes := map[string]struct{}{
		"ChillyKangaroo":    {},
		"FieryKoala":        {},
		"FiestyMarshmallow": {},
		"BreatheFashion":    {},
		"FashionableLife":   {},
	}
	code := c.Request.FormValue("code")
	if _, ok := allowedCodes[code]; ok {
		currentSession, _ := store.Get(c.Request, SESSION_NAME)
		currentSession.Values["hasAccess"] = true
		currentSession.Save(c.Request, c.Writer)
		c.Redirect(302, "/")
	} else {
		c.Set("template", "templates/access.html")
	}
}

/*
DASHBOARD
*/
// var LIMIT_DASHBOARD_ITEMS = 5

func GetUserDashboard(c *gin.Context) {
	userId := c.Params.ByName("ref")
	// dashboard := ProfileDashboard{Ref: userId}
	dashboard := PopulateDashboard(userId)
	c.JSON(200, gin.H{"dashboard": dashboard})
}
func GetAnkara(c *gin.Context) {
	ref := c.Params.ByName("ref")
	ankara := GetSingleAnkara(ref, true)
	if ankara.Ref != "" {
		currentUser, _ := c.Get("currentUser")
		displayName, _ := c.Get("displayName")
		if currentUser != nil {
			ankara.Edit = currentUser.(string) == ankara.OwnerRef
			ankara.IsInFavourites = ankara.IsInUsersFavourites(currentUser.(string))
		}
		relatedEntries := GetRelatedAnkara(ref)
		format, _ := c.Get("format")
		if format.(string) == "json" {
			c.JSON(200, ankara)
			return
		} else {
			// var current
			bootstrap, err := json.Marshal(map[string]interface{}{"entry": ankara, "user": currentUser.(string), "displayName": displayName, "relatedEntries": relatedEntries})
			if err != nil {
				fmt.Printf("\nError: %#v\n", err)
			}
			c.Set("template", "templates/index.html")
			c.Set("data", map[string]interface{}{"bootstrap": string(bootstrap), "user": currentUser.(string), "displayName": displayName})
		}
	} else {
		c.JSON(500, gin.H{"message": "Style not found"})
	}
	return
}
func GetMultiple(c *gin.Context) {
	pageString := c.Params.ByName("id")
	page, _ := strconv.ParseInt(pageString, 10, 64)
	currentUser, _ := c.Get("currentUser")
	results := GetMultipleAnkaraFromDB((page - 1) * ENTRIES_PER_PAGE)
	for index, _ := range results {
		if currentUser == nil {
			results[index].Edit = false
		} else {
			results[index].Edit = (results[index].OwnerRef == currentUser.(string))
		}
	}
	c.JSON(200, results)

}

func GetCollectionsForOwner(c *gin.Context) {
	owner := c.Params.ByName("ref")
	pageString := c.Params.ByName("page")
	page, _ := strconv.ParseInt(pageString, 10, 64)
	if owner == "mine" {
		currentUser, _ := c.Get("currentUser")
		if currentUser != nil {
			owner = currentUser.(string)
		}
	}
	userCollections := &UserCollections{Ref: owner}
	userCollections.PopulateCollections((page-1)*ENTRIES_PER_PAGE, ENTRIES_PER_PAGE)

	c.JSON(200, userCollections)
}
func UpdateAnkara(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	ref := c.Params.ByName("ref")
	ankara := GetSingleAnkara(ref, false)
	if currentUser.(string) != ankara.OwnerRef {
		c.Fail(400, nil)
		return
	}
	editedAnkara := &Ankara{}
	c.Bind(editedAnkara)
	if len(editedAnkara.Colours) == 0 || len(editedAnkara.Styles) == 0 || (editedAnkara.Description == "") {
		c.Fail(400, nil)
		return
	}
	ankara.Update(editedAnkara)
}
func PostAnkara(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser == nil {
		c.JSON(403, gin.H{"code": 403, "message": "Not allowed"})
		return
	}
	ankara := &Ankara{}
	c.Bind(ankara)
	if len(ankara.Colours) == 0 || len(ankara.Styles) == 0 || (ankara.Description == "") {
		c.Fail(400, nil)
		return
	}
	ankara.Ref, _ = randutil.String(10, randutil.Alphabet)
	ankara.Create(currentUser.(string))
	return
}
func GetRelated(c *gin.Context) {
	ref := c.Params.ByName("ref")
	relatedEntries := GetRelatedAnkara(ref)
	c.JSON(200, relatedEntries)
	return
}
func SaveWithoutPublishing(c *gin.Context) {
	return
}

func EditAnkara(c *gin.Context) {
	return
}

func DeleteAnkara(c *gin.Context) {
	ref := c.Params.ByName("ref")
	currentUser, _ := c.Get("currentUser")
	if currentUser == nil {
		c.JSON(403, gin.H{"code": 403, "message": "Not allowed"})
		return
	}
	ankara := GetSingleAnkara(ref, false)
	if currentUser.(string) != ankara.OwnerRef {
		c.Fail(400, nil)
		return
	}
	ok := ankara.Delete()
	if !ok {
		c.JSON(500, gin.H{"code": 500, "message": "An error occured while performing that operation"})
	} else {
		c.JSON(200, gin.H{"code": 200, "message": "Entry successfully deleted."})
	}
}

func Search(c *gin.Context) {
	searchQuery := SearchQuery{
		Colours:      ConvertToInterface(ALL_COLOURS),
		Styles:       ConvertToInterface(ALL_STYLES),
		Gender:       ConvertToInterface(ALL_GENDERS),
		FavouritedBy: "",
		Collection:   "",
		Owner:        "",
		Page:         1,
	}

	format, _ := c.Get("format")
	if format.(string) == "json" {
		c.Bind(&searchQuery)
		if len(searchQuery.Colours) == 0 {
			searchQuery.Colours = ConvertToInterface(ALL_COLOURS)
		}
		if len(searchQuery.Styles) == 0 {
			searchQuery.Styles = ConvertToInterface(ALL_STYLES)
		}
		if len(searchQuery.Gender) == 0 {
			searchQuery.Gender = ConvertToInterface(ALL_GENDERS)
		}
		if searchQuery.Page < 1 {
			searchQuery.Page = 1
		}
		matchingEntries := FetchMatchingAnkara(searchQuery)
		c.JSON(200, matchingEntries)
		return
	} else {
		queryString := c.Request.URL.Query().Get("q")
		styleMatches := []string{}
		colourMatches := []string{}
		genderMatches := []string{}

		for _, v := range STYLE_REGEXP.FindAllStringSubmatch(queryString, -1) {
			styleMatches = append(styleMatches, v[1])
		}
		if len(styleMatches) > 0 {
			searchQuery.Styles = ConvertToInterface(styleMatches)
		}
		for _, v := range GENDER_REGEXP.FindAllStringSubmatch(queryString, -1) {
			genderMatches = append(genderMatches, v[1])
		}
		if len(genderMatches) > 0 {
			searchQuery.Gender = ConvertToInterface(genderMatches)
		}
		for _, v := range COLOUR_REGEXP.FindAllStringSubmatch(queryString, -1) {
			colourMatches = append(colourMatches, v[1])
		}
		if len(colourMatches) > 0 {
			searchQuery.Colours = ConvertToInterface(colourMatches)
		}
		page := PAGE_REGEXP.FindStringSubmatch(queryString)
		if len(page) > 0 {
			intPage, err := strconv.ParseInt(page[1], 0, 64)
			if err == nil {
				searchQuery.Page = intPage
			}
		}
		favouritedBy := FAVOURITED_REGEXP.FindStringSubmatch(queryString)
		if len(favouritedBy) > 0 {
			searchQuery.FavouritedBy = favouritedBy[1]
		} else {
			collection := COLLECTION_REGEXP.FindStringSubmatch(queryString)
			if len(collection) > 0 {
				searchQuery.Collection = collection[1]
			} else {
				owner := BRAND_REGEXP.FindStringSubmatch(queryString)
				if len(owner) > 0 {
					searchQuery.Owner = owner[1]
				}
			}
		}
		fmt.Printf("\n\nSearch Query: %#v\n\n\n", searchQuery)
		matchingEntries := FetchMatchingAnkara(searchQuery)

		//For Brands
		// for _, v := range COLOUR_REGEXP.FindAllStringSubmatch(queryString, -1) {
		// 	ownerMatches = append(colourMatches, v[1])
		// }
		currentUser, _ := c.Get("currentUser")
		displayName, _ := c.Get("displayName")
		fmt.Printf("\n%#v\n%#v\n%#v\n", styleMatches, colourMatches, queryString)
		bootstrap, err := json.Marshal(map[string]interface{}{"entries": matchingEntries})
		if err != nil {
		}
		c.Set("template", "templates/index.html")
		c.Set("data", map[string]interface{}{"bootstrap": string(bootstrap), "user": currentUser, "displayName": displayName})
	}
}

/*========================================*/
//Home Page stuff

func ShowExplorePage(c *gin.Context) {

}
func ShowHomePage(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})
	c.Set("template", "templates/homepage.html")
}

func GetUploadPage(c *gin.Context) {
	session, _ := store.Get(c.Request, SESSION_NAME)
	session.Values["last_time"] = time.Now().String()
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})
	c.Set("template", "templates/index.html")
}

/*========================================*/
func ProcessImage(c *gin.Context) {
	file, _, err := c.Request.FormFile("file")
	// the FormFile function takes in the POST input id file
	defer file.Close()
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
		return
	}
	stem, _ := randutil.String(12, randutil.Alphanumeric)
	rawFile := fmt.Sprintf("%s%s_raw.jpg", config.AnkaraDir, stem)
	out, err := os.Create(rawFile)
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
		return
	}
	_, err = io.Copy(out, file)
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
		return
	}
	out.Close()
	rawImage, err := imaging.Open(rawFile)
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
		return
	}
	medium := imaging.Resize(rawImage, 800, 0, imaging.Lanczos)
	err = imaging.Save(medium, fmt.Sprintf("%s%s_m.jpg", config.AnkaraDir, stem))
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
		return
	}
	imageFile, err := os.Open(fmt.Sprintf("%s%s_m.jpg", config.AnkaraDir, stem))
	if err != nil {
		c.JSON(500, gin.H{"message": "An error occured while processing the image. Please try again"})
	}
	defer imageFile.Close()
	imageProps, _ := jpeg.DecodeConfig(imageFile)
	go func() {
		thumbnail := imaging.Resize(rawImage, 450, 0, imaging.Lanczos)
		err = imaging.Save(thumbnail, fmt.Sprintf("%s%s_t.jpg", config.AnkaraDir, stem))
		large := imaging.Resize(rawImage, 1200, 0, imaging.Lanczos)
		err = imaging.Save(large, fmt.Sprintf("%s%s_l.jpg", config.AnkaraDir, stem))
	}()
	c.JSON(200, gin.H{"image": stem, "height": imageProps.Height, "width": imageProps.Width})
}

//Auth and Login
func ShowLoginPage(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	// Redirect user if already logged in.
	if currentUser != nil {
		c.Redirect(302, "/")
		return
	}
	c.Set("template", "templates/index.html")
}

func OAuthLogin(c *gin.Context) {
	/* Logs in a user via oauth, currently, only Google, Twitter and Facebook logins are supported.*/
	currentUser, _ := c.Get("currentUser")
	// Redirect user if already logged in.
	if currentUser != nil {
		c.Redirect(302, "/")
		return
	}
	provider := c.Request.FormValue("provider")
	fmt.Printf("Provider: %s", provider)
	if provider == "" {
		c.Set("template", "templates/login.html")
		c.Set("data", map[string]interface{}{})
		return
	}
	gothic.BeginAuthHandler(c.Writer, c.Request)
}

func OAuthCallback(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser != nil {
		c.Redirect(302, "/")
		return
	}
	provider := c.Request.URL.Query().Get("provider")
	if _, ok := PROVDIERS[provider]; ok {

		authUser, _ := gothic.CompleteUserAuth(c.Writer, c.Request)
		fmt.Printf("\nAuth User: %#v\n", authUser)
		//  Email:"fortheloveofremy@yahoo.com", Name:"Loveof RemyHair",
		// NickName:"Loveof RemyHair", Description:"", UserID:"409625945859073",
		// AvatarURL:"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/c14.14.180.180/s50x50/22003_145287122292958_765030765_n.jpg?oh=2018076117f5b70d56ac2dad3e75b27c&oe=54E6D104&__gda__=1423594497_9caf7c5547add81fbba273296763de9f", Location:"", AccessToken:"CAAVREhrFjYYBAI6dR5dxqEBgf0s5ReSXxnL7yQ2YX5743gSUBTTVzB1GWchIhlDFNjNUrUTaLtT4V9PUWZBM6fQO4Bv40TUBZCmi7kW78KKyIYZC84FAkstIiwnyRCwiZBxyALWXuMiWkKWQXfkfZA2Ar93FTRn3kBKZCzWRFrlwp1G7VPuIFKm8MaUtthldG7RTq1ZAccOSfM8kZAkIqZCog"
		stylestationUser := &StyleStationUser{Email: authUser.Email, AuthId: authUser.UserID, DisplayName: authUser.Name}
		stylestationUser.Ref, _ = randutil.String(8, randutil.Alphanumeric)
		stylestationUser.Provider = provider
		stylestationUser.Save()
		currentSession, _ := store.Get(c.Request, SESSION_NAME)

		currentSession.Values["userId"] = stylestationUser.Ref
		currentSession.Values["displayName"] = stylestationUser.DisplayName
		currentSession.Save(c.Request, c.Writer)
		c.Redirect(302, "/")
	}
}
func Logout(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser == nil {
		c.Redirect(302, "/")
		return
	}
	currentSession, _ := store.Get(c.Request, SESSION_NAME)
	currentSession.Values["userId"] = nil
	currentSession.Values["displayName"] = nil
	currentSession.Save(c.Request, c.Writer)
	c.Redirect(302, "/")
	return
}

//========================================

//Favourites
func PostFavourite(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	ankaraRef := c.Params.ByName("ref")
	if currentUser == "" || ankaraRef == "" {
		c.JSON(500, "Not allowed")
		return
	}
	err := AddFavouriteToDB(ankaraRef, currentUser.(string))
	if err != nil {
		c.JSON(500, "Whoops something bad happened!! Please try again.")
	} else {
		c.JSON(200, "Style added to favourites")
	}
	return
}

func DeleteFavourite(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	ankaraRef := c.Params.ByName("ref")
	if currentUser == "" || ankaraRef == "" {
		c.JSON(500, "Not allowed")
		return
	}
	err := DeleteFavouriteFromDB(ankaraRef, currentUser.(string))
	if err != nil {
		c.JSON(500, "Whoops something bad happened!! Please try again.")
	} else {
		c.JSON(200, "Style removed from favourites")
	}
	return
}

func GetFavourites(c *gin.Context) {
	return
}

/*
*
	PROFILE
*
*/

func GetUserProfile(c *gin.Context) {
	profileId := c.Params.ByName("ref")
	fmt.Printf("\nUserID: %v\n", profileId)
	// currentUser, _ := c.Get("currentUser")
	// No need to do this now, since we have a separate endpoint for /me
	// if currentUser == profileId {
	// 	c.Redirect(302, "/me")
	// 	return
	// }
	profile := FetchProfileFromDB(profileId)
	profile.ContactEmail = ""
	profile.PhoneNumber = ""
	format, _ := c.Get("format")
	if format.(string) == "json" {
		c.JSON(200, profile)
	} else {
		bootstrap, err := json.Marshal(map[string]interface{}{"profile": profile})
		if err != nil {
			// fmt.Printf("\nError: %#v\n", err)
		}
		currentUser, _ := c.Get("currentUser")
		displayName, _ := c.Get("displayName")
		c.Set("template", "templates/index.html")
		user := ""
		if currentUser != nil {
			currentUser = currentUser.(string)
		}
		if displayName != nil {
			displayName = displayName.(string)
		}
		c.Set("data", map[string]interface{}{"bootstrap": string(bootstrap),
			"user":        user,
			"displayName": displayName.(string)})
	}

}
func GetOwnProfile(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	if currentUser.(string) == "" {
		c.Redirect(302, "/")
		return
	}
	profile := FetchProfileFromDB(currentUser.(string))
	profile.IsEditable = true
	format, _ := c.Get("format")
	if format.(string) == "json" {
		c.JSON(200, profile)
	} else {
		bootstrap, err := json.Marshal(map[string]interface{}{"profile": profile, "user": currentUser.(string)})
		if err != nil {
			// fmt.Printf("\nError: %#v\n", err)
		}
		c.Set("template", "templates/index.html")
		c.Set("data", map[string]interface{}{"bootstrap": string(bootstrap), "user": currentUser.(string), "displayName": displayName.(string)})
	}
}
func UpdateOwnProfile(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser.(string) == "" {
		c.Redirect(302, "/")
		return
	}
	profile := ProfileDisplay{}
	c.Bind(&profile)
	if profile.IsValid() {
		profile.Update(currentUser.(string))
	}
	// c.Update()

}
func PatchProfile(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser == "" {
		c.JSON(500, "Not allowed")
		return
	}
	err := EditProfile(currentUser.(string))
	if err != nil {
		c.JSON(500, "An error occurred while updating profile")
	} else {
		c.JSON(200, "Profile saved.")
	}
	return
}

/*
 Use this to delete profile.
*/
func DeleteProfile(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")
	if currentUser == "" {
		c.JSON(500, "Not allowed")
		return
	}
	err := EditProfile(currentUser.(string))
	if err != nil {
		c.JSON(500, "An error occurred while updating profile")
	} else {
		c.JSON(200, "Profile saved.")
	}
	return
}

func AboutPage(c *gin.Context) {
	c.Set("template", "templates/aboutus.html")
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})

	// c.HTML(200, "aboutus", nil)
}

func ContactPage(c *gin.Context) {
	c.Set("template", "templates/contactus.html")
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})
}
func SendContact(c *gin.Context) {
	c.Set("template", "templates/contactus.html")
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})
}
func Terms(c *gin.Context) {
	c.Set("template", "templates/termsofservicefinal.html")
	currentUser, _ := c.Get("currentUser")
	displayName, _ := c.Get("displayName")
	c.Set("data", map[string]interface{}{"bootstrap": nil, "user": currentUser, "displayName": displayName})
}

/*
	End Profile Section
*/

/*
	COLLECTIONS
*/

func CreateCollection(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")

	/* Check user is logged in */
	if currentUser == "" {
		c.JSON(500, "Not allowed")
		return
	}
	_ = c.Request.ParseForm()
	collectionName := c.Request.FormValue("name")
	isHidden := c.Request.FormValue("hidden") != ""
	if collectionName == "" {
		c.JSON(500, "No name given")
	}
	// if err != nil {
	fmt.Printf("\nRequest: %#v\n", collectionName)
	// }
	success := CreateUserCollection(currentUser.(string), collectionName, isHidden)
	if !success {
		c.JSON(500, "An error occurred")
	} else {
		c.JSON(200, "Successfully created")
	}
}

func DeleteCollection(c *gin.Context) {

}
func RemoveItemFromCollection(c *gin.Context) {

}
func AddItemToUserCollection(c *gin.Context) {
	currentUser, _ := c.Get("currentUser")

	/* Check user is logged in */
	if currentUser == "" {
		c.JSON(500, "Not allowed")
		return
	}
	collection := c.Params.ByName("collection")
	entry := c.Params.ByName("entry")
	if collection != "" && entry != "" {
		CreateUserCollectionItem(currentUser.(string), collection, entry)
	}
	/* Check user owns collection */
	/* Check item exists */
	/* Add item */
}

func SearchCollection(c *gin.Context) {
	collectionQuery := CollectionQuery{
		OwnedBy: "",
		Styles:  ConvertToInterface(ALL_STYLES),
		Colours: ConvertToInterface(ALL_COLOURS),
		Gender:  ConvertToInterface(ALL_GENDERS),
		Brand:   "",
		Page:    1,
	}

	format, _ := c.Get("format")
	if format.(string) == "json" {
		c.Bind(&collectionQuery)
		if len(collectionQuery.Colours) == 0 {
			collectionQuery.Colours = ConvertToInterface(ALL_COLOURS)
		}
		if len(collectionQuery.Styles) == 0 {
			collectionQuery.Styles = ConvertToInterface(ALL_STYLES)
		}
		if len(collectionQuery.Gender) == 0 {
			collectionQuery.Gender = ConvertToInterface(ALL_GENDERS)
		}
		if collectionQuery.Page < 1 {
			collectionQuery.Page = 1
		}
		matchingEntries := FetchMatchingCollections(collectionQuery)
		c.JSON(200, matchingEntries)
		return
	} else {
		queryString := c.Request.URL.Query().Get("q")
		styleMatches := []string{}
		colourMatches := []string{}

		for _, v := range STYLE_REGEXP.FindAllStringSubmatch(queryString, -1) {
			styleMatches = append(styleMatches, v[1])
		}
		if len(styleMatches) > 0 {
			collectionQuery.Styles = ConvertToInterface(styleMatches)
		}
		for _, v := range COLOUR_REGEXP.FindAllStringSubmatch(queryString, -1) {
			colourMatches = append(colourMatches, v[1])
		}
		if len(colourMatches) > 0 {
			collectionQuery.Colours = ConvertToInterface(colourMatches)
		}
		page := PAGE_REGEXP.FindStringSubmatch(queryString)
		if len(page) > 0 {
			intPage, err := strconv.ParseInt(page[1], 0, 64)
			if err == nil {
				collectionQuery.Page = intPage
			}
		}
		ownedBy := FAVOURITED_REGEXP.FindStringSubmatch(queryString)
		if len(ownedBy) > 0 {
			collectionQuery.OwnedBy = ownedBy[1]
		} else {
			owner := BRAND_REGEXP.FindStringSubmatch(queryString)
			if len(owner) > 0 {
				collectionQuery.Brand = owner[1]
			}
		}
		fmt.Printf("\n\nSearch Query: %#v\n\n\n", collectionQuery)
		matchingEntries := FetchMatchingCollections(collectionQuery)

		//For Brands
		// for _, v := range COLOUR_REGEXP.FindAllStringSubmatch(queryString, -1) {
		// 	ownerMatches = append(colourMatches, v[1])
		// }
		currentUser, _ := c.Get("currentUser")
		displayName, _ := c.Get("displayName")
		fmt.Printf("\n%#v\n%#v\n%#v\n", styleMatches, colourMatches, queryString)
		bootstrap, err := json.Marshal(map[string]interface{}{"collections": matchingEntries})
		if err != nil {
		}
		c.Set("template", "templates/index.html")
		c.Set("data", map[string]interface{}{"bootstrap": string(bootstrap), "user": currentUser, "displayName": displayName})
	}
}
func SendEmail(c *gin.Context) {
	SendWelcomeEmail("victor.akujobi@gmail.com")
}

/* SEO Pages */
func StaticStyle(c *gin.Context) {
	ref := c.Params.ByName("ref")
	if !r.MatchString(c.Request.UserAgent()) {
		c.Redirect(301, fmt.Sprintf("%s/#/a/%s", config.Hostname, ref))
		return
	}
	if ref == "" {
		return
	}
	ankara := GetSingleAnkara(ref, true)
	relatedEntries := GetRelatedAnkara(ref)
	c.HTML(200, "single.html", gin.H{"hostname": config.Hostname, "entry": ankara, "related": relatedEntries})
}
func StaticProfile(c *gin.Context) {

}

/* End SEO Pages */
