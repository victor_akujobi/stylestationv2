var gridify = require("./gridify")
var m = require("mithril")
var utils = require("./utils")
var fonts = require("./fonts")
var genders = require("./gender")
var tags = require("./tags")
var image = require("./image")
var colours = require("./colours")
var SSConfig = require("./config")
var animations = require("./animation")
var countUp = require("countup.js")
var Entry = require("./entry")
require("../SCSS/detail.scss")
require("../SCSS/hint.scss")
var countUpConfig = function (count, element, isInitialized, context) {
	if (!isInitialized) {
		var numAnim = new countUp(element, 1, count);
		numAnim.start();
	} else {
		if (count == context.count) {
			return
		} else {
			var numAnim = new countUp(element, context.count, count);
			numAnim.start();
		}
	}
	context.count = count
}
var animator = animations.animator
var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)
var detailPage = {}
detailPage.view = function (ctrl) {
	var relatedEntries = ctrl.relatedEntries() && ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries(), numPartitions: document.body.offsetWidth > 480 ? 1 : 2}) :<div class="loading_gif"></div>
	if (ctrl.entry()) {
		var entry = ctrl.entry()
		var url = document.location.origin + "/ankara/" + entry.Ref()
		// var colours = ent
		return <div class="ssdetail__wrapper">
				<div class="entry_wrapper">
				<div class="ssdetail__entry">
				<div class="ssdetail__image"><img class='image' src={"/static/images/ankara/"+entry.Image()+"_m.jpg"} /></div>
				<div class="sssharebar__wrapper">
					<p class="share_text">Psst... If you love this style, you can share it on </p>
					<a href={"http://www.facebook.com/sharer/sharer.php?u=" + url + "&title=" + entry.Description()} target="_blank" class="share_link facebook_share">
						{m.trust(fonts['facebook'])}
					</a>
					<a href={"http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url} target="_blank" class="share_link twitter_share">
						{m.trust(fonts['twitter'])}
					</a>
					<a href={"http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + entry.Description} target="_blank" class="share_link pinterest_share">
						{m.trust(fonts['pinterest'])}
					</a>
					<a target="_blank" class="share_link email_share">{m.trust(fonts['email'])}</a>
				</div>
				</div>
			<div class="ssdetail__info">
				<a href={entry.Edit ? "/me" : "/profile/" + entry.OwnerRef}> <p class="owner">{entry.OwnerName}</p></a>
				<p class="description">{entry.Description()}</p>
				<div class="stats">
				  <ul>
				    <li><span class="stats__stat" config={countUpConfig.bind(null, entry.Views)}></span><span class="stats__label stats__views">Views</span></li>
				    <li><span class="stats__stat" config={countUpConfig.bind(null, entry.Favourites())}></span><span class="stats__label stats__things">Favourite{entry.Favourites() == 1 ? "" : "s"}</span></li>
				  </ul>
				</div>
				<div style=" background-color:#fff; padding:0; box-shadow:0 0 2px rgba(0, 0, 0, 0.1); margin: 2em auto 0.5em auto" class="tag_wrapper">
					<p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 0.2em">Style</p>
					{entry.Styles().map(function (style) {
						return <p class="tag"><span class="tag_icon">{m.trust(fonts[style.toLowerCase()])}</span><span class="tag_text">{style}</span></p>
					})}
				</div>

				<div style="" class="colour_wrapper">
				<p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 1em">Colours</p>
					{entry.Colours().map(function (colour) {
						return <span  data-hint={colour} class="hint--bottom colour" style={{"width": ~~(90/entry.Colours().length || 1)+ "%", "background-color": colours.COLOURS[colour]}}></span>
					})}
				</div>
				<div>
				<p onclick={entry.Favourite} class={"favourite favourite--" + (entry.IsInFavourites() ? "added" : "false")}>{entry.IsInFavourites() ? "Added" : "+ Add"} To Favourites</p>
				</div>
			</div>
			</div>
			<p>We think you might like</p>
			<div class="related_entries">{relatedEntries}</div>
		</div>
	} else {
		return <div></div>
	}
}
// Share links courtesy of http://petragregorova.com/articles/social-share-buttons-with-custom-icons/
detailPage.controller = function (defaultOptions) {
	var ctrl = this
	ctrl.options = defaultOptions || {}
	ctrl.entry = m.prop(new Entry({}))
	console.log(window.bootstrap)
	ctrl.relatedEntries = m.prop([])
	if (!!window.bootstrap) {
		ctrl.entry = m.prop(new Entry(window.bootstrap.entry))
		ctrl.relatedEntries = m.prop(window.bootstrap.relatedEntries)
		window.bootstrap = false
	} else {
		var entryRef = ctrl.options.ref || "sxxeSLWUYS"
			m.request({url: "/related/"+ entryRef, method:"get"}).then(function (response) {
			ctrl.relatedEntries(response)
		}, function () {
			return []
		})
		m.request({url: "/a/"+ entryRef, method:"get", config:SSConfig.XHR}).then(function (response) {
			ctrl.entry(new Entry(response))
			console.log(ctrl.entry())
		}, function () {
			console.log("Error")
			return []
		})
	}
}
module.exports = detailPage