/** @jsx m */
var m = require("mithril")
require("../SCSS/login.scss")
var LoginPage = {}
LoginPage.controller = function () {
	// m.redraw.strategy('diff');
}
LoginPage.view = function () {
	return <div class="sslogin__wrapper">
<p class="sslogin__intro_text">Not logged in? Ok then, let&apos;s get you sorted.<br/> It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear</p>
<a href="/login?provider=facebook" class="srlogin__button srlogin__button__facebook">
	<span class="srlogin__provider__icon srlogin__provider__facebook">{m.trust("&#xE60F")}</span>
	<span class="srlogin__provider__text">Continue with your Facebook Account</span>
</a>
<a href="/login?provider=gplus" class="srlogin__button srlogin__button__google">
	<span class="srlogin__provider__icon srlogin__provider__google">{m.trust("&#xE610")}</span>
	<span class="srlogin__provider__text">Continue with your Google Account</span>
</a>
<a href="/login?provider=twitter" class="srlogin__button srlogin__button__twitter">
	<span class="srlogin__provider__icon srlogin__provider__twitter">{m.trust("&#xE60E")}</span>
	<span class="srlogin__provider__text">Continue with your Twitter Account</span>
</a>
</div>
}
module.exports = LoginPage