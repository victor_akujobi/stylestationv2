/** @jsx m */
'use strict';
var m = require("mithril")
require("../SCSS/image.scss")
var image = {}
var imageCache = new Image()
// imageCache.src = "/static/images/loading.gif"
image.onchange= function(vm, e) {
	// m.request(/)
	var file = e.target.files[0]
	var formData = new FormData
    // for (var i = 0; i < files.length; i++) {
        formData.append("file", file)
    // }
	// callback("jsdhjsh")
	vm.isLoading && vm.isLoading(true)
	m.redraw()
	return m.request({
        method: "POST",
        url: "/imageprocessor/entry",
        data: formData,
        //simply pass the FormData object intact to the underlying XMLHttpRequest, instead of JSON.stringify'ing it
        serialize: function(value) {return value}
    }).then(function (response) {
    	vm.entry.Image(response.image)
    	vm.entry.ImageHeight(response.height)
    	vm.entry.ImageWidth(response.width)
    	vm.isLoading && vm.isLoading(false)
    })
}
image.controller = function () {

}
image.view = function (vm) {
	var content;
	var imageInput = ""
	console.log(vm.isLoading())
	if (!!vm.isLoading()) {
		console.log("Inner")
		content = <div class="ssimage__loadingimage"></div>
	} else {
		imageInput = <input class="ssimage__imageinput" type="file" onchange={image.onchange.bind(null, vm)}/>
		if (!!vm.entry.Image()) {
			content = <div><img style="width:100%" src={"/static/images/ankara/"+vm.entry.Image()+"_m.jpg"}/><p class="ssimage__changephoto">change photo</p></div>
		} else {
			content = <div class="ssimage__noimage"><p class="ssimage__noimagetext">+ add a photo</p></div>
		}
	}
	return <div class="ssimage__wrapper">
	{content}
	{imageInput}
	</div>
}

module.exports = image