/** @jsx m */
var m = require("mithril")
require("../SCSS/utils.scss");
var fonts = require("./fonts")
var Popup = function () {
		var show = m.prop(true)
		var toggleDisplay = function () {
			show(!show())
		}
		var closePopup = function () {
			show(false)
		}
		var overlay = document.body.appendChild(document.createElement('div'))
		overlay.className = "overlay_hidden"
		return function (body) {
			overlay.className = "overlay_visible"
			m.render(overlay, body)
		}
}

var Panel = function (options) {
	var content = options.content
	if (!content) {return}
	var type = options.type || info
	var timeout = options.timeout || 6000
	var element = <div class={"srpanel__" + type}>{content}</div>
	var panelParent = document.createElement('div')
	panelParent.className = "srpanel";
	m.render(panelParent, element)
	document.body.appendChild(panelParent)
	window.setTimeout(function () {
		document.body.removeChild(panelParent)
	}, timeout)

}
var LoginPopup = function () {
	return <div class="sslogin__wrapper">
<p class="sslogin__intro_text">Not logged in? Ok then, let&apos;s get you sorted.<br/> It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear</p>
<a href="/login?provider=facebook" class="srlogin__button srlogin__button__facebook">
	<span class="srlogin__provider__icon srlogin__provider__facebook">{m.trust("&#xE60F")}</span>
	<span class="srlogin__provider__text">Continue with your Facebook Account</span>
</a>
<a href="/login?provider=gplus" class="srlogin__button srlogin__button__google">
	<span class="srlogin__provider__icon srlogin__provider__google">{m.trust("&#xE610")}</span>
	<span class="srlogin__provider__text">Continue with your Google Account</span>
</a>
<a href="/login?provider=twitter" class="srlogin__button srlogin__button__twitter">
	<span class="srlogin__provider__icon srlogin__provider__twitter">{m.trust("&#xE60E")}</span>
	<span class="srlogin__provider__text">Continue with your Twitter Account</span>
</a>
</div>
}

// var SearchPage = function (options) {
// 	return <div>
// 	<input type="checkbox" class="switch_toggle"/>
// 		<p>I am looking for</p>
// 		<p>Choose colours</p>
// 		<p>Choose styles</p>
// 		<p>for</p>
// 		<p>{Select({options:["Male", "Female", "Android"], selected:["Male", "Female"]})}</p>
// 	</div>
// }
var SearchPage = function (vm) {
	var onChange = function (section, option) {
		// console.log(section, option)
		vm.options[section][option](!vm.options[section][option]())
		vm.onChange()
		// m.redraw()
	}
	return <div class="sidebar__wrapper">
	        <label for="toggle_switch" class="toggle_label"></label>
	        <input class="toggle_switch" id="toggle_switch" type="checkbox"/>
	        <div id="search">
		          {
		          	Object.keys(vm.options).filter(function (section) {
		          		return section !== "Page"
		          	}).map(function (section, index) {
			          	return <div class="search_items_wrapper"><p class="search_header">{section}</p>{
			          		Object.keys(vm.options[section]).map(function (option, optionIndex) {
			          			console.log(section, option)
			          		iconFont = vm.options[section][option]() ? fonts['success'] : "-"
			          		return <p onclick={onChange.bind(null, section, option)} class={"search_item search_item_" + section.toLowerCase()}><span class="sstags__icon" config={vm.iconAnimate}>{m.trust(iconFont)}</span><label class="search_item_text" >{option}</label></p>
			          	})
			        }</div>
		          })
	          }
	          <label class="done_button" for="toggle_switch">Done</label>
	        </div>
	        <div></div>
	      </div>
}
SearchPage.prototype.toggle = function () {

}

var Select = function (state) {
	return <div tabindex="0"><span>{state.selected.join("/")}</span>
				{state.options.map(function (option) {
					return <p>{option}</p>
				})}
			</div>
}

module.exports = {Popup: Popup, LoginPopup: LoginPopup, Panel:Panel, Select:Select, SearchPage: SearchPage}