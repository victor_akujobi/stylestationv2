/** @jsx m */
var m = require("mithril")
console.log(m)
var animations = require("./animation")
var SSConfig = require("./config")
var animator = animations.animator
var Entry = require("./entry")
var page = require("page")
// console.log(snabbt)
// console.log(m)
var Middleware = function (module, options) {
	module.controller = module.controller.bind(null, options)
	return module
}
var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)
var slidingPage = animator(animations.slideIn, animations.slideOut, false)
require("../SCSS/main.scss")
require("../SCSS/detail.scss")
var genders = require("./gender")
var tags = require("./tags")
var UploadImage = require("./image").view
var colours = require("./colours")
// var partition = require("linear-partition")
var gridify = require("./gridify")
var utils = require("./utils")
var fonts = require("./fonts")
var Popup = utils.Popup()
var Panel = utils.Panel
var LoginPopup = utils.LoginPopup
var LoginPage = require("./login.js")
var SearchPage = utils.SearchPage
var ProfilePage = require("./profile.page.js")
var detailPage = require("./detail.page.js")
var NavBar = require("./nav_bar.js").view
var MenuBar = require("./menubar.js").view
var NUM_ENTRIES = 18
// var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
'use strict';
// var tagComponent = require('tags')
var error = m.prop("")
var searchQuery;
var resetSearchQuery = function () {
	searchQuery =  {
		Page:m.prop(1),
		Gender:{
			Male: m.prop(false),
			Female: m.prop(false)
		},
		Colours:{
			Red:m.prop(false),
			Orange:m.prop(false),
			Yellow:m.prop(false),
			Green:m.prop(false),
			Brown:m.prop(false),
			Purple:m.prop(false),
			Pink:m.prop(false),
			Blue:m.prop(false),
			Silver:m.prop(false),
			White:m.prop(false),
			Black:m.prop(false),
			Gold:m.prop(false)
		},
		Styles:{
			Top: m.prop(false),
			Trousers: m.prop(false),
			Shorts: m.prop(false),
			Blazer: m.prop(false),
			Accessories: m.prop(false),
			Dress: m.prop(false),
			Skirt: m.prop(false)
		}
	}
}
resetSearchQuery()

var STYLES = {
			"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt"],
			"Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories"]
		};

// var entry = {Gender: m.prop("Female"), Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([]), Image:m.prop(""), ImageWidth:m.prop(""), ImageHeight: m.prop("")}

var newPage = {}
newPage.controller = function (defaultOptions) {
	var ctrl = this;
	ctrl.options = defaultOptions || {}
	var savedEntry
	if (!!ctrl.options.editMode) {
		m.request({
			method: 'GET',
			url: '/a/' + ctrl.options.ref,
			config: SSConfig.XHR,
		}).then(function (response) {
			ctrl.vm.entry = new Entry(response)
			m.redraw()
		})
	} else {	
		var localStorage = window.localStorage
		savedEntry = JSON.parse(localStorage.getItem("entry"))
		if (!!savedEntry) {
			localStorage.removeItem("entry")
			localStorage.removeItem("redirect")
		}
		// if ctrl.options.edi
	}
		this.vm = {entry: new Entry(savedEntry), isLoadingImage: m.prop(false)}
	this.Send = function () {
		var endPoint = !!ctrl.options.editMode ? "/a/" + ctrl.options.ref : "/ankara"
		var method = !!ctrl.options.editMode ? "PUT" : "POST"
		m.request({url: endPoint, method: method, data:ctrl.vm.entry, "background": true}).then(
		function () {
			Panel({content:<div>Style added, looking good!!</div>, type:'success'})
			page("/")
		}, function (response) {
			if (!response) {
				var entry = ctrl.vm.entry
				if (!entry.Description()) {
					Panel({content:<div>Oh no, something bad happened. Hmm.., did you add a description</div>, type:'error'})
				} else if  (!entry.Image()) {
						Panel({content:<div>Oh no, something bad happened. Hmm.., did you add an image</div>, type:'error'})
				} else if (!entry.Colours().length) {
						Panel({content:<div>Oh no, something bad happened. Hmm.., did you select any colours</div>, type:'error'})
				} else {
						Panel({content:<div>Oh no, something bad happened. Hmm.., did you select any styles</div>, type:'error'})
				}
				return
			}
			switch (parseInt(response.code)) {
				case 403:
					var localStorage = window.localStorage
					localStorage.setItem("entry", JSON.stringify(ctrl.vm.entry))
					localStorage.setItem("redirect", "/new")
					Popup(LoginPopup())
					break
				default:
					Panel({content:<div>Oh no, something bad happened. Hmm.., are you logged in?</div>, type:'error'})
					break;
			}
		})
	}
}
newPage.view = function (ctrl) {
	var entryStyles = ctrl.vm.entry.Styles().reduce(function(dict, currentValue){dict[currentValue] = true; return dict}, {})
	var coloursComponent = colours.view({selected: ctrl.vm.entry.Colours(), onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Colours')})
	var styles = STYLES[ctrl.vm.entry.Gender()].map(function (style) {
				return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:ctrl.vm.entry.Gender().toLowerCase()||"female", onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Styles', style)})
			})
	return <div class="ssnewentry__wrapper">
				<div class="ssentry__image__wrapper">
					{UploadImage({entry:ctrl.vm.entry, isLoading: ctrl.vm.isLoadingImage})}
				</div>
				<div class="ssentry__details__wrapper">
					<input placeholder="Click to enter description" type="text" onchange={m.withAttr("value", ctrl.vm.entry.Description)} value={ctrl.vm.entry.Description()} class="ssentry__descriptioninput" />
					<div class="sstags__stylepickerwrapper"><h4 class="sstags__stylepicker">GENDER</h4>{genders.view({entry:ctrl.vm.entry, onclick:ctrl.vm.entry.Gender})} <br style="clear:both"/></div>
					<div class="sstags__stylepickerwrapper"><h4 class="sstags__stylepicker">STYLES</h4>{styles} <br style="clear:both"/></div>
					<div class="sstags__stylepickerwrapper"><h4 class="sstags__stylepicker">COLOURS</h4>{coloursComponent} <br style="clear:both"/></div>
					<button class="ssnewentry__submit" onclick={ctrl.Send}>Done</button>
				</div>
			</div>
}


// VIEW MULTIPLE MODULE

//
var viewMultiple = {}
viewMultiple.controller = function () {
	this.block = function (entry, style) {
		return <a href={"/a/"+entry.Ref}  class="ssgrid__innerblock"><img config={gridImageAnimation} style={style} src={"/static/images/ankara/"+entry.Image+"_t.jpg"}/></a>
	}
	var localStorage = window.localStorage
	var urlRedirect = localStorage.getItem("redirect")
	if (!!urlRedirect) {
		localStorage.getItem("redirect", false)
		m.route(urlRedirect)
	}
	var ctrl = this
	this.getNextPage = function () {
		var previousPage = searchQuery.Page() + 1
		searchQuery.Page(previousPage)
		ctrl.fetchData()
	}
	this.getPreviousPage = function () {
		var nextPage = searchQuery.Page() - 1
		searchQuery.Page(nextPage)
		ctrl.fetchData()
	}
	this.fetchData = function () {
		// var gender = 
		var q = Object.keys(searchQuery).filter(function (section) {return section !== "Page"}).reduce(function (obj, section) {
			var tmp = Object.keys(searchQuery[section])
			.filter(function (item) {
				return searchQuery[section][item]()
			})
			if (!!tmp.length) {
				obj[section] = tmp
			}
			return obj
		}, {})
		q.Page = searchQuery.Page()
		m.request({"url": "/search", "method": "POST", data:q, "background": true, config:SSConfig.XHR}).then(ctrl.vm.entries, function (response) {error(response.message)}).then(function () {m.redraw()})
	}
	if (!!window.bootstrap) {
		this.vm = {entries: m.prop(window.bootstrap.entries)}
		window.bootstrap = false
	} else {
		this.vm = {entries: m.prop([])}
		this.fetchData()
	}
}

viewMultiple.view = function (ctrl) {
	var search = SearchPage({options:searchQuery, iconAnimate:gridImageAnimation, onChange: ctrl.fetchData})
	var previousLink = null
	var nextLink = null
	if (searchQuery.Page() > 1) {
		previousLink = <a onclick={ctrl.getPreviousPage} class="prev_page">«</a>
	}
	if (ctrl.vm.entries().length == NUM_ENTRIES) {
		nextLink = <a onclick={ctrl.getNextPage} class="next_page">»</a>
	}
	if (!!ctrl.vm.entries().length) {
		// var entries = ctrl.vm.entries()
		// console
		return <div>
			{search}{gridify.view({"entries": ctrl.vm.entries(), animation:gridImageAnimation, view:ctrl.block})}
			{
				NavBar({
				showNext: ctrl.vm.entries().length == NUM_ENTRIES,
				showPrevious: searchQuery.Page() > 1,
				onClickNext: ctrl.getNextPage,
				onClickPrevious: ctrl.getPreviousPage
			})
			}
		</div>
	}
		return <div>
			{search}
			<p class="no_images_container">
				<span onclick={function () {resetSearchQuery(); page("/")}}  class="no_images_text">
					Oh noes!! We&apos;ve run out of images to show you. But never fear, tap or click &nbsp;
					<a href="/" config={m.route}>here</a>&nbsp;
					to go back to the main page
				</span>
			</p>
		</div>
}

// END VIEW MULTIPLE MODULE 


//ROUTE
// m.route.mode = 'pathname'
var API_ENDPOINT = ""
var updateRoute = (function () {
	var NAV_WRAPPER = document.getElementById("nav_wrapper")
	return function (path, ctx, next) {
		NAV_WRAPPER.className = path
		next()
	}
})()

var loadUser = function () {

}
var APP_ROOT_CONTAINER = document.getElementById('content')
// m.route(document.getElementById('content'), '/', {
page('/a/:entry', updateRoute.bind(null, 'explore'), function (ctx, next) {
	var entry = ctx.params.entry
	console.log("entry: ", entry)
	// detailPage = gridImageAnimation(detailPage)
	m.module(APP_ROOT_CONTAINER, gridImageAnimation(Middleware(detailPage, {ref: entry})))
})

page('/new', updateRoute.bind(null, 'new'), function () {
	m.module(APP_ROOT_CONTAINER,  gridImageAnimation(newPage))
})
page('/profile/:ref', function (ctx) {
	var userId = ctx.params.ref
	m.module(APP_ROOT_CONTAINER,  gridImageAnimation(Middleware(ProfilePage, {ref: userId})))
})
 page('/me', updateRoute.bind(null, ''), function () {
 	m.module(APP_ROOT_CONTAINER, Middleware(ProfilePage, {editMode: true}))
})
page('/edit/:ref', updateRoute.bind(null, 'new'), function (ctx) {
	var entryId = ctx.params.ref
	// m.route = null
	m.module(APP_ROOT_CONTAINER, Middleware(newPage, {editMode: true, ref: entryId}))
})
page('/login', updateRoute.bind(null, ''), function () {
	m.module(APP_ROOT_CONTAINER, LoginPage)
})
page('/register', updateRoute.bind(null, ''), function () {
	m.module(APP_ROOT_CONTAINER, gridImageAnimation(LoginPage))
})
page('/' , updateRoute.bind(null, 'explore'), function () {
	m.module(APP_ROOT_CONTAINER, gridImageAnimation(viewMultiple))
})
page()
// })
var menu = MenuBar()
// m.render(document.getElementById("nav_menu"), menu)
//END ROUTING



//CREATE UPLOAD BUTTON
// var createButton = <a class="create_button" href="/new" config={m.route}>Upload</a>
// m.render(document.getElementById("create"), createButton)
// END CREATE BUTTON

