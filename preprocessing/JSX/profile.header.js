/** @jsx m */
var m = require("mithril")
var ProfileHeader = {}
require("../SCSS/profile.header.scss")
ProfileHeader.controller = {}
// var profile = {}
ProfileHeader.writable = function (vm) {
	var profile = vm.profile
	// return function (vm) {	
		return <div class="profile_header">
				<div class="profile_image"></div>
				<div class="profile_personal">
					<label for="display_name" style="display:block" class="display_name_label hint--rounded hint--left hint--always hint--success" data-hint="Edit Display Name"><input id="display_name" class="display_name"  onchange={m.withAttr("value", profile.DisplayName)} value={profile.DisplayName()}/></label>
					<label style="display:block" class="description_label hint--left hint--rounded hint--always hint--info" data-hint="Edit Bio"><textarea rows="3" onchange={m.withAttr("value", profile.Description)} class="description" >{profile.Description() || "Tell us a bit about yourself. Go on, don't be shy..."}</textarea></label>
					<label style="display:block" class="hint--left hint--rounded hint--always hint--warning" data-hint="Edit URL"><input class="url" onchange={m.withAttr("value", profile.Url)} value={profile.Url() || "Enter URL"}/></label>
					<br style="clear:both"/>
					<p><a class="update_link" onclick={profile.update}>Update my Details</a></p>
				</div>
				<div class="profile_info">
				</div>
				<br style="clear:both"/>
			</div>
	// }
}
ProfileHeader.readable = function (vm) {
	return <div class="profile_header">
			<div class="profile_image"></div>
			<div class="profile_personal">
				<p class="display_name">{vm.profile.DisplayName()}</p>
				<p class="description">{vm.profile.Description()}</p>
				<p class="url">{vm.profile.Url()}</p>
				<br style="clear:both"/>
			</div>
			<div class="profile_info">
			</div>
		</div>
}
ProfileHeader.view = function (vm) {
	var content
	if (!!vm.editMode) {
		content =  this.writable(vm)
	} else {
		content = this.readable(vm)
	}
	return <div class="profile_header__wrapper">
		{content}
		<div class={"profile_stats " + vm.section}>
			<p onclick={vm.onSectionChange.bind(null, "collections")} class="stat collections"><span class="stat__label">Collections</span><span class="stat__value">{vm.profile.Collections()}</span></p>
			<p onclick={vm.onSectionChange.bind(null, "uploads")} class="stat uploads"><span class="stat__label">Uploads</span><span class="stat__value">{vm.profile.Uploads()}</span></p>
			<p onclick={vm.onSectionChange.bind(null, "collections")} class="stat collections"><span class="stat__label">Views</span><span class="stat__value">{vm.profile.Collections()}</span></p>
			<p onclick={vm.onSectionChange.bind(null, "uploads")} class="stat uploads"><span class="stat__label">Favourites</span><span class="stat__value">{vm.profile.Uploads()}</span></p>
		</div>
	</div>
}
module.exports = ProfileHeader