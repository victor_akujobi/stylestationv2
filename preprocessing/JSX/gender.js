/** @jsx m */
// require("../SCSS/gender.scss")
var m = require("mithril")
var defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}
// gender
var genders = {}
genders.controller = function () {

}
genders.view = function (vm) {
	var entry = vm.entry;
	var genderView = Object.keys(defaultGenders).map(function (gender) {
		var isSelected = gender == entry.Gender()
		var icon= isSelected ? m.trust("&#xE60A;") : m.trust(defaultGenders[gender])
		var iconClass = isSelected ? "sstags__icon--selected" : ""
		var genderClass = "sstags__wrapper--" + gender.toLowerCase()
		return <div key={gender} onclick={vm.onclick.bind(null, gender)} class={"sstags__wrapper "+genderClass}>
			<span class={"sstags__icon "+iconClass}>{icon}</span>
			<span class="sstags__text">{gender}</span>	
			</div>
	})
	return <div style="text-align:center"> {genderView}</div>
}

module.exports = genders