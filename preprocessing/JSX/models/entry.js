// var m = require("mithril")
var ajax = require("superagent")//.set("Response", "json")

var Entry = function (defaults) {
		var self  = this
		var options = defaults || {}
		this.Ref  = options.Ref || ""
		this.Gender = options.Gender || "Female"
		this.Description = options.Description || ""
		this.Colours = options.Colours || []
		this.Styles = options.Styles || []
		this.Image = options.Image || ""
		this.ImageWidth = options.ImageWidth || ""
		this.ImageHeight = options.ImageHeight || ""
		this.Favourites = options.Favourites || 0
		this.IsInFavourites = options.IsInFavourites || false
		this.Edit = options.Edit || false
		if (!!options.CreatedAt) {
			console.log(options.CreatedAt)
			createdDate = options.CreatedAt.toString()
			this.CreatedAt = new Date(Date.UTC(
				createdDate.substr(0, 4),
				parseInt(createdDate.substr(4, 2))-1,
				createdDate.substr(6,2),
				createdDate.substr(8,2),
				createdDate.substr(10,2),
				createdDate.substr(12,2)
				))
			// console.log(this.CreatedAt)
		}
		//No need to observe the properties below
		this.OwnerName = options.OwnerName || ""
		this.OwnerRef = options.OwnerRef || ""
		this.Views = options.Views || 0
	// this.toggle= function (type, value) {
	//  	var styles = this[type]()
	//  	var index = styles.indexOf(value)
	//  	if (index == -1) {
	//  		styles.push(value)
	//  	} else {
	//  		styles.splice(index, 1)
	//  	}
	//  		this[type](styles)
	// }
	// this.Update = function () {
	// 	var self = this
	// }
	// this.Save = function () {
	// 	self = this
	// 	m.request({"url": "/ankara", "method": "POST", data:self}).then(
	// 		function () {
	// 		}, function () {
	// 		})
	// }
}

Entry.prototype.Delete = function (onSuccess, onError) {
	if (!this.Ref || !this.Edit) {
		return
	}
	ajax
	.del("/a/" + this.Ref)
	.send()
	.end(function (err, res) {
		if (err) {
			onError && onError()
		} else {
			onSuccess && onSuccess()
		}
	})
}
Entry.prototype.Update = function () {
	var url = "/ankara" + this.Ref
	ajax
	.patch(url)
	.send(this)
	.end(function (err, res) {
		if (err) {
			//Handle Error
			onError && onError(err)
		} else {
			// Do something with response
			onSuccess && onSuccess()
		}
	})
}
Entry.prototype.toggleGender = function (gender) {
	if (this.Gender != gender) {
		this.Gender = gender
		this.Styles = []
	}
}
Entry.prototype.toggleStyle = function (style, callback) {
	var styleIndex = this.Styles.indexOf(style)
 	if (styleIndex == -1) {
 		this.Styles.push(style)
 	} else {
 		this.Styles.splice(styleIndex, 1)
 	}
	console.log(this)
 	callback && callback()
}

Entry.prototype.toggleColour = function (colour) {
	var colourIndex = this.Colours.indexOf(colour)
 	if (colourIndex == -1) {
 		this.Colours.push(colour)
 	} else {
 		this.Colours.splice(colourIndex, 1)
 	}
 	console.log(this)
}
Entry.prototype.Save = function (onSuccess, onError) {
	var req 
	if (!!this.Ref) {
		req = ajax.put("/a/" + this.Ref)
	} else {
		req = ajax.post("/ankara")
	}
	req
	.send(this)
	.end(function (err, res) {
		if (err) {
			console.log('error: ', err)
			//Handle Error
			onError && onError(err.status)
		} else {
			// Do something with response
			onSuccess && onSuccess()
		}
	})
}

Entry.prototype.getRelatedEntries = function (onSuccess, onError) {
	if (!this.Ref) {
		return
	}
	var url = "/related/" + this.Ref
	ajax.get(url)
	.end(function (err, response){
		if (err) {
			onError && onError(err.code)
		} else {
			onSuccess && onSuccess(response.body)
		}
	})
}
Entry.prototype.onImageChange = function (imageProps, callback) {
	this.Image = imageProps.image;
	this.ImageHeight = imageProps.height
	this.ImageWidth = imageProps.width
	console.log(this, imageProps, callback)
	callback && callback()
}
Entry.prototype.Validate = function () {
	return !!this.Image &&
			!!this.Description &&
			!!this.Image &&
			!!this.ImageWidth &&
			!!this.ImageHeight &&
			!!this.Colours.length &&
			!!this.Styles.length

}
Entry.prototype.Favourite = function (onSuccess, onError) {
	if (!this.Ref) {
		return
	}
	var url = "/favourite/" + this.Ref
	if (!this.IsInFavourites) {
		this.IsInFavourites = true
		this.Favourites++

		// Update on Server
		ajax.get(url)
		.end(function (err, response) {
			if (err) {
				//Undo all the things
				this.IsInFavourites = false
				this.Favourites--
				onError && onError(err)
			} else {
				onSuccess && onSuccess()
			}
		})
	} else {
		this.IsInFavourites = false
		oldFavouriteCount = this.Favourites
		this.Favourites = Math.max(0,  oldFavouriteCount - 1)
		ajax.delete(url)
		.end(function (err, response) {
			if (err) {
				this.Favourites = oldFavouriteCount
				onError && onError(err)
			} else {
				onSuccess && onSuccess()
			}
		})
	}
}
module.exports = Entry