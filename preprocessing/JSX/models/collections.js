var ajax = require ("superagent")
var Collection = function (defaultOptions) {
	this.Ref = defaultOptions.Ref || ""
	this.Description = defaultOptions.Description || ""
	this.Name = defaultOptions.Name || ""
	this.Count = defaultOptions.Count || 0
	this.Images = defaultOptions.Images || ""
	this.EntryImages = defaultOptions.EntryImages || []
	console.log(defaultOptions, this)
}

Collection.prototype.addItem = function (itemId /* string */, onSuccess /* function */, onError /* function */) {
	/*
	itemId::string
	onSuccess::function
	onError::function
	*/
	if (!itemId || !!this.Ref) {
		return
	}
	var url = "/collections/" + this.Ref + "/" + itemId
	ajax.post(url)
	.send(this)
	.end(function (err, response) {
		if (err) {
			!!onError && onError(err)
		} else {
			!!onSuccess && onSuccess()
		}
	})
}

module.exports = Collection