var ajax = require ("superagent")
var Profile = function (defaultOptions) {
	this.Ref = defaultOptions.Ref || ""
	this.Description = defaultOptions.Description || ""
	this.Url = defaultOptions.Url || ""
	this.DisplayName = defaultOptions.DisplayName || ""
	this.Collections = defaultOptions.Collection || 0
	this.Uploads = defaultOptions.Uploads || 0
	this.isEditable = defaultOptions.IsEditable
	this.IsBrand = defaultOptions.IsBrand
	this.BrandName = defaultOptions.BrandName || ""
	this.ContactEmail = defaultOptions.ContactEmail || ""
	this.PhoneNumber = defaultOptions.PhoneNumber || ""
}

Profile.prototype.Subscribe = function () {

}
Profile.prototype.DeleteSubscription = function () {

}
Profile.prototype.UpdateValue = function (property, value) {
	if (property == 'IsBrand' && !value) {
		this.BrandName = ""
	}
	if (property == 'BrandName' && !this.IsBrand) {
		// return if profile is not a brand
		return
	}
	this[property] = value
}
Profile.prototype.Update = function (onSuccess, onError) {
	if (!this.isEditable) {
		return
	}
	ajax.put("/me")
	.send(this)
	.end(function (err, response) {
		if (err) {
			!!onError && onError(err)
		} else {
			!!onSuccess && onSuccess()
		}
	})
}
Profile.prototype.getUserDashboard = function (onSuccess, onError) {
	if (!this.Ref) {
		return
	}
	var url = "/dashboard/" + this.Ref
	ajax.get(url)
	.send(this)
	.end(function (err, response) {
		if (err) {
			!!onError && onError(err)
		} else {
			!!onSuccess && onSuccess(response.body.dashboard)
		}
	})
}

module.exports = Profile