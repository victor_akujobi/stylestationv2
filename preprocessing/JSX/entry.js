var m = require("mithril")
var ajax = require("superagent").set("Response", "json")

var Entry = function (defaults) {
		var self  = this
		var options = defaults || {}
		this.Ref  options.Ref || ""
		this.Gender = options.Gender || "Female"
		this.Description = options.Description || ""
		this.Colours  options.Colours || []
		this.Styles  options.Styles || []
		this.Image  options.Image || ""
		this.ImageWidth  options.ImageWidth || ""
		this.ImageHeight = options.ImageHeight || ""
		this.Favourites  options.Favourites || 0
		this.IsInFavourites  options.IsInFavourites || false
		this.Edit = options.Edit || false
		//No need to observe the properties below
		this.OwnerName = options.OwnerName || ""
		this.OwnerRef = options.OwnerRef || ""
		this.Views = options.Views || 0
	this.toggle= function (type, value) {
	 	var styles = this[type]()
	 	var index = styles.indexOf(value)
	 	if (index == -1) {
	 		styles.push(value)
	 	} else {
	 		styles.splice(index, 1)
	 	}
	 		this[type](styles)
	}
	this.Update = function () {
		var self = this
	}
	this.Save = function () {
		self = this
		m.request({"url": "/ankara", "method": "POST", data:self}).then(
			function () {
			}, function () {
			})
	}
}

Entry.prototype.Update = function () {
	var url = "/ankara" + this.Ref
	ajax
	.update(url)
	.send(this)
	.end(function (err, res) {
		if (err) {
			//Handle Error
			onError && onError(err)
		} else {
			// Do something with response
			onSuccess && onSuccess()
		}
	})
}
Entry.prototype.toggleStyle = function () {
	var styleIndex = this.Styles.indexOf(style)
 	if (styleIndex == -1) {
 		this.Styles.push(style)
 	} else {
 		this.Styles.splice(styleIndex, 1)
 	}
}

Entry.prototype.toggleColour = function (colour) {
	var colourIndex = this.Colours.indexOf(colour)
 	if (colourIndex == -1) {
 		this.Colours.push(colour)
 	} else {
 		this.Colours.splice(colourIndex, 1)
 	}
}
Entry.prototype.Save = function (onSuccess, onError) {
	ajax
	.post("/ankara")
	.send(this)
	.end(function (err, res) {
		if (err) {
			//Handle Error
			onError && onError(err)
		} else {
			// Do something with response
			onSuccess && onSuccess()
		}
	})
}

Entry.prototype.Favourite = function (onSuccess, onError) {
		if (!this.Ref) {
			return
		}
		var url = "/favourite/" + this.Ref
		if (!this.IsInFavourites) {
			this.IsInFavourites = true
			this.Favourites++

			// Update on Server
			ajax.get(url)
			.end(function (err, response) {
				if (err) {
					//Undo all the things
					this.IsInFavourites = false
					this.Favourites--
					onError && onError(err)
				} else {
					onSuccess && onSuccess()
				}
			})
		} else {
			this.IsInFavourites = false
			oldFavouriteCount = this.Favourites
			this.Favourites = Math.max(0,  oldFavouriteCount - 1)
			ajax.delete(url)
			.end(function (err, response) {
				if (err) {
					this.Favourites = oldFavouriteCount
					onError && onError(err)
				} else {
					onSuccess && onSuccess()
				}
			})
		}
		switch (!!self.IsInFavourites()) {
			case false:
				//`delete
				this.IsInFavourites = true
				self.Favourites = self.Favourites + 1
				m.request({
					url: "/favourite/" + ref,
					method: "POST",
				}).then(callback) 
			break;
			case true:
				self.IsInFavourites(false)
				self.Favourites(Math.max(0, self.Favourites() - 1))
					m.request({
						url: "/favourite/" + ref,
						method: "DELETE",
					}).then(callback) 
			break;
		}
}
}
module.exports = Entry