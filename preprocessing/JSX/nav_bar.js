/** @jsx m */
var m = require("mithril")

NavBar = {}
NavBar.view = function (options) {
	var previousLink = null
	var nextLink = null
	if (options.showPrevious) {
		previousLink = <a  title="Previous Page" onclick={options.onClickPrevious} class="prev_page">&#x276e;</a>
	}
	if (!!options.showNext) {
		nextLink = <a title="Next Page" onclick={options.onClickNext} class="next_page">&#x276f;</a>
	}
	return <p class="nav">
		{previousLink}{nextLink}
	</p>
}

module.exports = NavBar