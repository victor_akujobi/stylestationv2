/** @jsx m */
var m = require("mithril")
var SSConfig = require("./config")
var ProfileHeader = require("./profile.header.js")
var NavBar = require("./nav_bar.js").view
var gridify = require("./gridify")
var animations = require("./animation")
var animator = animations.animator
var ownProfileGridImageAnimation = animator(animations.fadeScaleIn, animations.noop, false)
var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)

require("../SCSS/main.scss")
require("../SCSS/profile.scss")
var uploads
var Profile = function (options) {
	console.log("profile: ", this)
	this.Description = m.prop(options.Description || "")
	this.Url = m.prop(options.URL || "")
	this.DisplayName = m.prop(options.DisplayName || "")
	this.Collections = m.prop(options.Collection || 0)
	this.Uploads = m.prop(options.Uploads || 0)
	this.update = function () {
		console.log(this)
		var requestData = {
			Description: this.Description(),
			URL: this.Url(),
			DisplayName: this.DisplayName()
		}
		m.request({
			url: "/me",
			method: "PUT",
			data: requestData
		})
		return this	
	}.bind(this)
}
var ProfilePage = {}
ProfilePage.controller = function (defaultOptions) {
	console.log(defaultOptions)
	this.options = defaultOptions || {}
	var ctrl = this
	this.profileData = new Profile({})
	this.page = m.prop(1)
	this.section = m.prop("uploads")
	// this.profileData = new Profile({})
	this.ref = ctrl.options.ref
	this.entries = m.prop([])
	this.block = function (entry, style) {
		var urlStem = !!ctrl.options.editMode ? "/edit/" : "/a/"
		return <a href={urlStem + entry.Ref} class="ssgrid__innerblock">
			<img config={!!ctrl.options.editMode ? ownProfileGridImageAnimation: gridImageAnimation} style={style} src={"/static/images/ankara/"+entry.Image+"_t.jpg"}/>
		</a>
	}
	this.fetchProfileData = function () {
		if (!!ctrl.options.editMode) {
			// Set Entries endpoint appropriately.
			m.request({url: "/me", config:SSConfig.XHR, method: "GET"}).then(ctrl.profileData)
		} else {
			m.request({url: "/profile/" + ctrl.ref, config:SSConfig.XHR, method: "GET"}).then(function (response) {
				ctrl.profileData = new Profile(response)
			})
		}
	}

	this.fetchEntries = function () {
		var urlStem = !!ctrl.options.editMode ? "/my": "/profile/" + ctrl.ref()
		m.request({
			url: urlStem + "/" + ctrl.section(),
			method: "GET",
			data:{page: ctrl.page()},
			config: SSConfig.XHR
		}).then(ctrl.entries, console.log)
	}
	this.handleSectionChange = function (section) {
		if (section != ctrl.section()) {
			console.log(section)
			ctrl.section(section)
			ctrl.page(1)
			ctrl.fetchEntries()
		}
	}
	this.fetchNextEntries = function () {
		console.log("Nexting")
		var nextPage = ctrl.page() + 1
		ctrl.page(nextPage)
		ctrl.fetchEntries()
	}
	this.fetchPreviousEntries = function () {
		var previousPage = ctrl.page() - 1
		if (previousPage > 0) {
			ctrl.page(previousPage)
			ctrl.fetchEntries()
		}
	}
	this.resetPage = function () {
		ctrl.page(1)
	}
	this.name = m.prop()
	if (window.bootstrap) {
		this.profileData = new Profile(bootstrap.profile)
		this.fetchEntries()
		window.bootstrap = false
	} else {
		// this.profileData = new Profile({})
		this.fetchProfileData()
		this.fetchEntries()
	}
}
ProfilePage.view = function (ctrl) {
	var options
	var profile = ctrl.profileData
	return <div>
	{
		ProfileHeader.view({
			editMode: !!ctrl.options.editMode,
			onSectionChange: ctrl.handleSectionChange,
			profile: profile,
			section: ctrl.section()
		})
	}
	<div class="pad">
		{gridify.view({"entries": ctrl.entries(), view:ctrl.block})}
	</div>
	{NavBar({
		showNext: ctrl.entries().length == 12,
		showPrevious: ctrl.page() > 1,
		onClickNext: ctrl.fetchNextEntries,
		onClickPrevious: ctrl.fetchPreviousEntries
	})}
	</div>
}

module.exports = ProfilePage