var m = require("mithril")
require("../SCSS/menubar.scss")
var noop = function () {
	return ""
}
var NavBar = {}
NavBar.controller = function () {

}
NavBar.view = function (options) {
	// var current = typeof (options && options.current) == "function" ? options.current : noop
	return <ul class={"menubar__wrapper menubar__wrapper--"}>
			<li class="nav_item"><a href="/">EXPLORE</a></li>
			<li class="nav_item"><a rel="external" href="/blog">BLOG</a></li>
			<li class="nav_item"><a class="nav_item--bg create_button" href="/new">Upload</a></li>

			<li class="nav_item"><a href="/login">SIGN IN</a></li>
			<li class="nav_item nav_item--join"><a class="nav_item--bg" href="/register">JOIN</a></li>
		</ul>
}
module.exports = NavBar