var snabbt = require("snabbt.js")
var animating = false;

// Define an animator consisting of optional incoming and outgoing animations. 
// alwaysAnimate is false unless specified as true: false means an incoming animation will only trigger if an outgoing animation is also in progress.
// forcing dontClone to true means the outward animation will use the original element rather than a clone. This could improve performance by recycling elements, but can lead to trouble: clones have the advantage of being stripped of all event listeners.
function animator( incoming, outgoing, alwaysAnimate, dontClone ){
	// The resulting animator can be applied to any number of components
	return function animate( x, y, z ){
		var config;
		var parent;
		var next;

		// When used as a config function 
		if( x.nodeType ){
			return animationConfig( x, y, z );
		}
		// When passed a virtual DOM node (the output of m)
		else if( x.attrs ){
			config = x.attrs.config;

			x.attrs.config = animationConfig;

			return x;
		}
		// When applied to a Mithril module / component 
		else if( x.view ){
			return {
				controller : x.controller || noop,
				view       : function animatedView( ctrl ){
					var view = x.view( ctrl );

					config = view.config;

					view.attrs.config = animationConfig;

					return view;
				}
			};
		}

		function animationConfig( el, init, context ){
			var output;
			var onunload;

			if( config ){
				output   = config( el, init, context );
				// If the root element already has a config, it may also have an onunload which we should take care to preserve 
				onunload = context.onunload;
			}

			if( !init ){
				if( incoming && alwaysAnimate || animating ){
					incoming( el, noop, context );
				}

				context.onunload = outgoing ? onunload ? function onunloadWrapper(){
					teardown();
					onunload();
				} : teardown : onunload;

				parent = el.parentElement;
				next   = el.nextSibling;
			}

			return output;

			function teardown(){
				var insertion = dontClone ? el : el.cloneNode( true );
				var reference = null;

				if( next && parent && next.parentNode === parent ){
					reference = next;
				}
				
				animating = true;
				
				setTimeout( function resetAnimationFlag(){
					animating = false;
				}, 0 );

				parent.insertBefore( insertion, reference );

				outgoing( insertion, function destroy(){
					if( parent.contains( insertion ) ){
						parent.removeChild( insertion );
					}
				}, context );
			}
		}
	};
}

function noop(){}


var fadeScaleIn = function (element) {
	var options = {
		fromScale: [0, 0],
		scale: [1, 1],
		fromOpacity: 0,
		opacity:1,
		easing: 'spring',
		// duration:2000
		springConstant: 0.8,
		springDeceleration: 0.3,
		springMass: 5
	}
	snabbt(element, options)
}
var fadeScaleOut = function (element, callback) {
	snabbt(element, {
		fromScale: [0, 0],
		scale: [1, 1],
		fromOpacity: 0,
		opacity:1,
		easing: 'spring',
		springConstant: 0.3,
		springDeceleration: 0.8,
		callback: callback
})
}
var scaleInFrom = function () {

}
slideIn = function (element) {
	snabbt(element, {
		fromPosition: [-element.offsetWidth, 0, 0],
		position: [0, 0, 0],
		easing: 'easeInOut',
		duration:200
	})
}
slideOut = function (element, callback) {
	snabbt(element, {
		fromPosition: [0, 0, 0],
		position: [element.offsetWidth, 0, 0],
		easing: 'easeInOut',
		callback:callback,
		duration:200
	})
}
module.exports = {animator: animator, fadeScaleIn: fadeScaleIn, fadeScaleOut: fadeScaleOut, slideIn: slideIn, slideOut:slideOut}