/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;', 'facebook': '&#xE60F', 'pinterest': '&#xE611', 'email': '&#xE60D', 'twitter': '&#xE60E', 'google': '&#xE610'}
	var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	var m = __webpack_require__(7)
	var gender = __webpack_require__(1)
	var tags = __webpack_require__(2)
	var image = __webpack_require__(3)
	var colours = __webpack_require__(4)
	var linearPartition = __webpack_require__(8)
	var gridify = __webpack_require__(5)
	var popup = __webpack_require__(6)
	'use strict';
	// var tagComponent = require('tags')
	var error = m.prop("")
	var STYLES = {
				"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt"],
				"Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories"]
			};
	var Entry = function (url) {
		if (!!url) {

		} else {
			this.Gender =  m.prop("Female")
			this.Description =  m.prop("")
			this.Colours = m.prop([])
			this.Styles = m.prop([])
			this.Image = m.prop("")
			this.ImageWidth = m.prop("")
			this.ImageHeight =  m.prop("")
		}
		this.toggle= function (type, value) {
		 	var styles = this[type]()
		 	var index = styles.indexOf(value)
		 	if (index == -1) {
		 		styles.push(value)
		 	} else {
		 		styles.splice(index, 1)
		 	}
		 		this[type](styles)
		}
		this.Save = function () {
			self = this
			m.request({"url": "/ankara", "method": "POST", data:self}).then(
				function () {
				}, function () {
				})
		}
	}
	// var entry = {Gender: m.prop("Female"), Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([]), Image:m.prop(""), ImageWidth:m.prop(""), ImageHeight: m.prop("")}

	var newPage = {}
	newPage.controller = function () {
		this.vm = {entry:new Entry()}
		var ctrl = this;
		ctrl.popup = Popup
		ctrl.popup.view(LoginPage)
		ctrl.popup.show(true)
		this.Send = function () {
			console.log("Sending...")
			m.request({"url": "/ankara", "method": "POST", data:ctrl.vm.entry, "background": true}).then(
			function () {
				console.log("Success")
			}, function () {
				console.log("Error")
			})
		}
	}
	newPage.view = function (ctrl) {
		var entryStyles = ctrl.vm.entry.Styles().reduce(function(dict, currentValue){dict[currentValue] = true; return dict}, {})
		var coloursComponent = colours.view({selected: ctrl.vm.entry.Colours(), onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Colours')})
		var styles = STYLES[ctrl.vm.entry.Gender()].map(function (style) {
					return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:ctrl.vm.entry.Gender().toLowerCase()||"female", onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Styles', style)})
				})
		return {tag: "div", attrs: {class:"ssnewentry__wrapper"}, children: [
					image.view({entry:ctrl.vm.entry}),
					{tag: "input", attrs: {placeholder:"Click to enter description",type:"text",onchange:m.withAttr("value", ctrl.vm.entry.Description),value:ctrl.vm.entry.Description(),class:"ssentry__descriptioninput"}}, 
					{tag: "div", attrs: {class:"sstags__stylepickerwrapper"}, children: [{tag: "h4", attrs: {class:"sstags__stylepicker"}, children: ["Select Gender"]}, genders.view({entry:ctrl.vm.entry, onclick:ctrl.vm.entry.Gender})," ",{tag: "br", attrs: {style:"clear:both"}}]}, 
					{tag: "div", attrs: {class:"sstags__stylepickerwrapper"}, children: [{tag: "h4", attrs: {class:"sstags__stylepicker"}, children: ["Pick Relevant Styles)"]}, styles," ",{tag: "br", attrs: {style:"clear:both"}}]}, 
					{tag: "div", attrs: {class:"sstags__stylepickerwrapper"}, children: [{tag: "h4", attrs: {class:"sstags__stylepicker"}, children: ["Pick Colours"]}, coloursComponent," ",{tag: "br", attrs: {style:"clear:both"}}]}, 
					{tag: "button", attrs: {class:"ssnewentry__submit",onclick:ctrl.Send}, children: ["Done"]}
				]}
	}
	var viewMultiple = {}
	viewMultiple.controller = function () {
		var ctrl = this
		this.vm = {entries: m.prop([])}
		// this.grid = gridify.view(this.vm)
		m.request({"url": "/styles/1", "method": "GET", "background": true}).then(ctrl.vm.entries, function (response) {error(response.message)}).then(function () {m.redraw()})
	}
	viewMultiple.view = function (ctrl) {
		if (!!ctrl.vm.entries().length) {
			// var entries = ctrl.vm.entries()
			return gridify.view({"entries": ctrl.vm.entries()})
		}
			return {tag: "p", attrs: {}, children: ["Loading"]}
	}
	var detailPage = {}
	detailPage.view = function (ctrl) {
		// console.log(ctrl.entry)
		// ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries()}) : 
		console.log(ctrl.relatedEntries())
		console.log('test')
		var relatedEntries = ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries(), numPartitions: 1}) :{tag: "div", attrs: {class:"loading_gif"}}
		if (ctrl.entry()) {
			var entry = ctrl.entry()
			var url = document.location.origin + "/ankara/" + entry.Ref
			return {tag: "div", attrs: {class:"ssdetail__wrapper"}, children: [
					{tag: "div", attrs: {class:"entry_wrapper"}, children: [
					{tag: "div", attrs: {class:"ssdetail__entry"}, children: [
					{tag: "div", attrs: {class:"ssdetail__image"}, children: [{tag: "img", attrs: {class:"image",src:"/static/images/ankara/"+entry.Image+"_m.jpg"}}]}, 
					{tag: "div", attrs: {class:"sssharebar__wrapper"}, children: [{tag: "p", attrs: {class:"share_text"}, children: ["Psst... If you love this style, you can share it on "]}, {tag: "a", attrs: {href:"http://www.facebook.com/sharer/sharer.php?u=" + url + "&title=" + entry.Description,target:"_blank",class:"share_link facebook_share"}, children: [m.trust(fonts['facebook'])]}, {tag: "a", attrs: {href:"http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url,target:"_blank",class:"share_link twitter_share"}, children: [m.trust(fonts['twitter'])]}, {tag: "a", attrs: {href:"http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + entry.Description,target:"_blank",class:"share_link pinterest_share"}, children: [m.trust(fonts['pinterest'])]}, {tag: "a", attrs: {target:"_blank",class:"share_link email_share"}, children: [m.trust(fonts['email'])]}]}
					]}, 
				{tag: "div", attrs: {class:"ssdetail__info"}, children: [
					{tag: "p", attrs: {class:"owner"}, children: [entry.OwnerName]}, 
					{tag: "p", attrs: {class:"description"}, children: [entry.Description]}, 
					{tag: "div", attrs: {class:"stats"}, children: [
					  {tag: "ul", attrs: {}, children: [
					    {tag: "li", attrs: {}, children: ["98",{tag: "span", attrs: {}, children: ["Favs"]}]}, 
					    {tag: "li", attrs: {}, children: ["298",{tag: "span", attrs: {}, children: ["Things"]}]}, 
					    {tag: "li", attrs: {}, children: ["923",{tag: "span", attrs: {}, children: ["Objects"]}]}, 
					    {tag: "li", attrs: {}, children: ["98",{tag: "span", attrs: {}, children: ["More"]}]}
					  ]}
					]}, 
					{tag: "div", attrs: {class:"tag_wrapper"}, children: [
						entry.Styles.map(function (style) {
							return {tag: "p", attrs: {class:"tag"}, children: [{tag: "span", attrs: {class:"tag_icon"}, children: [m.trust(fonts[style.toLowerCase()])]}, {tag: "span", attrs: {class:"tag_text"}, children: [style]}]}
						})
					]}, 

					{tag: "div", attrs: {}, children: [
						entry.Colours.map(function (colour) {
							return {tag: "p", attrs: {class:"tag"}, children: [{tag: "span", attrs: {class:"colour",style:{"background-color": COLOURS[colour]}}}, {tag: "span", attrs: {class:"tag_text"}, children: [colour]}]}
						})
					]}
				]}
				]}, 
				{tag: "p", attrs: {}, children: ["We think you might like"]}, 
				{tag: "div", attrs: {class:"related_entries"}, children: [relatedEntries]}
			]}
		} else {
			return {tag: "div", attrs: {}}
		}
	}
	// Share links courtesy of http://petragregorova.com/articles/social-share-buttons-with-custom-icons/
	detailPage.controller = function () {
		var entryRef = m.route.param("entry") || "sxxeSLWUYS"
		var ctrl = this
		ctrl.entry = m.prop({})
		ctrl.relatedEntries = m.prop([])
			m.request({url: "/related/"+ entryRef, method:"get"}).then(function (response) {
			ctrl.relatedEntries(response)
		}), function () {
			console.log("Error")
			return []
		}
		m.request({url: "/a/"+ entryRef, method:"get"}).then(function (response) {
			ctrl.entry(response)
		}), function () {
			console.log("Error")
			return []
		}
	}
	m.route.mode = 'hash'
	m.route(document.getElementById('content'), '/', {
	'/' : viewMultiple,
	'/a/:entry': detailPage,
	'/new': newPage
	})

	// m.module(document.getElementById('content'), viewMultiple)
	// m.module(document.getElementById('content'), detailPage);
	//hwuhkm

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	// require("../SCSS/gender.scss")
	var defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}
	// gender
	var genders = {}
	genders.controller = function () {

	}
	genders.view = function (vm) {
		var entry = vm.entry;
		var genderView = Object.keys(defaultGenders).map(function (gender) {
			var isSelected = gender == entry.Gender()
			var icon= isSelected ? m.trust("&#xE60A;") : m.trust(defaultGenders[gender])
			var iconClass = isSelected ? "sstags__icon--selected" : ""
			var genderClass = "sstags__wrapper--" + gender.toLowerCase()
			return {tag: "div", attrs: {key:gender,onclick:vm.onclick.bind(null, gender),class:"sstags__wrapper "+genderClass}, children: [
				{tag: "span", attrs: {class:"sstags__icon "+iconClass}, children: [icon]}, 
				{tag: "span", attrs: {class:"sstags__text"}, children: [gender]}	
				]}
		})
		return {tag: "div", attrs: {style:"text-align:center"}, children: [" ",genderView]}
	}

	module.exports = genders

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	'use strict';
	// var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;'}
	var tags = {}
	tags.controller = function() {

	}
	// tags.vm = {isSelected: false}
	tags.view = function (vm) {
		var icon= vm.isSelected ? m.trust("&#xE60A;") : m.trust(fonts[vm.tagText.toLowerCase()])
		var iconClass = vm.isSelected ? "sstags__icon--selected" : ""
		var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
			return {tag: "div", attrs: {key:vm.tagText,onclick:vm.onclick,class:"sstags__wrapper "+gender}, children: [
				{tag: "span", attrs: {class:"sstags__icon "+iconClass}, children: [icon]}, 
				{tag: "span", attrs: {class:"sstags__text"}, children: [vm.tagText]}	
				]}
	}



/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	'use strict';
	__webpack_require__(10)
	var image = {}
	image.onchange= function(vm, e) {
		// m.request(/)
		var file = e.target.files[0]
		var formData = new FormData
	    // for (var i = 0; i < files.length; i++) {
	        formData.append("file", file)
	    // }
		// callback("jsdhjsh")
		return m.request({
	        method: "POST",
	        url: "/imageprocessor/entry",
	        data: formData,
	        //simply pass the FormData object intact to the underlying XMLHttpRequest, instead of JSON.stringify'ing it
	        serialize: function(value) {return value}
	    }).then(function (response) {
	    	console.log(response)
	    	console.log(vm.entry.Image())
	    	vm.entry.Image(response.image)
	    	console.log(vm.entry.Image())
	    	vm.entry.ImageHeight(response.height)
	    	vm.entry.ImageWidth(response.width)
	    })
	}
	image.controller = function () {

	}
	image.view = function (vm) {
		console.log(vm)
		var content;
		var imageInput = ""
		if (vm.isLoading) {
			content = {tag: "div", attrs: {class:"ssimage__loadingimage"}}
		} else {
			imageInput = {tag: "input", attrs: {class:"ssimage__imageinput",type:"file",onchange:image.onchange.bind(null, vm)}}
			if (!!vm.entry.Image()) {
				content = {tag: "div", attrs: {}, children: [{tag: "img", attrs: {style:"width:100%",src:"/static/images/ankara/"+vm.entry.Image()+"_m.jpg"}}, {tag: "p", attrs: {class:"ssimage__changephoto"}, children: ["change photo"]}]}
			} else {
				content = {tag: "div", attrs: {class:"ssimage__noimage"}, children: [{tag: "p", attrs: {class:"ssimage__noimagetext"}, children: ["+ add a photo"]}]}
			}
		}
		return {tag: "div", attrs: {class:"ssimage__wrapper"}, children: [
		content,
		imageInput
		]}
	}

	module.exports = image

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	// var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	__webpack_require__(12)
	var colours = {}
	colours.handleSelect = function (callback, value) {
		console.log(value)
		callback(value)
	}
	colours.view = function (vm) {
		console.log('seleted', vm.selected)
		var selected = vm.selected.reduce(function (dict, currentValue) {dict[currentValue] = true; return dict}, {})
		console.log(selected)
		return Object.keys(COLOURS).map(function (path) {
			var isSelected = !!selected[path] ? {tag: "span", attrs: {class:"sscolours__blockpallete--selected"}, children: [m.trust("&#xE60A;")]} : ""
			return {tag: "div", attrs: {class:"sscolours__block",onclick:colours.handleSelect.bind(null, vm.onclick, path)}, children: [{tag: "div", attrs: {class:"sscolours__blockpallete",style:{"background-color": COLOURS[path]}}, children: [isSelected]}, {tag: "span", attrs: {class:"sscolours__blocktext"}, children: [path]}]}
		})
	}
	module.exports = colours

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	// @prepros-prepend linear-partition.js
	//@prepros-prepend avatar.js
	__webpack_require__(8)
	__webpack_require__(9)
	var gridify = {}
	gridify.metrics = m.prop([])




	// gridify.engine = function (element, items, template) {
		
	// 	@element: DOMNode
	// 	@items: Array of items
	// 	@metric Array of geometries
	// 	@template Virtual DOM to render
		
	// 	// return function (element, isInit) {

	// 		m.render(element, this.items.map(function (item, index) {
	// 				return template(item, style)
	// 			}))
	// 	// }
	// #
	gridify.config = function(items, numPartitions, container, block) {
		// if (context.isInit) {
		// 	return
		// }
		var createPartitions = function () {
			var block = function (entry, style) {
				return {tag: "a", attrs: {href:"#/a/"+entry.Ref,class:"ssgrid__innerblock"}, children: [{tag: "img", attrs: {style:style,src:"/static/images/ankara/"+entry.Image+"_t.jpg"}}]}
			}
			var elementWidth = container.offsetWidth
			var WIDTH = 450;
			var INIT_HEIGHT = 150;
			var MAX_HEIGHT = 500;
			var widths = []
			var ratios = items.map(function(item) {
				return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
			})
		var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/300)))
			var test = partitions.map(function (row) {
				var sum = row.reduce(function (total, item) {
					return total + item
				}, 0)
				ratio = elementWidth / sum
				return row.map(function (item) {
					return [ratio * item, ratio * INIT_HEIGHT]
				})
			}.bind(this))
			var metrics = [].concat.apply([], test)
			m.render(container, metrics.map(function (metric, index) {
				var style = {width:~~metric[0] + "px", height:~~metric[1] + "px"}
				var item = items[index]
				return block(item, style)
			}))
		}
			window.onresize = function () {
				createPartitions()
			}
			createPartitions()
	}
	gridify.view = function (vm) {
		var entries = vm.entries || []
		var metrics  = gridify.metrics()
		console.log(metrics, "MET")
		var blocks = metrics.map(function (metric, style) {
			var entry = entries[index]
			// var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
			console.log(style)
			return {tag: "div", attrs: {class:"ssgrid__block"}, children: [{tag: "a", attrs: {href:"/a/"+entry.Ref,class:"ssgrid__innerblock"}, children: [{tag: "img", attrs: {style:style,src:"/static/images/ankara/"+entry.Image+"_t.jpg"}}]}, {tag: "p", attrs: {class:"ssgrid__details_holder"}, children: [avatar.view(entry.OwnerName)]}]}	
		})
		return {tag: "div", attrs: {config:gridify.config.bind(null, entries, vm.numPartitions|| false)}, children: [
			blocks
		]}
	}

	module.exports = gridify

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var Popup = {
		show:m.prop(false),
		view: function (body) {
			return this.show() ? {tag: "div", attrs: {class:"overlay"}, children: [body()]}: ""
		}
	}

	var LoginPopup = function () {
		return {tag: "div", attrs: {class:"sslogin__wrapper"}, children: [
	{tag: "p", attrs: {class:"sslogin__intro_text"}, children: ["Not logged in? Ok then, let's get you sorted.",{tag: "br", attrs: {}}, " It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear"]}, 
	{tag: "a", attrs: {href:"/login?provider=facebook",class:"srlogin__button srlogin__button__facebook"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__facebook"}, children: [m.trust("&#xE60F")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Facebook Account"]}
	]}, 
	{tag: "a", attrs: {href:"/login?provider=gplus",class:"srlogin__button srlogin__button__google"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__google"}, children: [m.trust("&#xE610")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Google Account"]}
	]}, 
	{tag: "a", attrs: {href:"/login?provider=twitter",class:"srlogin__button srlogin__button__twitter"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__twitter"}, children: [m.trust("&#xE60E")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Twitter Account"]}
	]}
	]}
	}

	module.exports = {Popup: Popup, LoginPopup: LoginPopup}

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {var m = (function app(window, undefined) {
		var OBJECT = "[object Object]", ARRAY = "[object Array]", STRING = "[object String]", FUNCTION = "function";
		var type = {}.toString;
		var parser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[.+?\])/g, attrParser = /\[(.+?)(?:=("|'|)(.*?)\2)?\]/;
		var voidElements = /^(AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR)$/;

		// caching commonly used variables
		var $document, $location, $requestAnimationFrame, $cancelAnimationFrame;

		// self invoking function needed because of the way mocks work
		function initialize(window){
			$document = window.document;
			$location = window.location;
			$cancelAnimationFrame = window.cancelAnimationFrame || window.clearTimeout;
			$requestAnimationFrame = window.requestAnimationFrame || window.setTimeout;
		}

		initialize(window);


		/**
		 * @typedef {String} Tag
		 * A string that looks like -> div.classname#id[param=one][param2=two]
		 * Which describes a DOM node
		 */

		/**
		 *
		 * @param {Tag} The DOM node tag
		 * @param {Object=[]} optional key-value pairs to be mapped to DOM attrs
		 * @param {...mNode=[]} Zero or more Mithril child nodes. Can be an array, or splat (optional)
		 *
		 */
		function m() {
			var args = [].slice.call(arguments);
			var hasAttrs = args[1] != null && type.call(args[1]) === OBJECT && !("tag" in args[1]) && !("subtree" in args[1]);
			var attrs = hasAttrs ? args[1] : {};
			var classAttrName = "class" in attrs ? "class" : "className";
			var cell = {tag: "div", attrs: {}};
			var match, classes = [];
			if (type.call(args[0]) != STRING) throw new Error("selector in m(selector, attrs, children) should be a string")
			while (match = parser.exec(args[0])) {
				if (match[1] === "" && match[2]) cell.tag = match[2];
				else if (match[1] === "#") cell.attrs.id = match[2];
				else if (match[1] === ".") classes.push(match[2]);
				else if (match[3][0] === "[") {
					var pair = attrParser.exec(match[3]);
					cell.attrs[pair[1]] = pair[3] || (pair[2] ? "" :true)
				}
			}
			if (classes.length > 0) cell.attrs[classAttrName] = classes.join(" ");


			var children = hasAttrs ? args[2] : args[1];
			if (type.call(children) === ARRAY) {
				cell.children = children
			}
			else {
				cell.children = hasAttrs ? args.slice(2) : args.slice(1)
			}

			for (var attrName in attrs) {
				if (attrName === classAttrName) {
					if (attrs[attrName] !== "") cell.attrs[attrName] = (cell.attrs[attrName] || "") + " " + attrs[attrName];
				}
				else cell.attrs[attrName] = attrs[attrName]
			}
			return cell
		}
		function build(parentElement, parentTag, parentCache, parentIndex, data, cached, shouldReattach, index, editable, namespace, configs) {
			//`build` is a recursive function that manages creation/diffing/removal of DOM elements based on comparison between `data` and `cached`
			//the diff algorithm can be summarized as this:
			//1 - compare `data` and `cached`
			//2 - if they are different, copy `data` to `cached` and update the DOM based on what the difference is
			//3 - recursively apply this algorithm for every array and for the children of every virtual element

			//the `cached` data structure is essentially the same as the previous redraw's `data` data structure, with a few additions:
			//- `cached` always has a property called `nodes`, which is a list of DOM elements that correspond to the data represented by the respective virtual element
			//- in order to support attaching `nodes` as a property of `cached`, `cached` is *always* a non-primitive object, i.e. if the data was a string, then cached is a String instance. If data was `null` or `undefined`, cached is `new String("")`
			//- `cached also has a `configContext` property, which is the state storage object exposed by config(element, isInitialized, context)
			//- when `cached` is an Object, it represents a virtual element; when it's an Array, it represents a list of elements; when it's a String, Number or Boolean, it represents a text node

			//`parentElement` is a DOM element used for W3C DOM API calls
			//`parentTag` is only used for handling a corner case for textarea values
			//`parentCache` is used to remove nodes in some multi-node cases
			//`parentIndex` and `index` are used to figure out the offset of nodes. They're artifacts from before arrays started being flattened and are likely refactorable
			//`data` and `cached` are, respectively, the new and old nodes being diffed
			//`shouldReattach` is a flag indicating whether a parent node was recreated (if so, and if this node is reused, then this node must reattach itself to the new parent)
			//`editable` is a flag that indicates whether an ancestor is contenteditable
			//`namespace` indicates the closest HTML namespace as it cascades down from an ancestor
			//`configs` is a list of config functions to run after the topmost `build` call finishes running

			//there's logic that relies on the assumption that null and undefined data are equivalent to empty strings
			//- this prevents lifecycle surprises from procedural helpers that mix implicit and explicit return statements (e.g. function foo() {if (cond) return m("div")}
			//- it simplifies diffing code
			//data.toString() is null if data is the return value of Console.log in Firefox
			if (data == null || data.toString() == null) data = "";
			if (data.subtree === "retain") return cached;
			var cachedType = type.call(cached), dataType = type.call(data);
			if (cached == null || cachedType !== dataType) {
				if (cached != null) {
					if (parentCache && parentCache.nodes) {
						var offset = index - parentIndex;
						var end = offset + (dataType === ARRAY ? data : cached.nodes).length;
						clear(parentCache.nodes.slice(offset, end), parentCache.slice(offset, end))
					}
					else if (cached.nodes) clear(cached.nodes, cached)
				}
				cached = new data.constructor;
				if (cached.tag) cached = {}; //if constructor creates a virtual dom element, use a blank object as the base cached node instead of copying the virtual el (#277)
				cached.nodes = []
			}

			if (dataType === ARRAY) {
				//recursively flatten array
				for (var i = 0, len = data.length; i < len; i++) {
					if (type.call(data[i]) === ARRAY) {
						data = data.concat.apply([], data);
						i-- //check current index again and flatten until there are no more nested arrays at that index
					}
				}
				
				var nodes = [], intact = cached.length === data.length, subArrayCount = 0;

				//keys algorithm: sort elements without recreating them if keys are present
				//1) create a map of all existing keys, and mark all for deletion
				//2) add new keys to map and mark them for addition
				//3) if key exists in new list, change action from deletion to a move
				//4) for each key, handle its corresponding action as marked in previous steps
				//5) copy unkeyed items into their respective gaps
				var DELETION = 1, INSERTION = 2 , MOVE = 3;
				var existing = {}, unkeyed = [], shouldMaintainIdentities = false;
				for (var i = 0; i < cached.length; i++) {
					if (cached[i] && cached[i].attrs && cached[i].attrs.key != null) {
						shouldMaintainIdentities = true;
						existing[cached[i].attrs.key] = {action: DELETION, index: i}
					}
				}
				if (shouldMaintainIdentities) {
					if (data.indexOf(null) > -1) data = data.filter(function(x) {return x != null})
					
					var keysDiffer = false
					if (data.length != cached.length) keysDiffer = true
					else for (var i = 0, cachedCell, dataCell; cachedCell = cached[i], dataCell = data[i]; i++) {
						if (cachedCell.attrs && dataCell.attrs && cachedCell.attrs.key != dataCell.attrs.key) {
							keysDiffer = true
							break
						}
					}
					
					if (keysDiffer) {
						for (var i = 0, len = data.length; i < len; i++) {
							if (data[i] && data[i].attrs) {
								if (data[i].attrs.key != null) {
									var key = data[i].attrs.key;
									if (!existing[key]) existing[key] = {action: INSERTION, index: i};
									else existing[key] = {
										action: MOVE,
										index: i,
										from: existing[key].index,
										element: cached.nodes[existing[key].index] || $document.createElement("div")
									}
								}
								else unkeyed.push({index: i, element: parentElement.childNodes[i] || $document.createElement("div")})
							}
						}
						var actions = []
						for (var prop in existing) actions.push(existing[prop])
						var changes = actions.sort(sortChanges);
						var newCached = new Array(cached.length)

						for (var i = 0, change; change = changes[i]; i++) {
							if (change.action === DELETION) {
								clear(cached[change.index].nodes, cached[change.index]);
								newCached.splice(change.index, 1)
							}
							if (change.action === INSERTION) {
								var dummy = $document.createElement("div");
								dummy.key = data[change.index].attrs.key;
								parentElement.insertBefore(dummy, parentElement.childNodes[change.index] || null);
								newCached.splice(change.index, 0, {attrs: {key: data[change.index].attrs.key}, nodes: [dummy]})
							}

							if (change.action === MOVE) {
								if (parentElement.childNodes[change.index] !== change.element && change.element !== null) {
									parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null)
								}
								newCached[change.index] = cached[change.from]
							}
						}
						for (var i = 0, len = unkeyed.length; i < len; i++) {
							var change = unkeyed[i];
							parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null);
							newCached[change.index] = cached[change.index]
						}
						cached = newCached;
						cached.nodes = new Array(parentElement.childNodes.length);
						for (var i = 0, child; child = parentElement.childNodes[i]; i++) cached.nodes[i] = child
					}
				}
				//end key algorithm

				for (var i = 0, cacheCount = 0, len = data.length; i < len; i++) {
					//diff each item in the array
					var item = build(parentElement, parentTag, cached, index, data[i], cached[cacheCount], shouldReattach, index + subArrayCount || subArrayCount, editable, namespace, configs);
					if (item === undefined) continue;
					if (!item.nodes.intact) intact = false;
					if (item.$trusted) {
						//fix offset of next element if item was a trusted string w/ more than one html element
						//the first clause in the regexp matches elements
						//the second clause (after the pipe) matches text nodes
						subArrayCount += (item.match(/<[^\/]|\>\s*[^<]/g) || []).length
					}
					else subArrayCount += type.call(item) === ARRAY ? item.length : 1;
					cached[cacheCount++] = item
				}
				if (!intact) {
					//diff the array itself
					
					//update the list of DOM nodes by collecting the nodes from each item
					for (var i = 0, len = data.length; i < len; i++) {
						if (cached[i] != null) nodes.push.apply(nodes, cached[i].nodes)
					}
					//remove items from the end of the array if the new array is shorter than the old one
					//if errors ever happen here, the issue is most likely a bug in the construction of the `cached` data structure somewhere earlier in the program
					for (var i = 0, node; node = cached.nodes[i]; i++) {
						if (node.parentNode != null && nodes.indexOf(node) < 0) clear([node], [cached[i]])
					}
					if (data.length < cached.length) cached.length = data.length;
					cached.nodes = nodes
				}
			}
			else if (data != null && dataType === OBJECT) {
				if (!data.attrs) data.attrs = {};
				if (!cached.attrs) cached.attrs = {};

				var dataAttrKeys = Object.keys(data.attrs)
				var hasKeys = dataAttrKeys.length > ("key" in data.attrs ? 1 : 0)
				//if an element is different enough from the one in cache, recreate it
				if (data.tag != cached.tag || dataAttrKeys.join() != Object.keys(cached.attrs).join() || data.attrs.id != cached.attrs.id) {
					if (cached.nodes.length) clear(cached.nodes);
					if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload()
				}
				if (type.call(data.tag) != STRING) return;

				var node, isNew = cached.nodes.length === 0;
				if (data.attrs.xmlns) namespace = data.attrs.xmlns;
				else if (data.tag === "svg") namespace = "http://www.w3.org/2000/svg";
				else if (data.tag === "math") namespace = "http://www.w3.org/1998/Math/MathML";
				if (isNew) {
					if (data.attrs.is) node = namespace === undefined ? $document.createElement(data.tag, data.attrs.is) : $document.createElementNS(namespace, data.tag, data.attrs.is);
					else node = namespace === undefined ? $document.createElement(data.tag) : $document.createElementNS(namespace, data.tag);
					cached = {
						tag: data.tag,
						//set attributes first, then create children
						attrs: hasKeys ? setAttributes(node, data.tag, data.attrs, {}, namespace) : data.attrs,
						children: data.children != null && data.children.length > 0 ?
							build(node, data.tag, undefined, undefined, data.children, cached.children, true, 0, data.attrs.contenteditable ? node : editable, namespace, configs) :
							data.children,
						nodes: [node]
					};
					if (cached.children && !cached.children.nodes) cached.children.nodes = [];
					//edge case: setting value on <select> doesn't work before children exist, so set it again after children have been created
					if (data.tag === "select" && data.attrs.value) setAttributes(node, data.tag, {value: data.attrs.value}, {}, namespace);
					parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				else {
					node = cached.nodes[0];
					if (hasKeys) setAttributes(node, data.tag, data.attrs, cached.attrs, namespace);
					cached.children = build(node, data.tag, undefined, undefined, data.children, cached.children, false, 0, data.attrs.contenteditable ? node : editable, namespace, configs);
					cached.nodes.intact = true;
					if (shouldReattach === true && node != null) parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				//schedule configs to be called. They are called after `build` finishes running
				if (typeof data.attrs["config"] === FUNCTION) {
					var context = cached.configContext = cached.configContext || {};

					// bind
					var callback = function(data, args) {
						return function() {
							return data.attrs["config"].apply(data, args)
						}
					};
					configs.push(callback(data, [node, !isNew, context, cached]))
				}
			}
			else if (typeof dataType != FUNCTION) {
				//handle text nodes
				var nodes;
				if (cached.nodes.length === 0) {
					if (data.$trusted) {
						nodes = injectHTML(parentElement, index, data)
					}
					else {
						nodes = [$document.createTextNode(data)];
						if (!parentElement.nodeName.match(voidElements)) parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null)
					}
					cached = "string number boolean".indexOf(typeof data) > -1 ? new data.constructor(data) : data;
					cached.nodes = nodes
				}
				else if (cached.valueOf() !== data.valueOf() || shouldReattach === true) {
					nodes = cached.nodes;
					if (!editable || editable !== $document.activeElement) {
						if (data.$trusted) {
							clear(nodes, cached);
							nodes = injectHTML(parentElement, index, data)
						}
						else {
							//corner case: replacing the nodeValue of a text node that is a child of a textarea/contenteditable doesn't work
							//we need to update the value property of the parent textarea or the innerHTML of the contenteditable element instead
							if (parentTag === "textarea") parentElement.value = data;
							else if (editable) editable.innerHTML = data;
							else {
								if (nodes[0].nodeType === 1 || nodes.length > 1) { //was a trusted string
									clear(cached.nodes, cached);
									nodes = [$document.createTextNode(data)]
								}
								parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null);
								nodes[0].nodeValue = data
							}
						}
					}
					cached = new data.constructor(data);
					cached.nodes = nodes
				}
				else cached.nodes.intact = true
			}

			return cached
		}
		function sortChanges(a, b) {return a.action - b.action || a.index - b.index}
		function setAttributes(node, tag, dataAttrs, cachedAttrs, namespace) {
			for (var attrName in dataAttrs) {
				var dataAttr = dataAttrs[attrName];
				var cachedAttr = cachedAttrs[attrName];
				if (!(attrName in cachedAttrs) || (cachedAttr !== dataAttr)) {
					cachedAttrs[attrName] = dataAttr;
					try {
						//`config` isn't a real attributes, so ignore it
						if (attrName === "config" || attrName == "key") continue;
						//hook event handlers to the auto-redrawing system
						else if (typeof dataAttr === FUNCTION && attrName.indexOf("on") === 0) {
							node[attrName] = autoredraw(dataAttr, node)
						}
						//handle `style: {...}`
						else if (attrName === "style" && dataAttr != null && type.call(dataAttr) === OBJECT) {
							for (var rule in dataAttr) {
								if (cachedAttr == null || cachedAttr[rule] !== dataAttr[rule]) node.style[rule] = dataAttr[rule]
							}
							for (var rule in cachedAttr) {
								if (!(rule in dataAttr)) node.style[rule] = ""
							}
						}
						//handle SVG
						else if (namespace != null) {
							if (attrName === "href") node.setAttributeNS("http://www.w3.org/1999/xlink", "href", dataAttr);
							else if (attrName === "className") node.setAttribute("class", dataAttr);
							else node.setAttribute(attrName, dataAttr)
						}
						//handle cases that are properties (but ignore cases where we should use setAttribute instead)
						//- list and form are typically used as strings, but are DOM element references in js
						//- when using CSS selectors (e.g. `m("[style='']")`), style is used as a string, but it's an object in js
						else if (attrName in node && !(attrName === "list" || attrName === "style" || attrName === "form" || attrName === "type")) {
							//#348 don't set the value if not needed otherwise cursor placement breaks in Chrome
							if (tag !== "input" || node[attrName] !== dataAttr) node[attrName] = dataAttr
						}
						else node.setAttribute(attrName, dataAttr)
					}
					catch (e) {
						//swallow IE's invalid argument errors to mimic HTML's fallback-to-doing-nothing-on-invalid-attributes behavior
						if (e.message.indexOf("Invalid argument") < 0) throw e
					}
				}
				//#348 dataAttr may not be a string, so use loose comparison (double equal) instead of strict (triple equal)
				else if (attrName === "value" && tag === "input" && node.value != dataAttr) {
					node.value = dataAttr
				}
			}
			return cachedAttrs
		}
		function clear(nodes, cached) {
			for (var i = nodes.length - 1; i > -1; i--) {
				if (nodes[i] && nodes[i].parentNode) {
					try {nodes[i].parentNode.removeChild(nodes[i])}
					catch (e) {} //ignore if this fails due to order of events (see http://stackoverflow.com/questions/21926083/failed-to-execute-removechild-on-node)
					cached = [].concat(cached);
					if (cached[i]) unload(cached[i])
				}
			}
			if (nodes.length != 0) nodes.length = 0
		}
		function unload(cached) {
			if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload();
			if (cached.children) {
				if (type.call(cached.children) === ARRAY) {
					for (var i = 0, child; child = cached.children[i]; i++) unload(child)
				}
				else if (cached.children.tag) unload(cached.children)
			}
		}
		function injectHTML(parentElement, index, data) {
			var nextSibling = parentElement.childNodes[index];
			if (nextSibling) {
				var isElement = nextSibling.nodeType != 1;
				var placeholder = $document.createElement("span");
				if (isElement) {
					parentElement.insertBefore(placeholder, nextSibling || null);
					placeholder.insertAdjacentHTML("beforebegin", data);
					parentElement.removeChild(placeholder)
				}
				else nextSibling.insertAdjacentHTML("beforebegin", data)
			}
			else parentElement.insertAdjacentHTML("beforeend", data);
			var nodes = [];
			while (parentElement.childNodes[index] !== nextSibling) {
				nodes.push(parentElement.childNodes[index]);
				index++
			}
			return nodes
		}
		function autoredraw(callback, object) {
			return function(e) {
				e = e || event;
				m.redraw.strategy("diff");
				m.startComputation();
				try {return callback.call(object, e)}
				finally {
					endFirstComputation()
				}
			}
		}

		var html;
		var documentNode = {
			appendChild: function(node) {
				if (html === undefined) html = $document.createElement("html");
				if ($document.documentElement && $document.documentElement !== node) {
					$document.replaceChild(node, $document.documentElement)
				}
				else $document.appendChild(node);
				this.childNodes = $document.childNodes
			},
			insertBefore: function(node) {
				this.appendChild(node)
			},
			childNodes: []
		};
		var nodeCache = [], cellCache = {};
		m.render = function(root, cell, forceRecreation) {
			var configs = [];
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var id = getCellCacheKey(root);
			var isDocumentRoot = root === $document;
			var node = isDocumentRoot || root === $document.documentElement ? documentNode : root;
			if (isDocumentRoot && cell.tag != "html") cell = {tag: "html", attrs: {}, children: cell};
			if (cellCache[id] === undefined) clear(node.childNodes);
			if (forceRecreation === true) reset(root);
			cellCache[id] = build(node, null, undefined, undefined, cell, cellCache[id], false, 0, null, undefined, configs);
			for (var i = 0, len = configs.length; i < len; i++) configs[i]()
		};
		function getCellCacheKey(element) {
			var index = nodeCache.indexOf(element);
			return index < 0 ? nodeCache.push(element) - 1 : index
		}

		m.trust = function(value) {
			value = new String(value);
			value.$trusted = true;
			return value
		};

		function gettersetter(store) {
			var prop = function() {
				if (arguments.length) store = arguments[0];
				return store
			};

			prop.toJSON = function() {
				return store
			};

			return prop
		}

		m.prop = function (store) {
			//note: using non-strict equality check here because we're checking if store is null OR undefined
			if (((store != null && type.call(store) === OBJECT) || typeof store === FUNCTION) && typeof store.then === FUNCTION) {
				return propify(store)
			}

			return gettersetter(store)
		};

		var roots = [], modules = [], controllers = [], lastRedrawId = null, lastRedrawCallTime = 0, computePostRedrawHook = null, prevented = false, topModule;
		var FRAME_BUDGET = 16; //60 frames per second = 1 call per 16 ms
		m.module = function(root, module) {
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var index = roots.indexOf(root);
			if (index < 0) index = roots.length;
			var isPrevented = false;
			if (controllers[index] && typeof controllers[index].onunload === FUNCTION) {
				var event = {
					preventDefault: function() {isPrevented = true}
				};
				controllers[index].onunload(event)
			}
			if (!isPrevented) {
				m.redraw.strategy("all");
				m.startComputation();
				roots[index] = root;
				var currentModule = topModule = module = module || {};
				var controller = new (module.controller || function() {});
				//controllers may call m.module recursively (via m.route redirects, for example)
				//this conditional ensures only the last recursive m.module call is applied
				if (currentModule === topModule) {
					controllers[index] = controller;
					modules[index] = module
				}
				endFirstComputation();
				return controllers[index]
			}
		};
		m.redraw = function(force) {
			//lastRedrawId is a positive number if a second redraw is requested before the next animation frame
			//lastRedrawID is null if it's the first redraw and not an event handler
			if (lastRedrawId && force !== true) {
				//when setTimeout: only reschedule redraw if time between now and previous redraw is bigger than a frame, otherwise keep currently scheduled timeout
				//when rAF: always reschedule redraw
				if (new Date - lastRedrawCallTime > FRAME_BUDGET || $requestAnimationFrame === window.requestAnimationFrame) {
					if (lastRedrawId > 0) $cancelAnimationFrame(lastRedrawId);
					lastRedrawId = $requestAnimationFrame(redraw, FRAME_BUDGET)
				}
			}
			else {
				redraw();
				lastRedrawId = $requestAnimationFrame(function() {lastRedrawId = null}, FRAME_BUDGET)
			}
		};
		m.redraw.strategy = m.prop();
		var blank = function() {return ""}
		function redraw() {
			var forceRedraw = m.redraw.strategy() === "all";
			for (var i = 0, root; root = roots[i]; i++) {
				if (controllers[i]) {
					m.render(root, (modules[i].view || blank)(controllers[i]), forceRedraw)
				}
			}
			//after rendering within a routed context, we need to scroll back to the top, and fetch the document title for history.pushState
			if (computePostRedrawHook) {
				computePostRedrawHook();
				computePostRedrawHook = null
			}
			lastRedrawId = null;
			lastRedrawCallTime = new Date;
			m.redraw.strategy("diff")
		}

		var pendingRequests = 0;
		m.startComputation = function() {pendingRequests++};
		m.endComputation = function() {
			pendingRequests = Math.max(pendingRequests - 1, 0);
			if (pendingRequests === 0) m.redraw()
		};
		var endFirstComputation = function() {
			if (m.redraw.strategy() == "none") {
				pendingRequests--
				m.redraw.strategy("diff")
			}
			else m.endComputation();
		}

		m.withAttr = function(prop, withAttrCallback) {
			return function(e) {
				e = e || event;
				var currentTarget = e.currentTarget || this;
				withAttrCallback(prop in currentTarget ? currentTarget[prop] : currentTarget.getAttribute(prop))
			}
		};

		//routing
		var modes = {pathname: "", hash: "#", search: "?"};
		var redirect = function() {}, routeParams, currentRoute;
		m.route = function() {
			//m.route()
			if (arguments.length === 0) return currentRoute;
			//m.route(el, defaultRoute, routes)
			else if (arguments.length === 3 && type.call(arguments[1]) === STRING) {
				var root = arguments[0], defaultRoute = arguments[1], router = arguments[2];
				redirect = function(source) {
					var path = currentRoute = normalizeRoute(source);
					if (!routeByValue(root, router, path)) {
						m.route(defaultRoute, true)
					}
				};
				var listener = m.route.mode === "hash" ? "onhashchange" : "onpopstate";
				window[listener] = function() {
					var path = $location[m.route.mode]
					if (m.route.mode === "pathname") path += $location.search
					if (currentRoute != normalizeRoute(path)) {
						redirect(path)
					}
				};
				computePostRedrawHook = setScroll;
				window[listener]()
			}
			//config: m.route
			else if (arguments[0].addEventListener) {
				var element = arguments[0];
				var isInitialized = arguments[1];
				var context = arguments[2];
				element.href = (m.route.mode !== 'pathname' ? $location.pathname : '') + modes[m.route.mode] + this.attrs.href;
				element.removeEventListener("click", routeUnobtrusive);
				element.addEventListener("click", routeUnobtrusive)
			}
			//m.route(route, params)
			else if (type.call(arguments[0]) === STRING) {
				currentRoute = arguments[0];
				var args = arguments[1] || {}
				var queryIndex = currentRoute.indexOf("?")
				var params = queryIndex > -1 ? parseQueryString(currentRoute.slice(queryIndex + 1)) : {}
				for (var i in args) params[i] = args[i]
				var querystring = buildQueryString(params)
				var currentPath = queryIndex > -1 ? currentRoute.slice(0, queryIndex) : currentRoute
				if (querystring) currentRoute = currentPath + (currentPath.indexOf("?") === -1 ? "?" : "&") + querystring;

				var shouldReplaceHistoryEntry = (arguments.length === 3 ? arguments[2] : arguments[1]) === true || currentRoute === arguments[0];

				if (window.history.pushState) {
					computePostRedrawHook = function() {
						window.history[shouldReplaceHistoryEntry ? "replaceState" : "pushState"](null, $document.title, modes[m.route.mode] + currentRoute);
						setScroll()
					};
					redirect(modes[m.route.mode] + currentRoute)
				}
				else $location[m.route.mode] = currentRoute
			}
		};
		m.route.param = function(key) {
			if (!routeParams) throw new Error("You must call m.route(element, defaultRoute, routes) before calling m.route.param()")
			return routeParams[key]
		};
		m.route.mode = "search";
		function normalizeRoute(route) {
			return route.slice(modes[m.route.mode].length)
		}
		function routeByValue(root, router, path) {
			routeParams = {};

			var queryStart = path.indexOf("?");
			if (queryStart !== -1) {
				routeParams = parseQueryString(path.substr(queryStart + 1, path.length));
				path = path.substr(0, queryStart)
			}

			for (var route in router) {
				if (route === path) {
					m.module(root, router[route]);
					return true
				}

				var matcher = new RegExp("^" + route.replace(/:[^\/]+?\.{3}/g, "(.*?)").replace(/:[^\/]+/g, "([^\\/]+)") + "\/?$");

				if (matcher.test(path)) {
					path.replace(matcher, function() {
						var keys = route.match(/:[^\/]+/g) || [];
						var values = [].slice.call(arguments, 1, -2);
						for (var i = 0, len = keys.length; i < len; i++) routeParams[keys[i].replace(/:|\./g, "")] = decodeURIComponent(values[i])
						m.module(root, router[route])
					});
					return true
				}
			}
		}
		function routeUnobtrusive(e) {
			e = e || event;
			if (e.ctrlKey || e.metaKey || e.which === 2) return;
			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;
			var currentTarget = e.currentTarget || this;
			var args = m.route.mode === "pathname" && currentTarget.search ? parseQueryString(currentTarget.search.slice(1)) : {};
			m.route(currentTarget[m.route.mode].slice(modes[m.route.mode].length), args)
		}
		function setScroll() {
			if (m.route.mode != "hash" && $location.hash) $location.hash = $location.hash;
			else window.scrollTo(0, 0)
		}
		function buildQueryString(object, prefix) {
			var str = [];
			for(var prop in object) {
				var key = prefix ? prefix + "[" + prop + "]" : prop, value = object[prop];
				var valueType = type.call(value)
				var pair = value != null && (valueType === OBJECT) ?
					buildQueryString(value, key) :
					valueType === ARRAY ?
						value.map(function(item) {return encodeURIComponent(key) + "=" + encodeURIComponent(item)}).join("&") :
						encodeURIComponent(key) + "=" + encodeURIComponent(value)
				str.push(pair)
			}
			return str.join("&")
		}
		
		function parseQueryString(str) {
			var pairs = str.split("&"), params = {};
			for (var i = 0, len = pairs.length; i < len; i++) {
				var pair = pairs[i].split("=");
				params[decodeURIComponent(pair[0])] = pair[1] ? decodeURIComponent(pair[1]) : ""
			}
			return params
		}
		function reset(root) {
			var cacheKey = getCellCacheKey(root);
			clear(root.childNodes, cellCache[cacheKey]);
			cellCache[cacheKey] = undefined
		}

		m.deferred = function () {
			var deferred = new Deferred();
			deferred.promise = propify(deferred.promise);
			return deferred
		};
		function propify(promise) {
			var prop = m.prop();
			promise.then(prop);
			prop.then = function(resolve, reject) {
				return propify(promise.then(resolve, reject))
			};
			return prop
		}
		//Promiz.mithril.js | Zolmeister | MIT
		//a modified version of Promiz.js, which does not conform to Promises/A+ for two reasons:
		//1) `then` callbacks are called synchronously (because setTimeout is too slow, and the setImmediate polyfill is too big
		//2) throwing subclasses of Error cause the error to be bubbled up instead of triggering rejection (because the spec does not account for the important use case of default browser error handling, i.e. message w/ line number)
		function Deferred(successCallback, failureCallback) {
			var RESOLVING = 1, REJECTING = 2, RESOLVED = 3, REJECTED = 4;
			var self = this, state = 0, promiseValue = 0, next = [];

			self["promise"] = {};

			self["resolve"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = RESOLVING;

					fire()
				}
				return this
			};

			self["reject"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = REJECTING;

					fire()
				}
				return this
			};

			self.promise["then"] = function(successCallback, failureCallback) {
				var deferred = new Deferred(successCallback, failureCallback);
				if (state === RESOLVED) {
					deferred.resolve(promiseValue)
				}
				else if (state === REJECTED) {
					deferred.reject(promiseValue)
				}
				else {
					next.push(deferred)
				}
				return deferred.promise
			};

			function finish(type) {
				state = type || REJECTED;
				next.map(function(deferred) {
					state === RESOLVED && deferred.resolve(promiseValue) || deferred.reject(promiseValue)
				})
			}

			function thennable(then, successCallback, failureCallback, notThennableCallback) {
				if (((promiseValue != null && type.call(promiseValue) === OBJECT) || typeof promiseValue === FUNCTION) && typeof then === FUNCTION) {
					try {
						// count protects against abuse calls from spec checker
						var count = 0;
						then.call(promiseValue, function(value) {
							if (count++) return;
							promiseValue = value;
							successCallback()
						}, function (value) {
							if (count++) return;
							promiseValue = value;
							failureCallback()
						})
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						failureCallback()
					}
				} else {
					notThennableCallback()
				}
			}

			function fire() {
				// check if it's a thenable
				var then;
				try {
					then = promiseValue && promiseValue.then
				}
				catch (e) {
					m.deferred.onerror(e);
					promiseValue = e;
					state = REJECTING;
					return fire()
				}
				thennable(then, function() {
					state = RESOLVING;
					fire()
				}, function() {
					state = REJECTING;
					fire()
				}, function() {
					try {
						if (state === RESOLVING && typeof successCallback === FUNCTION) {
							promiseValue = successCallback(promiseValue)
						}
						else if (state === REJECTING && typeof failureCallback === "function") {
							promiseValue = failureCallback(promiseValue);
							state = RESOLVING
						}
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						return finish()
					}

					if (promiseValue === self) {
						promiseValue = TypeError();
						finish()
					}
					else {
						thennable(then, function () {
							finish(RESOLVED)
						}, finish, function () {
							finish(state === RESOLVING && RESOLVED)
						})
					}
				})
			}
		}
		m.deferred.onerror = function(e) {
			if (type.call(e) === "[object Error]" && !e.constructor.toString().match(/ Error/)) throw e
		};

		m.sync = function(args) {
			var method = "resolve";
			function synchronizer(pos, resolved) {
				return function(value) {
					results[pos] = value;
					if (!resolved) method = "reject";
					if (--outstanding === 0) {
						deferred.promise(results);
						deferred[method](results)
					}
					return value
				}
			}

			var deferred = m.deferred();
			var outstanding = args.length;
			var results = new Array(outstanding);
			if (args.length > 0) {
				for (var i = 0; i < args.length; i++) {
					args[i].then(synchronizer(i, true), synchronizer(i, false))
				}
			}
			else deferred.resolve([]);

			return deferred.promise
		};
		function identity(value) {return value}

		function ajax(options) {
			if (options.dataType && options.dataType.toLowerCase() === "jsonp") {
				var callbackKey = "mithril_callback_" + new Date().getTime() + "_" + (Math.round(Math.random() * 1e16)).toString(36);
				var script = $document.createElement("script");

				window[callbackKey] = function(resp) {
					script.parentNode.removeChild(script);
					options.onload({
						type: "load",
						target: {
							responseText: resp
						}
					});
					window[callbackKey] = undefined
				};

				script.onerror = function(e) {
					script.parentNode.removeChild(script);

					options.onerror({
						type: "error",
						target: {
							status: 500,
							responseText: JSON.stringify({error: "Error making jsonp request"})
						}
					});
					window[callbackKey] = undefined;

					return false
				};

				script.onload = function(e) {
					return false
				};

				script.src = options.url
					+ (options.url.indexOf("?") > 0 ? "&" : "?")
					+ (options.callbackKey ? options.callbackKey : "callback")
					+ "=" + callbackKey
					+ "&" + buildQueryString(options.data || {});
				$document.body.appendChild(script)
			}
			else {
				var xhr = new window.XMLHttpRequest;
				xhr.open(options.method, options.url, true, options.user, options.password);
				xhr.onreadystatechange = function() {
					if (xhr.readyState === 4) {
						if (xhr.status >= 200 && xhr.status < 300) options.onload({type: "load", target: xhr});
						else options.onerror({type: "error", target: xhr})
					}
				};
				if (options.serialize === JSON.stringify && options.data && options.method !== "GET") {
					xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8")
				}
				if (options.deserialize === JSON.parse) {
					xhr.setRequestHeader("Accept", "application/json, text/*");
				}
				if (typeof options.config === FUNCTION) {
					var maybeXhr = options.config(xhr, options);
					if (maybeXhr != null) xhr = maybeXhr
				}

				var data = options.method === "GET" || !options.data ? "" : options.data
				if (data && (type.call(data) != STRING && data.constructor != window.FormData)) {
					throw "Request data should be either be a string or FormData. Check the `serialize` option in `m.request`";
				}
				xhr.send(data);
				return xhr
			}
		}
		function bindData(xhrOptions, data, serialize) {
			if (xhrOptions.method === "GET" && xhrOptions.dataType != "jsonp") {
				var prefix = xhrOptions.url.indexOf("?") < 0 ? "?" : "&";
				var querystring = buildQueryString(data);
				xhrOptions.url = xhrOptions.url + (querystring ? prefix + querystring : "")
			}
			else xhrOptions.data = serialize(data);
			return xhrOptions
		}
		function parameterizeUrl(url, data) {
			var tokens = url.match(/:[a-z]\w+/gi);
			if (tokens && data) {
				for (var i = 0; i < tokens.length; i++) {
					var key = tokens[i].slice(1);
					url = url.replace(tokens[i], data[key]);
					delete data[key]
				}
			}
			return url
		}

		m.request = function(xhrOptions) {
			if (xhrOptions.background !== true) m.startComputation();
			var deferred = m.deferred();
			var isJSONP = xhrOptions.dataType && xhrOptions.dataType.toLowerCase() === "jsonp";
			var serialize = xhrOptions.serialize = isJSONP ? identity : xhrOptions.serialize || JSON.stringify;
			var deserialize = xhrOptions.deserialize = isJSONP ? identity : xhrOptions.deserialize || JSON.parse;
			var extract = xhrOptions.extract || function(xhr) {
				return xhr.responseText.length === 0 && deserialize === JSON.parse ? null : xhr.responseText
			};
			xhrOptions.url = parameterizeUrl(xhrOptions.url, xhrOptions.data);
			xhrOptions = bindData(xhrOptions, xhrOptions.data, serialize);
			xhrOptions.onload = xhrOptions.onerror = function(e) {
				try {
					e = e || event;
					var unwrap = (e.type === "load" ? xhrOptions.unwrapSuccess : xhrOptions.unwrapError) || identity;
					var response = unwrap(deserialize(extract(e.target, xhrOptions)));
					if (e.type === "load") {
						if (type.call(response) === ARRAY && xhrOptions.type) {
							for (var i = 0; i < response.length; i++) response[i] = new xhrOptions.type(response[i])
						}
						else if (xhrOptions.type) response = new xhrOptions.type(response)
					}
					deferred[e.type === "load" ? "resolve" : "reject"](response)
				}
				catch (e) {
					m.deferred.onerror(e);
					deferred.reject(e)
				}
				if (xhrOptions.background !== true) m.endComputation()
			};
			ajax(xhrOptions);
			deferred.promise(xhrOptions.initialValue);
			return deferred.promise
		};

		//testing API
		m.deps = function(mock) {
			initialize(window = mock || window);
			return window;
		};
		//for internal testing only, do not use `m.deps.factory`
		m.deps.factory = app;

		return m
	})(typeof window != "undefined" ? window : {});

	if (typeof module != "undefined" && module !== null && module.exports) module.exports = m;
	else if (true) !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {return m}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(15)(module)))

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	// Generated by CoffeeScript 1.6.3
	var linear_partition, min;

	min = function(arr) {
	  var computed, result, x, _i, _len;
	  for (_i = 0, _len = arr.length; _i < _len; _i++) {
	    x = arr[_i];
	    computed = x[0];
	    if (!result || computed < result.computed) {
	      result = {
	        value: x,
	        computed: computed
	      };
	    }
	  }
	  return result.value;
	};

	linear_partition = function(seq, k) {
	  var ans, i, j, m, n, solution, table, x, y, _i, _j, _k, _l;
	  n = seq.length;
	  if (k <= 0) {
	    return [];
	  }
	  if (k > n) {
	    return seq.map(function(x) {
	      return [x];
	    });
	  }
	  table = (function() {
	    var _i, _results;
	    _results = [];
	    for (y = _i = 0; 0 <= n ? _i < n : _i > n; y = 0 <= n ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _results1;
	        _results1 = [];
	        for (x = _j = 0; 0 <= k ? _j < k : _j > k; x = 0 <= k ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  solution = (function() {
	    var _i, _ref, _results;
	    _results = [];
	    for (y = _i = 0, _ref = n - 1; 0 <= _ref ? _i < _ref : _i > _ref; y = 0 <= _ref ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _ref1, _results1;
	        _results1 = [];
	        for (x = _j = 0, _ref1 = k - 1; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  for (i = _i = 0; 0 <= n ? _i < n : _i > n; i = 0 <= n ? ++_i : --_i) {
	    table[i][0] = seq[i] + (i ? table[i - 1][0] : 0);
	  }
	  for (j = _j = 0; 0 <= k ? _j < k : _j > k; j = 0 <= k ? ++_j : --_j) {
	    table[0][j] = seq[0];
	  }
	  for (i = _k = 1; 1 <= n ? _k < n : _k > n; i = 1 <= n ? ++_k : --_k) {
	    for (j = _l = 1; 1 <= k ? _l < k : _l > k; j = 1 <= k ? ++_l : --_l) {
	      m = min((function() {
	        var _m, _results;
	        _results = [];
	        for (x = _m = 0; 0 <= i ? _m < i : _m > i; x = 0 <= i ? ++_m : --_m) {
	          _results.push([Math.max(table[x][j - 1], table[i][0] - table[x][0]), x]);
	        }
	        return _results;
	      })());
	      table[i][j] = m[0];
	      solution[i - 1][j - 1] = m[1];
	    }
	  }
	  n = n - 1;
	  k = k - 2;
	  ans = [];
	  while (k >= 0) {
	    ans = [
	      (function() {
	        var _m, _ref, _ref1, _results;
	        _results = [];
	        for (i = _m = _ref = solution[n - 1][k] + 1, _ref1 = n + 1; _ref <= _ref1 ? _m < _ref1 : _m > _ref1; i = _ref <= _ref1 ? ++_m : --_m) {
	          _results.push(seq[i]);
	        }
	        return _results;
	      })()
	    ].concat(ans);
	    n = solution[n - 1][k];
	    k = k - 1;
	  }
	  return [
	    (function() {
	      var _m, _ref, _results;
	      _results = [];
	      for (i = _m = 0, _ref = n + 1; 0 <= _ref ? _m < _ref : _m > _ref; i = 0 <= _ref ? ++_m : --_m) {
	        _results.push(seq[i]);
	      }
	      return _results;
	    })()
	  ].concat(ans);
	};

	module.exports = function(seq, k) {
	  if (k <= 0) {
	    return [];
	  }
	  while (k) {
	    try {
	      return linear_partition(seq, k--);
	    } catch (_error) {}
	  }
	};


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	reauire("../SCSS/avatar.scss")
	var avatar = {}
	avatar.view = function (user) {
		return {tag: "span", attrs: {class:"ssavatar__wrapper"}, children: [{tag: "img", attrs: {class:"ssavatar__image"}}, user]}
	}
	module.exports = avatar
	//

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(11);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	exports.push([module.id, ".ssimage__noimagetext{transition:all 0.5s ease-in}.ssimage__wrapper{width:100%;position:relative;min-height:200px;background-color:#fefefe;border:solid 1px #f5f5f5}.ssimage__wrapper:hover .ssimage__noimagetext{opacity:0.85}.ssimage__imageinput{position:absolute;top:0;left:0;position:absolute;display:block;width:100%;height:100%;opacity:0}.ssimage__changephoto{text-decoration:underline;cursor:pointer;text-align:center;color:#999}.ssimage__noimagetext{width:30%;margin:20% auto;font-size:1em;line-height:1em;text-align:center;height:auto;padding:1.2em;color:#fff;opacity:1;border-radius:24px;background-color:#67C400}.ssimage__loadingimage{height:100%;width:100%;background:transparent url(/static/loading.png) center center no-repeat}", ""]);

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(13);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(14)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(16)();
	exports.push([module.id, "@font-face{font-family:\"ssicons\";src:url('/static/fonts/ss.eot');src:local('/static/fonts/ss');src:url('/static/fonts/ss.svg#ss') format('svg');src:url('/static/fonts/ss.woff') format('woff');}.sscolours__block{width:14%;margin:1%;display:inline-block;text-align:center}.sscolours__block .sscolours__blocktext{font-size:0.8em;font-weight:bold;color:#666;display:block}.sscolours__block .sscolours__blockpallete{margin:auto;box-shadow:inset 0 1px 3px rgba(0,0,0,0.2);width:35px;height:35px;border-radius:5px}.sscolours__block .sscolours__blockpallete--selected{color:#68BB0D !important;background-color:#fff;background-color:rgba(255,255,255,0.9);font-size:0.9em;padding:0.5em;border-radius:5px;line-height:35px;font-family:ssicons}", ""]);

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ }
/******/ ])