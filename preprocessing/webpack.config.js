var webpack = require("/Users/victor/node_modules/webpack");
module.exports = { 
    entry: {
        "app": './Riot/app.tag',
        home: './Riot/home.tag',
        },
    output: {
        filename: '../static/[name].bundle.js', //this is the default name, so you can skip it
    },
    module: {
        preLoaders: [
            {
                test: /\.tag$/,
                loader: 'tag'
            }
        ],
        loaders: [
            {
                test: /\.scss$/,
                loader: 'style!css!sass'
            },
            {
                test: /\.js$/,
                loader:'msx-loader'
            }
        ]
    },
    resolve:{root: ["/home/victor/node_modules"]},
    plugins: [
    new webpack.optimize.UglifyJsPlugin({minimize: true})
  ]
}
