/** @jsx m */
// @prepros-prepend linear-partition.js
var gridify = function(items, numPartitions) {
	var items = items || []
	var WIDTH = 450;
	var INIT_HEIGHT = 150;
	var MAX_HEIGHT = 500;
	var widths = []
	var index = -1
		var ratios = items.map(function(item) {
			return INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
		var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(screenWidth/350)))//Math.ceil(items.length/Math.ceil(screenWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item){
				return total + item
			}, 0)
			var ratio = (screenWidth / sum)
			return row.map(function (item){
				index += 1

				var width = (item/sum) * screenWidth
				return [width, ratio * INIT_HEIGHT]
			})
		}.bind(this))
		var tmp = []
		return tmp.concat.apply(tmp, test)	
}