/** @jsx m */
'use strict';
var tags = {}
tags.controller = function() {

}
// tags.vm = {isSelected: false}
tags.view = function (vm) {
	var icon= vm.isSelected ? "\e600" : "\e600"
	var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
		return m("div", {class:"sstags__wrapper "+gender}, [
			m("span", {class:"sstags__icon"}, [icon]),
			m("span", {class:"sstags__text"}, [vm.tagText])	
			])
}

