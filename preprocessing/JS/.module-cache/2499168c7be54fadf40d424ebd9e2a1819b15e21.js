/** @jsx m */
'use strict';
var image = {}
image.controller = function () {

}
image.view = function (vm) {
	console.log(vm)
	var content = ""
	if (vm.isLoading) {
		content = m("div", {class:"ssimage__loadingimage"})
	} else {
		if (!!vm.entry.Image()) {
			content = m("div", [m("img", {src:""}),m("p", ["Click to change photo"])])
		} else {
			content = m("div", {class:"ssimage__noimage"}, [m("p", {class:"ssimage__noimagetext"}, ["+ click to add a photo"])])
		}
	}
	return m("div", [
	content
	])
}