/** @jsx m */
'use strict';
var image = {}
image.onchange= function(callback) {

}
image.controller = function () {

}
image.view = function (vm) {
	console.log(vm)
	var content;
	var imageInput = ""
	if (vm.isLoading) {
		content = m("div", {class:"ssimage__loadingimage"})
	} else {
		imageInput = m("input", {class:"ssimage__imageinput", type:"file", onchange:""})
		if (!!vm.entry.Image()) {
			content = m("div", [m("img", {src:""}),m("p", ["change photo"])])
		} else {
			content = m("div", {class:"ssimage__noimage"}, [m("p", {class:"ssimage__noimagetext"}, ["+ add a photo"])])
		}
	}
	return m("div", {class:"ssimage__wrapper"}, [
	content,
	imageInput
	])
}