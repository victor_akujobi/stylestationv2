/** @jsx m */
var m = require("mithril")
// require
var ProfileHeader = {}
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	return m("div", {class:"profile_header__wrapper"}, [
		m("div", {class:""}, ["Large Image"]),
		m("div", {class:"profile_main"}, [
			m("div", {class:"profile_info"}, [
			m("p", [vm.profile.DisplayName]),
			m("p", [vm.profile.Description]),
			m("p", [vm.profile.Description])
			]),
			m("div", {class:"profile_stats"}),
			m("div", [vm.profile.Collections]),
			m("div", [vm.profile.Uploads])
		])
	])
}
module.exports = ProfileHeader