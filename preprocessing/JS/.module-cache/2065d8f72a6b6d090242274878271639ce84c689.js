/** @jsx m */
'use strict';
var image = {}
image.onchange= function(callback, e) {
	// m.request(/)
	console.log(e)
	var formData = new FormData
    for (var i = 0; i < files.length; i++) {
        formData.append("file" + i, files[i])
    }
	callback("jsdhjsh")
	return m.request({
        method: "POST",
        url: "/api/files",
        data: formData,
        //simply pass the FormData object intact to the underlying XMLHttpRequest, instead of JSON.stringify'ing it
        serialize: function(value) {return value}
    })
}
image.controller = function () {

}
image.view = function (vm) {
	console.log(vm)
	var content;
	var imageInput = ""
	if (vm.isLoading) {
		content = m("div", {class:"ssimage__loadingimage"})
	} else {
		imageInput = m("input", {class:"ssimage__imageinput", type:"file", onchange:image.onchange.bind(null, vm.onImageChange)})
		if (!!vm.entry.Image()) {
			content = m("div", [m("img", {src:""}),m("p", ["change photo"])])
		} else {
			content = m("div", {class:"ssimage__noimage"}, [m("p", {class:"ssimage__noimagetext"}, ["+ add a photo"])])
		}
	}
	return m("div", {class:"ssimage__wrapper"}, [
	content,
	imageInput
	])
}