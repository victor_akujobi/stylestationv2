/** @jsx m */
var image = {}
image.controller = function () {

}
image.view = function (vm) {
	if (vm.isLoading) {
		content = m("div", {class:"ssimage__loading"})
	} else {
		if (!!vm.entry.Image()) {
			m("img", {src:""})
		} else {
			m("div", {class:"ssimage__noimage"}, [m("p", ["Click to Upload"])])
		}
	}
	return m("div"
	)
}