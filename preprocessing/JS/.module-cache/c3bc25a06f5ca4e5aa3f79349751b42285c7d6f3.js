/** @jsx m */
var defaultGenders = ["Male", "Female"]
var genders = {}
genders.controller = function () {

}
genders.view = function (vm) {
	var entry = vm.entry;
	var genderView = defaultGenders.map(function (gender) {
		var icon= vm.isSelected ? m.trust("&#xE60A;") : "U"//m.trust(genderFonts[vm.tagText.toLowerCase()])
		var iconClass = gender == entry.Gender() ? "sstags__icon--selected" : ""
		return m("div", {key:gender, onclick:vm.onclick.bind(null, gender), class:"sstags__wrapper "+gender.toLowerCase()}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [gender])	
			])
	})
	return m("div", [ " ", entry.Gender()])
}