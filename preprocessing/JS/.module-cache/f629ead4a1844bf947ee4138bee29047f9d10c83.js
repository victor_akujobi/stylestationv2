/** @jsx m */
var m = require("mithril")
require("../SCSS/utils.scss");
var fonts = require("./fonts")
var Popup = function () {
		var show = m.prop(true)
		var toggleDisplay = function () {
			show(!show())
		}
		var closePopup = function () {
			show(false)
		}
		var overlay = document.body.appendChild(document.createElement('div'))
		overlay.className = "overlay_hidden"
		return function (body) {
			overlay.className = "overlay_visible"
			m.render(overlay, body)
		}
}

var Panel = function (options) {
	var content = options.content
	if (!content) {return}
	var type = options.type || info
	var timeout = options.timeout || 6000
	var element = m("div", {class:"srpanel__" + type}, [content])
	var panelParent = document.createElement('div')
	panelParent.className = "srpanel";
	m.render(panelParent, element)
	document.body.appendChild(panelParent)
	window.setTimeout(function () {
		document.body.removeChild(panelParent)
	}, timeout)

}
var LoginPopup = function () {
	return m("div", {class:"sslogin__wrapper"}, [
m("p", {class:"sslogin__intro_text"}, ["Not logged in? Ok then, let's get you sorted.",m("br"), " It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear"]),
m("a", {href:"/login?provider=facebook", class:"srlogin__button srlogin__button__facebook"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__facebook"}, [m.trust("&#xE60F")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Facebook Account"])
]),
m("a", {href:"/login?provider=gplus", class:"srlogin__button srlogin__button__google"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__google"}, [m.trust("&#xE610")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Google Account"])
]),
m("a", {href:"/login?provider=twitter", class:"srlogin__button srlogin__button__twitter"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__twitter"}, [m.trust("&#xE60E")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Twitter Account"])
])
])
}

// var SearchPage = function (options) {
// 	return <div>
// 	<input type="checkbox" class="switch_toggle"/>
// 		<p>I am looking for</p>
// 		<p>Choose colours</p>
// 		<p>Choose styles</p>
// 		<p>for</p>
// 		<p>{Select({options:["Male", "Female", "Android"], selected:["Male", "Female"]})}</p>
// 	</div>
// }
var SearchPage = function (vm) {
	var onChange = function (section, option) {
		// console.log(section, option)
		vm.options[section][option](!vm.options[section][option]())
		vm.onChange()
		// m.redraw()
	}
	return m("div", {class:"sidebar__wrapper"}, [
	        m("label", {for:"toggle_switch", class:"toggle_label"}),
	        m("input", {class:"toggle_switch", id:"toggle_switch", type:"checkbox"}),
	        m("div", {id:"search"}, [
		          
		          	Object.keys(vm.options).filter(function (section) {
		          		return section !== "Page"
		          	}).map(function (section, index) {
			          	return m("div", {class:"search_items_wrapper"}, [m("p", {class:"search_header"}, [section]),
			          		Object.keys(vm.options[section]).map(function (option, optionIndex) {
			          			console.log(section, option)
			          		iconFont = vm.options[section][option]() ? fonts['success'] : "-"
			          		return m("p", {onclick:onChange.bind(null, section, option), class:"search_item search_item_" + section.toLowerCase()}, [m("span", {class:"sstags__icon", config:vm.iconAnimate}, [m.trust(iconFont)]),m("label", {class:"search_item_text"} , [option])])
			          	})
			        ])
		          }),
	          
	          m("label", {class:"done_button", for:"toggle_switch"}, ["Done"])
	        ]),
	        m("div")
	      ])
}
SearchPage.prototype.toggle = function () {

}

var Select = function (state) {
	return m("div", {tabindex:"0"}, [m("span", [state.selected.join("/")]),
				state.options.map(function (option) {
					return m("p", [option])
				})
			])
}

module.exports = {Popup: Popup, LoginPopup: LoginPopup, Panel:Panel, Select:Select, SearchPage: SearchPage}