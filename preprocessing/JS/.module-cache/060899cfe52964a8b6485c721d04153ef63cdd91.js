/** @jsx m */
var m = require("mithril")
var ProfileHeader = {}
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	console.log(vm)
	return m("div", {class:"profile_header_wrapper"}, [
		m("div", {class:""}, ["Image"]),
		m("div", [vm.profile.Collections]),
		m("div", [vm.profile.Uploads])
	])
}
module.exports = ProfileHeader