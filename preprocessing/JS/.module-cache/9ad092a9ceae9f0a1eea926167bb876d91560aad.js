/** @jsx m */
require("../SCSS/colours.scss")
var m = require("mithril")
var colours = {}
colours.COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
colours.handleSelect = function (callback, value) {
	callback(value)
}
colours.view = function (vm) {
	var selected = vm.selected.reduce(function (dict, currentValue) {
		dict[currentValue] = true;
		return dict
	}, {})
	return Object.keys(colours.COLOURS).map(function (path) {
		var isSelected = !!selected[path] ? m("span", {class:"sscolours__blockpallete--selected"}, [m.trust("&#xE60A;")]) : ""
		return m("div", {class:"sscolours__block", onclick:colours.handleSelect.bind(null, vm.onclick, path)} , [m("div", {class:"sscolours__blockpallete", style:{"background-color": colours.COLOURS[path]}}, [isSelected]),m("span", {class:"sscolours__blocktext"}, [path])])
	})
}
module.exports = colours


