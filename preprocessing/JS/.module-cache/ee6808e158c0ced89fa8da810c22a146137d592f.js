/** @jsx m */
'use strict';
var image = {}
image.onchange= function(vm, e) {
	// m.request(/)
	var file = e.target.files[0]
	var formData = new FormData
    // for (var i = 0; i < files.length; i++) {
        formData.append("file", file)
    // }
	// callback("jsdhjsh")
	return m.request({
        method: "POST",
        url: "/imageprocessor/entry",
        data: formData,
        //simply pass the FormData object intact to the underlying XMLHttpRequest, instead of JSON.stringify'ing it
        serialize: function(value) {return value}
    }).then(function (response) {
    	console.log(vm.entry.Image())
    	vm.entry.Image(response.image)
    	console.log(vm.entry.Image())
    	vm.entry.ImageHeight(response.height)
    	vm.entry.ImageWidth(response.width)
    })
}
image.controller = function () {

}
image.view = function (vm) {
	console.log(vm)
	var content;
	var imageInput = ""
	if (vm.isLoading) {
		content = m("div", {class:"ssimage__loadingimage"})
	} else {
		imageInput = m("input", {class:"ssimage__imageinput", type:"file", onchange:image.onchange.bind(null, vm)})
		if (!!vm.entry.Image()) {
			content = m("div", [m("img", {style:"width:100%", src:"/static/images/ankara/"+vm.entry.Image()+"_m.jpg"}),m("p", ["change photo"])])
		} else {
			content = m("div", {class:"ssimage__noimage"}, [m("p", {class:"ssimage__noimagetext"}, ["+ add a photo"])])
		}
	}
	return m("div", {class:"ssimage__wrapper"}, [
	content,
	imageInput
	])
}