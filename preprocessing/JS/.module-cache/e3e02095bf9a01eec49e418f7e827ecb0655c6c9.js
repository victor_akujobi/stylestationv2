/** @jsx m */
//@prepros-prepend tags.js
'use strict';
// var tagComponent = require('tags')
var STYLES = ["Top", "Trousers", "Skirt", "Blazer", "Accessories"];
var entry = {Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([])}
entry.toggleStyle = function (value) {
	console.log(this)
 	var styles = this.Styles()
 	console.log(styles)
 	var index = styles.indexOf(value)
 	if (index == -1) {
 		styles.push(value)
 	} else {
 		styles.splice(index, 1)
 	}
 		this.Styles(styles)
}
var newPage = {}
newPage.controller = function () {
	this.vm = {entry:entry}
}
newPage.view = function (ctrl) {
	var entryColours = ctrl.vm.entry.Colours()
	var entryStyles = ctrl.vm.entry.Styles()
	console.log(entryStyles)
	var styles = STYLES.map(function (style) {
				return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:"male", onclick: entry.toggle.bind(entry, style)})
			})
	return m("div", [
			styles
			])
}
m.module(document.getElementById('content'), newPage)
