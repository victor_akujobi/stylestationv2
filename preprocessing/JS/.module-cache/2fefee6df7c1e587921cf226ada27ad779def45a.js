/** @jsx m */
'use strict';
var tags = {}
tags.controller = {}
tags.vm = {isSelected: false}
tags.view = function (vm) {
	var icon= vm.isSelected ? "" : ""
	var subState = !!vm.gender ? " sstags__wrapper--" + gender : ""
		return m("div", {class:"sstags__wrapper "+subState}, [
			m("span", {class:"sstags__icon"}),
			m("span", {class:"sstags__text"})
			])
}

modules.export = tags