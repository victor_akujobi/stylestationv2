/** @jsx m */
// @prepros-prepend linear-partition.js

var gridify = {}
gridify.metrics = m.prop([])
gridify.config = function(items, numPartitions, container, isInit) {
	console.log(isInit)
	if (isInit) {
		return
	}
	var createPartitions = function () {
		var elementWidth = container.offsetWidth
		items = items || []
		var WIDTH = 450;
		var INIT_HEIGHT = 150;
		var MAX_HEIGHT = 500;
		var widths = []
		console.log(items, numPartitions, elementWidth)
		var ratios = items.map(function(item) {
			return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})

	
	var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			ratio = elementWidth / sum
			return row.map(function (item) {
				return [ratio * item, ratio * 150]
			})
		}.bind(this))
		var tmp = []
		gridify.metrics(tmp.concat.apply(tmp, test))
		m.redraw()
	}
		window.onresize = function () {
			console.log("Resizing")
			createPartitions()
		}
		createPartitions()
		// m.redraw()
}
gridify.view = function (entries) {
	var metrics  = gridify.metrics()
	console.log(metrics, "MET")
	var blocks = metrics.map(function (metric, index) {
		var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
		console.log(style)
		return m("div", {class:"ssgrid__block", style:style}, [m("div", {class:"ssgrid__innerblock"}, [m("img", {style:"width:100%", src:"/static/images/ankara/"+entries[index].Image+"_m.jpg"})]),m("p", ["entries[index].Owner"])])	
	})
	return m("div", {config:gridify.config.bind(null, entries, false)}, [
		blocks
	])
}