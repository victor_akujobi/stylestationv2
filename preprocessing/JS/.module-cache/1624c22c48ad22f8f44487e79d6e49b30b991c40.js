/** @jsx m */
// @prepros-prepend linear-partition.js

var gridify = {}
gridify.metrics = []
gridify.config = function(items, numPartitions, container, isInit) {
	console.log(isInit)
	if (!isInit) {
		return
	}
	var elementWidth = container.offsetWidth
	items = items || []
	var WIDTH = 450;
	var INIT_HEIGHT = 150;
	var MAX_HEIGHT = 500;
	var widths = []
	var index = -1
	console.log(items, numPartitions, elementWidth)
	var ratios = items.map(function(item) {
		return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
	})

	var partitions = partition(ratios, numPartitions || 1)//Math.ceil(items.length/Math.ceil(screenWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			ratio = elementWidth / sum
			return row.map(function (item) {
				return [ratio * item, ratio * 150]
			})
		}.bind(this))
		var tmp = []
		gridify.metrics(tmp.concat.apply(tmp, test))
		// m.redraw()
		console.log(gridify.metrics())
}
gridify.view = function (vm) {
	var entries = vm.entries()
	var metrics  = gridify.metrics()
	console.log(metrics, "MET")
	var blocks = metrics.map(function (metric, index) {
		var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
		console.log(style)
		return m("div", {class:"ssgrid__block", style:style}, [m("div", {class:"ssgrid__innerblock"}, [m("img", {style:"width:100%", src:"/static/images/ankara/"+entries[index].Image+"_m.jpg"})])])	
	})
	return m("div", {config:gridify.config.bind(null, entries, vm.numPartitions)}, [
		blocks
	])
}