/** @jsx m */
var m = require("mithril")
// require
var ProfileHeader = {}
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	return m("div", {class:"profile_header__wrapper"}, [
		m("div", {class:""}, ["Large Image"]),
		m("div", {class:""}, [
			m("div", {class:"profile_info"}),
			m("div", [vm.profile.Collections]),
			m("div", [vm.profile.Uploads])
		])
	])
}
module.exports = ProfileHeader