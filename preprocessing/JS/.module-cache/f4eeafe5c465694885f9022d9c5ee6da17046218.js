/** @jsx m */
var Popup = {
	show:m.prop(false),
	view: function (body) {
		return this.show() ? m("div", {class:"overlay"}, [body()]): ""
	}
}

var LoginPopup = function () {
	return m("div", {class:"sslogin__wrapper"}, [
m("p", {class:"sslogin__intro_text"}, ["Not logged in? Ok then, let's get you sorted.",m("br"), " It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear"]),
m("a", {href:"/login?provider=facebook", class:"srlogin__button srlogin__button__facebook"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__facebook"}, [m.trust("&#xE60F")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Facebook Account"])
]),
m("a", {href:"/login?provider=gplus", class:"srlogin__button srlogin__button__google"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__google"}, [m.trust("&#xE610")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Google Account"])
]),
m("a", {href:"/login?provider=twitter", class:"srlogin__button srlogin__button__twitter"}, [
	m("span", {class:"srlogin__provider__icon srlogin__provider__twitter"}, [m.trust("&#xE60E")]),
	m("span", {class:"srlogin__provider__text"}, ["Continue with your Twitter Account"])
])
])
}

module.exports = {Popup: Popup, LoginPopup: LoginPopup}