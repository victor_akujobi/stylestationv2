/** @jsx m */
//@prepros-prepend gender.js
//@prepros-prepend tags.js
//@prepros-prepend image.js
//@prepros-prepend colours.js
'use strict';
// var tagComponent = require('tags')
var error = m.prop("")
var STYLES = {"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt"], "Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories"]};
var Entry = function (url) {
	if (!!url) {

	} else {
		this.Gender =  m.prop("Female")
		this.Description =  m.prop("")
		this.Colours = m.prop([])
		this.Styles = m.prop([])
		this.Image = m.prop("")
		this.ImageWidth = m.prop("")
		this.ImageHeight =  m.prop("")
	}
	this.toggle= function (type, value) {
	 	var styles = this[type]()
	 	var index = styles.indexOf(value)
	 	if (index == -1) {
	 		styles.push(value)
	 	} else {
	 		styles.splice(index, 1)
	 	}
	 		this[type](styles)
	}
	this.Save = function () {
		self = this
		m.request({"url": "/ankara", "method": "POST", data:self}).then(
			function () {
				console.log("Success")
			}, function () {
				console.log("Error")
			})
	}
}
// var entry = {Gender: m.prop("Female"), Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([]), Image:m.prop(""), ImageWidth:m.prop(""), ImageHeight: m.prop("")}

var newPage = {}
newPage.controller = function () {
	this.vm = {entry:new Entry()}
	var self = this;
	this.Send = function () {
		console.log("Sending...")
		m.request({"url": "/ankara", "method": "POST", data:self.vm.entry, "background": true}).then(
		function () {
			console.log("Success")
		}, function () {
			console.log("Error")
		})
	}
}
newPage.view = function (ctrl) {
	var entryStyles = ctrl.vm.entry.Styles().reduce(function(dict, currentValue){console.log(dict); dict[currentValue] = true; return dict}, {})
	var coloursComponent = colours.view({selected: ctrl.vm.entry.Colours(), onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Colours')})
	var styles = STYLES[ctrl.vm.entry.Gender()].map(function (style) {
				return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:ctrl.vm.entry.Gender().toLowerCase()||"female", onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Styles', style)})
			})
	return m("div", {class:"ssnewentry__wrapper"}, [
			image.view({entry:ctrl.vm.entry}),
			m("input", {placeholder:"Click to enter description", type:"text", onchange:m.withAttr("value", ctrl.vm.entry.Description), value:ctrl.vm.entry.Description(), class:"ssentry__descriptioninput"} ),
			m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["Select Gender"]),genders.view({entry:ctrl.vm.entry, onclick:ctrl.vm.entry.Gender}), " ", m("br", {style:"clear:both"})]),
			m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["Pick Relevant Styles)"]),styles, " ", m("br", {style:"clear:both"})]),
			m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["Pick Colours"]),coloursComponent, " ", m("br", {style:"clear:both"})]),
			m("button", {class:"ssnewentry__submit", onclick:ctrl.Send}, ["Done"])
			])
}
var viewMultiple = {}
viewMultiple.controller = function () {
	var ctrl = this
	this.vm = {entries: m.prop([])}
	m.request({"url": "/styles/1", "method": "GET", "background": true}).then(function (response) {ctrl.vm.entries(response)}, function (response) {error(response.message)}).then(m.redraw())
}
viewMultiple.view = function (ctrl) {
	if (!!ctrl.vm.entries()) {
		return m("p", ["Loading"])
	}
	return m("div", ["ctrl.entries"])
}
m.module(document.getElementById('content'), viewMultiple)
