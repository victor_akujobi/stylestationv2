/** @jsx m */
// @prepros-prepend linear-partition.js
'use strict';
var metrics = m.prop([])
var gridify = function (vm, element, isInitialized) {
	if (!isInitialized) {
		
	}
}
var partitionImagesInto = function(items, element, numPartitions) {
	var screenWidth = element.offsetWidth
	items = items || []
	var WIDTH = 450;
	var INIT_HEIGHT = 150;
	var MAX_HEIGHT = 500;
	var widths = []
	var index = -1
		var ratios = items.map(function(item) {
			return INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
		var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(screenWidth/350)))//Math.ceil(items.length/Math.ceil(screenWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item){
				return total + item
			}, 0)
			var ratio = (screenWidth / sum)
			return row.map(function (item){
				index += 1

				var width = (item/sum) * screenWidth
				return [width, ratio * INIT_HEIGHT]
			})
		}.bind(this))
		var tmp = []
		return tmp.concat.apply(tmp, test)	
}
	var partitionEntries =  function(options, element, isInitialized) {
		if (isInitialized) {
			return
		}
		console.log("Options", options)
		var entries = options.entries() || []
		console.log("Partitioning", entries, options.numPartitions)
		// var metrics = options.metrics || m.prop([])
		// m.startComputation()
		metrics(partitionImagesInto(entries, element, options.numPartitions))
		// m.endComputation()
		// m.redraw()
		element.focus()
		window.onresize = function () {
			metrics(partitionImagesInto(entries, element, ctrl.numPartitions))
			// m.redraw()
			element.focus()
		}.bind(this)
	}
var Grid = function () {
	// self.metrics = m.prop([])

	this.partitionImages = function (metric, element, isInitialized) {
	if (isInitialized) {
		return;
	}
	m.startComputation()
	// console.log(ctrl.entries())
	// var entries = ctrl.entries().length > self.NUM_ITEMS ? ctrl.entries().slice(0, self.NUM_ITEMS) : entries
	
	m.endComputation()
	

	// m.redraw()
	window.onresize = function () {
		ctrl.metrics(partitionImagesInto(ctrl.entries, element))
		m.redraw()
	}.bind(this)
}
 this.controller= function (options) {
		// console.log(entries)
		var ctrl = this
		ctrl.metrics == m.prop([])
		ctrl.NUM_ITEMS = options.numItems || 0
		ctrl.newerEntries = options.previous || ""
		ctrl.previousEntries = options.next || ""

	}

	this.view =  function(vm) {
		console.log("Entries: ", vm.entries)
		var entries = vm.entries()//self ? ctrl.entries().slice(0, self.previousEntriesIndex)
		// var metrics = this.metrics
		// if (entries.length > self.NUM_ITEMS) {
		// 	entries  = entries.slice(0, self.previousEntriesIndex)
		// }
		var nextEntry  = ""//ctrl.previousEntrie
		var  previousEntry = ""// ctrl.newerEntries
	 	var images = metrics().slice(0, self.previousEntriesIndex).map(function (metric, index) {
	 			var entry = entries[index]
	 			var imageSrc = "/images/ankara/" + entry.Image
	 			var height = parseInt(metric[1])+ "px"
	 			var width = parseInt(metric[0])+ "px"

  		 		return (
  		 			m("div", {style:{"width": width, "height": height}, class:"hover-tile-outer"}, [
					  m("div", {class:"hover-tile-container"}, [
					  	m("img", {style: {"position": "absolute", "height":height, "width":width}, src:imageSrc}, null),
					    m("div", {class:"hover-tile hover-tile-visible"}, [m("a", {href:"#view/"+entry.Ref, style:{"height":"100%", "width":"100%", "display":"inline-block"}})]
					      
					    ),
					    m("div", {class:"hover-tile hover-tile-hidden"}, [
					      // m("h5.low_margin", entry.Title()),
					      m("p.low_margin", entry.Description)
					    ])
					  ])
				])
)			
		})
		console.log("Metrics", metrics(), images) 
	 // console.log("Metrics: ", ctrl.metrics())
	 	 var previousEntries = nextEntry ? m("a", {href:"#search/" +"styles=skirt|top|blazer&gender=Guy|Girl&from=" + nextEntry}, ["Older Styles"]) : ""
	 var prevLink = previousEntry ? m("a", {href:"#search/" +"styles=skirt|top|blazer&gender=Guy|Girl&from=" + previousEntry}, ["Newer Styles"]) : "" 
	 var navigationBar = m("div", {class:"navigation_bar"}, [
	 						previousEntries,
	 						prevLink	
	 					])
	 					console.log(self.newerEntries)
	return (
		m("div",{config:self.partitionEntries.bind(null, {"entries": vm.entries, "numPartitions": 2})}, [images, navigationBar])
		)
	}
	// console.log(this)
}
// module.exports = Grid