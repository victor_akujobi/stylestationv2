/** @jsx m */
var m = require("mithril")
var ProfileHeader = {}
require("../SCSS/profile.header.scss")
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	return m("div", {class:"profile_header__wrapper"}, [
		m("div", {class:""}, ["Large Image"]),
		m("div", {class:"profile_main"}, [
			m("div", {class:"profile_info"}, [
				m("p", {class:"display_name"}, [vm.profile.DisplayName]),
				m("p", [vm.profile.Description]),
				m("p", [vm.profile.Url])
			]),
			m("div", {class:"profile_stats"}, [
				m("p", [vm.profile.Collections]),
				m("p", [vm.profile.Uploads])
			])
		])
	])
}
module.exports = ProfileHeader