/** @jsx m */
var m = require("mithril")
var ProfileHeader = {}
require("../SCSS/profile.header.scss")
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	return m("div", {class:"profile_header__wrapper"}, [
		m("div", {class:""}, ["Large Image"]),
		m("div", {class:"profile_main clearfix"}, [
			m("div", {class:"profile_info"}, [
				m("p", {class:"display_name"}, [vm.profile.DisplayName]),
				m("p", {class:"description"}, [vm.profile.Description]),
				m("p", {class:"url"}, [vm.profile.Url])
			]),
			m("div", {class:"profile_stats"}, [
				m("p", {class:"collections"}, ["Collections ", vm.profile.Collections]),
				m("p", {class:"uploads"}, ["Uploads ", vm.profile.Uploads])
			])
		])
	])
}
module.exports = ProfileHeader