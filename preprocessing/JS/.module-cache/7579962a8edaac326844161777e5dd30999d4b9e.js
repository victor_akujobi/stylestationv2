/** @jsx m */
var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
var colours = {}
colours.handleSelect = function (callback, value) {
	console.log(value)
	callback(value)
}
colours.view = function (vm) {
	console.log(vm.selected)
	var selectedColours = vm.entry.Colours().reduce(function () {

	})
	return Object.keys(COLOURS).map(function (path) {
		return m("div", {class:"sscolours__block", onclick:colours.handleSelect.bind(null, vm.onclick, path)} , [m("div", {class:"sscolours__blockpallete", style:{"background-color": COLOURS[path]}}),m("span", {class:"sscolours__blocktext"}, [path])])
	})
}