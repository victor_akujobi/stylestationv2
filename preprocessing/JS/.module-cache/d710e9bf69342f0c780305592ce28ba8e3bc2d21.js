/** @jsx m */
var defaultGenders = ["Male", "Female"]
var genders = {}
genders.controller = function () {

}
genders.view = function (vm) {
	var entry = vm.entry;
	var genderView = defaultGenders.map(function (gender) {
		var isSelected = gender == entry.Gender()
		var icon= isSelected ? m.trust("&#xE60A;") : "U"//m.trust(genderFonts[vm.tagText.toLowerCase()])
		var iconClass = isSelected ? "sstags__icon--selected" : ""
		var genderClass = "sstags__wrapper--" + gender.toLowerCase()
		return m("div", {key:gender, onclick:vm.onclick.bind(null, gender), class:"sstags__wrapper "+genderClass}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [gender])	
			])
	})
	return m("div", [ " ", genderView])
}