/** @jsx m */
var defaultGenders = ["Male", "Female"]
var genders = {}
genders.controller = function () {

}
genders.view = function (vm) {
	var entry = vm.entry;
		// var icon= vm.isSelected ? m.trust("&#xE60A;") : m.trust(genderFonts[vm.tagText.toLowerCase()])
	// var iconClass = vm.isSelected ? "sstags__icon--selected" : ""
	var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
		return m("div", {key:vm.tagText, onclick:vm.onclick, class:"sstags__wrapper "+gender}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [vm.tagText])	
			])
	var genderView = defaultGenders.map(function (gender) {
		return m("div", {key:gender, onclick:vm.onclick, class:"sstags__wrapper "+gender}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [vm.tagText])	
			])
	})
	return m("div", [ " ", entry.Gender()])
}