/** @jsx m */
// @prepros-prepend linear-partition.js
//@prepros-prepend avatar.js
var gridify = {}
gridify.metrics = m.prop([])




// gridify.engine = function (element, items, template) {
	
// 	@element: DOMNode
// 	@items: Array of items
// 	@metric Array of geometries
// 	@template Virtual DOM to render
	
// 	// return function (element, isInit) {

// 		m.render(element, this.items.map(function (item, index) {
// 				return template(item, style)
// 			}))
// 	// }
// }
gridify.config = function(items, numPartitions, container, isInit, context ) {
	// if (context.isInit) {
	// 	return
	// }
	var createPartitions = function () {
		var elementWidth = container.offsetWidth
		items = items || []
		var WIDTH = 450;
		var INIT_HEIGHT = 150;
		var MAX_HEIGHT = 500;
		var widths = []
		var ratios = items.map(function(item) {
			return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
	var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			ratio = elementWidth / sum
			return row.map(function (item) {
				return [ratio * item - 10, ratio * 150 - 10]
			})
		}.bind(this))
		var tmp = []
		// metrics = 
		m.redraw()
	}
		window.onresize = function () {
			console.log("Resizing")
			createPartitions()
		}
		createPartitions()
		// context.isInit = true
		//
}
gridify.view = function (vm) {
	var entries = vm.entries || []
	var metrics  = gridify.metrics()
	console.log(metrics, "MET")
	var blocks = metrics.map(function (metric, index) {
		var entry = entries[index]
		var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
		console.log(style)
		return m("div", {class:"ssgrid__block"}, [m("a", {href:"/a/"+entry.Ref, class:"ssgrid__innerblock"}, [m("img", {style:style, src:"/static/images/ankara/"+entry.Image+"_m.jpg"})]),m("p", {class:"ssgrid__details_holder"}, [avatar.view(entry.OwnerName)])])	
	})
	return m("div", {config:gridify.config.bind(null, entries, false)}, [
		blocks
	])
}