/** @jsx m */
var Popup = {
	show:m.prop(false),
	view: function (body) {
		return this.show() ? m("div", {class:"overlay"}, [body()]): ""
	}
}