/** @jsx m */
var avatar = {}
avatar.view = function (user) {
	return m("strong", {class:"avatar_wrapper"}, [m("img", {class:"ssavatar__image"}),user])
}