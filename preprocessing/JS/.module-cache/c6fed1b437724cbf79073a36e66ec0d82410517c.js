/** @jsx m */
'use strict';
var m = require("mithril")
// var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;'}
var tags = {}
tags.controller = function() {

}
// tags.vm = {isSelected: false}
tags.view = function (vm) {
	var icon= vm.isSelected ? m.trust("&#xE60A;") : m.trust(fonts[vm.tagText.toLowerCase()])
	var iconClass = vm.isSelected ? "sstags__icon--selected" : ""
	var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
		return m("div", {key:vm.tagText, onclick:vm.onclick, class:"sstags__wrapper "+gender}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [vm.tagText])	
			])
}

module.exports = tags
