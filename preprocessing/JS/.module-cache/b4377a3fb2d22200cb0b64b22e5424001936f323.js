/** @jsx m */
var m = require("mithril")
var ProfileHeader = {}
ProfileHeader.controller = {}
ProfileHeader.view = function (vm) {
	return m("div", {class:"profile_header_wrapper"}, [
		m("div", {class:""}, ["Image"]),
		m("div", [vm.Collections])
	])
}
module.exports = ProfileHeader