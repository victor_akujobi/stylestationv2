/** @jsx m */
'use strict';
var fonts = {'trousers': '&#xE602;', 'top': '&#xE600;', 'skirt':'&#xE608;', 'blazer': '&#xE605;', 'shorts': '&#xE608;'}
var tags = {}
tags.controller = function() {

}
// tags.vm = {isSelected: false}
tags.view = function (vm) {
	var icon= vm.isSelected ? m.trust("&#xE60A;") : m.trust(fonts[vm.tagText.toLowerCase()])
	var iconClass = vm.isSelected ? "sstags__icon--selected" : ""
	var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
		return m("div", {onclick:vm.onclick, class:"sstags__wrapper "+gender}, [
			m("span", {class:"sstags__icon "+iconClass}, [icon]),
			m("span", {class:"sstags__text"}, [vm.tagText])	
			])
}

