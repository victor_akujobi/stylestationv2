/** @jsx m */
// var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
require("../SCSS/colours.scss")
var colours = {}
colours.handleSelect = function (callback, value) {
	console.log(value)
	callback(value)
}
colours.view = function (vm) {
	console.log('seleted', vm.selected)
	var selected = vm.selected.reduce(function (dict, currentValue) {dict[currentValue] = true; return dict}, {})
	console.log(selected)
	return Object.keys(COLOURS).map(function (path) {
		var isSelected = !!selected[path] ? m("span", {class:"sscolours__blockpallete--selected"}, [m.trust("&#xE60A;")]) : ""
		return m("div", {class:"sscolours__block", onclick:colours.handleSelect.bind(null, vm.onclick, path)} , [m("div", {class:"sscolours__blockpallete", style:{"background-color": COLOURS[path]}}, [isSelected]),m("span", {class:"sscolours__blocktext"}, [path])])
	})
}
module.exports = colours