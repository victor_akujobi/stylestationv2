/** @jsx m */
var m = require("mithril")
require("../SCSS/profile.scss")
var uploads
m.route(document.getElementById('content'), '/', {
'/' : gridImageAnimation(viewMultiple),
'/a/:entry': gridImageAnimation(detailPage),
'/new': gridImageAnimation(newPage)
})