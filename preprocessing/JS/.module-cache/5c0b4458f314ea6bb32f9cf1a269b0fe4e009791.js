/** @jsx m */
'use strict';
var tags = {}
tags.controller = {}
tags.vm = {isSelected: false}
tags.view = function (vm) {
	var icon= vm.isSelected ? "" : ""
	return m("div", [
			m("span", {class:"sstag__icon"})
			])
}

modules.export = tags