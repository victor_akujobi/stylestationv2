/** @jsx m */
//@prepros-prepend tags.js
'use strict';
// var tagComponent = require('tags')
var styles = ["Top", "Trousers", "Skirt", "Blazer", "Accessories"];
var entry = {Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([])}
var newPage = {}
newPage.controller = function () {
	this.vm = {entry:entry}
}
newPage.view = function (ctrl) {
	entryColours = ctrl.vm.entry.Colours()
	entryStyles = ctrl.vm.entry.Styles()
	tags = styles.map(function (style) {
				return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:"female"})
			})
	return m("div", [
			styles
			])
}
m.module(document.getElementById('content'), newPage)
