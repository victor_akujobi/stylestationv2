/** @jsx m */
// @prepros-prepend linear-partition.js
//@prepros-prepend avatar.js
var gridify = {}
gridify.metrics = m.prop([])




// gridify.engine = function (element, items, template) {
	
// 	@element: DOMNode
// 	@items: Array of items
// 	@metric Array of geometries
// 	@template Virtual DOM to render
	
// 	// return function (element, isInit) {

// 		m.render(element, this.items.map(function (item, index) {
// 				return template(item, style)
// 			}))
// 	// }
// #
gridify.config = function(items, numPartitions, container, block) {
	// if (context.isInit) {
	// 	return
	// }
	var createPartitions = function () {
		var block = function (entry, style) {
			return m("a", {href:"#/a/"+entry.Ref, class:"ssgrid__innerblock"}, [m("img", {style:style, src:"/static/images/ankara/"+entry.Image+"_t.jpg"})])
		}
		var elementWidth = container.offsetWidth
		var WIDTH = 450;
		var INIT_HEIGHT = 150;
		var MAX_HEIGHT = 500;
		var widths = []
		var ratios = items.map(function(item) {
			return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
	var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			ratio = elementWidth / sum
			return row.map(function (item) {
				return [ratio * item, ratio * INIT_HEIGHT]
			})
		}.bind(this))
		var metrics = [].concat.apply([], test)
		m.render(container, metrics.map(function (metric, index) {
			var style = {width:~~metric[0] + "px", height:~~metric[1] + "px"}
			var item = items[index]
			return block(item, style)
		}))
	}
		window.onresize = function () {
			createPartitions()
		}
		createPartitions()
}
gridify.view = function (vm) {
	var entries = vm.entries || []
	var metrics  = gridify.metrics()
	console.log(metrics, "MET")
	var blocks = metrics.map(function (metric, style) {
		var entry = entries[index]
		// var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
		console.log(style)
		return m("div", {class:"ssgrid__block"}, [m("a", {href:"/a/"+entry.Ref, class:"ssgrid__innerblock"}, [m("img", {style:style, src:"/static/images/ankara/"+entry.Image+"_t.jpg"})]),m("p", {class:"ssgrid__details_holder"}, [avatar.view(entry.OwnerName)])])	
	})
	return m("div", {config:gridify.config.bind(null, entries, vm.numPartitions|| false)}, [
		blocks
	])
}