/** @jsx m */
var m = require("mithril")
var SSConfig = require("./config")
require("../SCSS/main.scss")
require("../SCSS/profile.scss")
var uploads
var UploadsPanel = {}
UploadsPanel.controller = function () {
	var ctrl = this
	if (window.bootstrap) {
		this.profileData = m.prop(bootstrap.profile)
		this.entries = m.prop([])
		this.ref = m.prop(m.route.params("ref"))
		m.request({url: ctrl.ref, config:SSConfig.XHR}).then(this.entries)
	}
	this.name = m.prop()
	// console.log(m.route())
	// m.request({"url": "/profile/"})
}
UploadsPanel.view = function (ctrl) {
	var options
	return m("div", [ctrl.entries().map(function (entry) {
		return m("p", [entry.Ref])
	}),m("p", ["Profile"])])
}
m.route.mode = "pathname"
m.route(document.getElementById('content'), '/profile/sdjkhkj', {
	"/profile/:ref": UploadsPanel
})