/** @jsx m */
// @prepros-prepend linear-partition.js

var gridify = {}
gridify.metrics = m.prop([])
gridify.config = function(items, numPartitions, elementWidth) {
	items = items || []
	var WIDTH = 450;
	var INIT_HEIGHT = 150;
	var MAX_HEIGHT = 500;
	var widths = []
	var index = -1
	console.log(items, numPartitions, elementWidth)
	var ratios = items.map(function(item) {
		return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
	})

	var partitions = partition(ratios, numPartitions || 1)//Math.ceil(items.length/Math.ceil(screenWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			console.log(row)
			return row.map(function (item) {
				return item/sum * 100
			})
		}.bind(this))
		// console.log(test)
		var tmp = []
		return tmp.concat.apply(tmp, test)	
}
gridify.view = function (vm) {
	var entries = vm.entries()
	return m("div", {config:gridify.config.bind(null, entries, vm.numPartitions)}, [
	entries
	])
}