/** @jsx m */
// @prepros-prepend linear-partition.js
var gridify = function(items, numPartitions, elementWidth) {
	items = items || []
	var WIDTH = 450;
	var INIT_HEIGHT = 150;
	var MAX_HEIGHT = 500;
	var widths = []
	var index = -1
		var ratios = items.map(function(item) {
			return INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
		console.log(ratios)
		var partitions = partition(ratios, numPartitions || 3)//Math.ceil(items.length/Math.ceil(screenWidth/300)))
		console.log("Partitions: ", partitions)


		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item){
				return total + item
			}, 0)
			console.log(sum)
			var ratio = sum/elementWidth
			return row.map(function (item){
				index += 1

				var width = Math.round(((item/sum) * 100) - 2)
				var height  = INIT_HEIGHT * elementWidth/sum
				return [width, height]
			})
		}.bind(this))
		// console.log(test)
		var tmp = []
		return tmp.concat.apply(tmp, test)	
}