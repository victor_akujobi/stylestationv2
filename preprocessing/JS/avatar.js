/** @jsx m */
require("../SCSS/avatar.scss")
var avatar = {}
avatar.view = function (user) {
	return m("span", {class:"ssavatar__wrapper"}, [m("img", {class:"ssavatar__image"}),user])
}
module.exports = avatar
//