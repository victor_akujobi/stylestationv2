/** @jsx m */
var m = require("mithril")
var animations = require("./animation")
var SSConfig = require("./config")
var animator = animations.animator

// console.log(snabbt)
// console.log(m)
var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)
var slidingPage = animator(animations.slideIn, animations.slideOut, true)
require("../SCSS/main.scss")
require("../SCSS/detail.scss")
var genders = require("./gender")
var tags = require("./tags")
var image = require("./image")
var colours = require("./colours")
// var partition = require("linear-partition")
var gridify = require("./gridify")
var utils = require("./utils")
var fonts = require("./fonts")
var Popup = utils.Popup()
var Panel = utils.Panel
var LoginPopup = utils.LoginPopup
var SearchPage = utils.SearchPage
var NUM_ENTRIES = 12
// var COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
'use strict';
// var tagComponent = require('tags')
var error = m.prop("")
var searchQuery = {
	Page:m.prop(1),
	Gender:{
		Male: m.prop(false),
		Female: m.prop(false)
	},
	Colours:{
		Red:m.prop(false),
		Orange:m.prop(false),
		Yellow:m.prop(false),
		Green:m.prop(false),
		Brown:m.prop(false),
		Purple:m.prop(false),
		Pink:m.prop(false),
		Blue:m.prop(false),
		Silver:m.prop(false),
		White:m.prop(false),
		Black:m.prop(false),
		Gold:m.prop(false)
	},
	Styles:{
		Top: m.prop(false),
		Trousers: m.prop(false),
		Shorts: m.prop(false),
		Blazer: m.prop(false),
		Accessories: m.prop(false),
		Dress: m.prop(false),
		Skirt: m.prop(false)
	}
}

var STYLES = {
			"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt"],
			"Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories"]
		};
var Entry = function (defaults) {
		var options = defaults || {}
		this.Gender =  m.prop(options.Gender || "Female")
		this.Description =  m.prop(options.Description || "")
		this.Colours = m.prop(options.Colours || [])
		this.Styles = m.prop(options.Styles || [])
		this.Image = m.prop(options.Image || "")
		this.ImageWidth = m.prop(options.ImageWidth || "")
		this.ImageHeight =  m.prop(options.ImageHeight || "")
	this.toggle= function (type, value) {
	 	var styles = this[type]()
	 	var index = styles.indexOf(value)
	 	if (index == -1) {
	 		styles.push(value)
	 	} else {
	 		styles.splice(index, 1)
	 	}
	 		this[type](styles)
	}
	this.Save = function () {
		self = this
		m.request({"url": "/ankara", "method": "POST", data:self}).then(
			function () {
			}, function () {
			})
	}
}
// var entry = {Gender: m.prop("Female"), Description: m.prop(""), Colours:m.prop([]), Styles:m.prop([]), Image:m.prop(""), ImageWidth:m.prop(""), ImageHeight: m.prop("")}

var newPage = {}
newPage.controller = function () {
	var localStorage = window.localStorage
	var savedEntry = JSON.parse(localStorage.getItem("entry"))
	if (!!savedEntry) {
		localStorage.removeItem("entry")
		localStorage.removeItem("redirect")
	}
	this.vm = {entry: new Entry(savedEntry)}
	var ctrl = this;
	this.Send = function () {
		m.request({"url": "/ankara", "method": "POST", data:ctrl.vm.entry, "background": true}).then(
		function () {
			Panel({content:m("div", ["Style added, looking good!!"]), type:'success'})
			m.route("/")
		}, function (response) {
			if (!response) {
				var entry = ctrl.vm.entry
				if (!entry.Description()) {
					Panel({content:m("div", ["Oh no, something bad happened. Hmm.., did you add a description"]), type:'error'})
				} else if  (!entry.Image()) {
						Panel({content:m("div", ["Oh no, something bad happened. Hmm.., did you add an image"]), type:'error'})
				} else if (!entry.Colours().length) {
						Panel({content:m("div", ["Oh no, something bad happened. Hmm.., did you select any colours"]), type:'error'})
				} else {
						Panel({content:m("div", ["Oh no, something bad happened. Hmm.., did you select any styles"]), type:'error'})
				}
				return
			}
			switch (parseInt(response.code)) {
				case 403:
					var localStorage = window.localStorage
					localStorage.setItem("entry", JSON.stringify(ctrl.vm.entry))
					localStorage.setItem("redirect", "/new")
					Popup(LoginPopup())
					break
				default:
					Panel({content:m("div", ["Oh no, something bad happened. Hmm.., are you logged in?"]), type:'error'})
					break;
			}
		})
	}
}
newPage.view = function (ctrl) {
	var entryStyles = ctrl.vm.entry.Styles().reduce(function(dict, currentValue){dict[currentValue] = true; return dict}, {})
	var coloursComponent = colours.view({selected: ctrl.vm.entry.Colours(), onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Colours')})
	var styles = STYLES[ctrl.vm.entry.Gender()].map(function (style) {
				return tags.view({isSelected: !!entryStyles[style], tagText:style, gender:ctrl.vm.entry.Gender().toLowerCase()||"female", onclick: ctrl.vm.entry.toggle.bind(ctrl.vm.entry, 'Styles', style)})
			})
	return m("div", {class:"ssnewentry__wrapper"}, [
				m("div", {class:"ssentry__image__wrapper"}, [
					image.view({entry:ctrl.vm.entry})
				]),
				m("div", {class:"ssentry__details__wrapper"}, [
					m("input", {placeholder:"Click to enter description", type:"text", onchange:m.withAttr("value", ctrl.vm.entry.Description), value:ctrl.vm.entry.Description(), class:"ssentry__descriptioninput"} ),
					m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["GENDER"]),genders.view({entry:ctrl.vm.entry, onclick:ctrl.vm.entry.Gender}), " ", m("br", {style:"clear:both"})]),
					m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["STYLES"]),styles, " ", m("br", {style:"clear:both"})]),
					m("div", {class:"sstags__stylepickerwrapper"}, [m("h4", {class:"sstags__stylepicker"}, ["COLOURS"]),coloursComponent, " ", m("br", {style:"clear:both"})]),
					m("button", {class:"ssnewentry__submit", onclick:ctrl.Send}, ["Done"])
				])
			])
}
var viewMultiple = {}
viewMultiple.controller = function () {
	this.block = function (entry, style) {
		return m("a", {href:"/a/"+entry.Ref, config:m.route, class:"ssgrid__innerblock"}, [m("img", {config:gridImageAnimation, style:style, src:"/static/images/ankara/"+entry.Image+"_t.jpg"})])
	}
	var localStorage = window.localStorage
	var urlRedirect = localStorage.getItem("redirect")
	if (!!urlRedirect) {
		localStorage.getItem("redirect", false)
		m.route(urlRedirect)
	}
	var ctrl = this
	this.getNextPage = function () {
		var previousPage = searchQuery.Page() + 1
		searchQuery.Page(previousPage)
		ctrl.fetchData()
	}
	this.getPreviousPage = function () {
		var nextPage = searchQuery.Page() - 1
		searchQuery.Page(nextPage)
		ctrl.fetchData()
	}
	this.fetchData = function () {
		// var gender = 
		var q = Object.keys(searchQuery).filter(function (section) {return section !== "Page"}).reduce(function (obj, section) {
			var tmp = Object.keys(searchQuery[section])
			.filter(function (item) {
				return searchQuery[section][item]()
			})
			if (!!tmp.length) {
				obj[section] = tmp
			}
			return obj
		}, {})
		q.Page = searchQuery.Page()
		m.request({"url": "/search", "method": "POST", data:q, "background": true, config:SSConfig.XHR}).then(ctrl.vm.entries, function (response) {error(response.message)}).then(function () {m.redraw()})
	}
	if (!!window.bootstrap) {
		this.vm = {entries: m.prop(window.bootstrap.entries)}
		window.bootstrap = false
	} else {
		this.vm = {entries: m.prop([])}
		this.fetchData()
	}
}
viewMultiple.view = function (ctrl) {
	var search = SearchPage({options:searchQuery, iconAnimate:gridImageAnimation, onChange: ctrl.fetchData})
	var previousLink = null
	var nextLink = null
	if (searchQuery.Page() > 1) {
		previousLink = m("a", {onclick:ctrl.getPreviousPage, class:"prev_page"}, ["«"])
	}
	if (ctrl.vm.entries().length == NUM_ENTRIES) {
		nextLink = m("a", {onclick:ctrl.getNextPage, class:"next_page"}, ["»"])
	}
	if (!!ctrl.vm.entries().length) {
		// var entries = ctrl.vm.entries()
		// console
		return m("div", [search,gridify.view({"entries": ctrl.vm.entries(), animation:gridImageAnimation, view:ctrl.block}),
		m("p", {class:"nav"}, [previousLink,nextLink])])
	}
		return m("div", [search,m("p", ["Loading"])])
}
var detailPage = {}
detailPage.view = function (ctrl) {
	// console.log(ctrl.entry)
	// ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries()}) : 
	var relatedEntries = ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries(), numPartitions: 1}) :m("div", {class:"loading_gif"})
	if (ctrl.entry()) {
		var entry = ctrl.entry()
		var url = document.location.origin + "/ankara/" + entry.Ref
		return m("div", {class:"ssdetail__wrapper"}, [
				m("div", {class:"entry_wrapper"}, [
				m("div", {class:"ssdetail__entry"}, [
				m("div", {class:"ssdetail__image"}, [m("img", {class:"image", src:"/static/images/ankara/"+entry.Image+"_m.jpg"} )]),
				m("div", {class:"sssharebar__wrapper"}, [m("p", {class:"share_text"}, ["Psst... If you love this style, you can share it on " ]),m("a", {href:"http://www.facebook.com/sharer/sharer.php?u=" + url + "&title=" + entry.Description, target:"_blank", class:"share_link facebook_share"}, [m.trust(fonts['facebook'])]),m("a", {href:"http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url, target:"_blank", class:"share_link twitter_share"}, [m.trust(fonts['twitter'])]),m("a", {href:"http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + entry.Description, target:"_blank", class:"share_link pinterest_share"}, [m.trust(fonts['pinterest'])]),m("a", {target:"_blank", class:"share_link email_share"}, [m.trust(fonts['email'])])])
				]),
			m("div", {class:"ssdetail__info"}, [
				m("p", {class:"owner"}, [entry.OwnerName]),
				m("p", {class:"description"}, [entry.Description]),
				m("div", {class:"stats"}, [
				  m("ul", [
				    m("li", ["98",m("span", ["Favs"])]),
				    m("li", ["298",m("span", ["Things"])]),
				    m("li", ["923",m("span", ["Objects"])]),
				    m("li", ["98",m("span", ["More"])])
				  ])
				]),
				m("div", {class:"tag_wrapper"}, [
					entry.Styles.map(function (style) {
						return m("p", {class:"tag"}, [m("span", {class:"tag_icon"}, [m.trust(fonts[style.toLowerCase()])]),m("span", {class:"tag_text"}, [style])])
					})
				]),

				m("div", [
					entry.Colours.map(function (colour) {
						return m("span", {class:"colour", style:{"background-color": colours.COLOURS[colour]}})
					})
				])
			])
			]),
			m("p", ["We think you might like"]),
			m("div", {class:"related_entries"}, [relatedEntries])
		])
	} else {
		return m("div")
	}
}
// Share links courtesy of http://petragregorova.com/articles/social-share-buttons-with-custom-icons/
detailPage.controller = function () {
	var ctrl = this
	if (!!window.bootstrap) {
		ctrl.entry = m.prop(window.bootstrap.entry)
		ctrl.relatedEntries = m.prop(window.bootstrap.relatedEntries)
		window.bootstrap = false
	} else {
		ctrl.entry = m.prop({})
		ctrl.relatedEntries = m.prop([])
		var entryRef = m.route.param("entry") || "sxxeSLWUYS"
			m.request({url: "/related/"+ entryRef, method:"get"}).then(function (response) {
			ctrl.relatedEntries(response)
		}), function () {
			console.log("Error")
			return []
		}
		m.request({url: "/a/"+ entryRef, method:"get", config:StyleStationXHRConfig}).then(function (response) {
			ctrl.entry(response)
		}), function () {
			console.log("Error")
			return []
		}
	}
}

m.route.mode = 'pathname'
var createButton = m("a", {class:"create_button", href:"/new", config:m.route}, ["Upload"])
m.render(document.getElementById("create"), createButton)
m.route(document.getElementById('content'), '/', {
'/' : gridImageAnimation(viewMultiple),
'/a/:entry': gridImageAnimation(detailPage),
'/new': gridImageAnimation(newPage)
})

// m.module(document.getElementById('content'), viewMultiple)
// m.module(document.getElementById('content'), detailPage);
//hwuhkm