/** @jsx m */
// @prepros-prepend linear-partition.js
//@prepros-prepend avatar.js
var m = require("mithril")
var partition = require("linear-partition")
require("./avatar")
var gridify = {}
// gridify.metrics = m.prop([])




// gridify.engine = function (element, items, template) {
	
// 	@element: DOMNode
// 	@items: Array of items
// 	@metric Array of geometries
// 	@template Virtual DOM to render
	
// 	// return function (element, isInit) {

// 		m.render(element, this.items.map(function (item, index) {
// 				return template(item, style)
// 			}))
// 	// }
// #
gridify.config = function(options, container) {
	// if (context.isInit) {
	// 	return
	// }
	// if (options.animation) {
		var config = options.animation ? options.animation : null
	// }
	var block = options.view || function (entry, style) {
		return m("a", {href:"/a/"+entry.Ref, config:m.route, class:"ssgrid__innerblock"}, [m("img", {config:config, style:style, src:"/static/images/ankara/"+entry.Image+"_t.jpg"})])
	}
	var items = options.entries || []
	var numPartitions = options.numPartitions || false
	var createPartitions = function () {
		var elementWidth = container.offsetWidth
		var WIDTH = 450;
		var INIT_HEIGHT = 150;
		var MAX_HEIGHT = 500;
		var widths = []
		var ratios = items.map(function(item) {
			return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
		})
	var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/300)))
		var test = partitions.map(function (row) {
			var sum = row.reduce(function (total, item) {
				return total + item
			}, 0)
			ratio = elementWidth / sum
			return row.map(function (item) {
				return [ratio * item, ratio * INIT_HEIGHT]
			})
		}.bind(this))
		var metrics = [].concat.apply([], test)
		m.render(container, null)
		m.render(container, metrics.map(function (metric, index) {
			var style = {width:~~metric[0] + "px", height:~~metric[1] + "px"}
			var item = items[index]
			return block(item, style)
		}))
	}
		window.onresize = function () {
			createPartitions()
		}
		createPartitions()
}
gridify.view = function (vm) {
	var entries = vm.entries || []
	// var metrics  = gridify.metrics()
	// console.log(metrics, "MET")
	// var blocks = metrics.map(function (metric, style) {
	// 	var entry = entries[index]
	// 	// var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
	// 	console.log(style)
	// 	return <div class="ssgrid__block"><a href={"/a/"+entry.Ref} class="ssgrid__innerblock"><img style={style} src={"/static/images/ankara/"+entry.Image+"_t.jpg"}/></a><p class="ssgrid__details_holder">{avatar.view(entry.OwnerName)}</p></div>	
	// })
	return m("div", {config:gridify.config.bind(null, {entries: entries, numPartitions: vm.numPartitions || false, view: vm.view || false, animation: vm.animation})}
	)
}

module.exports = gridify