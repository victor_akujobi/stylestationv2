var page = require("page")
var Profile = require ("../JSX/models/profile.js")
var Entry = require("../JSX/models/entry.js")
var Collection = require("../JSX/models/collections.js")
require("./collections.tag")
var analytics = require("../JSX/analytics.js")
var grid = require("./grid.tag")
var Entries = require("./entries.tag")
var EntryPage = require("./entry.tag")
var profile = require("./profile.tag")
var login = require("./login.tag")
require("./new-page.tag")
require("./profile-edit.tag")
require("./avatar_wrapper.tag")

require("../SCSS/header.scss");
var ajax = require("superagent")
var API_ENDPOINT = ""

var search = {
	styles: {
		Top: false,
		Trousers: false,
		Shorts: false,
		Blazer: false,
		Accessories: false,
		Dress: false,
		Skirt: false,
		Kaftan: false,
		Jumpsuit: false,

	},
	genders: {
		Male: false,
		Female: false
	},
	colours: {
		Red: false,
		Orange: false,
		Yellow: false,
		Green: false,
		Brown: false,
		Purple: false,
		Pink: false,
		Blue: false,
		Silver: false,
		White: false,
		Black: false,
		Gold: false
	}
}
window.NUM_ENTRIES = 24
var APP_CONTAINER = document.getElementById("content")
var updateRoute = (function () {
	var NAV_WRAPPER = document.getElementById("nav_wrapper")
	return function (path, ctx, next) {
		NAV_WRAPPER.className = path
		next()
	}
})()


var loadSearchQuery = function (ctx, next) {
	if (!!ctx.params) {
		ctx.params.colors
	}
	next()
}
var loadMe = function (ctx, next) {
	if (!!window.bootstrap) {
		ctx.profile = new Profile(window.bootstrap.profile)
		window.bootstrap = null
		next()
	} else {
		ajax
		.get("/me")
		.set("Content-Type", "application/json")
		.end(function (err, response) {
			if (err) {
			} else {
				ctx.profile = new Profile(response.body)
				next()
			}
		})
	}
}
var loadProfile = function (ctx, next) {
	if (!!window.bootstrap) {
		ctx.profile = new Profile(window.bootstrap.profile)
		window.bootstrap = null
		next()
	} else {
		ajax
		.get("/profile/" + ctx.params.ref)
		.set("Content-Type", "application/json")
		.end(function (err, response) {
			if (err) {
			} else {
				ctx.profile = new Profile(response.body)
				next()
			}
		})
	}
}
var loadCollections = function (ctx, next) {
	if (!!window.bootstrap) {
		ctx.collections = window.bootstrap.collections.map(function (collection) {
			return new Collection(collection)
		})
		window.bootstrap = null
		next()
	} else {
		ajax
		.get("/collections")
		.send({
			Colours: ctx.params.colours,
			Brands: ctx.params.brands,
			Styles: ctx.params.styles,
			Owner: ctx.params.brand,
			FavouritedBy: ctx.params.favouritedBy,
			Collection: ctx.params.collection,
			Page: parseInt(ctx.params.page)
		})
		.set("Content-Type", "application/json")
		.end(function (err, response) {
			if (err) {
			} else {
				ctx.collections = response.body.map(function (collection) {return new Collection(collection)})
				next()
				// riot.update()
			}
		})
	}
}
var loadStyle = function (ctx, next) {
	console.log(window.bootstrap, !!window.bootstrap)
	if (!!window.bootstrap) {
		ctx.entry = new Entry(window.bootstrap.entry)
		window.bootstrap = null
		next()
	} else {
		ajax
		.get("/a/" + ctx.params.entry + "?ajax=1")
		.set("Content-Type", "application/json")
		.end(function (err, response) {
			if (err) {
			} else {
				ctx.entry = new Entry(response.body)
				next()
			}
		})
	}
}
var loadStyles = function (ctx, next) {
	if (!!window.bootstrap) {
		ctx.entries = window.bootstrap.entries.map(function (entry) {return new Entry(entry)})
		window.bootstrap = null
		next()
	} else {

		ajax
		.post("/search")
		.send({
			Colours: ctx.params.colours,
			Brands: ctx.params.brands,
			Styles: ctx.params.styles,
			Owner: ctx.params.brand,
			Gender: ctx.params.genders,
			FavouritedBy: ctx.params.favouritedBy,
			Collection: ctx.params.collection,
			Page: parseInt(ctx.params.page)
		})
		.set("Content-Type", "application/json")
		.end(function (err, response) {
			if (err) {
			} else {
				ctx.entries = response.body.map(function (entry) {return new Entry(entry)})
				next()
				// riot.update()
			}
		})
	}
}

var matchParams = function (string, regex, index) {
	index || (index = 1);
	var matches = [];
	var match;
	while (match = regex.exec(string)) {
		matches.push(match[index]);
	}
	return matches;
}
extractParams = function (ctx, next) {
	var params = {}
	params.styles = matchParams(ctx.querystring, /s:(\w+)/g)
	params.colours = matchParams(ctx.querystring, /c:(\w+)/g)
	params.genders = matchParams(ctx.querystring, /g:(\w+)/g)
	var brands = matchParams(ctx.querystring, /b:(\w+)/g)
	if (brands && brands.length) {
		params.brand = brands[0]
	}
	var favourites = matchParams(ctx.querystring, /f:(\w+)/g)
	if (favourites && favourites.length) {
		params.favouritedBy = favourites[0]
	}
	var collections = matchParams(ctx.querystring, /co:(\w+)/g)
	if (collections && collections.length) {
		params.collection = collections[0]
	}
	var pageMatch = matchParams(ctx.querystring, /p:(\w+)/g)
	params.page = pageMatch.length && parseInt(pageMatch[0])
	if (search) {
		params.styles.length && params.styles.reduce(function (accumulator, style) {
			accumulator[style] = true
			return accumulator
		}, search.styles)
		params.colours.length && params.colours.reduce(function (accumulator, colour) {
			accumulator[colour] = true
			return accumulator
		}, search.colours)
		params.genders.length && params.genders.reduce(function (accumulator, gender) {
			accumulator[gender] = true
			return accumulator
		}, search.genders)
	}
	ctx.params = params
	console.log(ctx.params)
	next()
}
var AVATAR_CONTAINER = document.getElementById('avatar')

var MOBILE_NAV_TOGGLE = document.getElementById('mobile__hamburger__toggle')
var APP_ROOT_CONTAINER = document.getElementById('content')
var OVERLAY_CONTAINER = document.getElementById('overlay')
page('*', function(ctx,  next){
	MOBILE_NAV_TOGGLE.checked = false
	/* Send to analytics */
	analytics(ctx)
	next()
})
// m.route(document.getElementById('content'), '/', {
page('/a/:entry', loadStyle, updateRoute.bind(null, 'explore'), function (ctx, next) {
	// var entry = ctx.params.entry
	// console.log(ctx.entry)
	var entry = new Entry(ctx.entry)
	riot.mount(APP_CONTAINER, "entry", {entry:entry})

})

page('/new', updateRoute.bind(null, 'new'), function () {

riot.mount(APP_ROOT_CONTAINER, "new-page", {entry: new Entry({})})
})
page('/profile/:ref', loadProfile, function (ctx) {
	riot.mount(APP_CONTAINER, "profile", {profile: ctx.profile})
})

page('/me', updateRoute.bind(null, ''), loadMe, function (ctx) {
	riot.mount(APP_CONTAINER, "profile-edit", {profile: ctx.profile})
})

page('/edit/:entry', updateRoute.bind(null, 'new'), loadStyle, function (ctx) {
	riot.mount(APP_ROOT_CONTAINER, "new-page", {entry: new Entry(ctx.entry || {})})
})
page('/login', updateRoute.bind(null, ''), function (ctx) {
	document.body.style.overflow = "hidden"
	OVERLAY_CONTAINER.className = "overlay overlay--open"
	ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "login", back:page.back})
})
page.exit('/login', function (ctx, next) {
	document.body.style.overflow = "scroll"	
	OVERLAY_CONTAINER.className = "overlay overlay--closed"
	!!ctx.overlay.length && ctx.overlay[0].unmount(true)
	ctx.overlay = null
	next()
})
page('/register', updateRoute.bind(null, ''), function (ctx) {
	document.body.style.overflow = "hidden"
	OVERLAY_CONTAINER.className = "overlay overlay--open"
	ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "register", back:page.back})
})
page.exit('/register', function (ctx, next) {
	OVERLAY_CONTAINER.className = "overlay overlay--closed"
	document.body.style.overflow = "scroll"
	!!ctx.overlay.length && ctx.overlay[0].unmount(true)
	ctx.overlay = null
	next()
})
page('/' , updateRoute.bind(null, 'explore'), loadStyles, function (ctx) {
	var entries = (ctx.entries || []).map(function (entry) {
		return new Entry(entry)
	})
	riot.mount(APP_CONTAINER, "entries", {entries: entries, page: 1})
})
page('/explore' , updateRoute.bind(null, 'explore'), extractParams, loadStyles, function (ctx) {
	var entries = (ctx.entries || []).map(function (entry) {
		return new Entry(entry)
	})
	riot.mount(APP_CONTAINER, "entries", {entries: entries, params:ctx.params, page: ctx.page, search: search})
})
page('/collections', updateRoute.bind(null, 'collections'), extractParams, loadCollections, function (ctx) {
	var collections = (ctx.collections || []).map(function (collection) {
		return new Collection(collection)
	})
	riot.mount(APP_CONTAINER, "collections", {collections: collections, params:ctx.params, page: ctx.page})
})
if (window.currentUser && AVATAR_CONTAINER) {
	// if (AVATAR_CONTAINER) {
		console.log('user: ', window.currentUser)
		AVATAR_CONTAINER && riot.mount(AVATAR_CONTAINER, 'avatar-wrapper', {ref:window.currentUser, displayName: window.displayName, url: '/me'})
	// }
}
page()
var EVENTS = {
	DISMISS_OVERLAY: "dismissOverlay"
}