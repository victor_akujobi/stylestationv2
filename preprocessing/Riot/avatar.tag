<avatar>
	<span class="avatar__image">{initials}</span>
	<a if={!opts.hidename} href={url} class="avatar__text">{opts.name}</a>
	<script>
		console.log('url: ', opts)
		this.url = !!opts.url ? opts.url : "/profile/" + opts.ref
		this.initials = (opts.name || "").split(" ").map(function (word) {
			return word[0]
		}).join("")
		console.log(this.intials)
	</script>
	<style>
		.avatar__image {
			width: 2em;
			height: 2em;
			margin-left: 0.5em;
			border-radius: 50%;
			background-color: pink;
			box-sizing:border-box;
			color:#fff;
			text-align:center;
			font: 0.75em/2em Lato,sans-serif;
			display:inline-block;
			/*padding: 0.5em 5px 0 0;*/
		}
		.avatar__text {
			height:2em;
			text-overflow: ellipsis;
			display: inline-block;
			overflow: hidden;
			vertical-align:middle;
			white-space: nowrap;
			width: calc(100% - 3em);
			text-decoration:none;
			color:inherit;
			font: 0.75em/2em Lato,sans-serif;
			box-sizing:border-box;
		}
	</style>
</avatar>