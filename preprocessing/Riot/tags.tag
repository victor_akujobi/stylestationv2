	
require("../SCSS/tags.scss")
var fonts = require("../JSX/fonts.js")
require("./utils.tag")
<tag>
	<div key={opts.text} onclick={opts.toggle} class="sstags__wrapper">
		<span class={'sstags__icon ' + (opts.isselected ? 'sstags__icon--selected' : '')}><raw value={!!opts.isselected ? FONTS['success'] : FONTS[opts.text.toLowerCase()]}/></span>
		<span class="sstags__text">{opts.text}</span>	
	</div>
	<script>
	this.FONTS = fonts
	var self = this
	this.on('update', function () {
		// self.fonts.update()
	// console.log(opts)
	})
	</script>
</tag>