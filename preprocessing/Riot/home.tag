var page = require("page")
var login = require("./login.tag")
require("../SCSS/main.scss")
require("./avatar_wrapper.tag")
var analytics = require("../JSX/analytics.js")

var updateRoute = (function () {
	var NAV_WRAPPER = document.getElementById("nav_wrapper")
	return function (path, ctx, next) {
		NAV_WRAPPER.className = path
		next()
	}
})()

var AVATAR_CONTAINER = document.getElementById('avatar')
var MOBILE_NAV_TOGGLE = document.getElementById('mobile__hamburger__toggle')
var OVERLAY_CONTAINER = document.getElementById('overlay')

page('*', function(ctx,  next){
	MOBILE_NAV_TOGGLE.checked = false
	analytics(ctx)
	next()
})
page('/login', updateRoute.bind(null, ''), function (ctx) {
	document.body.style.overflow = "hidden"
	OVERLAY_CONTAINER.className = "overlay overlay--open"
	ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "login", back:page.back})
})
page.exit('/login', function (ctx, next) {
	document.body.style.overflow = "scroll"	
	OVERLAY_CONTAINER.className = "overlay overlay--closed"
	!!ctx.overlay.length && ctx.overlay[0].unmount(true)
	ctx.overlay = null
	next()
})
page('/register', updateRoute.bind(null, ''), function (ctx) {
	document.body.style.overflow = "hidden"
	OVERLAY_CONTAINER.className = "overlay overlay--open"
	ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "register", back:page.back})
})
page.exit('/register', function (ctx, next) {
	OVERLAY_CONTAINER.className = "overlay overlay--closed"
	document.body.style.overflow = "scroll"
	!!ctx.overlay.length && ctx.overlay[0].unmount(true)
	ctx.overlay = null
	next()
})
if (window.currentUser && AVATAR_CONTAINER) {
		AVATAR_CONTAINER && riot.mount(AVATAR_CONTAINER, 'avatar-wrapper', {ref:window.currentUser, displayName: window.displayName, url:'/me'})
}
page()