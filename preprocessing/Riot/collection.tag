<collection>
	<div class="collection__item__wrapper">
		<div class="collection__image__wrapper">
			<grid details={false} rows={2} items={collection.EntryImages}/>
		</div>
			<a class="collection__name">{collection.Name}</a>
		<br style="clear:both" />
	</div>
	<style>
		.collection__item__wrapper {
			box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
			margin: 0.9em auto ;
			width: 300px;
			height:300px;
			float:left;
			overflow:hidden;
		}
		.collection__image__wrapper {
			background-size:contain;
			margin: 0.2em auto ;
			padding: 1em;
			width:200px;
			height:200px;
			/*flex-direction:column;*/
			align-items:flex-start;
			flex-flow: column wrap;
		}
		.collection__image {
			width:150px;
			flex:1 0 auto;
		}
		.collection__image__body, .collection__image__wrapper {
			vertical-align:middle;
		}
		.collection__image__body{
			display:inline-block;
			width: 100%;
		}
		.collection__name {
			display:block;
			font-size:1.4em;
			font-family:'Gentium Basic', serif;
			color:#564345;
		}
	</style>
</collection>