var countUp = require("countup.js")

<raw>
	<span></span>
	<script>
		this.root.innerHTML = opts.value
		this.on('update', function() {
			this.root.innerHTML = opts.value		
		})
	</script>
</raw>
<time-ago>
	<span>{value}</span>
	<script>
	this.timeSince = function(date) {
		console.log(date)
	    var seconds = Math.floor((new Date() - date) / 1000);
	    var interval = Math.floor(seconds / 31536000);

	    if (interval > 1) {
	        return interval + " years ago";
	    }
	    interval = Math.floor(seconds / 2592000);
	    if (interval > 1) {
	        return interval + " months ago";
	    }
	    interval = Math.floor(seconds / 86400);
	    if (interval > 1) {
	        return interval + " days ago";
	    }
	    interval = Math.floor(seconds / 3600);
	    if (interval > 1) {
	        return interval + " hours ago";
	    }
	    interval = Math.floor(seconds / 60);
	    if (interval > 1) {
	        return interval + " minutes ago";
	    }
	    return "just now";
}
	this.value = this.timeSince(opts.date)
		this.on('update', function () {
			// this.
		})
	</script>
</time-ago>

<count>
	<span></span>
	<script>
		this.on('mount update', function () {
			var anim = new countUp(this.root, 0, opts.value);
			anim.start()
		})
	</script>
</count>
<message>
	<div class={'message message--' + opts.type + ' message--'  +opts.visibility}>{opts.message}</div>
		<script>
			this.duration = opts.duration || 5000
			var self = this
			this.on('update', function () {
				if (opts.visibility == 'visible') {
					self.timer = window.setTimeout(opts.dismiss, self.duration)
				} else {
					window.clearTimeout(self.timer)
				}	
			})
			require("../SCSS/messages.scss");
	</script>
</message>


