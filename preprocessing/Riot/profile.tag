var utils = require("./utils.tag")
<profile>
	<div class="profile__header" style="color:#fff; min-height: 300px;background: transparent url(/static/images/hero.jpg) center center no-repeat; background-size: cover">
		<div style="transform: translateY(50%); font-size: 2.5em; color:#fff">
		<avatar name={opts.profile.DisplayName}></avatar>
		<p class="profile__description">{opts.profile.Description || opts.profile.DisplayName + " has not written a description yet"}</p>
		</div>

	</div>
	<div class="profile_main">
		<div class="profile__section profile__section--uploads">
			<h5 class="profile__header__text"><count value={uploads} /> <br/>Uploads</h5>
			<a href="/explore?q=b:{opts.profile.Ref}" style='background: transparent url(/static/images/ankara/{uploadsImage}_m.jpg) center 10% no-repeat' class="profile__section__body">
			</a>
		</div>
		<div class="profile__section profile__section--uploads">
			<h5 class="profile__header__text"> <count value={favourites} /> <br />Favourites</h5>
			<a href="/explore?q=f:{opts.profile.Ref}" style='background: transparent url(/static/images/ankara/{uploads[0].Image}_m.jpg) center 10% no-repeat' class="profile__section__body">
			</a>
		</div>
		<div class="profile__section profile__section--uploads">
			<h5 class="profile__header__text"><count value={collections} /> <br /> Collections</h5>
			<a href="/explore" style='background: transparent url(/static/images/ankara/{uploads[0].Image}_m.jpg) center 10% no-repeat' class="profile__section__body">
			</a>
		</div>
	</div>
	<script>
	var self = this
	var DEFAULT_IMAGE = {Image: "/static/images/hero.jpg"}
	this.uploads = 0
	this.collections = 0
	this.favourites = 0
	this.loading = true
	this.on('update', function () {console.log(this.collections)})
	this.on("mount", function () {
		opts.profile.getUserDashboard(function (dashboard) {
			self.uploads = dashboard.Uploads
			self.favourites = dashboard.Favourites
			self.collections = dashboard.Collections
			self.update()
			// self.update({uploads: uploads || [], favourites: favourites || [], collections: collections || [], loading: false})
			console.log(self)
		})
	})
	</script>
	<style>
		
	</style>
</profile>