require("../SCSS/pagination.scss")
<pagination>
	<div class="nav__wrapper">
	<span class="nav__item nav__item--prev" if={parseInt(opts.params.page) > 1}><a class="nav__item--link" href='{prevLink}'>«</a></span>
	<span class="nav__item nav__item--next" if={opts.numentries == opts.maxentries}><a class="nav__item--link" href='{nextLink}'>»</a></span>
	</div>
	<script>
		this.getNextLink = function () {
			var page = parseInt(opts.params.page) || 1
			page += 1
			return this.getLink(page)
		}
		this.getPrevLink = function () {
			var page = parseInt(opts.params.page) || 1
			if (page > 1) {
				page -= 1
				console.log(page)
			}
			return this.getLink(page)
		}
		this.getLink = function (page) {
			return '/explore?q=' +
				(opts.params.colours.length ? opts.params.colours.map(function (colour) {return "c:" + colour}).join('|') + '|': '')+
				(opts.params.styles.length ? opts.params.styles.map(function (style) {return "s:" + style}).join('|') + '|' : '') +
				(opts.params.genders.length ? opts.params.genders.map(function (gender) {return "g:" + gender}).join('|') + '|' : '') +
				(opts.params.brand ? 'b:' + opts.params.brand + '|' : '') +
				(opts.params.favouritedBy ? 'b:' + opts.params.favouritedBy + '|' : '') +
				'p:' + page
		}
		this.nextLink = this.getNextLink()
		this.prevLink = this.getPrevLink()
	</script>
</pagination>
