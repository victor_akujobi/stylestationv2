var FONTS = require("../JSX/fonts.js")
require("../SCSS/search.scss")
var page = require("page")
<search>
	<label class="search__toggle__label" for="search__toggle">
	<span class="search__icon--filter" ><raw value={FONTS.filter} /></span>
	<span class="search__icon--close"><raw value="&times;" />
	</span>
	</label>
	<div class="ssentry__details__wrapper">
		<h3 style="text-align:center">FILTER UPLOADS</h3>
		<div class="sstags__stylepickerwrapper">
			<h4 class="sstags__stylepicker">GENDER</h4>
			<gender each="{label, icon in defaultGenders}"
	label={label}
	 onclick={parent.toggleGender.bind(null, label)}
	}
	isselected={parent.opts.search.genders[label]}
	selectedicon={FONTS.success}
	defaulticon = {parent.defaultGenders[label]} />
			<br style="clear:both"/>
		</div>
		<div class="sstags__stylepickerwrapper">
		<h4 class="sstags__stylepicker">STYLES</h4>
			<tag toggle={parent.addStyle.bind(null, style)} isselected={parent.opts.search.styles[style]} each={style in STYLES} text={style}/>
			<br style="clear:both"/>
		</div>
		<div class="sstags__stylepickerwrapper">
			<h4 class="sstags__stylepicker">COLOURS</h4>
			<colours  label={text} isselected={parent.opts.search.colours[text]} background={parent.COLOURS_MAP[text]} each={text, colour in COLOURS} toggle={parent.addColour.bind(null, text)}/>
			<br style="clear:both"/>
		</div>
		<button class="ssnewentry__submit" onclick={handleSearch}>
			Done
		</button>
	</div>
	<script>
	this.FONTS = FONTS
	this.STYLES = ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt", "Kaftan", "Jumpsuit"];
	this.STYLES.sort()
	this.COLOURS_MAP = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	this.defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}
	this.COLOURS = Object.keys(this.COLOURS_MAP)
	this.COLOURS.sort()

	this.toggleGender = function (gender) {
		var genders =JSON.parse(JSON.stringify(opts.search.genders))
		genders[gender] = !genders[gender]
		if (Object.keys(genders).filter(function (gender) {
			return genders[gender]
		}).length > 0) {
			opts.search.genders= genders
		}
	}
	this.addStyle = function (style) {
		var styles =JSON.parse(JSON.stringify(opts.search.styles))
		styles[style] = !styles[style]
		if (Object.keys(styles).filter(function (style) {
			return styles[style]
		}).length > 0) {
			opts.search.styles= styles
		}
	}

	this.addColour = function (colour) {
		var colours =JSON.parse(JSON.stringify(opts.search.colours))
		colours[colour] = !colours[colour]
		if (Object.keys(colours).filter(function (colour) {
			return colours[colour]
		}).length > 0) {
			opts.search.colours = colours
	}
	}
 	this.getSelectedOptions = function (options) {
 		return Object.keys(options || {}).filter(function (option) {
			return options[option]
		})
 	}
	this.handleSearch = function () {
		var selectedGenders = this.getSelectedOptions(opts.search.genders)
		var selectedStyles = this.getSelectedOptions(opts.search.styles)
		var selectedColours = this.getSelectedOptions(opts.search.colours)
		 var url = '/explore?q=' +
				(selectedColours.length ? selectedColours.map(function (colour) {return "c:" + colour}).join('|') + '|': '') +
				(selectedStyles.length ? selectedStyles.map(function (style) {return "s:" + style}).join('|') + '|' : '') + 
				(selectedGenders.length ? selectedGenders.map(function (gender) {return "g:" + gender}).join('|') + '|': '')
		page(url)
		}
	</script>
</search>