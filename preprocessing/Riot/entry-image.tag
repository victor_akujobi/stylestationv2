require("../SCSS/image.scss")
var ajax = require("superagent")
<entry-image>
	<div if={loading} class="ssimage__loadingimage"></div>
	<input class="ssimage__imageinput" type="file" onchange={ onImageChange }/>
	<div if='{!!opts.image}'>
		<img if={!!opts.image} style="width:100%" src='/static/images/ankara/{ opts.image }_m.jpg'}/>
		<p class="ssimage__changephoto">change photo</p>
	</div>
	<div if={!loading && !opts.image} class="ssimage__noimage">
	<p class="ssimage__noimagetext">+ add a photo</p>
	</div>
	<script>
		// console.log(riot.version)
		this.loading = false
		var self = this
		console.log(opts)
		this.onImageChange = function (e) {
			console.log(this)
			var file = e.target.files[0]
			var formData = new FormData
			// for (var i = 0; i < files.length; i++) {
			    formData.append("file", file)
			// }
			console.log("changing: ", file)
			self.update({loading: true})
			ajax
				.post("/imageprocessor/entry")
				.attach("file", file)
				.end(function (error, response) {
					if (!error) {
						opts.onchange(response.body)
					}
					self.update({loading:false})
				})
		}
	</script>
</entry-image>