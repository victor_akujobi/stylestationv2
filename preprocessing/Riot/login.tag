require("../SCSS/login.scss")
require("./utils.tag")
<login>
	<div class="sslogin__wrapper">
	<p class="close" onclick={opts.back}>&times;</p>
		<p class="sslogin__intro_text">Not {opts.mode == 'register' ? 'registered' : 'logged in'}? Ok then, let&apos;s get you sorted.<br/> It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear</p>
		<a rel="external" href="/login?provider=facebook" class="srlogin__button srlogin__button__facebook">
			<span class="srlogin__provider__icon srlogin__provider__facebook"><raw value="&#xE60F;"/></span>
			<span class="srlogin__provider__text">{opts.mode == 'register' ? 'Register' : 'Continue'} with your Facebook Account</span>
		</a>
		<a rel="external" href="/login?provider=gplus" class="srlogin__button srlogin__button__google">
			<span class="srlogin__provider__icon srlogin__provider__google"><raw value="&#xE610;" /></span>
			<span class="srlogin__provider__text">{opts.mode == 'register' ? 'Register' : 'Continue'} with your Google Account</span>
		</a>
		<a rel="external" href="/login?provider=twitter" class="srlogin__button srlogin__button__twitter">
			<span class="srlogin__provider__icon srlogin__provider__twitter"><raw value="&#xE60E;"/></span>
			<span class="srlogin__provider__text">{opts.mode == 'register' ? 'Register' : 'Continue'} with your Twitter Account</span>
		</a>
		<a if={opts.mode == 'register'} style="display:block; color:inherit" href="/explore" class="not_ready">I'm not ready for a relationship, I just want to browse</a>
	</div>
</login>