var pagination = require("./pagination.tag")
require("./search.tag")
<entries>
	<grid details={true} items={entries}>
	</grid>
	<pagination params={opts.params} numEntries={entries.length} maxEntries={window.NUM_ENTRIES}>
	</pagination>
	<input type="checkbox" id="search__toggle" />
	<div class="search" riot-tag="search"  search={opts.search}></div>
	<script>
		this.entries = opts.entries
		this.page = opts.page
	</script>

</entries>