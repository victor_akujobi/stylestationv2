<flexgrid>
	<div style="display:flex; flex-wrap:wrap">
		<div style="flex: 1 0 auto; padding: 0.5em 0; display:inline-block" each={item in items}>
			<a href='/a/{item.Ref}'>
				<div style="height: 250px; background-size:cover; background: transparent url(/static/images/ankara/{item.Image}_m.jpg) center center no-repeat"></div>
			</a>
		</div>
	</div>
	<script>
	this.on("update", function () {
		this.items = opts.items
		console.log(this.items)
	})
	</script>
</flexgrid>