var avatar = require("./avatar.tag")
var partition = require("linear-partition")
<grid>
	<div name="grid_container" style="">
		<div style="padding: 0.5em 0; display:inline-block" each={metric, i in metrics}>
			<a href="/a/{parent.items[i].Ref}">
				<div style="width: {metric[0]}px; height: {metric[1]}px; background-size: cover; background-image: url(/static/images/ankara/{parent.items[i].Image}_m.jpg)">
				</div>
			</a>
			<div if={parent.details} style="color: #788; padding: 5px 0px; width: {metric[0]}px; overflow:hidden">
				<avatar ref={parent.items[i].OwnerRef} name={parent.items[i].OwnerName} image={null}/>
			</div>
		</div>
	</div>
	<script>
		var debounce = function (callback, timeout) {
			var t
			return function () {
				window.clearTimeout(t)		
				t = window.setTimeout(callback, timeout)
			}
		}
		this.details = opts.details
		this.metrics = []
		this.arrangeItems = function () {
			this.items = opts.items
			// var items = this.items 
			var elementWidth = this.root.children[0].clientWidth
			var INIT_HEIGHT = 150;
			var MAX_HEIGHT = 500;
			
			var widths = this.items.map(function(item) {
				return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
			})
			var partitions = partition(widths, opts.rows || Math.ceil(this.items.length/Math.ceil(elementWidth/350)))
			var rows = (partitions||[]).map(function (row) {
				var sum = row.reduce(function (total, item) {
					return total + item
				}, 0)
				var ratio = elementWidth / sum
					return row.map(function (item) {
						return [ratio * item, ratio * INIT_HEIGHT]
					})
				})
			// console.log
			this.metrics = [].concat.apply([], rows)
			this.update()
		}
		this.on('mount',  function () {
			var callback = this.arrangeItems.bind(this)
			callback()
			var currentResize = window.onresize
			console.log(window.onresize)
			window.onresize = debounce(function () {
				currentResize && currentResize()
				callback()
				// debounce(callback, 50)
			}, 50)
		})
		this.on('update', this.arrangeItems.bind(this))
		this.on('unmount', function () {
			window.onresize = null
		})
	</script>
</grid>