
<colours>
	<div class="sscolours__block" onclick={opts.toggle} >
	 	<div class="sscolours__blockpallete" style='background-color: {opts.background}'>
	 	 <span if="{!!opts.isselected}" class="sscolours__blockpallete--selected">
	 		<raw value='&#xE60A;'/></span>
	 	</div>
	 	<span class="sscolours__blocktext">{opts.label}</span>
	</div>
	<script>
		this.on('update', function() {
		// this.isSelected = parent.opts.colours.indexOf(colour) !== -1}

		})
	</script>
</colours>