
require("../SCSS/main.scss")
var entryImage = require("./entry-image.tag")
require("./tags.tag")
require("./colours.tag")
require("../SCSS/colours.scss")
require("./genders.tag")
require("./utils.tag")
var riot = require("riot")
var page = require("page")

<new-page>
<div class="ssnewentry__wrapper">
	<message visibility={(!isValid && !!message) ? 'visible': 'hidden'} dismiss={clearMessages} type={messageType} message={message}/>
	<div class="ssentry__image__wrapper">
		<entry-image onchange={onImageChange} image={entry.Image}/>
	</div>
	<div class="ssentry__details__wrapper">
		<input placeholder="Click to enter description" type="text" onchange={updateEntryDescription} value={entry.Description} class="ssentry__descriptioninput" />
		<div class="sstags__stylepickerwrapper">
			<h4 class="sstags__stylepicker">GENDER</h4>
			<genders defaultgenders={defaultGenders} toggle={updateGender} selectedgender={entry.Gender}/>
			<br style="clear:both"/>
		</div>
		<div class="sstags__stylepickerwrapper">
		<h4 class="sstags__stylepicker">STYLES</h4>
			<tag toggle={parent.toggleEntryStyles.bind(null, style)} isselected={parent.entry.Styles.indexOf(style) !== -1} gender={parent.entry.Gender} each={style in STYLES[entry.Gender]} text={style}/>
			<br style="clear:both"/>
		</div>
		<div class="sstags__stylepickerwrapper">
			<h4 class="sstags__stylepicker">COLOURS</h4>
			<colours  label={text} isselected={parent.entry.Colours.indexOf(text) !== -1} background={parent.COLOURS[text]} each={text, colour in COLOURS} toggle={parent.toggleEntryColours.bind(null, text)}/>
			<br style="clear:both"/>
		</div>
		<button class="ssnewentry__submit" onclick={isValid ? submitEntry : showErrors}>
			Done
		</button>
	</div>
</div>
<script>
	this.STYLES = { 
			"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt", "Kaftan", "Jumpsuit"],
			"Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Kaftan"]
	};
	this.COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	this.defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}
	// this.colours={}
	this.entry = opts.entry
	var self = this
	this.validateEntry = function (entry) {
		self.isValid = entry.Validate()
		console.log(self.isValid)
		// if (isNotValid)
	}
	this.submitEntry = function () {
		self.update({isValid: false})
		var successCallback = function () {
			page("/explore")
			// self.update({message: 'Looking great!!', messageType: 'success', dimiss: page.bind(null, "/explore")})
		}
		var errorCallback = function (responseCode) {
			// console.log()
			if (responseCode == 403) {
				localStorage.entry = JSON.stringify(self.entry)
				page("/login")
			}
		}
		self.entry.Save(successCallback, errorCallback)
	}
	this.clearMessages = function () {
		self.update({message: null, messageType: false})
	}
	this.showErrors = function () {
		var message;
		if (!self.entry.Description) {
			message = "Don't forget to tell us a bit about your style. Add some text in the description box"
		} else if (!self.entry.Colours.length) {
			message = "You have not selected any colours. I'm sure everyone would love to know what colours you're wearing!"
		} else if (!self.entry.Styles.length) {
			message = "You have not selected any styles. Hey now, don't keep all that knowledge to yourself!"
		} else if (!self.entry.Image) {
			message = "You have not added any images. You're not camera shy, are you?"
		} else if (!self.entry.ImageHeight || !self.entry.ImageWidth) {
			message = "Hmm... something's wrong with the image you've tried to upload"
		} else {
			message = "Oh noes!!, something happened, and we don't what it is yet. Would you mind reloading the page and trying again"
		}
		self.update({message: message, messageType: 'error'})
	}
	this.toggleEntryStyles = function (style) {
		self.entry.toggleStyle(style)
		self.validateEntry(self.entry)
		self.update()
	}
	this.toggleEntryColours = function (colour) {
		self.entry.toggleColour(colour)
		self.validateEntry(self.entry)
		self.update()
	}
	this.updateEntryDescription = function (e) {
		self.entry.Description = e.target.value
		self.validateEntry(self.entry)
		console.log(self.entry)
	}
	this.updateGender = function (gender) {
		self.entry.toggleGender(gender)
		self.validateEntry(self.entry)
		self.update()
	}
	this.onImageChange = function (props) {
		self.entry.onImageChange(props)
		self.validateEntry(self.entry)
		self.update()
	}
</script>
</new-page>

