require("../SCSS/profile.scss")
require("./profile-image.tag")
<profile-edit>
	<div style="" class="profile__editor__header">
		<div class="header_contents">
			<p>
				<profile-image />
			</p>
			<p class="header__avatar__wrapper">
				{profile.DisplayName}
			</p>
			<p class="header__profile__description">{profile.Description}</p>
		</div>
	</div>
	<div class="profile__editor__body">
	<h4 style="text-align:center;">UPDATE YOUR PROFILE</h4>
		<div class="profile__body__wrapper">
		<form onsubmit={saveProfile}>
		<div>
			<p><span class="profile__input__label">DISPLAY NAME</span> <input class="profile__input" name="description" onchange={updateProfile.bind(null, 'DisplayName')} value='{profile.DisplayName}'/></p>
		</div>
		<div>
			<p><span class="profile__input__label">BIO</span> <input class="profile__input" name="description" onchange={updateProfile.bind(null, 'Description')} value='{profile.Description}'/></p>
		</div>
		<div>
			<p>
				<span style="display:inline-block; vertical-align:middle">ARE YOU A BRAND?</span><input class="profile__input--isbrand" type='checkbox' checked={profile.IsBrand} onchange={updateProfile.bind(null, 'IsBrand')} id="isbrand" name="isbrand"/>
				<label  style="display:inline-block; vertical-align:middle;" for="isbrand" off="YES" on="NO" class="switch__toggle__wrapper">
					<span class="switch__toggle"></span>
				</label>
				<input class="profile__input profile__input--brandname" placeholder="Tell us your brand name" name='brandname' value={profile.BrandName} onchange={updateProfile.bind(null, 'BrandName')} />
				<span style="margin-top:0; font-size: 0.75em">Please note that your brand name is subject to approval</span>
			</p>
		</div>
		<div>
			<p>
				<span class="profile__input__label">PHONE NUMBER<span style="margin-left:0.3em; font-size: 0.8em;">(Optional)</span></span> <input class="profile__input" name="phone" type="phone" onchange={updateProfile.bind(null, 'PhoneNumber')} placeholder="0712345872"value={profile.PhoneNumber}/>
				
			</p>
		</div>
		<div>
			<p>
				<span class="profile__input__label">EMAIL </span><input class="profile__input" type="email" name="email" value='{profile.ContactEmail}' onchange={updateProfile.bind(null, 'ContactEmail')} placeholder="me@mywebsite.com" />
			</p>
		</div>
		<div>
			<p>
				<span class="profile__input__label">WEBSITE<span style="margin-left:0.3em; font-size: 0.8em;">(Optional)</span></span> <input class="profile__input" type="website" name="website" value='{profile.Url}' onchange={updateProfile.bind(null, 'Url')} placeholder="www.mywebsite.com"/>
			</p>
		</div>
		<div><button class="profile__done__button">DONE</button></div>
		</div>
		</form>
	</div>
	<script>
		var self = this
		this.profile = opts.profile
		this.saveProfile = function (e) {
			e.preventDefault && e.preventDefault()
			this.profile.Update()
			console.log(this.profile)
			return false
		}
		this.updateProfile = function (property, evt) {
			var newValue
			console.log(typeof evt)
			if (typeof evt === "string") {
				newValue = evt
			} else {
				switch(evt.target.type) {
					case 'checkbox':
					case 'radio':
						newValue = evt.target.checked
					break
					default:
						newValue = evt.target.value
					break
				}
			}
			self.profile.UpdateValue(property, newValue)
			console.log(self.profile)
			self.update()
		}
	</script>
</profile-edit>