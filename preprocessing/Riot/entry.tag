var colours = require("../JSX/colours.js");
require("../SCSS/detail.scss");
var fonts = require("../JSX/fonts.js")
var utils = require("./utils.tag")
var avatar = require("./avatar.tag")
require("../SCSS/hint.scss")
var grid = require("./grid.tag")
var flexgrid = require("./flexgrid.tag")
var page = require("page")
var swAlert = require("sweetalert")
require("sweetalert/dev/sweetalert.scss")
<entry>
	<div class="ssdetail__wrapper">
				<div class="entry_wrapper">
				<div class="ssdetail__entry">
					<div class="ssdetail__image"><img class='image' src='/static/images/ankara/{entry.Image}_m.jpg' />
					</div>
					<div class="sssharebar__wrapper">
						<p class="share_text">Psst... If you love this style, you can share it on </p>
						<a onclick={Share.bind(null, 'facebook')} class="share_link facebook_share">
							 <raw value={FONTS['facebook']} />
						</a>
						<a onclick={Share.bind(null, 'twitter')} target="_blank" class="share_link twitter_share">
							<raw value={FONTS['twitter']} />
						</a>
						
						<a target="_blank" class="share_link email_share"><raw value={FONTS['email']}/></a>
					</div>
				</div>
				<div class="ssdetail__info">
					<a style="text-decoration:none; font-size: 2em;" href={entry.Edit ? '/me' : '/profile/{entry.OwnerRef}'}><p class="owner"><avatar name={entry.OwnerName} ref={entry.OwnerRef} /></p></a>
					<p class="description">{entry.Description}</p>
					<div class="stats">
					  <ul>
					    <li>
						    <span class="stats__stat">
							    <count value={entry.Views}/>
						    </span>
						    <span class="stats__label stats__views">
							    Views
						    </span></li>
					    <li><span class="stats__stat"><count value={entry.Favourites}/></span><span class="stats__label stats__things">{entry.Favourites == 1 ? 'Favourite' : 'Favourites'}</span></li>
					  </ul>
					</div>
					<div style=" background-color:#fff; padding:0; box-shadow:0 0 2px rgba(0, 0, 0, 0.1); margin: 2em auto 0.5em auto" class="tag_wrapper">
						<p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 0.2em">Style</p>
							<a href='/explore?q=s:{style}' each={style, i in entry.Styles} class="tag">
								<span class="tag_icon">
									<raw value={parent.FONTS[style.toLowerCase()]}/>
								</span>
								<span class="tag_text">
									{style}
								</span>
							</a>
					</div>

					<div style="" class="colour_wrapper">
					<p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 1em">Colours</p>
							<a  href='/explore?q=c:{colour}' each={colour, index in entry.Colours} data-hint={colour} class="hint--bottom colour" style="width: {~~(90/parent.entry.Colours.length || 1)}%; background-color: {parent.COLOURS[colour]}"></a>
					</div>
					<div>
					<p if={false} onclick={entry.Favourite} class={"favourite favourite--" + (entry.IsInFavourites ? "added" : "false")}>{entry.IsInFavourites ? "Added" : "+ Add"} To Favourites</p>
					<span class="delete__text" href="/" onclick={Delete} if={entry.Edit}>DELETE ENTRY</span>
					<br style="clear:both"/>
					</div>
				</div>
			</div>
			<p>We think you might like</p>
			<div if={!!relatedEntries.length} style="width: 100%" class=""> <grid  rows={document.body.clientWidth > 320 ? 1 : 2} items={relatedEntries} /></grid>
		</div>
		<script>
		var self = this
			this.onDeleteSuccess = function () {
				swAlert({
					title: "Deleted",
					text: "Entry Deleted",
					type:"success"
				},
				page.back)
			}
			this.Delete = function () {
				swAlert({
			      title: "Are you sure?", 
			      text: "Are you sure that you want to delete this entry?", 
			      type: "warning",
			      showCancelButton: true,
			      closeOnConfirm: false,
			      confirmButtonText: "Yes, delete it!",
			      confirmButtonColor: "#ec6c62"
			    }, function (ifIsConfirm) {
					self.entry.Delete(self.onDeleteSuccess)	
			    })
				// this.entry.Delete()
			}
			this.entry = opts.entry
			this.COLOURS = colours.COLOURS
			this.FONTS = fonts
			this.relatedEntries = []
			this.entry.getRelatedEntries(function (response) {
				this.relatedEntries = response
				this.update()
			}.bind(this))
		this.Share = function (medium) {
			console.log("sharing")
			var url = window.location.href
			switch (medium) {
				case "twitter":
					var shareLink = "http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url
				break
				case "facebook":
					var shareLink = "http://www.facebook.com/sharer/sharer.php?u=" + url + "&title=" + opts.entry.Description
				break
				case "pinterest":
					var shareLink = "http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + opts.entry.Description
				break
				default:
					return
				break
			}
			window.open(shareLink, "Sharing_Link", "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes")
			return
		}
		</script>
		<style>
			@import url(../SCSS/detail.scss);
		</style>
</entry>