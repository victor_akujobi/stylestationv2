/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var m = __webpack_require__(17)
	var ProfilePage = __webpack_require__(12)
	var detailPage = __webpack_require__(13)
	var animations = __webpack_require__(1)

	var animator = animations.animator

	// console.log(snabbt)
	// console.log(m)
	var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)
	var slidingPage = animator(animations.slideIn, animations.slideOut, true)
	m.route.mode = "pathname"
	m.route(document.getElementById('content'), '/profile/sdjkhkj', {
		"/profile/:ref": ProfilePage,
		'/a/:entry': detailPage,

	})

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var snabbt = __webpack_require__(41)
	var animating = false;

	// Define an animator consisting of optional incoming and outgoing animations. 
	// alwaysAnimate is false unless specified as true: false means an incoming animation will only trigger if an outgoing animation is also in progress.
	// forcing dontClone to true means the outward animation will use the original element rather than a clone. This could improve performance by recycling elements, but can lead to trouble: clones have the advantage of being stripped of all event listeners.
	function animator( incoming, outgoing, alwaysAnimate, dontClone ){
		// The resulting animator can be applied to any number of components
		return function animate( x, y, z ){
			var config;
			var parent;
			var next;

			// When used as a config function 
			if( x.nodeType ){
				return animationConfig( x, y, z );
			}
			// When passed a virtual DOM node (the output of m)
			else if( x.attrs ){
				config = x.attrs.config;

				x.attrs.config = animationConfig;

				return x;
			}
			// When applied to a Mithril module / component 
			else if( x.view ){
				return {
					controller : x.controller || noop,
					view       : function animatedView( ctrl ){
						var view = x.view( ctrl );

						config = view.config;

						view.attrs.config = animationConfig;

						return view;
					}
				};
			}

			function animationConfig( el, init, context ){
				var output;
				var onunload;

				if( config ){
					output   = config( el, init, context );
					// If the root element already has a config, it may also have an onunload which we should take care to preserve 
					onunload = context.onunload;
				}

				if( !init ){
					if( incoming && alwaysAnimate || animating ){
						incoming( el, noop, context );
					}

					context.onunload = outgoing ? onunload ? function onunloadWrapper(){
						teardown();
						onunload();
					} : teardown : onunload;

					parent = el.parentElement;
					next   = el.nextSibling;
				}

				return output;

				function teardown(){
					var insertion = dontClone ? el : el.cloneNode( true );
					var reference = null;

					if( next && parent && next.parentNode === parent ){
						reference = next;
					}
					
					animating = true;
					
					setTimeout( function resetAnimationFlag(){
						animating = false;
					}, 0 );

					parent.insertBefore( insertion, reference );

					outgoing( insertion, function destroy(){
						if( parent.contains( insertion ) ){
							parent.removeChild( insertion );
						}
					}, context );
				}
			}
		};
	}

	function noop(){}


	var fadeScaleIn = function (element) {
		var options = {
			fromScale: [0, 0],
			scale: [1, 1],
			fromOpacity: 0,
			opacity:1,
			easing: 'spring',
			// duration:2000
			springConstant: 0.8,
			springDeceleration: 0.3,
			springMass: 5
		}
		snabbt(element, options)
	}
	var fadeScaleOut = function (element, callback) {
		snabbt(element, {
			fromScale: [0, 0],
			scale: [1, 1],
			fromOpacity: 0,
			opacity:1,
			easing: 'spring',
			springConstant: 0.3,
			springDeceleration: 0.8,
			callback: callback
	})
	}
	var scaleInFrom = function () {

	}
	slideIn = function (element) {
		snabbt(element, {
			fromPosition: [-element.offsetWidth, 0, 0],
			position: [0, 0, 0],
			easing: 'easeInOut',
			duration:200
		})
	}
	slideOut = function (element, callback) {
		snabbt(element, {
			fromPosition: [0, 0, 0],
			position: [element.offsetWidth, 0, 0],
			easing: 'easeInOut',
			callback:callback,
			duration:200
		})
	}
	module.exports = {animator: animator, fadeScaleIn: fadeScaleIn, fadeScaleOut: fadeScaleOut, slideIn: slideIn, slideOut:slideOut}

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var SSConfig = {
		XHR: function (xhr) {
			 xhr.setRequestHeader("Response", "json")
		}
	}

	module.exports = SSConfig

/***/ },
/* 3 */,
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	// require("../SCSS/gender.scss")
	var m = __webpack_require__(17)
	var defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}
	// gender
	var genders = {}
	genders.controller = function () {

	}
	genders.view = function (vm) {
		var entry = vm.entry;
		var genderView = Object.keys(defaultGenders).map(function (gender) {
			var isSelected = gender == entry.Gender()
			var icon= isSelected ? m.trust("&#xE60A;") : m.trust(defaultGenders[gender])
			var iconClass = isSelected ? "sstags__icon--selected" : ""
			var genderClass = "sstags__wrapper--" + gender.toLowerCase()
			return {tag: "div", attrs: {key:gender,onclick:vm.onclick.bind(null, gender),class:"sstags__wrapper "+genderClass}, children: [
				{tag: "span", attrs: {class:"sstags__icon "+iconClass}, children: [icon]}, 
				{tag: "span", attrs: {class:"sstags__text"}, children: [gender]}	
				]}
		})
		return {tag: "div", attrs: {style:"text-align:center"}, children: [" ",genderView]}
	}

	module.exports = genders

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	'use strict';
	var m = __webpack_require__(17)
	var fonts = __webpack_require__(10)
	__webpack_require__(22)
	// var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;'}
	var tags = {}
	tags.controller = function() {

	}
	// tags.vm = {isSelected: false}
	tags.view = function (vm) {
		var icon= vm.isSelected ? m.trust(fonts['success']) : m.trust(fonts[vm.tagText.toLowerCase()])
		var iconClass = vm.isSelected ? "sstags__icon--selected" : ""
		var gender = !!vm.gender ? "sstags__wrapper--" + vm.gender : ""
			return {tag: "div", attrs: {key:vm.tagText,onclick:vm.onclick,class:"sstags__wrapper "+gender}, children: [
				{tag: "span", attrs: {class:"sstags__icon "+iconClass}, children: [icon]}, 
				{tag: "span", attrs: {class:"sstags__text"}, children: [vm.tagText]}	
				]}
	}

	module.exports = tags


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	'use strict';
	var m = __webpack_require__(17)
	__webpack_require__(24)
	var image = {}
	var imageCache = new Image()
	// imageCache.src = "/static/images/loading.gif"
	image.onchange= function(vm, e) {
		// m.request(/)
		var file = e.target.files[0]
		var formData = new FormData
	    // for (var i = 0; i < files.length; i++) {
	        formData.append("file", file)
	    // }
		// callback("jsdhjsh")
		vm.isLoading && vm.isLoading(true)
		m.redraw()
		return m.request({
	        method: "POST",
	        url: "/imageprocessor/entry",
	        data: formData,
	        //simply pass the FormData object intact to the underlying XMLHttpRequest, instead of JSON.stringify'ing it
	        serialize: function(value) {return value}
	    }).then(function (response) {
	    	vm.entry.Image(response.image)
	    	vm.entry.ImageHeight(response.height)
	    	vm.entry.ImageWidth(response.width)
	    	vm.isLoading && vm.isLoading(false)
	    })
	}
	image.controller = function () {

	}
	image.view = function (vm) {
		var content;
		var imageInput = ""
		console.log(vm.isLoading())
		if (!!vm.isLoading()) {
			console.log("Inner")
			content = {tag: "div", attrs: {class:"ssimage__loadingimage"}}
		} else {
			imageInput = {tag: "input", attrs: {class:"ssimage__imageinput",type:"file",onchange:image.onchange.bind(null, vm)}}
			if (!!vm.entry.Image()) {
				content = {tag: "div", attrs: {}, children: [{tag: "img", attrs: {style:"width:100%",src:"/static/images/ankara/"+vm.entry.Image()+"_m.jpg"}}, {tag: "p", attrs: {class:"ssimage__changephoto"}, children: ["change photo"]}]}
			} else {
				content = {tag: "div", attrs: {class:"ssimage__noimage"}, children: [{tag: "p", attrs: {class:"ssimage__noimagetext"}, children: ["+ add a photo"]}]}
			}
		}
		return {tag: "div", attrs: {class:"ssimage__wrapper"}, children: [
		content,
		imageInput
		]}
	}

	module.exports = image

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	__webpack_require__(26)
	var m = __webpack_require__(17)
	var colours = {}
	colours.COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	colours.handleSelect = function (callback, value) {
		callback(value)
	}
	colours.view = function (vm) {
		var selected = vm.selected.reduce(function (dict, currentValue) {
			dict[currentValue] = true;
			return dict
		}, {})
		return Object.keys(colours.COLOURS).map(function (path) {
			var isSelected = !!selected[path] ? {tag: "span", attrs: {class:"sscolours__blockpallete--selected"}, children: [m.trust("&#xE60A;")]} : ""
			return {tag: "div", attrs: {class:"sscolours__block",onclick:colours.handleSelect.bind(null, vm.onclick, path)}, children: [{tag: "div", attrs: {class:"sscolours__blockpallete",style:{"background-color": colours.COLOURS[path]}}, children: [isSelected]}, {tag: "span", attrs: {class:"sscolours__blocktext"}, children: [path]}]}
		})
	}
	module.exports = colours




/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	// @prepros-prepend linear-partition.js
	//@prepros-prepend avatar.js
	var m = __webpack_require__(17)
	var partition = __webpack_require__(42)
	__webpack_require__(30)
	var gridify = {}
	// gridify.metrics = m.prop([])




	// gridify.engine = function (element, items, template) {
		
	// 	@element: DOMNode
	// 	@items: Array of items
	// 	@metric Array of geometries
	// 	@template Virtual DOM to render
		
	// 	// return function (element, isInit) {

	// 		m.render(element, this.items.map(function (item, index) {
	// 				return template(item, style)
	// 			}))
	// 	// }
	// #
	gridify.config = function(options, container) {
		// if (context.isInit) {
		// 	return
		// }
		// if (options.animation) {
			var config = options.animation ? options.animation : null
		// }
		var block = options.view || function (entry, style) {
			return {tag: "a", attrs: {href:"/a/"+entry.Ref,class:"ssgrid__innerblock"}, children: [{tag: "img", attrs: {config:config,style:style,src:"/static/images/ankara/"+entry.Image+"_t.jpg"}}]}
		}
		var items = options.entries || []
		var numPartitions = options.numPartitions || false
		var createPartitions = function () {
			var elementWidth = container.offsetWidth
			var WIDTH = 450;
			var INIT_HEIGHT = 150;
			var MAX_HEIGHT = 500;
			var widths = []
			var ratios = items.map(function(item) {
				return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
			})
		var partitions = partition(ratios, numPartitions || Math.ceil(items.length/Math.ceil(elementWidth/200)))
			var test = partitions.map(function (row) {
				var sum = row.reduce(function (total, item) {
					return total + item
				}, 0)
				ratio = elementWidth / sum
				return row.map(function (item) {
					return [ratio * item, ratio * INIT_HEIGHT]
				})
			}.bind(this))
			var metrics = [].concat.apply([], test)
			m.render(container, null)
			m.render(container, metrics.map(function (metric, index) {
				var style = {width:~~metric[0] + "px", height:~~metric[1] + "px"}
				var item = items[index]
				return block(item, style)
			}))
		}
			window.onresize = function () {
				createPartitions()
			}
			createPartitions()
	}
	gridify.view = function (vm) {
		var entries = vm.entries || []
		// var metrics  = gridify.metrics()
		// console.log(metrics, "MET")
		// var blocks = metrics.map(function (metric, style) {
		// 	var entry = entries[index]
		// 	// var style = {"width":metric[0]+"px", "height": metric[1] + "px"};
		// 	console.log(style)
		// 	return <div class="ssgrid__block"><a href={"/a/"+entry.Ref} class="ssgrid__innerblock"><img style={style} src={"/static/images/ankara/"+entry.Image+"_t.jpg"}/></a><p class="ssgrid__details_holder">{avatar.view(entry.OwnerName)}</p></div>	
		// })
		return {tag: "div", attrs: {config:gridify.config.bind(null, {entries: entries, numPartitions: vm.numPartitions || false, view: vm.view || false, animation: vm.animation})}
		}
	}

	module.exports = gridify

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var m = __webpack_require__(17)
	__webpack_require__(28);
	var fonts = __webpack_require__(10)
	var Popup = function () {
			var show = m.prop(true)
			var toggleDisplay = function () {
				show(!show())
			}
			var closePopup = function () {
				show(false)
			}
			var overlay = document.body.appendChild(document.createElement('div'))
			overlay.className = "overlay_hidden"
			return function (body) {
				overlay.className = "overlay_visible"
				m.render(overlay, body)
			}
	}

	var Panel = function (options) {
		var content = options.content
		if (!content) {return}
		var type = options.type || info
		var timeout = options.timeout || 6000
		var element = {tag: "div", attrs: {class:"srpanel__" + type}, children: [content]}
		var panelParent = document.createElement('div')
		panelParent.className = "srpanel";
		m.render(panelParent, element)
		document.body.appendChild(panelParent)
		window.setTimeout(function () {
			document.body.removeChild(panelParent)
		}, timeout)

	}
	var LoginPopup = function () {
		return {tag: "div", attrs: {class:"sslogin__wrapper"}, children: [
	{tag: "p", attrs: {class:"sslogin__intro_text"}, children: ["Not logged in? Ok then, let's get you sorted.",{tag: "br", attrs: {}}, " It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear"]}, 
	{tag: "a", attrs: {href:"/login?provider=facebook",class:"srlogin__button srlogin__button__facebook"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__facebook"}, children: [m.trust("&#xE60F")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Facebook Account"]}
	]}, 
	{tag: "a", attrs: {href:"/login?provider=gplus",class:"srlogin__button srlogin__button__google"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__google"}, children: [m.trust("&#xE610")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Google Account"]}
	]}, 
	{tag: "a", attrs: {href:"/login?provider=twitter",class:"srlogin__button srlogin__button__twitter"}, children: [
		{tag: "span", attrs: {class:"srlogin__provider__icon srlogin__provider__twitter"}, children: [m.trust("&#xE60E")]}, 
		{tag: "span", attrs: {class:"srlogin__provider__text"}, children: ["Continue with your Twitter Account"]}
	]}
	]}
	}

	// var SearchPage = function (options) {
	// 	return <div>
	// 	<input type="checkbox" class="switch_toggle"/>
	// 		<p>I am looking for</p>
	// 		<p>Choose colours</p>
	// 		<p>Choose styles</p>
	// 		<p>for</p>
	// 		<p>{Select({options:["Male", "Female", "Android"], selected:["Male", "Female"]})}</p>
	// 	</div>
	// }
	var SearchPage = function (vm) {
		var onChange = function (section, option) {
			// console.log(section, option)
			vm.options[section][option](!vm.options[section][option]())
			vm.onChange()
			// m.redraw()
		}
		return {tag: "div", attrs: {class:"sidebar__wrapper"}, children: [
		        {tag: "label", attrs: {for:"toggle_switch",class:"toggle_label"}}, 
		        {tag: "input", attrs: {class:"toggle_switch",id:"toggle_switch",type:"checkbox"}}, 
		        {tag: "div", attrs: {id:"search"}, children: [
			          
			          	Object.keys(vm.options).filter(function (section) {
			          		return section !== "Page"
			          	}).map(function (section, index) {
				          	return {tag: "div", attrs: {class:"search_items_wrapper"}, children: [{tag: "p", attrs: {class:"search_header"}, children: [section]}, 
				          		Object.keys(vm.options[section]).map(function (option, optionIndex) {
				          			console.log(section, option)
				          		iconFont = vm.options[section][option]() ? fonts['success'] : "-"
				          		return {tag: "p", attrs: {onclick:onChange.bind(null, section, option),class:"search_item search_item_" + section.toLowerCase()}, children: [{tag: "span", attrs: {class:"sstags__icon",config:vm.iconAnimate}, children: [m.trust(iconFont)]}, {tag: "label", attrs: {class:"search_item_text"}, children: [option]}]}
				          	})
				        ]}
			          }),
		          
		          {tag: "label", attrs: {class:"done_button",for:"toggle_switch"}, children: ["Done"]}
		        ]}, 
		        {tag: "div", attrs: {}}
		      ]}
	}
	SearchPage.prototype.toggle = function () {

	}

	var Select = function (state) {
		return {tag: "div", attrs: {tabindex:"0"}, children: [{tag: "span", attrs: {}, children: [state.selected.join("/")]}, 
					state.options.map(function (option) {
						return {tag: "p", attrs: {}, children: [option]}
					})
				]}
	}

	module.exports = {Popup: Popup, LoginPopup: LoginPopup, Panel:Panel, Select:Select, SearchPage: SearchPage}

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;', 'facebook': '&#xE60F', 'pinterest': '&#xE611', 'email': '&#xE60D', 'twitter': '&#xE60E', 'google': '&#xE610', success:'&#xE60A'}
	module.exports = fonts

/***/ },
/* 11 */,
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var m = __webpack_require__(17)
	var SSConfig = __webpack_require__(2)
	var ProfileHeader = __webpack_require__(33)
	var NavBar = __webpack_require__(14).view
	var gridify = __webpack_require__(8)
	var animations = __webpack_require__(1)
	var animator = animations.animator
	var ownProfileGridImageAnimation = animator(animations.fadeScaleIn, animations.noop, false)
	var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)

	__webpack_require__(18)
	__webpack_require__(34)
	var uploads
	var Profile = function (options) {
		console.log("profile: ", this)
		this.Description = m.prop(options.Description || "")
		this.Url = m.prop(options.URL || "")
		this.DisplayName = m.prop(options.DisplayName || "")
		this.Collections = m.prop(options.Collection || 0)
		this.Uploads = m.prop(options.Uploads || 0)
		this.update = function () {
			console.log(this)
			var requestData = {
				Description: this.Description(),
				URL: this.Url(),
				DisplayName: this.DisplayName()
			}
			m.request({
				url: "/me",
				method: "PUT",
				data: requestData
			})
			return this	
		}.bind(this)
	}
	var ProfilePage = {}
	ProfilePage.controller = function (defaultOptions) {
		console.log(defaultOptions)
		this.options = defaultOptions || {}
		var ctrl = this
		this.profileData = new Profile({})
		this.page = m.prop(1)
		this.section = m.prop("uploads")
		// this.profileData = new Profile({})
		this.ref = ctrl.options.ref
		this.entries = m.prop([])
		this.block = function (entry, style) {
			var urlStem = !!ctrl.options.editMode ? "/edit/" : "/a/"
			return {tag: "a", attrs: {href:urlStem + entry.Ref,class:"ssgrid__innerblock"}, children: [
				{tag: "img", attrs: {config:!!ctrl.options.editMode ? ownProfileGridImageAnimation: gridImageAnimation,style:style,src:"/static/images/ankara/"+entry.Image+"_t.jpg"}}
			]}
		}
		this.fetchProfileData = function () {
			if (!!ctrl.options.editMode) {
				// Set Entries endpoint appropriately.
				m.request({url: "/me", config:SSConfig.XHR, method: "GET"}).then(ctrl.profileData)
			} else {
				m.request({url: "/profile/" + ctrl.ref, config:SSConfig.XHR, method: "GET"}).then(function (response) {
					ctrl.profileData = new Profile(response)
				})
			}
		}

		this.fetchEntries = function () {
			var urlStem = !!ctrl.options.editMode ? "/my": "/profile/" + ctrl.ref()
			m.request({
				url: urlStem + "/" + ctrl.section(),
				method: "GET",
				data:{page: ctrl.page()},
				config: SSConfig.XHR
			}).then(ctrl.entries, console.log)
		}
		this.handleSectionChange = function (section) {
			if (section != ctrl.section()) {
				console.log(section)
				ctrl.section(section)
				ctrl.page(1)
				ctrl.fetchEntries()
			}
		}
		this.fetchNextEntries = function () {
			console.log("Nexting")
			var nextPage = ctrl.page() + 1
			ctrl.page(nextPage)
			ctrl.fetchEntries()
		}
		this.fetchPreviousEntries = function () {
			var previousPage = ctrl.page() - 1
			if (previousPage > 0) {
				ctrl.page(previousPage)
				ctrl.fetchEntries()
			}
		}
		this.resetPage = function () {
			ctrl.page(1)
		}
		this.name = m.prop()
		if (window.bootstrap) {
			this.profileData = new Profile(bootstrap.profile)
			this.fetchEntries()
			window.bootstrap = false
		} else {
			// this.profileData = new Profile({})
			this.fetchProfileData()
			this.fetchEntries()
		}
	}
	ProfilePage.view = function (ctrl) {
		var options
		var profile = ctrl.profileData
		return {tag: "div", attrs: {}, children: [
		
			ProfileHeader.view({
				editMode: !!ctrl.options.editMode,
				onSectionChange: ctrl.handleSectionChange,
				profile: profile,
				section: ctrl.section()
			}),
		
		{tag: "div", attrs: {class:"pad"}, children: [
			gridify.view({"entries": ctrl.entries(), view:ctrl.block})
		]}, 
		NavBar({
			showNext: ctrl.entries().length == 12,
			showPrevious: ctrl.page() > 1,
			onClickNext: ctrl.fetchNextEntries,
			onClickPrevious: ctrl.fetchPreviousEntries
		})
		]}
	}

	module.exports = ProfilePage

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var gridify = __webpack_require__(8)
	var m = __webpack_require__(17)
	var utils = __webpack_require__(9)
	var fonts = __webpack_require__(10)
	var genders = __webpack_require__(4)
	var tags = __webpack_require__(5)
	var image = __webpack_require__(6)
	var colours = __webpack_require__(7)
	var SSConfig = __webpack_require__(2)
	var animations = __webpack_require__(1)
	var countUp = __webpack_require__(43)
	var Entry = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"./entry\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()))
	__webpack_require__(20)
	__webpack_require__(36)
	var countUpConfig = function (count, element, isInitialized, context) {
		if (!isInitialized) {
			var numAnim = new countUp(element, 1, count);
			numAnim.start();
		} else {
			if (count == context.count) {
				return
			} else {
				var numAnim = new countUp(element, context.count, count);
				numAnim.start();
			}
		}
		context.count = count
	}
	var animator = animations.animator
	var gridImageAnimation = animator(animations.fadeScaleIn, animations.noop, true)
	var detailPage = {}
	detailPage.view = function (ctrl) {
		var relatedEntries = ctrl.relatedEntries() && ctrl.relatedEntries().length > 0 ? gridify.view({entries: ctrl.relatedEntries(), numPartitions: document.body.offsetWidth > 480 ? 1 : 2}) :{tag: "div", attrs: {class:"loading_gif"}}
		if (ctrl.entry()) {
			var entry = ctrl.entry()
			var url = document.location.origin + "/ankara/" + entry.Ref()
			// var colours = ent
			return {tag: "div", attrs: {class:"ssdetail__wrapper"}, children: [
					{tag: "div", attrs: {class:"entry_wrapper"}, children: [
					{tag: "div", attrs: {class:"ssdetail__entry"}, children: [
					{tag: "div", attrs: {class:"ssdetail__image"}, children: [{tag: "img", attrs: {class:"image",src:"/static/images/ankara/"+entry.Image()+"_m.jpg"}}]}, 
					{tag: "div", attrs: {class:"sssharebar__wrapper"}, children: [
						{tag: "p", attrs: {class:"share_text"}, children: ["Psst... If you love this style, you can share it on "]}, 
						{tag: "a", attrs: {href:"http://www.facebook.com/sharer/sharer.php?u=" + url + "&title=" + entry.Description(),target:"_blank",class:"share_link facebook_share"}, children: [
							m.trust(fonts['facebook'])
						]}, 
						{tag: "a", attrs: {href:"http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url,target:"_blank",class:"share_link twitter_share"}, children: [
							m.trust(fonts['twitter'])
						]}, 
						{tag: "a", attrs: {href:"http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + entry.Description,target:"_blank",class:"share_link pinterest_share"}, children: [
							m.trust(fonts['pinterest'])
						]}, 
						{tag: "a", attrs: {target:"_blank",class:"share_link email_share"}, children: [m.trust(fonts['email'])]}
					]}
					]}, 
				{tag: "div", attrs: {class:"ssdetail__info"}, children: [
					{tag: "a", attrs: {href:entry.Edit ? "/me" : "/profile/" + entry.OwnerRef}, children: [" ",{tag: "p", attrs: {class:"owner"}, children: [entry.OwnerName]}]}, 
					{tag: "p", attrs: {class:"description"}, children: [entry.Description()]}, 
					{tag: "div", attrs: {class:"stats"}, children: [
					  {tag: "ul", attrs: {}, children: [
					    {tag: "li", attrs: {}, children: [{tag: "span", attrs: {class:"stats__stat",config:countUpConfig.bind(null, entry.Views)}}, {tag: "span", attrs: {class:"stats__label stats__views"}, children: ["Views"]}]}, 
					    {tag: "li", attrs: {}, children: [{tag: "span", attrs: {class:"stats__stat",config:countUpConfig.bind(null, entry.Favourites())}}, {tag: "span", attrs: {class:"stats__label stats__things"}, children: ["Favourite",entry.Favourites() == 1 ? "" : "s"]}]}
					  ]}
					]}, 
					{tag: "div", attrs: {style:" background-color:#fff; padding:0; box-shadow:0 0 2px rgba(0, 0, 0, 0.1); margin: 2em auto 0.5em auto",class:"tag_wrapper"}, children: [
						{tag: "p", attrs: {style:"background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 0.2em"}, children: ["Style"]}, 
						entry.Styles().map(function (style) {
							return {tag: "p", attrs: {class:"tag"}, children: [{tag: "span", attrs: {class:"tag_icon"}, children: [m.trust(fonts[style.toLowerCase()])]}, {tag: "span", attrs: {class:"tag_text"}, children: [style]}]}
						})
					]}, 

					{tag: "div", attrs: {style:"",class:"colour_wrapper"}, children: [
					{tag: "p", attrs: {style:"background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 1em"}, children: ["Colours"]}, 
						entry.Colours().map(function (colour) {
							return {tag: "span", attrs: {"data-hint":colour,class:"hint--bottom colour",style:{"width": ~~(90/entry.Colours().length || 1)+ "%", "background-color": colours.COLOURS[colour]}}}
						})
					]}, 
					{tag: "div", attrs: {}, children: [
					{tag: "p", attrs: {onclick:entry.Favourite,class:"favourite favourite--" + (entry.IsInFavourites() ? "added" : "false")}, children: [entry.IsInFavourites() ? "Added" : "+ Add"," To Favourites"]}
					]}
				]}
				]}, 
				{tag: "p", attrs: {}, children: ["We think you might like"]}, 
				{tag: "div", attrs: {class:"related_entries"}, children: [relatedEntries]}
			]}
		} else {
			return {tag: "div", attrs: {}}
		}
	}
	// Share links courtesy of http://petragregorova.com/articles/social-share-buttons-with-custom-icons/
	detailPage.controller = function (defaultOptions) {
		var ctrl = this
		ctrl.options = defaultOptions || {}
		ctrl.entry = m.prop(new Entry({}))
		console.log(window.bootstrap)
		ctrl.relatedEntries = m.prop([])
		if (!!window.bootstrap) {
			ctrl.entry = m.prop(new Entry(window.bootstrap.entry))
			ctrl.relatedEntries = m.prop(window.bootstrap.relatedEntries)
			window.bootstrap = false
		} else {
			var entryRef = ctrl.options.ref || "sxxeSLWUYS"
				m.request({url: "/related/"+ entryRef, method:"get"}).then(function (response) {
				ctrl.relatedEntries(response)
			}, function () {
				return []
			})
			m.request({url: "/a/"+ entryRef, method:"get", config:SSConfig.XHR}).then(function (response) {
				ctrl.entry(new Entry(response))
				console.log(ctrl.entry())
			}, function () {
				console.log("Error")
				return []
			})
		}
	}
	module.exports = detailPage

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var m = __webpack_require__(17)

	NavBar = {}
	NavBar.view = function (options) {
		var previousLink = null
		var nextLink = null
		if (options.showPrevious) {
			previousLink = {tag: "a", attrs: {title:"Previous Page",onclick:options.onClickPrevious,class:"prev_page"}, children: ["❮"]}
		}
		if (!!options.showNext) {
			nextLink = {tag: "a", attrs: {title:"Next Page",onclick:options.onClickNext,class:"next_page"}, children: ["❯"]}
		}
		return {tag: "p", attrs: {class:"nav"}, children: [
			previousLink,nextLink
		]}
	}

	module.exports = NavBar

/***/ },
/* 15 */,
/* 16 */,
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {var m = (function app(window, undefined) {
		var OBJECT = "[object Object]", ARRAY = "[object Array]", STRING = "[object String]", FUNCTION = "function";
		var type = {}.toString;
		var parser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[.+?\])/g, attrParser = /\[(.+?)(?:=("|'|)(.*?)\2)?\]/;
		var voidElements = /^(AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR)$/;

		// caching commonly used variables
		var $document, $location, $requestAnimationFrame, $cancelAnimationFrame;

		// self invoking function needed because of the way mocks work
		function initialize(window){
			$document = window.document;
			$location = window.location;
			$cancelAnimationFrame = window.cancelAnimationFrame || window.clearTimeout;
			$requestAnimationFrame = window.requestAnimationFrame || window.setTimeout;
		}

		initialize(window);


		/**
		 * @typedef {String} Tag
		 * A string that looks like -> div.classname#id[param=one][param2=two]
		 * Which describes a DOM node
		 */

		/**
		 *
		 * @param {Tag} The DOM node tag
		 * @param {Object=[]} optional key-value pairs to be mapped to DOM attrs
		 * @param {...mNode=[]} Zero or more Mithril child nodes. Can be an array, or splat (optional)
		 *
		 */
		function m() {
			var args = [].slice.call(arguments);
			var hasAttrs = args[1] != null && type.call(args[1]) === OBJECT && !("tag" in args[1]) && !("subtree" in args[1]);
			var attrs = hasAttrs ? args[1] : {};
			var classAttrName = "class" in attrs ? "class" : "className";
			var cell = {tag: "div", attrs: {}};
			var match, classes = [];
			if (type.call(args[0]) != STRING) throw new Error("selector in m(selector, attrs, children) should be a string")
			while (match = parser.exec(args[0])) {
				if (match[1] === "" && match[2]) cell.tag = match[2];
				else if (match[1] === "#") cell.attrs.id = match[2];
				else if (match[1] === ".") classes.push(match[2]);
				else if (match[3][0] === "[") {
					var pair = attrParser.exec(match[3]);
					cell.attrs[pair[1]] = pair[3] || (pair[2] ? "" :true)
				}
			}
			if (classes.length > 0) cell.attrs[classAttrName] = classes.join(" ");


			var children = hasAttrs ? args[2] : args[1];
			if (type.call(children) === ARRAY) {
				cell.children = children
			}
			else {
				cell.children = hasAttrs ? args.slice(2) : args.slice(1)
			}

			for (var attrName in attrs) {
				if (attrName === classAttrName) {
					if (attrs[attrName] !== "") cell.attrs[attrName] = (cell.attrs[attrName] || "") + " " + attrs[attrName];
				}
				else cell.attrs[attrName] = attrs[attrName]
			}
			return cell
		}
		function build(parentElement, parentTag, parentCache, parentIndex, data, cached, shouldReattach, index, editable, namespace, configs) {
			//`build` is a recursive function that manages creation/diffing/removal of DOM elements based on comparison between `data` and `cached`
			//the diff algorithm can be summarized as this:
			//1 - compare `data` and `cached`
			//2 - if they are different, copy `data` to `cached` and update the DOM based on what the difference is
			//3 - recursively apply this algorithm for every array and for the children of every virtual element

			//the `cached` data structure is essentially the same as the previous redraw's `data` data structure, with a few additions:
			//- `cached` always has a property called `nodes`, which is a list of DOM elements that correspond to the data represented by the respective virtual element
			//- in order to support attaching `nodes` as a property of `cached`, `cached` is *always* a non-primitive object, i.e. if the data was a string, then cached is a String instance. If data was `null` or `undefined`, cached is `new String("")`
			//- `cached also has a `configContext` property, which is the state storage object exposed by config(element, isInitialized, context)
			//- when `cached` is an Object, it represents a virtual element; when it's an Array, it represents a list of elements; when it's a String, Number or Boolean, it represents a text node

			//`parentElement` is a DOM element used for W3C DOM API calls
			//`parentTag` is only used for handling a corner case for textarea values
			//`parentCache` is used to remove nodes in some multi-node cases
			//`parentIndex` and `index` are used to figure out the offset of nodes. They're artifacts from before arrays started being flattened and are likely refactorable
			//`data` and `cached` are, respectively, the new and old nodes being diffed
			//`shouldReattach` is a flag indicating whether a parent node was recreated (if so, and if this node is reused, then this node must reattach itself to the new parent)
			//`editable` is a flag that indicates whether an ancestor is contenteditable
			//`namespace` indicates the closest HTML namespace as it cascades down from an ancestor
			//`configs` is a list of config functions to run after the topmost `build` call finishes running

			//there's logic that relies on the assumption that null and undefined data are equivalent to empty strings
			//- this prevents lifecycle surprises from procedural helpers that mix implicit and explicit return statements (e.g. function foo() {if (cond) return m("div")}
			//- it simplifies diffing code
			//data.toString() is null if data is the return value of Console.log in Firefox
			if (data == null || data.toString() == null) data = "";
			if (data.subtree === "retain") return cached;
			var cachedType = type.call(cached), dataType = type.call(data);
			if (cached == null || cachedType !== dataType) {
				if (cached != null) {
					if (parentCache && parentCache.nodes) {
						var offset = index - parentIndex;
						var end = offset + (dataType === ARRAY ? data : cached.nodes).length;
						clear(parentCache.nodes.slice(offset, end), parentCache.slice(offset, end))
					}
					else if (cached.nodes) clear(cached.nodes, cached)
				}
				cached = new data.constructor;
				if (cached.tag) cached = {}; //if constructor creates a virtual dom element, use a blank object as the base cached node instead of copying the virtual el (#277)
				cached.nodes = []
			}

			if (dataType === ARRAY) {
				//recursively flatten array
				for (var i = 0, len = data.length; i < len; i++) {
					if (type.call(data[i]) === ARRAY) {
						data = data.concat.apply([], data);
						i-- //check current index again and flatten until there are no more nested arrays at that index
					}
				}
				
				var nodes = [], intact = cached.length === data.length, subArrayCount = 0;

				//keys algorithm: sort elements without recreating them if keys are present
				//1) create a map of all existing keys, and mark all for deletion
				//2) add new keys to map and mark them for addition
				//3) if key exists in new list, change action from deletion to a move
				//4) for each key, handle its corresponding action as marked in previous steps
				//5) copy unkeyed items into their respective gaps
				var DELETION = 1, INSERTION = 2 , MOVE = 3;
				var existing = {}, unkeyed = [], shouldMaintainIdentities = false;
				for (var i = 0; i < cached.length; i++) {
					if (cached[i] && cached[i].attrs && cached[i].attrs.key != null) {
						shouldMaintainIdentities = true;
						existing[cached[i].attrs.key] = {action: DELETION, index: i}
					}
				}
				if (shouldMaintainIdentities) {
					if (data.indexOf(null) > -1) data = data.filter(function(x) {return x != null})
					
					var keysDiffer = false
					if (data.length != cached.length) keysDiffer = true
					else for (var i = 0, cachedCell, dataCell; cachedCell = cached[i], dataCell = data[i]; i++) {
						if (cachedCell.attrs && dataCell.attrs && cachedCell.attrs.key != dataCell.attrs.key) {
							keysDiffer = true
							break
						}
					}
					
					if (keysDiffer) {
						for (var i = 0, len = data.length; i < len; i++) {
							if (data[i] && data[i].attrs) {
								if (data[i].attrs.key != null) {
									var key = data[i].attrs.key;
									if (!existing[key]) existing[key] = {action: INSERTION, index: i};
									else existing[key] = {
										action: MOVE,
										index: i,
										from: existing[key].index,
										element: cached.nodes[existing[key].index] || $document.createElement("div")
									}
								}
								else unkeyed.push({index: i, element: parentElement.childNodes[i] || $document.createElement("div")})
							}
						}
						var actions = []
						for (var prop in existing) actions.push(existing[prop])
						var changes = actions.sort(sortChanges);
						var newCached = new Array(cached.length)

						for (var i = 0, change; change = changes[i]; i++) {
							if (change.action === DELETION) {
								clear(cached[change.index].nodes, cached[change.index]);
								newCached.splice(change.index, 1)
							}
							if (change.action === INSERTION) {
								var dummy = $document.createElement("div");
								dummy.key = data[change.index].attrs.key;
								parentElement.insertBefore(dummy, parentElement.childNodes[change.index] || null);
								newCached.splice(change.index, 0, {attrs: {key: data[change.index].attrs.key}, nodes: [dummy]})
							}

							if (change.action === MOVE) {
								if (parentElement.childNodes[change.index] !== change.element && change.element !== null) {
									parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null)
								}
								newCached[change.index] = cached[change.from]
							}
						}
						for (var i = 0, len = unkeyed.length; i < len; i++) {
							var change = unkeyed[i];
							parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null);
							newCached[change.index] = cached[change.index]
						}
						cached = newCached;
						cached.nodes = new Array(parentElement.childNodes.length);
						for (var i = 0, child; child = parentElement.childNodes[i]; i++) cached.nodes[i] = child
					}
				}
				//end key algorithm

				for (var i = 0, cacheCount = 0, len = data.length; i < len; i++) {
					//diff each item in the array
					var item = build(parentElement, parentTag, cached, index, data[i], cached[cacheCount], shouldReattach, index + subArrayCount || subArrayCount, editable, namespace, configs);
					if (item === undefined) continue;
					if (!item.nodes.intact) intact = false;
					if (item.$trusted) {
						//fix offset of next element if item was a trusted string w/ more than one html element
						//the first clause in the regexp matches elements
						//the second clause (after the pipe) matches text nodes
						subArrayCount += (item.match(/<[^\/]|\>\s*[^<]/g) || []).length
					}
					else subArrayCount += type.call(item) === ARRAY ? item.length : 1;
					cached[cacheCount++] = item
				}
				if (!intact) {
					//diff the array itself
					
					//update the list of DOM nodes by collecting the nodes from each item
					for (var i = 0, len = data.length; i < len; i++) {
						if (cached[i] != null) nodes.push.apply(nodes, cached[i].nodes)
					}
					//remove items from the end of the array if the new array is shorter than the old one
					//if errors ever happen here, the issue is most likely a bug in the construction of the `cached` data structure somewhere earlier in the program
					for (var i = 0, node; node = cached.nodes[i]; i++) {
						if (node.parentNode != null && nodes.indexOf(node) < 0) clear([node], [cached[i]])
					}
					if (data.length < cached.length) cached.length = data.length;
					cached.nodes = nodes
				}
			}
			else if (data != null && dataType === OBJECT) {
				if (!data.attrs) data.attrs = {};
				if (!cached.attrs) cached.attrs = {};

				var dataAttrKeys = Object.keys(data.attrs)
				var hasKeys = dataAttrKeys.length > ("key" in data.attrs ? 1 : 0)
				//if an element is different enough from the one in cache, recreate it
				if (data.tag != cached.tag || dataAttrKeys.join() != Object.keys(cached.attrs).join() || data.attrs.id != cached.attrs.id) {
					if (cached.nodes.length) clear(cached.nodes);
					if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload()
				}
				if (type.call(data.tag) != STRING) return;

				var node, isNew = cached.nodes.length === 0;
				if (data.attrs.xmlns) namespace = data.attrs.xmlns;
				else if (data.tag === "svg") namespace = "http://www.w3.org/2000/svg";
				else if (data.tag === "math") namespace = "http://www.w3.org/1998/Math/MathML";
				if (isNew) {
					if (data.attrs.is) node = namespace === undefined ? $document.createElement(data.tag, data.attrs.is) : $document.createElementNS(namespace, data.tag, data.attrs.is);
					else node = namespace === undefined ? $document.createElement(data.tag) : $document.createElementNS(namespace, data.tag);
					cached = {
						tag: data.tag,
						//set attributes first, then create children
						attrs: hasKeys ? setAttributes(node, data.tag, data.attrs, {}, namespace) : data.attrs,
						children: data.children != null && data.children.length > 0 ?
							build(node, data.tag, undefined, undefined, data.children, cached.children, true, 0, data.attrs.contenteditable ? node : editable, namespace, configs) :
							data.children,
						nodes: [node]
					};
					if (cached.children && !cached.children.nodes) cached.children.nodes = [];
					//edge case: setting value on <select> doesn't work before children exist, so set it again after children have been created
					if (data.tag === "select" && data.attrs.value) setAttributes(node, data.tag, {value: data.attrs.value}, {}, namespace);
					parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				else {
					node = cached.nodes[0];
					if (hasKeys) setAttributes(node, data.tag, data.attrs, cached.attrs, namespace);
					cached.children = build(node, data.tag, undefined, undefined, data.children, cached.children, false, 0, data.attrs.contenteditable ? node : editable, namespace, configs);
					cached.nodes.intact = true;
					if (shouldReattach === true && node != null) parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				//schedule configs to be called. They are called after `build` finishes running
				if (typeof data.attrs["config"] === FUNCTION) {
					var context = cached.configContext = cached.configContext || {};

					// bind
					var callback = function(data, args) {
						return function() {
							return data.attrs["config"].apply(data, args)
						}
					};
					configs.push(callback(data, [node, !isNew, context, cached]))
				}
			}
			else if (typeof dataType != FUNCTION) {
				//handle text nodes
				var nodes;
				if (cached.nodes.length === 0) {
					if (data.$trusted) {
						nodes = injectHTML(parentElement, index, data)
					}
					else {
						nodes = [$document.createTextNode(data)];
						if (!parentElement.nodeName.match(voidElements)) parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null)
					}
					cached = "string number boolean".indexOf(typeof data) > -1 ? new data.constructor(data) : data;
					cached.nodes = nodes
				}
				else if (cached.valueOf() !== data.valueOf() || shouldReattach === true) {
					nodes = cached.nodes;
					if (!editable || editable !== $document.activeElement) {
						if (data.$trusted) {
							clear(nodes, cached);
							nodes = injectHTML(parentElement, index, data)
						}
						else {
							//corner case: replacing the nodeValue of a text node that is a child of a textarea/contenteditable doesn't work
							//we need to update the value property of the parent textarea or the innerHTML of the contenteditable element instead
							if (parentTag === "textarea") parentElement.value = data;
							else if (editable) editable.innerHTML = data;
							else {
								if (nodes[0].nodeType === 1 || nodes.length > 1) { //was a trusted string
									clear(cached.nodes, cached);
									nodes = [$document.createTextNode(data)]
								}
								parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null);
								nodes[0].nodeValue = data
							}
						}
					}
					cached = new data.constructor(data);
					cached.nodes = nodes
				}
				else cached.nodes.intact = true
			}

			return cached
		}
		function sortChanges(a, b) {return a.action - b.action || a.index - b.index}
		function setAttributes(node, tag, dataAttrs, cachedAttrs, namespace) {
			for (var attrName in dataAttrs) {
				var dataAttr = dataAttrs[attrName];
				var cachedAttr = cachedAttrs[attrName];
				if (!(attrName in cachedAttrs) || (cachedAttr !== dataAttr)) {
					cachedAttrs[attrName] = dataAttr;
					try {
						//`config` isn't a real attributes, so ignore it
						if (attrName === "config" || attrName == "key") continue;
						//hook event handlers to the auto-redrawing system
						else if (typeof dataAttr === FUNCTION && attrName.indexOf("on") === 0) {
							node[attrName] = autoredraw(dataAttr, node)
						}
						//handle `style: {...}`
						else if (attrName === "style" && dataAttr != null && type.call(dataAttr) === OBJECT) {
							for (var rule in dataAttr) {
								if (cachedAttr == null || cachedAttr[rule] !== dataAttr[rule]) node.style[rule] = dataAttr[rule]
							}
							for (var rule in cachedAttr) {
								if (!(rule in dataAttr)) node.style[rule] = ""
							}
						}
						//handle SVG
						else if (namespace != null) {
							if (attrName === "href") node.setAttributeNS("http://www.w3.org/1999/xlink", "href", dataAttr);
							else if (attrName === "className") node.setAttribute("class", dataAttr);
							else node.setAttribute(attrName, dataAttr)
						}
						//handle cases that are properties (but ignore cases where we should use setAttribute instead)
						//- list and form are typically used as strings, but are DOM element references in js
						//- when using CSS selectors (e.g. `m("[style='']")`), style is used as a string, but it's an object in js
						else if (attrName in node && !(attrName === "list" || attrName === "style" || attrName === "form" || attrName === "type")) {
							//#348 don't set the value if not needed otherwise cursor placement breaks in Chrome
							if (tag !== "input" || node[attrName] !== dataAttr) node[attrName] = dataAttr
						}
						else node.setAttribute(attrName, dataAttr)
					}
					catch (e) {
						//swallow IE's invalid argument errors to mimic HTML's fallback-to-doing-nothing-on-invalid-attributes behavior
						if (e.message.indexOf("Invalid argument") < 0) throw e
					}
				}
				//#348 dataAttr may not be a string, so use loose comparison (double equal) instead of strict (triple equal)
				else if (attrName === "value" && tag === "input" && node.value != dataAttr) {
					node.value = dataAttr
				}
			}
			return cachedAttrs
		}
		function clear(nodes, cached) {
			for (var i = nodes.length - 1; i > -1; i--) {
				if (nodes[i] && nodes[i].parentNode) {
					try {nodes[i].parentNode.removeChild(nodes[i])}
					catch (e) {} //ignore if this fails due to order of events (see http://stackoverflow.com/questions/21926083/failed-to-execute-removechild-on-node)
					cached = [].concat(cached);
					if (cached[i]) unload(cached[i])
				}
			}
			if (nodes.length != 0) nodes.length = 0
		}
		function unload(cached) {
			if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload();
			if (cached.children) {
				if (type.call(cached.children) === ARRAY) {
					for (var i = 0, child; child = cached.children[i]; i++) unload(child)
				}
				else if (cached.children.tag) unload(cached.children)
			}
		}
		function injectHTML(parentElement, index, data) {
			var nextSibling = parentElement.childNodes[index];
			if (nextSibling) {
				var isElement = nextSibling.nodeType != 1;
				var placeholder = $document.createElement("span");
				if (isElement) {
					parentElement.insertBefore(placeholder, nextSibling || null);
					placeholder.insertAdjacentHTML("beforebegin", data);
					parentElement.removeChild(placeholder)
				}
				else nextSibling.insertAdjacentHTML("beforebegin", data)
			}
			else parentElement.insertAdjacentHTML("beforeend", data);
			var nodes = [];
			while (parentElement.childNodes[index] !== nextSibling) {
				nodes.push(parentElement.childNodes[index]);
				index++
			}
			return nodes
		}
		function autoredraw(callback, object) {
			return function(e) {
				e = e || event;
				m.redraw.strategy("diff");
				m.startComputation();
				try {return callback.call(object, e)}
				finally {
					endFirstComputation()
				}
			}
		}

		var html;
		var documentNode = {
			appendChild: function(node) {
				if (html === undefined) html = $document.createElement("html");
				if ($document.documentElement && $document.documentElement !== node) {
					$document.replaceChild(node, $document.documentElement)
				}
				else $document.appendChild(node);
				this.childNodes = $document.childNodes
			},
			insertBefore: function(node) {
				this.appendChild(node)
			},
			childNodes: []
		};
		var nodeCache = [], cellCache = {};
		m.render = function(root, cell, forceRecreation) {
			var configs = [];
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var id = getCellCacheKey(root);
			var isDocumentRoot = root === $document;
			var node = isDocumentRoot || root === $document.documentElement ? documentNode : root;
			if (isDocumentRoot && cell.tag != "html") cell = {tag: "html", attrs: {}, children: cell};
			if (cellCache[id] === undefined) clear(node.childNodes);
			if (forceRecreation === true) reset(root);
			cellCache[id] = build(node, null, undefined, undefined, cell, cellCache[id], false, 0, null, undefined, configs);
			for (var i = 0, len = configs.length; i < len; i++) configs[i]()
		};
		function getCellCacheKey(element) {
			var index = nodeCache.indexOf(element);
			return index < 0 ? nodeCache.push(element) - 1 : index
		}

		m.trust = function(value) {
			value = new String(value);
			value.$trusted = true;
			return value
		};

		function gettersetter(store) {
			var prop = function() {
				if (arguments.length) store = arguments[0];
				return store
			};

			prop.toJSON = function() {
				return store
			};

			return prop
		}

		m.prop = function (store) {
			//note: using non-strict equality check here because we're checking if store is null OR undefined
			if (((store != null && type.call(store) === OBJECT) || typeof store === FUNCTION) && typeof store.then === FUNCTION) {
				return propify(store)
			}

			return gettersetter(store)
		};

		var roots = [], modules = [], controllers = [], lastRedrawId = null, lastRedrawCallTime = 0, computePostRedrawHook = null, prevented = false, topModule;
		var FRAME_BUDGET = 16; //60 frames per second = 1 call per 16 ms
		m.module = function(root, module) {
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var index = roots.indexOf(root);
			if (index < 0) index = roots.length;
			var isPrevented = false;
			if (controllers[index] && typeof controllers[index].onunload === FUNCTION) {
				var event = {
					preventDefault: function() {isPrevented = true}
				};
				controllers[index].onunload(event)
			}
			if (!isPrevented) {
				m.redraw.strategy("all");
				m.startComputation();
				roots[index] = root;
				var currentModule = topModule = module = module || {};
				var controller = new (module.controller || function() {});
				//controllers may call m.module recursively (via m.route redirects, for example)
				//this conditional ensures only the last recursive m.module call is applied
				if (currentModule === topModule) {
					controllers[index] = controller;
					modules[index] = module
				}
				endFirstComputation();
				return controllers[index]
			}
		};
		m.redraw = function(force) {
			//lastRedrawId is a positive number if a second redraw is requested before the next animation frame
			//lastRedrawID is null if it's the first redraw and not an event handler
			if (lastRedrawId && force !== true) {
				//when setTimeout: only reschedule redraw if time between now and previous redraw is bigger than a frame, otherwise keep currently scheduled timeout
				//when rAF: always reschedule redraw
				if (new Date - lastRedrawCallTime > FRAME_BUDGET || $requestAnimationFrame === window.requestAnimationFrame) {
					if (lastRedrawId > 0) $cancelAnimationFrame(lastRedrawId);
					lastRedrawId = $requestAnimationFrame(redraw, FRAME_BUDGET)
				}
			}
			else {
				redraw();
				lastRedrawId = $requestAnimationFrame(function() {lastRedrawId = null}, FRAME_BUDGET)
			}
		};
		m.redraw.strategy = m.prop();
		var blank = function() {return ""}
		function redraw() {
			var forceRedraw = m.redraw.strategy() === "all";
			for (var i = 0, root; root = roots[i]; i++) {
				if (controllers[i]) {
					m.render(root, modules[i].view ? modules[i].view(controllers[i]) : blank(), forceRedraw)
				}
			}
			//after rendering within a routed context, we need to scroll back to the top, and fetch the document title for history.pushState
			if (computePostRedrawHook) {
				computePostRedrawHook();
				computePostRedrawHook = null
			}
			lastRedrawId = null;
			lastRedrawCallTime = new Date;
			m.redraw.strategy("diff")
		}

		var pendingRequests = 0;
		m.startComputation = function() {pendingRequests++};
		m.endComputation = function() {
			pendingRequests = Math.max(pendingRequests - 1, 0);
			if (pendingRequests === 0) m.redraw()
		};
		var endFirstComputation = function() {
			if (m.redraw.strategy() == "none") {
				pendingRequests--
				m.redraw.strategy("diff")
			}
			else m.endComputation();
		}

		m.withAttr = function(prop, withAttrCallback) {
			return function(e) {
				e = e || event;
				var currentTarget = e.currentTarget || this;
				withAttrCallback(prop in currentTarget ? currentTarget[prop] : currentTarget.getAttribute(prop))
			}
		};

		//routing
		var modes = {pathname: "", hash: "#", search: "?"};
		var redirect = function() {}, routeParams, currentRoute;
		m.route = function() {
			//m.route()
			if (arguments.length === 0) return currentRoute;
			//m.route(el, defaultRoute, routes)
			else if (arguments.length === 3 && type.call(arguments[1]) === STRING) {
				var root = arguments[0], defaultRoute = arguments[1], router = arguments[2];
				redirect = function(source) {
					var path = currentRoute = normalizeRoute(source);
					if (!routeByValue(root, router, path)) {
						m.route(defaultRoute, true)
					}
				};
				var listener = m.route.mode === "hash" ? "onhashchange" : "onpopstate";
				window[listener] = function() {
					var path = $location[m.route.mode]
					if (m.route.mode === "pathname") path += $location.search
					if (currentRoute != normalizeRoute(path)) {
						redirect(path)
					}
				};
				computePostRedrawHook = setScroll;
				window[listener]()
			}
			//config: m.route
			else if (arguments[0].addEventListener) {
				var element = arguments[0];
				var isInitialized = arguments[1];
				var context = arguments[2];
				element.href = (m.route.mode !== 'pathname' ? $location.pathname : '') + modes[m.route.mode] + this.attrs.href;
				element.removeEventListener("click", routeUnobtrusive);
				element.addEventListener("click", routeUnobtrusive)
			}
			//m.route(route, params)
			else if (type.call(arguments[0]) === STRING) {
				var oldRoute = currentRoute;
				currentRoute = arguments[0];
				var args = arguments[1] || {}
				var queryIndex = currentRoute.indexOf("?")
				var params = queryIndex > -1 ? parseQueryString(currentRoute.slice(queryIndex + 1)) : {}
				for (var i in args) params[i] = args[i]
				var querystring = buildQueryString(params)
				var currentPath = queryIndex > -1 ? currentRoute.slice(0, queryIndex) : currentRoute
				if (querystring) currentRoute = currentPath + (currentPath.indexOf("?") === -1 ? "?" : "&") + querystring;

				var shouldReplaceHistoryEntry = (arguments.length === 3 ? arguments[2] : arguments[1]) === true || oldRoute === arguments[0];

				if (window.history.pushState) {
					computePostRedrawHook = function() {
						window.history[shouldReplaceHistoryEntry ? "replaceState" : "pushState"](null, $document.title, modes[m.route.mode] + currentRoute);
						setScroll()
					};
					redirect(modes[m.route.mode] + currentRoute)
				}
				else $location[m.route.mode] = currentRoute
			}
		};
		m.route.param = function(key) {
			if (!routeParams) throw new Error("You must call m.route(element, defaultRoute, routes) before calling m.route.param()")
			return routeParams[key]
		};
		m.route.mode = "search";
		function normalizeRoute(route) {
			return route.slice(modes[m.route.mode].length)
		}
		function routeByValue(root, router, path) {
			routeParams = {};

			var queryStart = path.indexOf("?");
			if (queryStart !== -1) {
				routeParams = parseQueryString(path.substr(queryStart + 1, path.length));
				path = path.substr(0, queryStart)
			}

			for (var route in router) {
				if (route === path) {
					m.module(root, router[route]);
					return true
				}

				var matcher = new RegExp("^" + route.replace(/:[^\/]+?\.{3}/g, "(.*?)").replace(/:[^\/]+/g, "([^\\/]+)") + "\/?$");

				if (matcher.test(path)) {
					path.replace(matcher, function() {
						var keys = route.match(/:[^\/]+/g) || [];
						var values = [].slice.call(arguments, 1, -2);
						for (var i = 0, len = keys.length; i < len; i++) routeParams[keys[i].replace(/:|\./g, "")] = decodeURIComponent(values[i])
						m.module(root, router[route])
					});
					return true
				}
			}
		}
		function routeUnobtrusive(e) {
			e = e || event;
			if (e.ctrlKey || e.metaKey || e.which === 2) return;
			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;
			var currentTarget = e.currentTarget || this;
			var args = m.route.mode === "pathname" && currentTarget.search ? parseQueryString(currentTarget.search.slice(1)) : {};
			m.route(currentTarget[m.route.mode].slice(modes[m.route.mode].length), args)
		}
		function setScroll() {
			if (m.route.mode != "hash" && $location.hash) $location.hash = $location.hash;
			else window.scrollTo(0, 0)
		}
		function buildQueryString(object, prefix) {
			var str = [];
			for(var prop in object) {
				var key = prefix ? prefix + "[" + prop + "]" : prop, value = object[prop];
				var valueType = type.call(value)
				var pair = value != null && (valueType === OBJECT) ?
					buildQueryString(value, key) :
					valueType === ARRAY ?
						value.map(function(item) {return encodeURIComponent(key + "[]") + "=" + encodeURIComponent(item)}).join("&") :
						encodeURIComponent(key) + "=" + encodeURIComponent(value)
				str.push(pair)
			}
			return str.join("&")
		}
		
		function parseQueryString(str) {
			var pairs = str.split("&"), params = {};
			for (var i = 0, len = pairs.length; i < len; i++) {
				var pair = pairs[i].split("=");
				params[decodeURIComponent(pair[0])] = pair[1] ? decodeURIComponent(pair[1]) : ""
			}
			return params
		}
		function reset(root) {
			var cacheKey = getCellCacheKey(root);
			clear(root.childNodes, cellCache[cacheKey]);
			cellCache[cacheKey] = undefined
		}

		m.deferred = function () {
			var deferred = new Deferred();
			deferred.promise = propify(deferred.promise);
			return deferred
		};
		function propify(promise) {
			var prop = m.prop();
			promise.then(prop);
			prop.then = function(resolve, reject) {
				return propify(promise.then(resolve, reject))
			};
			return prop
		}
		//Promiz.mithril.js | Zolmeister | MIT
		//a modified version of Promiz.js, which does not conform to Promises/A+ for two reasons:
		//1) `then` callbacks are called synchronously (because setTimeout is too slow, and the setImmediate polyfill is too big
		//2) throwing subclasses of Error cause the error to be bubbled up instead of triggering rejection (because the spec does not account for the important use case of default browser error handling, i.e. message w/ line number)
		function Deferred(successCallback, failureCallback) {
			var RESOLVING = 1, REJECTING = 2, RESOLVED = 3, REJECTED = 4;
			var self = this, state = 0, promiseValue = 0, next = [];

			self["promise"] = {};

			self["resolve"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = RESOLVING;

					fire()
				}
				return this
			};

			self["reject"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = REJECTING;

					fire()
				}
				return this
			};

			self.promise["then"] = function(successCallback, failureCallback) {
				var deferred = new Deferred(successCallback, failureCallback);
				if (state === RESOLVED) {
					deferred.resolve(promiseValue)
				}
				else if (state === REJECTED) {
					deferred.reject(promiseValue)
				}
				else {
					next.push(deferred)
				}
				return deferred.promise
			};

			function finish(type) {
				state = type || REJECTED;
				next.map(function(deferred) {
					state === RESOLVED && deferred.resolve(promiseValue) || deferred.reject(promiseValue)
				})
			}

			function thennable(then, successCallback, failureCallback, notThennableCallback) {
				if (((promiseValue != null && type.call(promiseValue) === OBJECT) || typeof promiseValue === FUNCTION) && typeof then === FUNCTION) {
					try {
						// count protects against abuse calls from spec checker
						var count = 0;
						then.call(promiseValue, function(value) {
							if (count++) return;
							promiseValue = value;
							successCallback()
						}, function (value) {
							if (count++) return;
							promiseValue = value;
							failureCallback()
						})
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						failureCallback()
					}
				} else {
					notThennableCallback()
				}
			}

			function fire() {
				// check if it's a thenable
				var then;
				try {
					then = promiseValue && promiseValue.then
				}
				catch (e) {
					m.deferred.onerror(e);
					promiseValue = e;
					state = REJECTING;
					return fire()
				}
				thennable(then, function() {
					state = RESOLVING;
					fire()
				}, function() {
					state = REJECTING;
					fire()
				}, function() {
					try {
						if (state === RESOLVING && typeof successCallback === FUNCTION) {
							promiseValue = successCallback(promiseValue)
						}
						else if (state === REJECTING && typeof failureCallback === "function") {
							promiseValue = failureCallback(promiseValue);
							state = RESOLVING
						}
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						return finish()
					}

					if (promiseValue === self) {
						promiseValue = TypeError();
						finish()
					}
					else {
						thennable(then, function () {
							finish(RESOLVED)
						}, finish, function () {
							finish(state === RESOLVING && RESOLVED)
						})
					}
				})
			}
		}
		m.deferred.onerror = function(e) {
			if (type.call(e) === "[object Error]" && !e.constructor.toString().match(/ Error/)) throw e
		};

		m.sync = function(args) {
			var method = "resolve";
			function synchronizer(pos, resolved) {
				return function(value) {
					results[pos] = value;
					if (!resolved) method = "reject";
					if (--outstanding === 0) {
						deferred.promise(results);
						deferred[method](results)
					}
					return value
				}
			}

			var deferred = m.deferred();
			var outstanding = args.length;
			var results = new Array(outstanding);
			if (args.length > 0) {
				for (var i = 0; i < args.length; i++) {
					args[i].then(synchronizer(i, true), synchronizer(i, false))
				}
			}
			else deferred.resolve([]);

			return deferred.promise
		};
		function identity(value) {return value}

		function ajax(options) {
			if (options.dataType && options.dataType.toLowerCase() === "jsonp") {
				var callbackKey = "mithril_callback_" + new Date().getTime() + "_" + (Math.round(Math.random() * 1e16)).toString(36);
				var script = $document.createElement("script");

				window[callbackKey] = function(resp) {
					script.parentNode.removeChild(script);
					options.onload({
						type: "load",
						target: {
							responseText: resp
						}
					});
					window[callbackKey] = undefined
				};

				script.onerror = function(e) {
					script.parentNode.removeChild(script);

					options.onerror({
						type: "error",
						target: {
							status: 500,
							responseText: JSON.stringify({error: "Error making jsonp request"})
						}
					});
					window[callbackKey] = undefined;

					return false
				};

				script.onload = function(e) {
					return false
				};

				script.src = options.url
					+ (options.url.indexOf("?") > 0 ? "&" : "?")
					+ (options.callbackKey ? options.callbackKey : "callback")
					+ "=" + callbackKey
					+ "&" + buildQueryString(options.data || {});
				$document.body.appendChild(script)
			}
			else {
				var xhr = new window.XMLHttpRequest;
				xhr.open(options.method, options.url, true, options.user, options.password);
				xhr.onreadystatechange = function() {
					if (xhr.readyState === 4) {
						if (xhr.status >= 200 && xhr.status < 300) options.onload({type: "load", target: xhr});
						else options.onerror({type: "error", target: xhr})
					}
				};
				if (options.serialize === JSON.stringify && options.data && options.method !== "GET") {
					xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8")
				}
				if (options.deserialize === JSON.parse) {
					xhr.setRequestHeader("Accept", "application/json, text/*");
				}
				if (typeof options.config === FUNCTION) {
					var maybeXhr = options.config(xhr, options);
					if (maybeXhr != null) xhr = maybeXhr
				}

				var data = options.method === "GET" || !options.data ? "" : options.data
				if (data && (type.call(data) != STRING && data.constructor != window.FormData)) {
					throw "Request data should be either be a string or FormData. Check the `serialize` option in `m.request`";
				}
				xhr.send(data);
				return xhr
			}
		}
		function bindData(xhrOptions, data, serialize) {
			if (xhrOptions.method === "GET" && xhrOptions.dataType != "jsonp") {
				var prefix = xhrOptions.url.indexOf("?") < 0 ? "?" : "&";
				var querystring = buildQueryString(data);
				xhrOptions.url = xhrOptions.url + (querystring ? prefix + querystring : "")
			}
			else xhrOptions.data = serialize(data);
			return xhrOptions
		}
		function parameterizeUrl(url, data) {
			var tokens = url.match(/:[a-z]\w+/gi);
			if (tokens && data) {
				for (var i = 0; i < tokens.length; i++) {
					var key = tokens[i].slice(1);
					url = url.replace(tokens[i], data[key]);
					delete data[key]
				}
			}
			return url
		}

		m.request = function(xhrOptions) {
			if (xhrOptions.background !== true) m.startComputation();
			var deferred = m.deferred();
			var isJSONP = xhrOptions.dataType && xhrOptions.dataType.toLowerCase() === "jsonp";
			var serialize = xhrOptions.serialize = isJSONP ? identity : xhrOptions.serialize || JSON.stringify;
			var deserialize = xhrOptions.deserialize = isJSONP ? identity : xhrOptions.deserialize || JSON.parse;
			var extract = xhrOptions.extract || function(xhr) {
				return xhr.responseText.length === 0 && deserialize === JSON.parse ? null : xhr.responseText
			};
			xhrOptions.url = parameterizeUrl(xhrOptions.url, xhrOptions.data);
			xhrOptions = bindData(xhrOptions, xhrOptions.data, serialize);
			xhrOptions.onload = xhrOptions.onerror = function(e) {
				try {
					e = e || event;
					var unwrap = (e.type === "load" ? xhrOptions.unwrapSuccess : xhrOptions.unwrapError) || identity;
					var response = unwrap(deserialize(extract(e.target, xhrOptions)));
					if (e.type === "load") {
						if (type.call(response) === ARRAY && xhrOptions.type) {
							for (var i = 0; i < response.length; i++) response[i] = new xhrOptions.type(response[i])
						}
						else if (xhrOptions.type) response = new xhrOptions.type(response)
					}
					deferred[e.type === "load" ? "resolve" : "reject"](response)
				}
				catch (e) {
					m.deferred.onerror(e);
					deferred.reject(e)
				}
				if (xhrOptions.background !== true) m.endComputation()
			};
			ajax(xhrOptions);
			deferred.promise(xhrOptions.initialValue);
			return deferred.promise
		};

		//testing API
		m.deps = function(mock) {
			initialize(window = mock || window);
			return window;
		};
		//for internal testing only, do not use `m.deps.factory`
		m.deps.factory = app;

		return m
	})(typeof window != "undefined" ? window : {});

	if (typeof module != "undefined" && module !== null && module.exports) module.exports = m;
	else if (true) !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {return m}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(51)(module)))

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(19);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/main.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/main.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, "@font-face{font-family:\"Lato-Light\";src:url('/static/fonts/Lato-Light.eot');src:local('/static/fonts/Lato-Light');src:url('/static/fonts/Lato-Light.svg#Lato-Light') format('svg');src:url('/static/fonts/Lato-Light.woff') format('woff');}/*! normalize.css v2.1.0 | MIT License | git.io/normalize */article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block}audio,canvas,video{display:inline-block}audio:not([controls]){display:none;height:0}[hidden]{display:none}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;min-height:100%}body{margin:0}a:focus{outline:thin dotted}a:active,a:hover{outline:0}h1{font-size:2em;margin:0.67em 0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:bold}dfn{font-style:italic}hr{-moz-box-sizing:content-box;box-sizing:content-box;height:0}mark{background:#ff0;color:#000}code,kbd,pre,samp{font-family:monospace,serif;font-size:1em}pre{white-space:pre-wrap}q{quotes:\"\\201C\" \"\\201D\" \"\\2018\" \"\\2019\"}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-0.5em}sub{bottom:-0.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:0}fieldset{margin:0 2px;padding:0.35em 0.625em 0.75em}legend{border:0;padding:0}button,input,select,textarea{font-family:inherit;font-size:100%;font-weight:inherit;margin:0;padding:0}button{background:transparent}button,input{line-height:normal}button,select{text-transform:none}select{-webkit-appearance:none}button,html input[type=\"button\"],input[type=\"reset\"],input[type=\"submit\"]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}input[type=\"checkbox\"],input[type=\"radio\"]{box-sizing:border-box;padding:0}input[type=\"search\"]{-webkit-appearance:textfield;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;box-sizing:content-box}input[type=\"search\"]::-webkit-search-cancel-button,input[type=\"search\"]::-webkit-search-decoration{-webkit-appearance:none}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}textarea{overflow:auto;vertical-align:top;resize:none}table{border-collapse:collapse;border-spacing:0}html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,font,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td{margin:0;padding:0;border:0;outline:0;font-weight:inherit;font-style:inherit;font-size:100%;font-family:inherit;vertical-align:baseline}:focus{outline:0}body{line-height:1;color:black;background:white}ol,ul,li{list-style:none}table{border-collapse:separate;border-spacing:0}caption,th,td{text-align:left;font-weight:normal}blockquote:before,blockquote:after,q:before,q:after{content:\"\"}blockquote,q{quotes:\"\" \"\"}a,a:hover,a:visited{text-decoration:none}button,input,fieldset,textarea,select{font-size:inherit;color:inherit;border:none;border-radius:0}img{width:100%;height:auto}.clearfix:before,.clearfix:after{content:\"\";display:table}.clearfix:after{clear:both}.clearfix{*zoom:1}*{-moz-box-sizing:border-box;box-sizing:border-box}.fluid-span-1{width:6.5%}.fluid-span-2{width:15%}.fluid-span-3{width:23.5%}.fluid-span-4{width:32%}.fluid-span-5{width:40.5%}.fluid-span-6{width:49%}.fluid-span-7{width:57.5%}.fluid-span-8{width:66%}.fluid-span-9{width:74.5%}.fluid-span-10{width:83%}.fluid-span-11{width:91.5%}.fluid-span-12{width:100%}.fluid-offset-1{margin-left:8.5%}.fluid-offset-2{margin-left:17%}.fluid-offset-3{margin-left:25.5%}.fluid-offset-4{margin-left:34%}.fluid-offset-5{margin-left:42.5%}.fluid-offset-6{margin-left:51%}.fluid-offset-7{margin-left:59.5%}.fluid-offset-8{margin-left:68%}.fluid-offset-9{margin-left:76.5%}.fluid-offset-10{margin-left:85%}.fluid-offset-11{margin-left:93.5%}body{background-color:white;font:300 0.875em/1.38 'Open Sans';color:#181818}.aboutsub{float:left;clear:both;width:95.30779%;min-height:24px;margin-top:10px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f0f0f0}.aboutusheading{float:left;width:100%;min-height:40px;font-size:1em;font-weight:400;line-height:1.38;text-align:center;color:#f0f0f0}.aboutuswrapper{float:left;width:25.58333%;min-height:180px;margin:50px 0 0 10%}.advertisesub,.signupsub{float:left;clear:both;width:100%;min-height:24px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f0f0f0}.blogsub{float:left;clear:both;width:100%;min-height:24px;margin-top:10px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f0f0f0}.contactussub{float:left;clear:both;width:95.30779%;min-height:24px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f0f0f0}.contactuswrapper{width:70%;height:700px;margin:100px auto 0;background-color:rgba(222,222,222,0.9)}.exploresub{float:left;clear:both;width:100%;min-height:21px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f0f0f0}.facebookbutton{float:left;clear:both;width:100%;height:40px;min-height:40px;margin-top:10px;background-image:url('/static/images/facebook.png');background-repeat:no-repeat;background-size:contain;background-position:center center}.footernav{float:left;clear:both;width:100%;min-height:400px;background-color:#282828}.header{display:flex;align-items:center;float:left;width:100%;height:85px}.input_enteremail{display:block;float:left;clear:both;width:56%;height:35px;margin:4px 0 0 19.98698%;padding:0 10px;background-color:#f2f2f2;font-style:italic;font-size:1.143em;font-weight:300;line-height:1.38;color:#676767}.input_entername,.input_entersubject{display:block;float:left;clear:both;width:56%;height:35px;margin:4px 0 0 19.99442%;padding:0 10px;background-color:#f2f2f2;font-style:italic;font-size:1.143em;font-weight:300;line-height:1.38;color:#676767}.instagram{float:left;clear:both;width:100%;height:40px;min-height:40px;margin-top:15px;background-image:url('/static/images/Instagram.png');background-repeat:no-repeat;background-size:contain;background-position:center center}.logo{float:left;flex:1 0 230px;vertical-align:middle;height:15px;min-height:15px;margin:auto 0 auto 10%;background-image:url('/static/images/stylestation_light.png');background-repeat:no-repeat;background-size:contain}.pinterest{float:left;clear:both;width:100%;height:40px;min-height:40px;margin-top:15px;background-image:url('/static/images/pinterest.png');background-repeat:no-repeat;background-size:contain;background-position:center center}.quicklinksheading{float:left;width:99.99491%;min-height:40px;font-size:1em;font-weight:400;line-height:1.38;text-align:center;color:#f0f0f0}.quicklinkswrapper{float:left;width:25.59896%;min-height:180px;margin:50px 0 0 1.61589%}.rightreserved{float:left;clear:both;width:100%;height:46px}.section1{float:left;clear:both;width:100%;height:900px;background-image:url('/static/images/ankara_background_dark2.jpg');background-size:cover;background-position:center center;box-shadow:0 2px 5px rgba(0,0,0,0.5)}.section2{float:left;clear:both;width:100%;height:100px;box-shadow:0 2px 5px rgba(0,0,0,0.5)}.socialheadding{float:left;width:100%;min-height:35px;font-size:1em;line-height:1.38;text-align:center;color:#f0f0f0}.socialmediawrapper{float:right;width:25.59896%;min-height:340px;margin:50px 10.00391% 0 0}.termsofservicesub{float:left;clear:both;width:93.66625%;min-height:24px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#f7f7f7}.text_contactemailaddress{float:left;width:100%;min-height:37px;margin-top:35px;font-size:1.143em;line-height:1.38;text-align:center;color:#181818}.text_copyright{float:left;width:100%;min-height:30px;margin-top:10px;font-size:1em;font-weight:300;line-height:1.38;text-align:center;color:#181818}.text_message{float:left;clear:both;margin:25px 0 0 20.0093%;font-size:1.143em;line-height:1.38;color:#181818}.text_name{float:left;margin:60px 0 0 19.99442%;font-size:1.143em;color:#181818}.text_email,.text_subject{float:left;clear:both;margin:25px 0 0 19.99442%;font-size:1.143em;line-height:1.38;color:#181818}.textarea_message{display:block;float:left;clear:both;width:66.79162%;height:212px;margin:4px 0 0 19.99442%;padding:5px;background-color:#f2f2f2;font:400 1.143em/1.38 'Open Sans Condensed';color:black;resize:none}.twitter{float:left;clear:both;width:100%;height:40px;min-height:40px;margin-top:15px;background-image:url('/static/images/twitter.png');background-repeat:no-repeat;background-size:contain;background-position:center center}.wrapper{float:left;width:100%;background-color:#fafafa}.youtube{float:left;clear:both;width:100%;height:40px;margin-top:15px;background-image:url('/static/images/Youtube.png');background-repeat:no-repeat;background-size:contain;background-position:center center}._button{display:block;float:left;clear:both;width:25.86589%;height:39px;margin:32px 0 0 37.06287%;border-radius:3px;background-color:#b7293b;font-size:1.286em;line-height:1.38;text-align:center;color:#eee6e7}@media (max-width: 1024px){.contactuswrapper .input_enteremail{margin-left:19.9891%}.contactuswrapper .text_message{margin-left:20.00654%}.contactuswrapper .input_entername,.contactuswrapper .input_entersubject,.contactuswrapper .text_email,.contactuswrapper .text_name,.contactuswrapper .text_subject,.contactuswrapper .textarea_message{margin-left:19.99564%}.footernav .aboutuswrapper{margin-left:9.99756%}.footernav .quicklinkswrapper{margin-left:1.61438%}.footernav .socialmediawrapper{margin-right:10.00214%}.header .logo{margin-left:3.91541%}.socialmediawrapper .socialheadding{margin-top:3px}.contactuswrapper ._button{width:25.86659%;margin-left:37.05722%}}@media (max-width: 950px){body{font-size:0.875em}.contactuswrapper .input_enteremail{margin-left:19.98825%}.contactuswrapper .text_message{margin-left:20.00235%}.contactuswrapper .input_entername,.contactuswrapper .input_entersubject,.contactuswrapper .text_email,.contactuswrapper .text_name,.contactuswrapper .text_subject,.contactuswrapper .textarea_message{margin-left:19.9953%}.footernav .aboutuswrapper{margin-left:10%}.footernav .quicklinkswrapper{margin-left:1.61349%}.footernav .socialmediawrapper{margin-right:10.00329%}.header .logo{margin-left:3.91612%}.contactuswrapper ._button{width:25.86623%;margin-left:37.05592%}}@media (max-width: 750px){.contactuswrapper .input_enteremail{margin-left:19.98438%}.contactuswrapper .text_message{margin-left:20.01042%}.contactuswrapper .input_entername,.contactuswrapper .input_entersubject,.contactuswrapper .text_email,.contactuswrapper .text_name,.contactuswrapper .text_subject,.contactuswrapper .textarea_message{margin-left:19.99479%}.footernav .quicklinkswrapper{margin-left:1.6125%}.footernav .socialmediawrapper{margin-right:10%}.header .logo{margin-left:3.91667%}.section1 .contactuswrapper{width:80%}.contactuswrapper ._button{width:25.86458%;margin-left:37.0625%}}@media (max-width: 500px){body{font-size:0.875em}.contactuswrapper .input_entername{width:73.77778%;margin-left:13.10526%;font-size:1.071em}.contactuswrapper .input_enteremail,.contactuswrapper .input_entersubject{width:73.78%;margin-left:13.10526%;font-size:1.071em}.contactuswrapper .text_email,.contactuswrapper .text_message,.contactuswrapper .text_name,.contactuswrapper .text_subject{margin-left:13.10526%;font-size:1.071em}.contactuswrapper .textarea_message{width:73.78%;margin-left:13.10526%}.footernav .quicklinkswrapper{margin-left:1.61563%}.footernav .socialmediawrapper{margin-right:10.00313%}.header .logo{margin-left:3.91563%}.section1 .contactuswrapper{width:95%;height:669px;margin-top:70px}.section2 .text_contactemailaddress{font-size:1.071em}.wrapper .section1{height:800px}}@media (max-width: 375px){.contactuswrapper .input_entername{width:75.55556%;height:30px;min-height:30px;margin-left:13.05702%}.contactuswrapper .input_enteremail,.contactuswrapper .input_entersubject{width:75.56%;height:30px;min-height:30px;margin-left:13.05702%}.contactuswrapper .text_message{margin-left:13.0614%}.contactuswrapper .text_email,.contactuswrapper .text_name,.contactuswrapper .text_subject{margin-left:13.05702%}.contactuswrapper .textarea_message{width:75.56%;margin-left:13.05702%}.footernav .quicklinkswrapper{margin-left:1.6125%}.footernav .socialmediawrapper{margin-right:10%}.header .logo{width:170px;min-width:170px;margin-left:4%}.section1 .contactuswrapper{height:650px}.socialmediawrapper .socialheadding{margin-top:0}.wrapper{min-width:300px}.contactuswrapper ._button{width:75.55556%;margin-left:13.07895%}}body{font-family:Lato-Light;font-weight:100;text-transform:uppercase;color:#222 !important}#content{margin:8em auto 5em 0}.ssentry__image__wrapper,.ssentry__details__wrapper{text-align:left;display:inline;clear:none;width:auto;margin-left:0;margin-right:0;*zoom:1;float:left;clear:none;text-align:inherit;width:48.5%;margin-left:0%;margin-right:3%}.ssentry__image__wrapper:first-child,.ssentry__details__wrapper:first-child{margin-left:0}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:0}.ssentry__image__wrapper:before,.ssentry__image__wrapper:after,.ssentry__details__wrapper:before,.ssentry__details__wrapper:after{content:'';display:table}.ssentry__image__wrapper:after,.ssentry__details__wrapper:after{clear:both}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:0%}@media only screen and (max-width: 960px){.ssentry__image__wrapper,.ssentry__details__wrapper{display:block;clear:both;float:none;width:100%;margin-left:auto;margin-right:auto;}.ssentry__image__wrapper:first-child,.ssentry__details__wrapper:first-child{margin-left:auto}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:auto}}.ssentry__image__wrapper{position:relative;background-color:#fefefe}.ssentry__image__wrapper:hover .ssimage__noimagetext{opacity:0.85}.ssentry__descriptioninput{border:none;outline:none;display:block;width:100%;font-size:2em;color:#666;text-align:center}.ssnewentry__wrapper{*zoom:1;width:auto;max-width:90%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;margin:auto}.ssnewentry__wrapper:before,.ssnewentry__wrapper:after{content:'';display:table}.ssnewentry__wrapper:after{clear:both}.sstags__stylepicker{text-align:center;color:#6f6f6f;background-color:#fcfcfc;padding:0.3em;font-weight:normal;font-size:1.1em}.sstags__stylepickerwrapper{background-color:#fefefe;box-shadow:0 1px 3px rgba(0,0,0,0.1);margin:1em 0 2em}.ssnewentry__submit{border:none;border-radius:7px;background-image:-o-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-moz-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-webkit-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-ms-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:linear-gradient(-180deg, #8AE160 0%, #52AE26 100%);-moz-box-shadow:0px 2px 2px 0px #2A7206;-webkit-box-shadow:0px 2px 2px 0px #2A7206;box-shadow:0px 2px 2px 0px #2A7206;display:block;width:40%;margin:1em auto;font-family:OpenSans;font-size:1.3em;color:#FFFFFF;text-transform:uppercase;padding:0.5em;text-shadow:0px 1px 0px rgba(255,255,255,0.16)}.ssnewentry__submit:hover{background-image:-o-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-moz-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-webkit-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-ms-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:linear-gradient(-180deg, #58B32C 0%, #52AE26 100%);-moz-box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82);-webkit-box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82);box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82)}.nav{*zoom:1;width:auto;max-width:30%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;margin-top:2em}.nav:before,.nav:after{content:'';display:table}.nav:after{clear:both}@media only screen and (max-width: 960px){.nav{*zoom:1;width:auto;max-width:60%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.nav:before,.nav:after{content:'';display:table}.nav:after{clear:both}}.nav .next_page,.nav .prev_page{display:block;width:50%;background-color:#d2d2d2;border-radius:50%;color:#f9f9f9;border:solid 1px #dfdfdf;width:3.5em;height:3.5em;font-size:1.75em;text-align:center;line-height:3.5em}.nav .next_page,.nav .prev_page{cursor:pointer}.nav .next_page{float:right}.nav .prev_page{float:left}.no_images_container{width:80%;margin:auto;text-align:center;min-height:300px;height:300px}.no_images_container .no_images_text{vertical-align:middle;display:inline-block;padding:2em;background-color:#fff;color:#657657;border-radius:5px;box-shadow:0 0 4px rgba(0,0,0,0.5)}.no_images_container:before{vertical-align:middle;content:\"\";height:100%;display:inline-block}", ""]);

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(21);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/detail.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/detail.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, ".ssdetail__wrapper{color:#666;background:#FDFDFD;border:1px solid #F6F6F6;overflow:auto;width:80%;margin:1em auto;padding:1em}.ssdetail__wrapper .entry_wrapper{overflow:hidden}.ssdetail__wrapper .related_entries{margin-top:2em}.ssdetail__wrapper .ssdetail__image{text-align:center}.ssdetail__wrapper .ssdetail__image>.image{width:100%;max-width:700px}@media only screen and (max-width: 960px){.ssdetail__wrapper{width:100%;}}.ssdetail__entry{*zoom:1;float:left;clear:none;text-align:inherit;width:65.66667%;margin-left:0%;margin-right:3%}.ssdetail__entry:before,.ssdetail__entry:after{content:'';display:table}.ssdetail__entry:after{clear:both}.ssdetail__entry:last-child{margin-right:0%}.ssdetail__info{*zoom:1;float:left;clear:none;text-align:inherit;width:31.33333%;margin-left:0%;margin-right:3%}.ssdetail__info:before,.ssdetail__info:after{content:'';display:table}.ssdetail__info:after{clear:both}.ssdetail__info:last-child{margin-right:0%}.ssdetail__entry,.ssdetail__info{}@media only screen and (max-width: 960px){.ssdetail__entry,.ssdetail__info{*zoom:1;float:left;clear:none;text-align:inherit;width:100%;margin-left:0%;margin-right:3%;}.ssdetail__entry:before,.ssdetail__entry:after,.ssdetail__info:before,.ssdetail__info:after{content:'';display:table}.ssdetail__entry:after,.ssdetail__info:after{clear:both}.ssdetail__entry:last-child,.ssdetail__info:last-child{margin-right:0%}}.ssdetail__info .owner{font-size:21px;color:#4A90E2;line-height:45px}.ssdetail__info .description{font-family:OpenSans;font-size:14px;color:#878787;line-height:19px;margin-bottom:0}.ssdetail__info .tags_header{font-size:24px;color:#9B9B9B;line-height:33px}.ssdetail__info .tag{border:1px solid #E0E0E0;border-radius:4px;padding-right:0.3em;display:inline-block;margin:0.5em}.ssdetail__info .tag_icon,.ssdetail__info .colour{color:#6C6C6C;padding:0.2em}.ssdetail__info .tag_icon,.share_link{font-family:ssicons}.tag_icon,.tag_text{font-size:1em;vertical-align:middle}.ssdetail__info .colour{display:inline-block;height:2em;width:2em;cursor:pointer;margin:0;padding:0;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1)}.ssdetail__info .colour:first-of-type{border-radius:5px 0 0 5px}.ssdetail__info .colour:last-child{border-radius:0 5px 5px 0}.ssdetail__wrapper .entry_wraper{overflow:auto}.ssdetail__info .tag_wrapper{margin-top:1em}.colour_wrapper{position:relative;text-align:center;background-color:#fff;padding:0;padding-bottom:1em;box-shadow:0 0 2px rgba(0,0,0,0.1);margin:1.85em auto}.colour_wrapper:hover{z-index:3}.ssdetail__info .tag_text{vertical-align:middle;color:#959595;line-height:2em}.sssharebar__wrapper{text-align:center}.share_link{font-size:1.5em;color:#888;display:inline-block;padding:0.25em 0.5em;text-decoration:none;line-height:1.5em;transition:color 0.2s ease-out}.share_text{font-size:0.8em;margin:5px;vertical-align:middle;color:#888}.twitter_share:hover{color:#4099FF}.facebook_share:hover{color:#3b5999}.pinterest_share:hover{color:#C92228}.stats ul{padding:0;overflow:auto}.stats li{border-bottom:1px solid rgba(102,102,102,0.2);color:#666;font-size:1.2em;text-align:right;line-height:1.1em;padding:0.2em}.stats li:first-child{}.stats li:last-child{border-bottom:0}.stats .stats__stat{font-size:1.2em}.stats .stats__label{font-size:0.8em;display:inline-block;padding-left:0.6em}.favourite{text-align:center;display:block;max-width:300px;*zoom:1;width:auto;max-width:70%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;border:1px solid #dadada;padding:0.3em 0;border-radius:3px;cursor:pointer;transition:color 0.1s ease-out,background-color 0.1s ease-out}.favourite:before,.favourite:after{content:'';display:table}.favourite:after{clear:both}.favourite.favourite--added{color:#fff;background:#84df84;text-shadow:0 1px 1px rgba(0,0,0,0.2);border:none}.description{border-bottom:1px solid rgba(102,102,102,0.2);padding-bottom:1em}", ""]);

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(23);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/tags.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/tags.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, ".sstags__wrapper--male,.sstags__wrapper--female{transition:all 0.1s ease-in;color:#989898}.sstags__wrapper--male .sstags__icon,.sstags__wrapper--female .sstags__icon{color:#989898}.sstags__wrapper{box-shadow:0 1px 2px rgba(0,0,0,0.2);*zoom:1;float:left;clear:none;text-align:inherit;width:28.57143%;margin-left:0%;margin-right:0%;padding:0.6em;cursor:pointer;margin:0.5em;font-size:0.85em;float:left;text-align:right;background-color:#fdfdfd}.sstags__wrapper:before,.sstags__wrapper:after{content:'';display:table}.sstags__wrapper:after{clear:both}.sstags__wrapper:hover{box-shadow:inset 0 1px 2px rgba(0,0,0,0.2)}.sstags__wrapper:hover .sstags__icon{box-shadow:none}.sstags__wrapper--male{border-bottom:2px solid #458AD7}.sstags__wrapper--female{border-bottom:2px solid #EB6161}.sstags__icon{box-shadow:0 0 3px rgba(0,0,0,0.2)}.sstags__text{float:left;display:inline-block;margin-left:0.5em;line-height:2.25em}.sstags__icon--selected,.sstags__icon{font-family:ssicons;text-align:center;width:1.5em;display:inline-block;border-radius:50%;height:1.5em;font-size:1.5em;line-height:1.5em;float:left;background-color:#fff}.sstags__icon--selected{color:#68BB0D !important}", ""]);

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(25);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, ".ssimage__noimagetext{transition:all 0.5s ease-in}.ssimage__wrapper{width:100%;position:relative;min-height:500px;background-color:#fefefe}.ssimage__wrapper:hover .ssimage__noimagetext{opacity:0.85}.ssimage__imageinput{position:absolute;top:0;left:0;position:absolute;display:block;width:100%;height:100%;opacity:0}.ssimage__changephoto{text-decoration:underline;cursor:pointer;text-align:center;color:#999}.ssimage__noimagetext{width:30%;position:absolute;transform-style:preserve-3d;top:50%;left:50%;transform:translate(-50%, -50%);font-size:1em;line-height:1em;text-align:center;height:auto;padding:1.2em;color:#fff;opacity:1;border-radius:24px;background-color:#67C400}.ssimage__loadingimage{height:500px;width:100%;background:transparent url(/static/images/loading.gif) center center no-repeat}", ""]);

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(27);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, "@font-face{font-family:\"ssicons\";src:url('/static/fonts/ss.eot');src:local('/static/fonts/ss');src:url('/static/fonts/ss.svg#ss') format('svg');src:url('/static/fonts/ss.woff') format('woff');}.sscolours__block{width:14%;margin:1%;display:inline-block;text-align:center}.sscolours__block .sscolours__blocktext{font-size:0.8em;font-weight:bold;color:#666;display:block}.sscolours__block .sscolours__blockpallete{margin:auto;box-shadow:inset 0 1px 3px rgba(0,0,0,0.2);width:35px;height:35px;border-radius:5px}.sscolours__block .sscolours__blockpallete--selected{color:#68BB0D !important;background-color:#fff;background-color:rgba(255,255,255,0.9);font-size:0.9em;padding:0.5em;border-radius:5px;line-height:35px;font-family:ssicons}", ""]);

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(29);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/utils.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/utils.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, ".srpanel{*zoom:1;float:left;clear:none;text-align:inherit;width:100%;margin-left:0%;margin-right:3%;margin-top:2.5em;z-index:2;position:fixed}.srpanel:before,.srpanel:after{content:'';display:table}.srpanel:after{clear:both}.srpanel:last-child{margin-right:0%}.panel,.srpanel__error,.srpanel__success{*zoom:1;width:auto;max-width:30%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;font-size:0.9em;text-align:center;color:#fff;padding:0.3em;border-radius:5px}.panel:before,.srpanel__error:before,.srpanel__success:before,.panel:after,.srpanel__error:after,.srpanel__success:after{content:'';display:table}.panel:after,.srpanel__error:after,.srpanel__success:after{clear:both}.srpanel__error{border:1px solid #fd866c;background-color:#fd6d4e;box-shadow:0 1px 8px rgba(0,0,0,0.3)}.srpanel__success{border:1px solid #6ac900;background-color:#67C400;box-shadow:0 1px 8px rgba(0,0,0,0.3)}.overlay_visible{top:0;left:0;*zoom:1;float:left;clear:none;text-align:inherit;width:100%;margin-left:0%;margin-right:3%;height:auto;min-height:100%;position:fixed;background-color:rgba(0,0,0,0.9)}.overlay_visible:before,.overlay_visible:after{content:'';display:table}.overlay_visible:after{clear:both}.overlay_visible:last-child{margin-right:0%}.sslogin__wrapper{width:50%;min-width:400px;margin:15% auto}.sslogin__wrapper .sslogin__intro_text{text-align:center;color:#fff;margin-bottom:2em}.sslogin__wrapper .srlogin__button{min-width:350px;width:50%;margin:auto;text-decoration:none;outline:none;text-align:center;border-radius:5px;margin-top:1.5em;display:block;color:#fff;text-shadow:0 1px 1px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button .srlogin__provider__icon{display:inline-block;width:10%;font-family:ssicons;font-size:1.25em;font-weight:bold}.sslogin__wrapper .srlogin__provider__icon{line-height:2.5em}.sslogin__wrapper .srlogin__button__facebook{background-color:#3b5999}.sslogin__wrapper .srlogin__button__facebook:hover{background-color:#2d4474;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button__twitter{background-color:#4099FF}.sslogin__wrapper .srlogin__button__twitter:hover{background-color:#0d7eff;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button__google{background-color:#DD4B39}.sslogin__wrapper .srlogin__button__google:hover{background-color:#c23321;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}#create{*zoom:1;width:auto;max-width:10%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0}#create:before,#create:after{content:'';display:table}#create:after{clear:both}@media only screen and (max-width: 960px){#create{float:right;margin-right:10%;width:20%;max-width:100%;}}#create .create_button{color:#fff;width:100%;display:inline-block;background-color:#67C400;border-radius:5px;text-align:center;font-size:1.2em;line-height:2.4em;margin-top:5px}.sidebar__wrapper{z-index:1;position:absolute;padding-top:5em;width:40%}.sidebar__wrapper .toggle_label{z-index:1;border-radius:0 5px 5px 0;width:4em;height:4em;top:10px;background-color:rgba(0,0,0,0.9);display:block;position:absolute}.sidebar__wrapper input.toggle_switch{display:none}.sidebar__wrapper #search{background-color:rgba(255,255,255,0.85);transition:all 100ms ease-out;padding:1em;min-height:400px;border-radius:0 10px 10px 0;box-shadow:0 0 15px rgba(0,0,0,0.1)}.sidebar__wrapper input.toggle_switch+#search{position:relative;width:0;visibility:hidden;height:0}.sidebar__wrapper input.toggle_switch:checked+#search{width:100%;visibility:visible;height:100%}.sidebar__wrapper .search_header{*zoom:1;width:auto;max-width:30%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;font-weight:bolder;text-align:center;font-size:1.2em;margin:1em auto 0.3em auto}.sidebar__wrapper .search_header:before,.sidebar__wrapper .search_header:after{content:'';display:table}.sidebar__wrapper .search_header:after{clear:both}.sidebar__wrapper .search_item_gender{*zoom:1;float:left;clear:none;text-align:inherit;width:31.33333%;margin-left:0%;margin-right:3%;text-align:center}.sidebar__wrapper .search_item_gender:before,.sidebar__wrapper .search_item_gender:after{content:'';display:table}.sidebar__wrapper .search_item_gender:after{clear:both}.sidebar__wrapper .search_item_gender:last-child{margin-right:0%}.sidebar__wrapper .search_items_wrapper{*zoom:1}.sidebar__wrapper .search_items_wrapper:before,.sidebar__wrapper .search_items_wrapper:after{content:'';display:table}.sidebar__wrapper .search_items_wrapper:after{clear:both}.sidebar__wrapper .search_item_styles,.sidebar__wrapper .search_item_colours{width:30%;float:left;text-align:center}.sidebar__wrapper .search_item{border:solid 1px #dfdfdf;background-color:#fff;margin:1%;padding:0.5em;border-radius:5px}.sidebar__wrapper .search_item .search_item_text{float:left}.sidebar__wrapper .search_item .sstags__icon{margin-right:0.5em;display:inline-block;font-size:0.8em}.sidebar__wrapper .done_button{background-color:#67C400;color:#fff;width:50%;margin:1em auto;padding:0.5em;text-align:center;font-size:1.3em;border-radius:7px;display:block}", ""]);

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	__webpack_require__(44)
	var avatar = {}
	avatar.view = function (user) {
		return {tag: "span", attrs: {class:"ssavatar__wrapper"}, children: [{tag: "img", attrs: {class:"ssavatar__image"}}, user]}
	}
	module.exports = avatar
	//

/***/ },
/* 31 */,
/* 32 */,
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	var m = __webpack_require__(17)
	var ProfileHeader = {}
	__webpack_require__(46)
	ProfileHeader.controller = {}
	// var profile = {}
	ProfileHeader.writable = function (vm) {
		var profile = vm.profile
		// return function (vm) {	
			return {tag: "div", attrs: {class:"profile_header"}, children: [
					{tag: "div", attrs: {class:"profile_image"}}, 
					{tag: "div", attrs: {class:"profile_personal"}, children: [
						{tag: "label", attrs: {for:"display_name",style:"display:block",class:"display_name_label hint--rounded hint--left hint--always hint--success","data-hint":"Edit Display Name"}, children: [{tag: "input", attrs: {id:"display_name",class:"display_name",onchange:m.withAttr("value", profile.DisplayName),value:profile.DisplayName()}}]}, 
						{tag: "label", attrs: {style:"display:block",class:"description_label hint--left hint--rounded hint--always hint--info","data-hint":"Edit Bio"}, children: [{tag: "textarea", attrs: {rows:"3",onchange:m.withAttr("value", profile.Description),class:"description"}, children: [profile.Description() || "Tell us a bit about yourself. Go on, don't be shy..."]}]}, 
						{tag: "label", attrs: {style:"display:block",class:"hint--left hint--rounded hint--always hint--warning","data-hint":"Edit URL"}, children: [{tag: "input", attrs: {class:"url",onchange:m.withAttr("value", profile.Url),value:profile.Url() || "Enter URL"}}]}, 
						{tag: "br", attrs: {style:"clear:both"}}, 
						{tag: "p", attrs: {}, children: [{tag: "a", attrs: {class:"update_link",onclick:profile.update}, children: ["Update my Details"]}]}
					]}, 
					{tag: "div", attrs: {class:"profile_info"}
					}, 
					{tag: "br", attrs: {style:"clear:both"}}
				]}
		// }
	}
	ProfileHeader.readable = function (vm) {
		return {tag: "div", attrs: {class:"profile_header"}, children: [
				{tag: "div", attrs: {class:"profile_image"}}, 
				{tag: "div", attrs: {class:"profile_personal"}, children: [
					{tag: "p", attrs: {class:"display_name"}, children: [vm.profile.DisplayName()]}, 
					{tag: "p", attrs: {class:"description"}, children: [vm.profile.Description()]}, 
					{tag: "p", attrs: {class:"url"}, children: [vm.profile.Url()]}, 
					{tag: "br", attrs: {style:"clear:both"}}
				]}, 
				{tag: "div", attrs: {class:"profile_info"}
				}
			]}
	}
	ProfileHeader.view = function (vm) {
		var content
		if (!!vm.editMode) {
			content =  this.writable(vm)
		} else {
			content = this.readable(vm)
		}
		return {tag: "div", attrs: {class:"profile_header__wrapper"}, children: [
			content,
			{tag: "div", attrs: {class:"profile_stats " + vm.section}, children: [
				{tag: "p", attrs: {onclick:vm.onSectionChange.bind(null, "collections"),class:"stat collections"}, children: [{tag: "span", attrs: {class:"stat__label"}, children: ["Collections"]}, {tag: "span", attrs: {class:"stat__value"}, children: [vm.profile.Collections()]}]}, 
				{tag: "p", attrs: {onclick:vm.onSectionChange.bind(null, "uploads"),class:"stat uploads"}, children: [{tag: "span", attrs: {class:"stat__label"}, children: ["Uploads"]}, {tag: "span", attrs: {class:"stat__value"}, children: [vm.profile.Uploads()]}]}, 
				{tag: "p", attrs: {onclick:vm.onSectionChange.bind(null, "collections"),class:"stat collections"}, children: [{tag: "span", attrs: {class:"stat__label"}, children: ["Views"]}, {tag: "span", attrs: {class:"stat__value"}, children: [vm.profile.Collections()]}]}, 
				{tag: "p", attrs: {onclick:vm.onSectionChange.bind(null, "uploads"),class:"stat uploads"}, children: [{tag: "span", attrs: {class:"stat__label"}, children: ["Favourites"]}, {tag: "span", attrs: {class:"stat__value"}, children: [vm.profile.Uploads()]}]}
			]}
		]}
	}
	module.exports = ProfileHeader

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(35);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, "#content{margin-top:4em}.profile_header__wrapper .profile_header{padding-top:60px;min-height:300px;background:transparent url(/static/images/hero.jpg) 0% 60% no-repeat;background-size:cover}.profile_header__wrapper .profile_header .profile_image{position:relative;left:20.6%;display:inline-block;height:150px;box-sizing:border-box;width:150px;border:5px solid #fcfcfc;vertical-align:middle;border-radius:50%;box-shadow:0px 2px 8px rgba(0,0,0,0.2);background:transparent url(/static/images/viccircle1.png) 0% 60% no-repeat;background-size:contain}@media only screen and (max-width: 960px){.profile_header__wrapper .profile_header .profile_image{position:static;left:0;margin:auto;*zoom:1;width:auto;max-width:150px;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.profile_header__wrapper .profile_header .profile_image:before,.profile_header__wrapper .profile_header .profile_image:after{content:'';display:table}.profile_header__wrapper .profile_header .profile_image:after{clear:both}}.profile_header__wrapper .profile_header .profile_personal{position:relative;left:29.42857%;text-shadow:1px 1px 2px rgba(0,0,0,0.5);display:inline-block;vertical-align:middle;color:#fff}@media only screen and (max-width: 960px){.profile_header__wrapper .profile_header .profile_personal{position:static;left:0;*zoom:1;width:auto;max-width:100%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;text-align:center;}.profile_header__wrapper .profile_header .profile_personal:before,.profile_header__wrapper .profile_header .profile_personal:after{content:'';display:table}.profile_header__wrapper .profile_header .profile_personal:after{clear:both}}.profile_header__wrapper .profile_header .display_name{font-size:2.5em}.profile_header__wrapper .profile_header .description{font-family:\"OpenSans\";font-size:1.1em}.profile_stats{background-color:#f9f9f9;*zoom:1;width:auto;max-width:80%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0}.profile_stats:before,.profile_stats:after{content:'';display:table}.profile_stats:after{clear:both}@media only screen and (max-width: 960px){.profile_stats{*zoom:1;width:auto;max-width:90%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.profile_stats:before,.profile_stats:after{content:'';display:table}.profile_stats:after{clear:both}}.profile_stats .stat{box-sizing:border-box;*zoom:1;float:left;clear:none;text-align:inherit;width:25%;margin-left:0%;margin-right:0%;font-size:1.5em;line-height:3em;text-align:center;border-left:1px solid #e2e2e2;border-right:1px solid #e9e9e9;color:#7d7d7d;text-shadow:1px 1px #fafafa}.profile_stats .stat:before,.profile_stats .stat:after{content:'';display:table}.profile_stats .stat:after{clear:both}.profile_main{*zoom:1}.profile_main:before,.profile_main:after{content:'';display:table}.profile_main:after{clear:both}.profile_stats .uploads,.profile_stats .collections{color:#444;cursor:pointer;transition:border-bottom 0.3s ease-out;font-size:1.5em;line-height:1.5em}.profile_stats .uploads .stat__label,.profile_stats .collections .stat__label{display:inline-block;padding:0.5em 0}.profile_stats .uploads .stat__value,.profile_stats .collections .stat__value{font-size:0.9em;color:#fff;display:inline-block;text-shadow:0 1px 2px rgba(0,0,0,0.2);margin-left:1em;padding:0 0.15em;min-width:1.5em;border-radius:5px;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1)}.profile_stats .uploads .stat__value{background-color:#68AAF8}.profile_stats .collections .stat__value{background-color:#fd453f}.profile_stats.uploads .uploads{border-bottom:3px solid #68AAF8}.profile_stats.uploads .collections{border-bottom:0}.profile_stats.collections .collections{border-bottom:3px solid #fd453f}.profile_stats.collections .uploads{border-bottom:0}.pad{background-color:#fff;display:block;width:97.5%;margin:auto;padding:7px;border-radius:5px;box-shadow:0 0 7px rgba(0,0,0,0.25)}", ""]);

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(37);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/hint.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/hint.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, "/*! Hint.css - v1.3.4 - 2015-02-28\n* http://kushagragour.in/lab/hint/\n* Copyright (c) 2015 Kushagra Gour; Licensed MIT */.hint,[data-hint]{position:relative;display:inline-block}.hint:before,.hint:after,[data-hint]:before,[data-hint]:after{position:absolute;-webkit-transform:translate3d(0, 0, 0);-moz-transform:translate3d(0, 0, 0);transform:translate3d(0, 0, 0);visibility:hidden;opacity:0;z-index:1000000;pointer-events:none;-webkit-transition:0.3s ease;-moz-transition:0.3s ease;transition:0.3s ease;-webkit-transition-delay:0ms;-moz-transition-delay:0ms;transition-delay:0ms}.hint:hover:before,.hint:hover:after,.hint:focus:before,.hint:focus:after,[data-hint]:hover:before,[data-hint]:hover:after,[data-hint]:focus:before,[data-hint]:focus:after{visibility:visible;opacity:1}.hint:hover:before,.hint:hover:after,[data-hint]:hover:before,[data-hint]:hover:after{-webkit-transition-delay:100ms;-moz-transition-delay:100ms;transition-delay:100ms}.hint:before,[data-hint]:before{content:'';position:absolute;background:transparent;border:6px solid transparent;z-index:1000001}.hint:after,[data-hint]:after{content:attr(data-hint);background:#383838;color:#fff;padding:8px 10px;font-size:12px;line-height:12px;white-space:nowrap}.hint--top:before{border-top-color:#383838}.hint--bottom:before{border-bottom-color:#383838}.hint--left:before{border-left-color:#383838}.hint--right:before{border-right-color:#383838}.hint--top:before{margin-bottom:-12px}.hint--top:after{margin-left:-18px}.hint--top:before,.hint--top:after{bottom:100%;left:50%}.hint--top:hover:after,.hint--top:hover:before,.hint--top:focus:after,.hint--top:focus:before{-webkit-transform:translateY(-8px);-moz-transform:translateY(-8px);transform:translateY(-8px)}.hint--bottom:before{margin-top:-12px}.hint--bottom:after{margin-left:-18px}.hint--bottom:before,.hint--bottom:after{top:100%;left:50%}.hint--bottom:hover:after,.hint--bottom:hover:before,.hint--bottom:focus:after,.hint--bottom:focus:before{-webkit-transform:translateY(8px);-moz-transform:translateY(8px);transform:translateY(8px)}.hint--right:before{margin-left:-12px;margin-bottom:-6px}.hint--right:after{margin-bottom:-14px}.hint--right:before,.hint--right:after{left:100%;bottom:50%}.hint--right:hover:after,.hint--right:hover:before,.hint--right:focus:after,.hint--right:focus:before{-webkit-transform:translateX(8px);-moz-transform:translateX(8px);transform:translateX(8px)}.hint--left:before{margin-right:-12px;margin-bottom:-6px}.hint--left:after{margin-bottom:-14px}.hint--left:before,.hint--left:after{right:100%;bottom:50%}.hint--left:hover:after,.hint--left:hover:before,.hint--left:focus:after,.hint--left:focus:before{-webkit-transform:translateX(-8px);-moz-transform:translateX(-8px);transform:translateX(-8px)}.hint:after,[data-hint]:after{text-shadow:0 -1px 0 #000;box-shadow:4px 4px 8px rgba(0,0,0,0.3)}.hint--error:after{background-color:#b34e4d;text-shadow:0 -1px 0 #592726}.hint--error.hint--top:before{border-top-color:#b34e4d}.hint--error.hint--bottom:before{border-bottom-color:#b34e4d}.hint--error.hint--left:before{border-left-color:#b34e4d}.hint--error.hint--right:before{border-right-color:#b34e4d}.hint--warning:after{background-color:#c09854;text-shadow:0 -1px 0 #6c5328}.hint--warning.hint--top:before{border-top-color:#c09854}.hint--warning.hint--bottom:before{border-bottom-color:#c09854}.hint--warning.hint--left:before{border-left-color:#c09854}.hint--warning.hint--right:before{border-right-color:#c09854}.hint--info:after{background-color:#3986ac;text-shadow:0 -1px 0 #193b4d}.hint--info.hint--top:before{border-top-color:#3986ac}.hint--info.hint--bottom:before{border-bottom-color:#3986ac}.hint--info.hint--left:before{border-left-color:#3986ac}.hint--info.hint--right:before{border-right-color:#3986ac}.hint--success:after{background-color:#458746;text-shadow:0 -1px 0 #1a321a}.hint--success.hint--top:before{border-top-color:#458746}.hint--success.hint--bottom:before{border-bottom-color:#458746}.hint--success.hint--left:before{border-left-color:#458746}.hint--success.hint--right:before{border-right-color:#458746}.hint--always:after,.hint--always:before{opacity:1;visibility:visible}.hint--always.hint--top:after,.hint--always.hint--top:before{-webkit-transform:translateY(-8px);-moz-transform:translateY(-8px);transform:translateY(-8px)}.hint--always.hint--bottom:after,.hint--always.hint--bottom:before{-webkit-transform:translateY(8px);-moz-transform:translateY(8px);transform:translateY(8px)}.hint--always.hint--left:after,.hint--always.hint--left:before{-webkit-transform:translateX(-8px);-moz-transform:translateX(-8px);transform:translateX(-8px)}.hint--always.hint--right:after,.hint--always.hint--right:before{-webkit-transform:translateX(8px);-moz-transform:translateX(8px);transform:translateX(8px)}.hint--rounded:after{border-radius:4px}.hint--no-animate:before,.hint--no-animate:after{-webkit-transition-duration:0ms;-moz-transition-duration:0ms;transition-duration:0ms}.hint--bounce:before,.hint--bounce:after{-webkit-transition:opacity 0.3s ease,visibility 0.3s ease,-webkit-transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24);-moz-transition:opacity 0.3s ease,visibility 0.3s ease,-moz-transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24);transition:opacity 0.3s ease,visibility 0.3s ease,transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24)}", ""]);

/***/ },
/* 38 */,
/* 39 */,
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	/*! snabbt.js v0.3.0 built: 2015-02-08  (c)2015 Daniel Lundin @license MIT */
	!function(a,b){var c=b();c.snabbt.Matrix=c.Matrix,c.snabbt.setElementTransform=c.updateElementTransform,true?module.exports=c.snabbt:"function"==typeof define&&define.amd?define([],function(){return a.returnExportsGlobal=c.snabbt}):a.snabbt=c.snabbt}(this,function(){var a=a||{};a.Animation=function(b){var c=a.optionOrDefault;this.startState=b.startState,this.endState=b.endState,this.duration=c(b.duration,500),this.delay=c(b.delay,0),this.perspective=b.perspective,this.easing=a.createEaser(c(b.easing,"linear"),b),this.currentState=this.startState.clone(),this.transformOrigin=b.transformOrigin,this.currentState.transformOrigin=b.transformOrigin,this.startTime=0,this.currentTime=0,this.stopped=!1,this.manual=b.manual,this.manualValue=0,this.manualDelayFactor=this.delay/this.duration,this.tweener=b.valueFeeder?new a.ValueFeederTweener(b.valueFeeder,this.startState,this.endState,this.currentState):new a.StateTweener(this.startState,this.endState,this.currentState)},a.Animation.prototype.stop=function(){this.stopped=!0},a.Animation.prototype.isStopped=function(){return this.stopped},a.Animation.prototype.finish=function(a){this.manual=!1;var b=this.duration*this.manualValue;this.startTime=this.currentTime-b,this.manualCallback=a,this.easing.resetFrom=this.manualValue},a.Animation.prototype.rollback=function(a){this.manual=!1,this.tweener.setReverse();var b=this.duration*(1-this.manualValue);this.startTime=this.currentTime-b,this.manualCallback=a,this.easing.resetFrom=this.manualValue},a.Animation.prototype.restart=function(){this.startTime=void 0,this.easing.resetFrom(0)},a.Animation.prototype.tick=function(a){if(!this.stopped){if(this.manual){var b=this.delay/this.duration;return this.currentTime=a,void(this.manualValue>b&&this.updateCurrentTransform())}this.startTime||(this.startTime=a),a-this.startTime>this.delay&&(this.currentTime=a-this.delay);var c=Math.min(Math.max(0,this.currentTime-this.startTime),this.duration);this.easing.tick(c/this.duration),this.updateCurrentTransform(),this.completed()&&this.manualCallback&&this.manualCallback()}},a.Animation.prototype.getCurrentState=function(){return this.currentState},a.Animation.prototype.setValue=function(a){this.manualValue=Math.min(Math.max(a,0),1+this.manualDelayFactor)},a.Animation.prototype.updateCurrentTransform=function(){var a=this.easing.value();this.manual&&(a=this.manualValue),this.tweener.tween(a)},a.Animation.prototype.completed=function(){return this.stopped?!0:0===this.startTime?!1:this.easing.completed()},a.Animation.prototype.updateElement=function(b){var c=this.tweener.asMatrix(),d=this.tweener.getProperties();a.updateElementTransform(b,c,this.perspective),a.updateElementProperties(b,d)},a.AttentionAnimation=function(b){this.movement=b.movement,this.currentMovement=new a.State({}),b.initialVelocity=.1,b.equilibriumPosition=0,this.spring=new a.SpringEasing(b),this.stopped=!1,this.options=b},a.AttentionAnimation.prototype.stop=function(){this.stopped=!0},a.AttentionAnimation.prototype.isStopped=function(){return this.stopped},a.AttentionAnimation.prototype.tick=function(){var a=this.spring;this.stopped||a.equilibrium||(a.tick(),this.updateMovement())},a.AttentionAnimation.prototype.updateMovement=function(){var a=this.currentMovement,b=this.movement,c=this.spring.position;a.position[0]=b.position[0]*c,a.position[1]=b.position[1]*c,a.position[2]=b.position[2]*c,a.rotation[0]=b.rotation[0]*c,a.rotation[1]=b.rotation[1]*c,a.rotation[2]=b.rotation[2]*c,a.rotationPost[0]=b.rotationPost[0]*c,a.rotationPost[1]=b.rotationPost[1]*c,a.rotationPost[2]=b.rotationPost[2]*c,1!==b.scale[0]&&1!==b.scale[1]&&(a.scale[0]=1+b.scale[0]*c,a.scale[1]=1+b.scale[1]*c),a.skew[0]=b.skew[0]*c,a.skew[1]=b.skew[1]*c},a.AttentionAnimation.prototype.updateElement=function(b){var c=this.currentMovement,d=c.asMatrix(),e=c.getProperties();a.updateElementTransform(b,d),a.updateElementProperties(b,e)},a.AttentionAnimation.prototype.getCurrentState=function(){return this.currentMovement},a.AttentionAnimation.prototype.completed=function(){return this.spring.equilibrium||this.stopped},a.AttentionAnimation.prototype.restart=function(){this.spring=new a.SpringEasing(this.options)},a.createAnimation=function(b){return new a.Animation(b)};var a=a||{};a.linearEasing=function(a){return a},a.ease=function(a){return(Math.cos(a*Math.PI+Math.PI)+1)/2},a.easeIn=function(a){return a*a},a.easeOut=function(a){return-Math.pow(a-1,2)+1},a.SpringEasing=function(b){var c=a.optionOrDefault;this.position=c(b.startPosition,0),this.equilibriumPosition=c(b.equilibriumPosition,1),this.velocity=c(b.initialVelocity,0),this.springConstant=c(b.springConstant,.8),this.deceleration=c(b.springDeceleration,.9),this.mass=c(b.springMass,10),this.equilibrium=!1},a.SpringEasing.prototype.tick=function(a){if(0!==a&&!this.equilibrium){var b=-(this.position-this.equilibriumPosition)*this.springConstant,c=b/this.mass;this.velocity+=c,this.position+=this.velocity,this.velocity*=this.deceleration,Math.abs(this.position-this.equilibriumPosition)<.001&&Math.abs(this.velocity)<.001&&(this.equilibrium=!0)}},a.SpringEasing.prototype.resetFrom=function(a){this.position=a,this.velocity=0},a.SpringEasing.prototype.value=function(){return this.equilibrium?this.equilibriumPosition:this.position},a.SpringEasing.prototype.completed=function(){return this.equilibrium},a.EASING_FUNCS={linear:a.linearEasing,ease:a.ease,easeIn:a.easeIn,easeOut:a.easeOut},a.Easer=function(a){this.easer=a,this._value=0},a.Easer.prototype.tick=function(a){this._value=this.easer(a),this.lastValue=a},a.Easer.prototype.resetFrom=function(){this.lastValue=0},a.Easer.prototype.value=function(){return this._value},a.Easer.prototype.completed=function(){return this.lastValue>=1?this.lastValue:!1},a.createEaser=function(b,c){if("spring"==b)return new a.SpringEasing(c);var d;return d=a.isFunction(b)?b:a.EASING_FUNCS[b],new a.Easer(d)},window.jQuery&&!function(b){b.fn.snabbt=function(b,c){return a.snabbt(this.get(),b,c)}}(jQuery);var a=a||{};a.snabbt=function(b,c,d){var e=b;if(e.hasOwnProperty("length")){for(var f={chainers:[],then:function(b){var c=this.chainers.length;return this.chainers.forEach(function(d,e){d.then(a.preprocessOptions(b,e,c))}),f},setValue:function(a){return this.chainers.forEach(function(b){b.setValue(a)}),f},finish:function(){return this.chainers.forEach(function(a){a.finish()}),f},rollback:function(){return this.chainers.forEach(function(a){a.rollback()}),f}},g=0,h=e.length;h>g;++g)f.chainers.push("string"==typeof c?a.snabbtSingleElement(e[g],c,a.preprocessOptions(d,g,h)):a.snabbtSingleElement(e[g],a.preprocessOptions(c,g,h),d));return f}return"string"==typeof c?a.snabbtSingleElement(e,c,a.preprocessOptions(d,0,1)):a.snabbtSingleElement(e,a.preprocessOptions(c,0,1),d)},a.preprocessOptions=function(b,c,d){if(!b)return b;var e=a.cloneObject(b);a.isFunction(b.delay)&&(e.delay=b.delay(c,d)),a.isFunction(b.callback)&&(e.callback=function(){b.callback(c,d)}),a.isFunction(b.valueFeeder)&&(e.valueFeeder=function(a,e){return b.valueFeeder(a,e,c,d)}),a.isFunction(b.easing)&&(e.easing=function(a){return b.easing(a,c,d)});var f=["position","rotation","skew","rotationPost","scale","width","height","opacity","fromPosition","fromRotation","fromSkew","fromRotationPost","fromScale","fromWidth","fromHeight","fromOpacity","transformOrigin","duration","delay"];return f.forEach(function(f){a.isFunction(b[f])&&(e[f]=b[f](c,d))}),e},a.snabbtSingleElement=function(b,c,d){function e(c){return k.tick(c),k.updateElement(b),k.isStopped()?void 0:k.completed()?void(f.loop>1&&!k.isStopped()?(f.loop-=1,k.restart(),a.requestAnimationFrame(e)):(f.callback&&f.callback(b),l.length&&(f=l.pop(),h=a.stateFromOptions(f,i,!0),i=a.stateFromOptions(f,a.cloneObject(i)),f=a.setupAnimationOptions(h,i,f),k=new a.Animation(f),a.runningAnimations.push([b,k]),k.tick(c),a.requestAnimationFrame(e)))):a.requestAnimationFrame(e)}if("attention"===c)return a.setupAttentionAnimation(b,d);if("stop"===c)return a.stopAnimation(b);var f=c;a.clearOphanedEndStates();var g=a.currentAnimationState(b),h=g;h=a.stateFromOptions(f,h,!0);var i=a.cloneObject(g);i=a.stateFromOptions(f,i);var j=a.setupAnimationOptions(h,i,f),k=a.createAnimation(j);a.runningAnimations.push([b,k]),k.updateElement(b);var l=[],m={then:function(b){return l.unshift(a.preprocessOptions(b,0,1)),m}};return a.requestAnimationFrame(e),f.manual?k:m},a.setupAttentionAnimation=function(b,c){function d(e){f.tick(e),f.updateElement(b),f.completed()?(c.callback&&c.callback(b),c.loop&&c.loop>1&&(c.loop--,f.restart(),a.requestAnimationFrame(d))):a.requestAnimationFrame(d)}var e=a.stateFromOptions(c);c.movement=e;var f=new a.AttentionAnimation(c);a.runningAnimations.push([b,f]),a.requestAnimationFrame(d)},a.stopAnimation=function(b){for(var c=a.runningAnimations,d=0,e=c.length;e>d;++d){var f=c[d],g=f[0],h=f[1];g===b&&h.stop()}},a.findAnimationState=function(a,b){for(var c=0,d=a.length;d>c;++c){var e=a[c],f=e[0],g=e[1];if(f===b){var h=g.getCurrentState();return g.stop(),h}}},a.currentAnimationState=function(b){var c=a.findAnimationState(a.runningAnimations,b);return c?c:a.findAnimationState(a.completedAnimations,b)},a.stateFromOptions=function(b,c,d){c||(c=new a.State({}));var e="position",f="rotation",g="skew",h="rotationPost",i="scale",j="width",k="height",l="opacity";d&&(e="fromPosition",f="fromRotation",g="fromSkew",h="fromRotationPost",i="fromScale",j="fromWidth",k="fromHeight",l="fromOpacity");var m=a.optionOrDefault;return c.position=m(b[e],c.position),c.rotation=m(b[f],c.rotation),c.rotationPost=m(b[h],c.rotationPost),c.skew=m(b[g],c.skew),c.scale=m(b[i],c.scale),c.opacity=b[l],c.width=b[j],c.height=b[k],c},a.setupAnimationOptions=function(a,b,c){return c.startState=a,c.endState=b,c},a.tickRequests=[],a.runningAnimations=[],a.completedAnimations=[],a.requestAnimationFrame=function(b){0===a.tickRequests.length&&window.requestAnimationFrame(a.tickAnimations),a.tickRequests.push(b)},a.tickAnimations=function(b){for(var c=a.tickRequests,d=c.length,e=0;d>e;++e)c[e](b);c.splice(0,d);var f=a.runningAnimations.filter(function(a){return a[1].completed()});a.completedAnimations=a.completedAnimations.filter(function(a){for(var b=0,c=f.length;c>b;++b)if(a[0]===f[b][0])return!1;return!0}),a.completedAnimations=a.completedAnimations.concat(f),a.runningAnimations=a.runningAnimations.filter(function(a){return!a[1].completed()}),0!==c.length&&window.requestAnimationFrame(a.tickAnimations)},a.clearOphanedEndStates=function(){a.completedAnimations=a.completedAnimations.filter(function(b){return a.findUltimateAncestor(b[0]).body})},a.findUltimateAncestor=function(a){for(var b=a;b.parentNode;)b=b.parentNode;return b};var a=a||{};a.assignTranslate=function(a,b,c,d){a[0]=1,a[1]=0,a[2]=0,a[3]=0,a[4]=0,a[5]=1,a[6]=0,a[7]=0,a[8]=0,a[9]=0,a[10]=1,a[11]=0,a[12]=b,a[13]=c,a[14]=d,a[15]=1},a.assignRotateX=function(a,b){a[0]=1,a[1]=0,a[2]=0,a[3]=0,a[4]=0,a[5]=Math.cos(b),a[6]=-Math.sin(b),a[7]=0,a[8]=0,a[9]=Math.sin(b),a[10]=Math.cos(b),a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.assignRotateY=function(a,b){a[0]=Math.cos(b),a[1]=0,a[2]=Math.sin(b),a[3]=0,a[4]=0,a[5]=1,a[6]=0,a[7]=0,a[8]=-Math.sin(b),a[9]=0,a[10]=Math.cos(b),a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.assignRotateZ=function(a,b){a[0]=Math.cos(b),a[1]=-Math.sin(b),a[2]=0,a[3]=0,a[4]=Math.sin(b),a[5]=Math.cos(b),a[6]=0,a[7]=0,a[8]=0,a[9]=0,a[10]=1,a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.assignSkew=function(a,b,c){a[0]=1,a[1]=Math.tan(b),a[2]=0,a[3]=0,a[4]=Math.tan(c),a[5]=1,a[6]=0,a[7]=0,a[8]=0,a[9]=0,a[10]=1,a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.assignScale=function(a,b,c){a[0]=b,a[1]=0,a[2]=0,a[3]=0,a[4]=0,a[5]=c,a[6]=0,a[7]=0,a[8]=0,a[9]=0,a[10]=1,a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.assignIdentity=function(a){a[0]=1,a[1]=0,a[2]=0,a[3]=0,a[4]=0,a[5]=1,a[6]=0,a[7]=0,a[8]=0,a[9]=0,a[10]=1,a[11]=0,a[12]=0,a[13]=0,a[14]=0,a[15]=1},a.copyArray=function(a,b){b[0]=a[0],b[1]=a[1],b[2]=a[2],b[3]=a[3],b[4]=a[4],b[5]=a[5],b[6]=a[6],b[7]=a[7],b[8]=a[8],b[9]=a[9],b[10]=a[10],b[11]=a[11],b[12]=a[12],b[13]=a[13],b[14]=a[14],b[15]=a[15]},a.Matrix=function(){this.data=new Float32Array(16),this.a=new Float32Array(16),this.b=new Float32Array(16),a.assignIdentity(this.data)},a.Matrix.prototype.asCSS=function(){for(var a="matrix3d(",b=0;15>b;++b)a+=Math.abs(this.data[b])<1e-4?"0,":this.data[b].toFixed(10)+",";return a+=Math.abs(this.data[15])<1e-4?"0)":this.data[15].toFixed(10)+")"},a.Matrix.prototype.clear=function(){a.assignIdentity(this.data)},a.Matrix.prototype.translate=function(b,c,d){return a.copyArray(this.data,this.a),a.assignTranslate(this.b,b,c,d),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.Matrix.prototype.rotateX=function(b){return a.copyArray(this.data,this.a),a.assignRotateX(this.b,b),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.Matrix.prototype.rotateY=function(b){return a.copyArray(this.data,this.a),a.assignRotateY(this.b,b),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.Matrix.prototype.rotateZ=function(b){return a.copyArray(this.data,this.a),a.assignRotateZ(this.b,b),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.Matrix.prototype.scale=function(b,c){return a.copyArray(this.data,this.a),a.assignScale(this.b,b,c),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.Matrix.prototype.skew=function(b,c){return a.copyArray(this.data,this.a),a.assignSkew(this.b,b,c),a.assignedMatrixMultiplication(this.a,this.b,this.data),this},a.assignedMatrixMultiplication=function(a,b,c){return c[0]=a[0]*b[0]+a[1]*b[4]+a[2]*b[8]+a[3]*b[12],c[1]=a[0]*b[1]+a[1]*b[5]+a[2]*b[9]+a[3]*b[13],c[2]=a[0]*b[2]+a[1]*b[6]+a[2]*b[10]+a[3]*b[14],c[3]=a[0]*b[3]+a[1]*b[7]+a[2]*b[11]+a[3]*b[15],c[4]=a[4]*b[0]+a[5]*b[4]+a[6]*b[8]+a[7]*b[12],c[5]=a[4]*b[1]+a[5]*b[5]+a[6]*b[9]+a[7]*b[13],c[6]=a[4]*b[2]+a[5]*b[6]+a[6]*b[10]+a[7]*b[14],c[7]=a[4]*b[3]+a[5]*b[7]+a[6]*b[11]+a[7]*b[15],c[8]=a[8]*b[0]+a[9]*b[4]+a[10]*b[8]+a[11]*b[12],c[9]=a[8]*b[1]+a[9]*b[5]+a[10]*b[9]+a[11]*b[13],c[10]=a[8]*b[2]+a[9]*b[6]+a[10]*b[10]+a[11]*b[14],c[11]=a[8]*b[3]+a[9]*b[7]+a[10]*b[11]+a[11]*b[15],c[12]=a[12]*b[0]+a[13]*b[4]+a[14]*b[8]+a[15]*b[12],c[13]=a[12]*b[1]+a[13]*b[5]+a[14]*b[9]+a[15]*b[13],c[14]=a[12]*b[2]+a[13]*b[6]+a[14]*b[10]+a[15]*b[14],c[15]=a[12]*b[3]+a[13]*b[7]+a[14]*b[11]+a[15]*b[15],c},a.State=function(b){var c=a.optionOrDefault;this.position=c(b.position,[0,0,0]),this.rotation=c(b.rotation,[0,0,0]),this.rotationPost=c(b.rotationPost,[0,0,0]),this.skew=c(b.skew,[0,0]),this.scale=c(b.scale,[1,1]),this.opacity=b.opacity,this.width=b.width,this.height=b.height,this.matrix=new a.Matrix,this.properties={opacity:void 0,width:void 0,height:void 0}},a.State.prototype.clone=function(){var b=new a.State({position:this.position.slice(0),rotation:this.rotation.slice(0),rotationPost:this.rotationPost.slice(0),skew:this.skew.slice(0),scale:this.scale.slice(0),height:this.height,width:this.width,opacity:this.opacity});return b},a.State.prototype.asMatrix=function(){var a=this.matrix;return a.clear(),this.transformOrigin&&a.translate(-this.transformOrigin[0],-this.transformOrigin[1],-this.transformOrigin[2]),(1!==this.scale[0]||1!==this.scale[1])&&a.scale(this.scale[0],this.scale[1]),(0!==this.skew[0]||0!==this.skew[1])&&a.skew(this.skew[0],this.skew[1]),(0!==this.rotation[0]||0!==this.rotation[1]||0!==this.rotation[2])&&(a.rotateX(this.rotation[0]),a.rotateY(this.rotation[1]),a.rotateZ(this.rotation[2])),(0!==this.position[0]||0!==this.position[1]||0!==this.position[2])&&a.translate(this.position[0],this.position[1],this.position[2]),(0!==this.rotationPost[0]||0!==this.rotationPost[1]||0!==this.rotationPost[2])&&(a.rotateX(this.rotationPost[0]),a.rotateY(this.rotationPost[1]),a.rotateZ(this.rotationPost[2])),this.transformOrigin&&a.translate(this.transformOrigin[0],this.transformOrigin[1],this.transformOrigin[2]),a},a.State.prototype.getProperties=function(){return this.properties.opacity=this.opacity,this.properties.width=this.width+"px",this.properties.height=this.height+"px",this.properties};var a=a||{};a.StateTweener=function(a,b,c){this.start=a,this.end=b,this.result=c},a.StateTweener.prototype.tween=function(a){var b=this.end.position[0]-this.start.position[0],c=this.end.position[1]-this.start.position[1],d=this.end.position[2]-this.start.position[2],e=this.end.rotation[0]-this.start.rotation[0],f=this.end.rotation[1]-this.start.rotation[1],g=this.end.rotation[2]-this.start.rotation[2],h=this.end.rotationPost[0]-this.start.rotationPost[0],i=this.end.rotationPost[1]-this.start.rotationPost[1],j=this.end.rotationPost[2]-this.start.rotationPost[2],k=this.end.scale[0]-this.start.scale[0],l=this.end.scale[1]-this.start.scale[1],m=this.end.skew[0]-this.start.skew[0],n=this.end.skew[1]-this.start.skew[1],o=this.end.width-this.start.width,p=this.end.height-this.start.height,q=this.end.opacity-this.start.opacity;this.result.position[0]=this.start.position[0]+a*b,this.result.position[1]=this.start.position[1]+a*c,this.result.position[2]=this.start.position[2]+a*d,this.result.rotation[0]=this.start.rotation[0]+a*e,this.result.rotation[1]=this.start.rotation[1]+a*f,this.result.rotation[2]=this.start.rotation[2]+a*g,this.result.rotationPost[0]=this.start.rotationPost[0]+a*h,this.result.rotationPost[1]=this.start.rotationPost[1]+a*i,this.result.rotationPost[2]=this.start.rotationPost[2]+a*j,this.result.skew[0]=this.start.skew[0]+a*m,this.result.skew[1]=this.start.skew[1]+a*n,this.result.scale[0]=this.start.scale[0]+a*k,this.result.scale[1]=this.start.scale[1]+a*l,void 0!==this.end.width&&(this.result.width=this.start.width+a*o),void 0!==this.end.height&&(this.result.height=this.start.height+a*p),void 0!==this.end.opacity&&(this.result.opacity=this.start.opacity+a*q)},a.StateTweener.prototype.asMatrix=function(){return this.result.asMatrix()},a.StateTweener.prototype.getProperties=function(){return this.result.getProperties()},a.StateTweener.prototype.setReverse=function(){var a=this.start;this.start=this.end,this.end=a},a.ValueFeederTweener=function(b,c,d,e){this.currentMatrix=b(0,new a.Matrix),this.valueFeeder=b,this.start=c,this.end=d,this.result=e},a.ValueFeederTweener.prototype.tween=function(a){this.reverse&&(a=1-a),this.currentMatrix.clear(),this.currentMatrix=this.valueFeeder(a,this.currentMatrix);var b=this.end.width-this.start.width,c=this.end.height-this.start.height,d=this.end.opacity-this.start.opacity;void 0!==this.end.width&&(this.result.width=this.start.width+a*b),void 0!==this.end.height&&(this.result.height=this.start.height+a*c),void 0!==this.end.opacity&&(this.result.opacity=this.start.opacity+a*d)},a.ValueFeederTweener.prototype.asMatrix=function(){return this.currentMatrix},a.ValueFeederTweener.prototype.getProperties=function(){return this.result.getProperties()},a.ValueFeederTweener.prototype.setReverse=function(){this.reverse=!0};var a=a||{};return a.optionOrDefault=function(a,b){return"undefined"==typeof a?b:a},a.updateElementTransform=function(a,b,c){var d="";c&&(d="perspective("+c+"px) ");var e=b.asCSS();a.style.webkitTransform=d+e,a.style.transform=d+e},a.updateElementProperties=function(a,b){for(var c in b)a.style[c]=b[c]},a.isFunction=function(a){return"function"==typeof a},a.cloneObject=function(a){if(!a)return a;var b={};for(var c in a)b[c]=a[c];return b},a});

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	// Generated by CoffeeScript 1.6.3
	var linear_partition, min;

	min = function(arr) {
	  var computed, result, x, _i, _len;
	  for (_i = 0, _len = arr.length; _i < _len; _i++) {
	    x = arr[_i];
	    computed = x[0];
	    if (!result || computed < result.computed) {
	      result = {
	        value: x,
	        computed: computed
	      };
	    }
	  }
	  return result.value;
	};

	linear_partition = function(seq, k) {
	  var ans, i, j, m, n, solution, table, x, y, _i, _j, _k, _l;
	  n = seq.length;
	  if (k <= 0) {
	    return [];
	  }
	  if (k > n) {
	    return seq.map(function(x) {
	      return [x];
	    });
	  }
	  table = (function() {
	    var _i, _results;
	    _results = [];
	    for (y = _i = 0; 0 <= n ? _i < n : _i > n; y = 0 <= n ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _results1;
	        _results1 = [];
	        for (x = _j = 0; 0 <= k ? _j < k : _j > k; x = 0 <= k ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  solution = (function() {
	    var _i, _ref, _results;
	    _results = [];
	    for (y = _i = 0, _ref = n - 1; 0 <= _ref ? _i < _ref : _i > _ref; y = 0 <= _ref ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _ref1, _results1;
	        _results1 = [];
	        for (x = _j = 0, _ref1 = k - 1; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  for (i = _i = 0; 0 <= n ? _i < n : _i > n; i = 0 <= n ? ++_i : --_i) {
	    table[i][0] = seq[i] + (i ? table[i - 1][0] : 0);
	  }
	  for (j = _j = 0; 0 <= k ? _j < k : _j > k; j = 0 <= k ? ++_j : --_j) {
	    table[0][j] = seq[0];
	  }
	  for (i = _k = 1; 1 <= n ? _k < n : _k > n; i = 1 <= n ? ++_k : --_k) {
	    for (j = _l = 1; 1 <= k ? _l < k : _l > k; j = 1 <= k ? ++_l : --_l) {
	      m = min((function() {
	        var _m, _results;
	        _results = [];
	        for (x = _m = 0; 0 <= i ? _m < i : _m > i; x = 0 <= i ? ++_m : --_m) {
	          _results.push([Math.max(table[x][j - 1], table[i][0] - table[x][0]), x]);
	        }
	        return _results;
	      })());
	      table[i][j] = m[0];
	      solution[i - 1][j - 1] = m[1];
	    }
	  }
	  n = n - 1;
	  k = k - 2;
	  ans = [];
	  while (k >= 0) {
	    ans = [
	      (function() {
	        var _m, _ref, _ref1, _results;
	        _results = [];
	        for (i = _m = _ref = solution[n - 1][k] + 1, _ref1 = n + 1; _ref <= _ref1 ? _m < _ref1 : _m > _ref1; i = _ref <= _ref1 ? ++_m : --_m) {
	          _results.push(seq[i]);
	        }
	        return _results;
	      })()
	    ].concat(ans);
	    n = solution[n - 1][k];
	    k = k - 1;
	  }
	  return [
	    (function() {
	      var _m, _ref, _results;
	      _results = [];
	      for (i = _m = 0, _ref = n + 1; 0 <= _ref ? _m < _ref : _m > _ref; i = 0 <= _ref ? ++_m : --_m) {
	        _results.push(seq[i]);
	      }
	      return _results;
	    })()
	  ].concat(ans);
	};

	module.exports = function(seq, k) {
	  if (k <= 0) {
	    return [];
	  }
	  while (k) {
	    try {
	      return linear_partition(seq, k--);
	    } catch (_error) {}
	  }
	};


/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	/*

	    countUp.js
	    by @hamedb89

	*/

	// target = id of html element or var of previously selected html element where counting occurs
	// startVal = the value you want to begin at
	// endVal = the value you want to arrive at
	// decimals = number of decimal places, default 0
	// duration = duration of animation in seconds, default 2
	// options = optional object of options (see below)

	module.exports = function(target, startVal, endVal, decimals, duration, options) {
	    // make sure requestAnimationFrame and cancelAnimationFrame are defined
	    // polyfill for browsers without native support
	    // by Opera engineer Erik Möller
	    var lastTime = 0;
	    var vendors = ['webkit', 'moz', 'ms', 'o'];
	    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
	        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
	        window.cancelAnimationFrame =
	          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	    }
	    if (!window.requestAnimationFrame) {
	        window.requestAnimationFrame = function(callback, element) {
	            var currTime = new Date().getTime();
	            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
	            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
	              timeToCall);
	            lastTime = currTime + timeToCall;
	            return id;
	        }
	    }
	    if (!window.cancelAnimationFrame) {
	        window.cancelAnimationFrame = function(id) {
	            clearTimeout(id);
	        }
	    }

	     // default options
	    this.options = options || {
	        useEasing : true, // toggle easing
	        useGrouping : true, // 1,000,000 vs 1000000
	        separator : ',', // character to use as a separator
	        decimal : '.', // character to use as a decimal
	    }
	    if (this.options.separator == '') this.options.useGrouping = false;
	    if (this.options.prefix == null) this.options.prefix = '';
	    if (this.options.suffix == null) this.options.suffix = '';

	    var self = this;

	    this.d = (typeof target === 'string') ? document.getElementById(target) : target;
	    this.startVal = Number(startVal);
	    this.endVal = Number(endVal);
	    this.countDown = (this.startVal > this.endVal) ? true : false;
	    this.startTime = null;
	    this.timestamp = null;
	    this.remaining = null;
	    this.frameVal = this.startVal;
	    this.rAF = null;
	    this.decimals = Math.max(0, decimals || 0);
	    this.dec = Math.pow(10, this.decimals);
	    this.duration = duration * 1000 || 2000;

	    this.version = function () { return '1.3.2' }

	    // Print value to target
	    this.printValue = function(value) {
	        var result = (!isNaN(value)) ? self.formatNumber(value) : '--';
	        if (self.d.tagName == 'INPUT') {
	            this.d.value = result;
	        }
	        else if (self.d.tagName == 'text') {
	            this.d.textContent = result;
	        }
	        else {
	            this.d.innerHTML = result;
	        }
	    }

	    // Robert Penner's easeOutExpo
	    this.easeOutExpo = function(t, b, c, d) {
	        return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b;
	    }
	    this.count = function(timestamp) {

	        if (self.startTime === null) self.startTime = timestamp;

	        self.timestamp = timestamp;

	        var progress = timestamp - self.startTime;
	        self.remaining = self.duration - progress;

	        // to ease or not to ease
	        if (self.options.useEasing) {
	            if (self.countDown) {
	                var i = self.easeOutExpo(progress, 0, self.startVal - self.endVal, self.duration);
	                self.frameVal = self.startVal - i;
	            } else {
	                self.frameVal = self.easeOutExpo(progress, self.startVal, self.endVal - self.startVal, self.duration);
	            }
	        } else {
	            if (self.countDown) {
	                var i = (self.startVal - self.endVal) * (progress / self.duration);
	                self.frameVal = self.startVal - i;
	            } else {
	                self.frameVal = self.startVal + (self.endVal - self.startVal) * (progress / self.duration);
	            }
	        }

	        // don't go past endVal since progress can exceed duration in the last frame
	        if (self.countDown) {
	            self.frameVal = (self.frameVal < self.endVal) ? self.endVal : self.frameVal;
	        } else {
	            self.frameVal = (self.frameVal > self.endVal) ? self.endVal : self.frameVal;
	        }

	        // decimal
	        self.frameVal = Math.round(self.frameVal*self.dec)/self.dec;

	        // format and print value
	        self.printValue(self.frameVal);

	        // whether to continue
	        if (progress < self.duration) {
	            self.rAF = requestAnimationFrame(self.count);
	        } else {
	            if (self.callback != null) self.callback();
	        }
	    }
	    this.start = function(callback) {
	        self.callback = callback;
	        // make sure values are valid
	        if (!isNaN(self.endVal) && !isNaN(self.startVal)) {
	            self.rAF = requestAnimationFrame(self.count);
	        } else {
	            console.log('countUp error: startVal or endVal is not a number');
	            self.printValue();
	        }
	        return false;
	    }
	    this.stop = function() {
	        cancelAnimationFrame(self.rAF);
	    }
	    this.reset = function() {
	        self.startTime = null;
	        self.startVal = startVal;
	        cancelAnimationFrame(self.rAF);
	        self.printValue(self.startVal);
	    }
	    this.resume = function() {
	        self.stop();
	        self.startTime = null;
	        self.duration = self.remaining;
	        self.startVal = self.frameVal;
	        requestAnimationFrame(self.count);
	    }
	    this.formatNumber = function(nStr) {
	        nStr = nStr.toFixed(self.decimals);
	        nStr += '';
	        var x, x1, x2, rgx;
	        x = nStr.split('.');
	        x1 = x[0];
	        x2 = x.length > 1 ? self.options.decimal + x[1] : '';
	        rgx = /(\d+)(\d{3})/;
	        if (self.options.useGrouping) {
	            while (rgx.test(x1)) {
	                x1 = x1.replace(rgx, '$1' + self.options.separator + '$2');
	            }
	        }
	        return self.options.prefix + x1 + x2 + self.options.suffix;
	    }

	    // format startVal on initialization
	    self.printValue(self.startVal);
	};

	// Example:
	// var numAnim = new countUp("SomeElementYouWantToAnimate", 0, 99.99, 2, 2.5);
	// numAnim.start();
	// with optional callback:
	// numAnim.start(someMethodToCallOnComplete);


/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(45);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/avatar.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/avatar.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, ".ssavatar__wrapper{float:right;text-align:right;padding-right:0.85em;font-size:0.75em;padding-bottom:0.85em}", ""]);

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(47);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(40)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.header.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.header.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(50)();
	exports.push([module.id, "textarea.description,input.url,input.display_name{padding:0.2em;display:block;width:100%;background-color:transparent}textarea.description{font-size:1.1em}a.update_link{width:50%;display:block;background-color:#76a698;cursor:pointer;color:#fff;padding:0.7em;margin-top:1em;font-size:0.9em;border-radius:5px;text-align:center}@media only screen and (max-width: 960px){a.update_link{margin:1em auto;}}@media only screen and (max-width: 960px){.profile_personal label{width:75%;display:block;text-align:center;margin-left:10em;}}textarea.description:focus,input.url:focus,input.display_name:focus{background-color:rgba(255,255,255,0.7);color:#233;font-weight:bold}.profile_personal .description_label.hint--left:before,.profile_personal .description_label.hint--left:after,.profile_personal .description_label.hint--right:before,.profile_personal .description_label.hint--right:after{bottom:80%}", ""]);

/***/ },
/* 48 */,
/* 49 */,
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ }
/******/ ])