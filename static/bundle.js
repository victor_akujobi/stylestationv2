/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var page = __webpack_require__(12)
	var Profile = __webpack_require__ (10)
	var Entry = __webpack_require__(11)



	var grid = __webpack_require__(1)
	var Entries = __webpack_require__(2)
	var EntryPage = __webpack_require__(3)
	var profile = __webpack_require__(4)
	var login = __webpack_require__(5)
	__webpack_require__(6)
	__webpack_require__(7)


	__webpack_require__(8)
	var ajax = __webpack_require__(37)
	var API_ENDPOINT = ""
	var APP_CONTAINER = document.getElementById("content")
	var updateRoute = (function () {
		var NAV_WRAPPER = document.getElementById("nav_wrapper")
		return function (path, ctx, next) {
			NAV_WRAPPER.className = path
			next()
		}
	})()
	var loadMe = function (ctx, next) {
		if (!!window.bootstrap) {
			ctx.profile = new Profile(window.bootstrap.profile)
			window.bootstrap = null
			next()
		} else {
			ajax
			.get("/me")
			.set("Response", "json")
			.end(function (err, response) {
				if (err) {
				} else {
					ctx.profile = new Profile(response.body)
					next()
				}
			})
		}
	}
	var loadProfile = function (ctx, next) {
		if (!!window.bootstrap) {
			ctx.profile = new Profile(window.bootstrap.profile)
			window.bootstrap = null
			next()
		} else {
			ajax
			.get("/profile/" + ctx.params.ref)
			.set("Response", "json")
			.end(function (err, response) {
				if (err) {
				} else {
					ctx.profile = new Profile(response.body)
					next()
				}
			})
		}
	}
	var loadStyle = function (ctx, next) {
		if (!!window.bootstrap) {
			ctx.entry = new Entry(window.bootstrap.entry)
			window.bootstrap = null
			next()
		} else {
			ajax
			.get("/a/" + ctx.params.entry)
			.set("Response", "json")
			.end(function (err, response) {
				if (err) {
				} else {
					ctx.entry = new Entry(response.body)
					next()
				}
			})
		}
	}
	var loadStyles = function (ctx, next) {
		if (!!window.bootstrap) {
			ctx.entries = window.bootstrap.entries.map(function (entry) {return new Entry(entry)})
			window.bootstrap = null
			next()
		} else {

			ajax
			.post("/search")
			.send({
				Colours: ctx.colours,
				Brands: ctx.brands,
				Styles: ctx.styles
			})
			.set("Response", "json")
			.end(function (err, response) {
				if (err) {
				} else {
					ctx.entries = response.body.map(function (entry) {return new Entry(entry)})
					next()
					// riot.update()
				}
			})
		}
	}

	var matchParams = function (string, regex, index) {
		index || (index = 1);
		var matches = [];
		var match;
		while (match = regex.exec(string)) {
			matches.push(match[index]);
		}
		return matches;
	}
	extractParams = function (ctx, next) {
		styles = matchParams(ctx.querystring, /s:(\w+)/g)
		colours = matchParams(ctx.querystring, /c:(\w+)/g)
		brands = matchParams(ctx.querystring, /b:(\w+)/g)
		ctx.styles = styles
		ctx.brands = brands
		ctx.colours = colours
		next()
	}

	var MOBILE_NAV_TOGGLE = document.getElementById('mobile__hamburger__toggle')
	var APP_ROOT_CONTAINER = document.getElementById('content')
	var OVERLAY_CONTAINER = document.getElementById('overlay')
	page('*', function(ctx,  next){
		MOBILE_NAV_TOGGLE.checked = false
		next()
	})
	// m.route(document.getElementById('content'), '/', {
	page('/a/:entry', loadStyle, updateRoute.bind(null, 'explore'), function (ctx, next) {
		// var entry = ctx.params.entry
		var entry = new Entry(ctx.entry)
		riot.mount(APP_CONTAINER, "entry", {entry:entry})

	})

	page('/new', updateRoute.bind(null, 'new'), function () {

	riot.mount(APP_ROOT_CONTAINER, "new-page", {entry: new Entry({})})
	})
	page('/profile/:ref', loadProfile, function (ctx) {
		riot.mount(APP_CONTAINER, "profile", {profile: ctx.profile})
	})

	page('/me', updateRoute.bind(null, ''), loadMe, function (ctx) {
		riot.mount(APP_CONTAINER, "profile-edit", {profile: ctx.profile})
	})

	page('/edit/:ref', updateRoute.bind(null, 'new'), function (ctx) {
		var entryId = ctx.params.ref
		// m.module(APP_ROOT_CONTAINER, Middleware(newPage, {editMode: true, ref: entryId}))
	})
	page('/login', updateRoute.bind(null, ''), function (ctx) {
		document.body.style.overflow = "hidden"
		OVERLAY_CONTAINER.className = "overlay overlay--open"
		ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "login", back:page.back})
	})
	page.exit('/login', function (ctx, next) {
		document.body.style.overflow = "scroll"	
		OVERLAY_CONTAINER.className = "overlay overlay--closed"
		!!ctx.overlay.length && ctx.overlay[0].unmount(true)
		ctx.overlay = null
		next()
	})
	page('/register', updateRoute.bind(null, ''), function (ctx) {
		document.body.style.overflow = "hidden"
		OVERLAY_CONTAINER.className = "overlay overlay--open"
		ctx.overlay = riot.mount(OVERLAY_CONTAINER, "login", {mode: "register", back:page.back})
	})
	page.exit('/register', function (ctx, next) {
		OVERLAY_CONTAINER.className = "overlay overlay--closed"
		document.body.style.overflow = "scroll"
		!!ctx.overlay.length && ctx.overlay[0].unmount(true)
		ctx.overlay = null
		next()
	})
	page('/' , updateRoute.bind(null, 'explore'), loadStyles, function (ctx) {
		var entries = (ctx.entries || []).map(function (entry) {
			return new Entry(entry)
		})
		riot.mount(APP_CONTAINER, "entries", {entries: entries, page: 1})
	})
	page('/explore' , updateRoute.bind(null, 'explore'), extractParams, loadStyles, function (ctx) {
		var entries = (ctx.entries || []).map(function (entry) {
			return new Entry(entry)
		})
		riot.mount(APP_CONTAINER, "entries", {entries: entries, page: 1})
	})
	page()
	var EVENTS = {
		DISMISS_OVERLAY: "dismissOverlay"
	}

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var avatar = __webpack_require__(14)
	var partition = __webpack_require__(38)
	riot.tag('grid', '<div name="grid_container" style=""> <div style="padding: 0.5em 0; display:inline-block" each="{metric, i in metrics}"> <a href="/a/{parent.items[i].Ref}"> <div riot-style="width: {metric[0]}px; height: {metric[1]}px; background-size: cover; background-image: url(/static/images/ankara/{parent.items[i].Image}_m.jpg)"> </div> </a> <div if="{parent.details}" riot-style="color: #788; padding: 5px 0px; width: {metric[0]}px; overflow:hidden"> <avatar ref="{parent.items[i].OwnerRef}" name="{parent.items[i].OwnerName}" image="{null}"></avatar> </div> </div> </div>', function(opts) {
			var debounce = function (callback, timeout) {
				var t
				return function () {
					window.clearTimeout(t)		
					t = window.setTimeout(callback, timeout)
				}
			}
			this.details = opts.details
			this.metrics = []
			this.arrangeItems = function () {
				this.items = opts.items

				var elementWidth = this.root.children[0].clientWidth
				var INIT_HEIGHT = 150;
				var MAX_HEIGHT = 500;
				
				var widths = this.items.map(function(item) {
					return  INIT_HEIGHT * item.ImageWidth / item.ImageHeight
				})
				var partitions = partition(widths, opts.rows || Math.ceil(this.items.length/Math.ceil(elementWidth/350)))
				var rows = (partitions||[]).map(function (row) {
					var sum = row.reduce(function (total, item) {
						return total + item
					}, 0)
					var ratio = elementWidth / sum
						return row.map(function (item) {
							return [ratio * item, ratio * INIT_HEIGHT]
						})
					})

				this.metrics = [].concat.apply([], rows)
				this.update()
			}
			this.on('mount',  function () {
				var callback = this.arrangeItems.bind(this)
				callback()
				var currentResize = window.onresize
				console.log(window.onresize)
				window.onresize = debounce(function () {
					currentResize && currentResize()
					callback()

				}, 50)
			})
			this.on('update', this.arrangeItems.bind(this))
			this.on('unmount', function () {
				window.onresize = null
			})
		
	});

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var pagination = __webpack_require__(15)
	riot.tag('entries', '<grid details="{true}" items="{entries}"> </grid> <pagination page="{page}"> </pagination>', function(opts) {
			this.entries = opts.entries
			this.page = opts.page
		
	});

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var colours = __webpack_require__(16);
	__webpack_require__(17);
	var fonts = __webpack_require__(19)
	var utils = __webpack_require__(20)
	var avatar = __webpack_require__(14)
	__webpack_require__(21)
	var grid = __webpack_require__(1)
	var flexgrid = __webpack_require__(23)
	riot.tag('entry', '<div class="ssdetail__wrapper"> <div class="entry_wrapper"> <div class="ssdetail__entry"> <div class="ssdetail__image"><img class="image" riot-src="/static/images/ankara/{entry.Image}_m.jpg"> </div> <div class="sssharebar__wrapper"> <p class="share_text">Psst... If you love this style, you can share it on </p> <a href="http://www.facebook.com/sharer/sharer.php?u={url}&title={entry.Description}" target="_blank" class="share_link facebook_share"> <raw value="{FONTS[\'facebook\']}"></raw> </a> <a href="{"http://twitter.com/intent/tweet?status=StyleStation.co Entry+" + url}" target="_blank" class="share_link twitter_share"> <raw value="{FONTS[\'twitter\']}"></raw> </a> <a href="{"http://pinterest.com/pin/create/bookmarklet/?media=[MEDIA]&url=" + url + "&is_video=false&description=" + entry.Description}" target="_blank" class="share_link pinterest_share"> <raw value="{FONTS[\'pinterest\']}"></raw> </a> <a target="_blank" class="share_link email_share"><raw value="{FONTS[\'email\']}"></raw></a> </div> </div> <div class="ssdetail__info"> <a style="text-decoration:none; font-size: 2em;" href={entry.Edit ? \'/me\' : \'/profile/{entry.OwnerRef}\'}><p class="owner"><avatar name="{entry.OwnerName}" ref="{entry.OwnerRef}"></avatar></p></a> <p class="description">{entry.Description}</p> <div class="stats"> <ul> <li> <span class="stats__stat"> <count value="{entry.Views}"></count> </span> <span class="stats__label stats__views"> Views </span></li> <li><span class="stats__stat"><count value="{entry.Favourites}"></count></span><span class="stats__label stats__things">{entry.Favourites == 1 ? \'Favourite\' : \'Favourites\'}</span></li> </ul> </div> <div style=" background-color:#fff; padding:0; box-shadow:0 0 2px rgba(0, 0, 0, 0.1); margin: 2em auto 0.5em auto" class="tag_wrapper"> <p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 0.2em">Style</p> <a href="/explore?q=s:{style}" each="{style, i in entry.Styles}" class="tag"> <span class="tag_icon"> <raw value="{parent.FONTS[style.toLowerCase()]}"></raw> </span> <span class="tag_text"> {style} </span> </a> </div> <div style="" class="colour_wrapper"> <p style="background-color:#fdfdfd; padding:10px; font-size: 1.2em; text-align:center; margin-bottom: 1em">Colours</p> <a href="/explore?q=c:{colour}" each="{colour, index in entry.Colours}" data-hint="{colour}" class="hint--bottom colour" riot-style="width: {~~(90/parent.entry.Colours.length || 1)}%; background-color: {parent.COLOURS[colour]}"></a> </div> <div> <p onclick="{entry.Favourite}" class="{"favourite favourite--" + (entry.IsInFavourites ? "added" : "false")}">{entry.IsInFavourites ? "Added" : "+ Add"} To Favourites</p> </div> </div> </div> <p>We think you might like</p> <div if="{!!relatedEntries.length}" style="width: 100%" class=""> <grid rows="{document.body.clientWidth > 320 ? 1 : 2}" items="{relatedEntries}" /></grid> </div>', '@import url(../SCSS/detail.scss);', function(opts) {
				this.entry = opts.entry
				this.COLOURS = colours.COLOURS
				this.FONTS = fonts
				this.relatedEntries = []
				this.entry.getRelatedEntries(function (response) {
					this.relatedEntries = response
					this.update()
				}.bind(this))
			
	});

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	riot.tag('profile', '<div class="profile__header" style="color:#fff; min-height: 200px;background: transparent url(/static/images/hero.jpg) center center no-repeat; background-size: cover"> <div style="transform: translateY(50%); font-size: 2.5em; color:#fff"> <avatar name="{opts.profile.DisplayName}"></avatar> <p class="profile__description">{opts.profile.Description || opts.profile.DisplayName + " has not written a description yet"}</p> </div> </div> <div class="profile_main"> <div class="profile__section profile__section--uploads"> <h5 class="profile__header__text" stat="{45}">{opts.profile.DisplayName.split(" ")[0]}\'s Uploads</h5> <div class="profile__section__body"> <div if="{!!uploads.length}"> <grid if="{!!uploads.length}" items="{uploads}"></grid> </div> </div> </div> <div class="profile__section profile__section--uploads"> <h5 class="profile__header__text" stat="{45}">{opts.profile.DisplayName.split(" ")[0]}\'s Favourites</h5> <div class="profile__section__body"> <div if="{!!favourites.length}"> <grid if="{!!favourites.length}" items="{favourites}"></grid> </div> </div> </div> <div class="profile__section profile__section--uploads"> <h5 class="profile__header__text" stat="{45}">{opts.profile.DisplayName.split(" ")[0]}\'s Collections</h5> <div class="profile__section__body"> <div if="{!!collections.length}"> <grid if="{!!collections.length}" items="{collections}"></grid> </div> </div> </div> </div>', '.profile__header__text { text-align:center; font-size: 1.2em; color: #000; margin: 0 auto; } .profile__header__text:after { content:attr(stat); display:inline-block; padding: 0.2em; border-radius: 7px; color:#fff; margin-left: 0.5em; font-size: 0.9em; background-color: #ccc; box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2); text-shadow: 0 1px 2px rgba(255, 255, 255, 0.4); } .profile__header { text-align:center; } .profile__description { font-size:0.4em; margin-top:0.5em; } .profile__section { margin-top: 1em; }', function(opts) {
		var self = this
		this.uploads = []
		this.collections = []
		this.favourites = []
		this.loading = true
		this.on("mount", function () {

			opts.profile.getUserDashboard(function (response) {
				console.log(response)
				self.update({uploads: response.Uploads || [], favourites: response.Favourites || [], collections: response.Collections || [], loading: false})
			})
		})
		
	});

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	__webpack_require__(24)
	riot.tag('login', '<div class="sslogin__wrapper"> <p class="close" onclick="{opts.back}">&times;</p> <p class="sslogin__intro_text">Not {opts.mode == \'register\' ? \'registered\' : \'logged in\'}? Ok then, let&apos;s get you sorted.<br> It takes less than 30 seconds to get started, and we promise not to touch your timeline, pinky swear</p> <a rel="external" href="/login?provider=facebook" class="srlogin__button srlogin__button__facebook"> <span class="srlogin__provider__icon srlogin__provider__facebook"><raw value="&#xE60F;"></raw></span> <span class="srlogin__provider__text">{opts.mode == \'register\' ? \'Register\' : \'Continue\'} with your Facebook Account</span> </a> <a rel="external" href="/login?provider=gplus" class="srlogin__button srlogin__button__google"> <span class="srlogin__provider__icon srlogin__provider__google"><raw value="&#xE610;"></raw></span> <span class="srlogin__provider__text">{opts.mode == \'register\' ? \'Register\' : \'Continue\'} with your Google Account</span> </a> <a rel="external" href="/login?provider=twitter" class="srlogin__button srlogin__button__twitter"> <span class="srlogin__provider__icon srlogin__provider__twitter"><raw value="&#xE60E;"></raw></span> <span class="srlogin__provider__text">{opts.mode == \'register\' ? \'Register\' : \'Continue\'} with your Twitter Account</span> </a> <a if="{opts.mode == \'register\'}" style="display:block; color:inherit" href="/explore" class="not_ready">I\'m not ready for a relationship, I just want to browse</a> </div>', function(opts) {

	});

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);


	__webpack_require__(26)
	var entryImage = __webpack_require__(28)
	__webpack_require__(29)
	__webpack_require__(30)
	__webpack_require__(31)
	__webpack_require__(33)
	__webpack_require__(20)
	var riot = __webpack_require__(13)
	var page = __webpack_require__(12)

	riot.tag('new-page', '<div class="ssnewentry__wrapper"> <message visibility="{(!isValid && !!message) ? \'visible\': \'hidden\'}" dismiss="{clearMessages}" type="{messageType}" message="{message}"></message> <div class="ssentry__image__wrapper"> <entry-image onchange="{onImageChange}" image="{entry.Image}"></entry-image> </div> <div class="ssentry__details__wrapper"> <input placeholder="Click to enter description" type="text" onchange="{updateEntryDescription}" value="{entry.Description}" class="ssentry__descriptioninput"> <div class="sstags__stylepickerwrapper"> <h4 class="sstags__stylepicker">GENDER</h4> <genders defaultgenders="{defaultGenders}" toggle="{updateGender}" selectedgender="{entry.Gender}"></genders> <br style="clear:both"> </div> <div class="sstags__stylepickerwrapper"> <h4 class="sstags__stylepicker">STYLES</h4> <tag toggle="{parent.toggleEntryStyles.bind(null, style)}" isselected="{parent.entry.Styles.indexOf(style) !== -1}" gender="{parent.entry.Gender}" each="{style in STYLES[entry.Gender]}" text="{style}"></tag> <br style="clear:both"> </div> <div class="sstags__stylepickerwrapper"> <h4 class="sstags__stylepicker">COLOURS</h4> <colours label="{text}" isselected="{parent.entry.Colours.indexOf(text) !== -1}" background="{parent.COLOURS[text]}" each="{text, colour in COLOURS}" toggle="{parent.toggleEntryColours.bind(null, text)}"></colours> <br style="clear:both"> </div> <button class="ssnewentry__submit" onclick="{isValid ? submitEntry : showErrors}"> Done </button> </div> </div>', function(opts) {
		this.STYLES = { 
				"Female": ["Top", "Trousers", "Shorts", "Blazer", "Accessories", "Dress", "Skirt"],
				"Male": ["Top", "Trousers", "Shorts", "Blazer", "Accessories"]
		};
		this.COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
		this.defaultGenders = {"Male": "&#xE60C;", "Female": "&#xE60B;"}

		this.entry = opts.entry
		var self = this
		this.validateEntry = function (entry) {
			self.isValid = entry.Validate()
			console.log(self.isValid)

		}
		this.submitEntry = function () {
			self.update({isValid: false})
			var successCallback = function () {
				page("/")

			}
			var errorCallback = function (responseCode) {

				if (responseCode == 403) {
					localStorage.entry = JSON.stringify(self.entry)
					page("/login")
				}
			}
			self.entry.Save(successCallback, errorCallback)
		}
		this.clearMessages = function () {
			self.update({message: null, messageType: false})
		}
		this.showErrors = function () {
			var message;
			if (!self.entry.Description) {
				message = "Don't forget to tell us a bit about your style. Add some text in the description box"
			} else if (!self.entry.Colours.length) {
				message = "You have not selected any colours. I'm sure everyone would love to know what colours you're wearing!"
			} else if (!self.entry.Styles.length) {
				message = "You have not selected any styles. Hey now, don't keep all that knowledge to yourself!"
			} else if (!self.entry.Image) {
				message = "You have not added any images. You're not camera shy, are you?"
			} else if (!self.entry.ImageHeight || !self.entry.ImageWidth) {
				message = "Hmm... something's wrong with the image you've tried to upload"
			} else {
				message = "Oh noes!!, something happened, and we don't what it is yet. Would you mind reloading the page and trying again"
			}
			self.update({message: message, messageType: 'error'})
		}
		this.toggleEntryStyles = function (style) {
			self.entry.toggleStyle(style)
			self.validateEntry(self.entry)
			self.update()
		}
		this.toggleEntryColours = function (colour) {
			self.entry.toggleColour(colour)
			self.validateEntry(self.entry)
			self.update()
		}
		this.updateEntryDescription = function (e) {
			self.entry.Description = e.target.value
			self.validateEntry(self.entry)
			console.log(self.entry)
		}
		this.updateGender = function (gender) {
			self.entry.toggleGender(gender)
			self.validateEntry(self.entry)
			self.update()
		}
		this.onImageChange = function (props) {
			self.entry.onImageChange(props)
			self.validateEntry(self.entry)
			self.update()
		}

	});



/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	__webpack_require__(34)
	riot.tag('profile-edit', '<div style="" class="profile__editor__header"> <div class="header_contents"> <p class="header__avatar__wrapper"> <avatar name="{profile.DisplayName}"></avatar> </p> <p class="header__profile__description">{profile.Description}</p> </div> </div> <div class="profile__editor__body"> <h4 style="text-align:center;">UPDATE YOUR PROFILE</h4> <div class="profile__body__wrapper"> <form onsubmit="{saveProfile}"> <div> <p><span class="profile__input__label">BIO</span> <input class="profile__input" name="description" onchange="{updateProfile.bind(null, \'Description\')}" value="{profile.Description}"></p> </div> <div> <p> <span style="display:inline-block; vertical-align:middle">ARE YOU A BRAND?</span><input class="profile__input--isbrand" type="checkbox" onchange="{updateProfile.bind(null, \'isBrand\')}" id="isbrand" name="isbrand"> <label style="display:inline-block; vertical-align:middle; margin-left:1.7em" for="isbrand" on="YES" off="NO" class="switch__toggle__wrapper"> <span class="switch__toggle"></span> </label> <input class="profile__input profile__input--brandname" placeholder="Tell us your brand name" name="brandname" value="{profile.BrandName}" onchange="{updateProfile.bind(null, \'BrandName\')}"> <span style="margin-top:0; font-size: 0.75em">Please note that your brand name is subject to approval</span> </p> </div> <div> <p> <span class="profile__input__label">PHONE NUMBER<span style="margin-left:0.3em; font-size: 0.8em;">(Optional)</span></span> <input class="profile__input" name="phone" type="phone" onchange="{updateProfile.bind(null, \'Phone\')}" placeholder="0712345872"> </p> </div> <div> <p> <span class="profile__input__label">EMAIL </span><input class="profile__input" type="email" name="email" value="{profile.Email}" onchange="{updateProfile.bind(null, \'Email\')}" placeholder="me@mywebsite.com"> </p> </div> <div> <p> <span class="profile__input__label">WEBSITE<span style="margin-left:0.3em; font-size: 0.8em;">(Optional)</span></span> <input class="profile__input" type="website" name="website" value="{profile.Url}" onchange="{updateProfile.bind(null, \'Url\')}" placeholder="www.mywebsite.com"> </p> </div> <div><button class="profile__done__button">DONE</button></div> </div> </form> </div>', function(opts) {
			var self = this
			this.profile = opts.profile
			this.saveProfile = function (e) {
				e.preventDefault && e.preventDefault()
				console.log(this.profile)
				return false
			}
			this.updateProfile = function (property, evt) {
				var newValue
				console.log(typeof evt)
				if (typeof evt === "string") {
					newValue = evt
				} else {
					switch(evt.target.type) {
						case 'checkbox':
						case 'radio':
							newValue = evt.target.checked
						break
						default:
							newValue = evt.target.value
						break
					}
				}
				self.profile.UpdateValue(property, newValue)
				console.log(self.profile)
				self.update()
			}
		
	});

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(9);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/header.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/header.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, "body{font:1em/normal Lato,sans-serif;margin:0;overflow-y:scroll}.overlay{position:fixed;background:rgba(0,0,0,0.85)}.overlay--open{top:0;bottom:0;left:0;right:0;width:100%;height:100%;display:flex}.mobile__hamburger__wrapper{display:none}@media only screen and (max-width: 960px){.mobile__hamburger__wrapper{cursor:pointer;display:block;width:30px;float:right;padding:0 0.3em;border-radius:2px;box-sizing:border-box;}}.mobile__hamburger__wrapper .mobile__hamburger{transform-origin:50% 50%;height:3px;margin:4px 0;border-radius:2px;border:none;background-color:#999;transition:transform 0.2s ease-out,opacity 0.1s linear}.mobile__hamburger__toggle{height:0;width:0;margin:0;padding:0;visibility:hidden}@media only screen and (max-width: 960px){.header .logo{flex:1 0 200px !important}.header .nav_menu{flex:3 0 0px !important}.menubar__wrapper{display:block;z-index:2;position:absolute;top:100%;right:0;overflow:hidden;max-height:0}.mobile__hamburger__toggle:checked+.nav_menu .mobile__hamburger--bottom{opacity:0}.mobile__hamburger__toggle:checked+.nav_menu .mobile__hamburger--middle{transform:rotateZ(-45deg)}.mobile__hamburger__toggle:checked+.nav_menu .mobile__hamburger--top{transform:translate(0, 7px) rotateZ(45deg)}.mobile__hamburger__toggle:checked+.nav_menu .menubar__wrapper:before{content:'';width:0;height:0;border:10px solid transparent;border-bottom:10px solid #fbfbfb;display:block;position:absolute;right:10px;bottom:100%}.mobile__hamburger__toggle:checked+.nav_menu .menubar__wrapper{overflow:visible;margin-top:10px;width:10em;max-height:400px;background-color:#fff;box-shadow:0 1px 3px rgba(0,0,0,0.2);padding:0 0.3em;border-radius:2px}.mobile__hamburger__toggle:checked+.nav_menu .menubar__wrapper .nav_item{color:#565656;display:block;width:100%;text-align:right;padding:0.25em 0}}#content{margin:auto}.header{box-sizing:border-box;display:flex;width:100%;margin:0 auto;height:50px;background-color:#000;border-bottom:1px solid #000;padding:0 10px}.header .logo{flex:1 0 40px;background:transparent url(/static/images/stylestation_dark.png) center center no-repeat;background-size:contain}.header .nav_menu{position:relative;text-transform:uppercase;flex:3 0 250px;margin:auto 10px auto}.header .nav_menu .menubar__wrapper{box-sizing:border-box;margin:auto 0;width:100%;margin:auto auto;text-align:right}.header .nav_menu .menubar__wrapper .nav_item{display:inline-block;width:10%;text-align:center}.header .nav_menu .menubar__wrapper .nav_item a{text-decoration:none;color:#fefefe}@media only screen and (max-width: 960px){.header .nav_menu .menubar__wrapper .nav_item a{color:#666;}}.header .nav_menu .menubar__wrapper .nav_item--join{padding:5px 0;border:1px solid #fff;border-radius:5px;transition:background-color 0.2s ease-out}.header .nav_menu .menubar__wrapper .nav_item--join:hover{background-color:#fff}.header .nav_menu .menubar__wrapper .nav_item--join:hover a{color:#000}@media only screen and (max-width: 960px){.header .nav_menu .menubar__wrapper .nav_item--join{border:none;}.header .nav_menu .menubar__wrapper .nav_item--join:hover{background-color:#fff;color:#000}}", ""]);

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var ajax = __webpack_require__ (37)
	var Profile = function (defaultOptions) {
		this.Ref = defaultOptions.Ref || ""
		this.Description = defaultOptions.Description || ""
		this.URL = defaultOptions.URL || ""
		this.DisplayName = defaultOptions.DisplayName || ""
		this.Collections = defaultOptions.Collection || 0
		this.Uploads = defaultOptions.Uploads || 0
		this.isEditable = defaultOptions.isEditable
		this.isBrand = defaultOptions.isBrand
		this.BrandName = defaultOptions.BrandName || ""
		this.Email = defaultOptions.Email || ""
	}

	Profile.prototype.Subscribe = function () {

	}
	Profile.prototype.DeleteSubscription = function () {

	}
	Profile.prototype.UpdateValue = function (property, value) {
		if (property == 'isBrand' && !value) {
			this.BrandName = ""
		}
		if (property == 'BrandName' && !this.isBrand) {
			// return if profile is not a brand
			return
		}
		this[property] = value
	}
	Profile.prototype.Update = function (onSuccess, onError) {
		if (!this.isEditable) {
			return
		}
		ajax.put("/me")
		.send(this)
		.end(function (err, response) {
			if (err) {
				!!onError && onError(err)
			} else {
				!!onSuccess && onSuccess()
			}
		})
	}
	Profile.prototype.getUserDashboard = function (onSuccess, onError) {
		if (!this.Ref) {
			return
		}
		var url = "/dashboard/" + this.Ref
		ajax.get(url)
		.send(this)
		.end(function (err, response) {
			if (err) {
				!!onError && onError(err)
			} else {
				!!onSuccess && onSuccess(response.body.dashboard)
			}
		})
	}

	module.exports = Profile

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var m = __webpack_require__(48)
	var ajax = __webpack_require__(37)//.set("Response", "json")

	var Entry = function (defaults) {
			var self  = this
			var options = defaults || {}
			this.Ref  = options.Ref || ""
			this.Gender = options.Gender || "Female"
			this.Description = options.Description || ""
			this.Colours = options.Colours || []
			this.Styles = options.Styles || []
			this.Image = options.Image || ""
			this.ImageWidth = options.ImageWidth || ""
			this.ImageHeight = options.ImageHeight || ""
			this.Favourites = options.Favourites || 0
			this.IsInFavourites = options.IsInFavourites || false
			this.Edit = options.Edit || false
			if (!!options.CreatedAt) {
				console.log(options.CreatedAt)
				createdDate = options.CreatedAt.toString()
				this.CreatedAt = new Date(Date.UTC(
					createdDate.substr(0, 4),
					parseInt(createdDate.substr(4, 2))-1,
					createdDate.substr(6,2),
					createdDate.substr(8,2),
					createdDate.substr(10,2),
					createdDate.substr(12,2)
					))
				// console.log(this.CreatedAt)
			}
			//No need to observe the properties below
			this.OwnerName = options.OwnerName || ""
			this.OwnerRef = options.OwnerRef || ""
			this.Views = options.Views || 0
		// this.toggle= function (type, value) {
		//  	var styles = this[type]()
		//  	var index = styles.indexOf(value)
		//  	if (index == -1) {
		//  		styles.push(value)
		//  	} else {
		//  		styles.splice(index, 1)
		//  	}
		//  		this[type](styles)
		// }
		// this.Update = function () {
		// 	var self = this
		// }
		// this.Save = function () {
		// 	self = this
		// 	m.request({"url": "/ankara", "method": "POST", data:self}).then(
		// 		function () {
		// 		}, function () {
		// 		})
		// }
	}

	Entry.prototype.Update = function () {
		var url = "/ankara" + this.Ref
		ajax
		.patch(url)
		.send(this)
		.end(function (err, res) {
			if (err) {
				//Handle Error
				onError && onError(err)
			} else {
				// Do something with response
				onSuccess && onSuccess()
			}
		})
	}
	Entry.prototype.toggleGender = function (gender) {
		if (this.Gender != gender) {
			this.Gender = gender
			this.Styles = []
		}
	}
	Entry.prototype.toggleStyle = function (style, callback) {
		var styleIndex = this.Styles.indexOf(style)
	 	if (styleIndex == -1) {
	 		this.Styles.push(style)
	 	} else {
	 		this.Styles.splice(styleIndex, 1)
	 	}
		console.log(this)
	 	callback && callback()
	}

	Entry.prototype.toggleColour = function (colour) {
		var colourIndex = this.Colours.indexOf(colour)
	 	if (colourIndex == -1) {
	 		this.Colours.push(colour)
	 	} else {
	 		this.Colours.splice(colourIndex, 1)
	 	}
	 	console.log(this)
	}
	Entry.prototype.Save = function (onSuccess, onError) {
		ajax
		.post("/ankara")
		.send(this)
		.end(function (err, res) {
			if (err) {
				console.log('error: ', err)
				//Handle Error
				onError && onError(err.status)
			} else {
				// Do something with response
				onSuccess && onSuccess()
			}
		})
	}

	Entry.prototype.getRelatedEntries = function (onSuccess, onError) {
		if (!this.Ref) {
			return
		}
		var url = "/related/" + this.Ref
		ajax.get(url)
		.end(function (err, response){
			if (err) {
				onError && onError(err.code)
			} else {
				onSuccess && onSuccess(response.body)
			}
		})
	}
	Entry.prototype.onImageChange = function (imageProps, callback) {
		this.Image = imageProps.image;
		this.ImageHeight = imageProps.height
		this.ImageWidth = imageProps.width
		console.log(this, imageProps, callback)
		callback && callback()
	}
	Entry.prototype.Validate = function () {
		return !!this.Image &&
				!!this.Description &&
				!!this.Image &&
				!!this.ImageWidth &&
				!!this.ImageHeight &&
				!!this.Colours.length &&
				!!this.Styles.length

	}
	Entry.prototype.Favourite = function (onSuccess, onError) {
		if (!this.Ref) {
			return
		}
		var url = "/favourite/" + this.Ref
		if (!this.IsInFavourites) {
			this.IsInFavourites = true
			this.Favourites++

			// Update on Server
			ajax.get(url)
			.end(function (err, response) {
				if (err) {
					//Undo all the things
					this.IsInFavourites = false
					this.Favourites--
					onError && onError(err)
				} else {
					onSuccess && onSuccess()
				}
			})
		} else {
			this.IsInFavourites = false
			oldFavouriteCount = this.Favourites
			this.Favourites = Math.max(0,  oldFavouriteCount - 1)
			ajax.delete(url)
			.end(function (err, response) {
				if (err) {
					this.Favourites = oldFavouriteCount
					onError && onError(err)
				} else {
					onSuccess && onSuccess()
				}
			})
		}
	}
	module.exports = Entry

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {  /* globals require, module */

	  'use strict';

	  /**
	   * Module dependencies.
	   */

	  var pathtoRegexp = __webpack_require__(47);

	  /**
	   * Module exports.
	   */

	  module.exports = page;

	  /**
	   * Detect click event
	   */
	  var clickEvent = ('undefined' !== typeof document) && document.ontouchstart ? 'touchstart' : 'click';

	  /**
	   * To work properly with the URL
	   * history.location generated polyfill in https://github.com/devote/HTML5-History-API
	   */

	  var location = ('undefined' !== typeof window) && (window.history.location || window.location);

	  /**
	   * Perform initial dispatch.
	   */

	  var dispatch = true;


	  /**
	   * Decode URL components (query string, pathname, hash).
	   * Accommodates both regular percent encoding and x-www-form-urlencoded format.
	   */
	  var decodeURLComponents = true;

	  /**
	   * Base path.
	   */

	  var base = '';

	  /**
	   * Running flag.
	   */

	  var running;

	  /**
	   * HashBang option
	   */

	  var hashbang = false;

	  /**
	   * Previous context, for capturing
	   * page exit events.
	   */

	  var prevContext;

	  /**
	   * Register `path` with callback `fn()`,
	   * or route `path`, or redirection,
	   * or `page.start()`.
	   *
	   *   page(fn);
	   *   page('*', fn);
	   *   page('/user/:id', load, user);
	   *   page('/user/' + user.id, { some: 'thing' });
	   *   page('/user/' + user.id);
	   *   page('/from', '/to')
	   *   page();
	   *
	   * @param {String|Function} path
	   * @param {Function} fn...
	   * @api public
	   */

	  function page(path, fn) {
	    // <callback>
	    if ('function' === typeof path) {
	      return page('*', path);
	    }

	    // route <path> to <callback ...>
	    if ('function' === typeof fn) {
	      var route = new Route(path);
	      for (var i = 1; i < arguments.length; ++i) {
	        page.callbacks.push(route.middleware(arguments[i]));
	      }
	      // show <path> with [state]
	    } else if ('string' === typeof path) {
	      page['string' === typeof fn ? 'redirect' : 'show'](path, fn);
	      // start [options]
	    } else {
	      page.start(path);
	    }
	  }

	  /**
	   * Callback functions.
	   */

	  page.callbacks = [];
	  page.exits = [];

	  /**
	   * Current path being processed
	   * @type {String}
	   */
	  page.current = '';

	  /**
	   * Number of pages navigated to.
	   * @type {number}
	   *
	   *     page.len == 0;
	   *     page('/login');
	   *     page.len == 1;
	   */

	  page.len = 0;

	  /**
	   * Get or set basepath to `path`.
	   *
	   * @param {String} path
	   * @api public
	   */

	  page.base = function(path) {
	    if (0 === arguments.length) return base;
	    base = path;
	  };

	  /**
	   * Bind with the given `options`.
	   *
	   * Options:
	   *
	   *    - `click` bind to click events [true]
	   *    - `popstate` bind to popstate [true]
	   *    - `dispatch` perform initial dispatch [true]
	   *
	   * @param {Object} options
	   * @api public
	   */

	  page.start = function(options) {
	    options = options || {};
	    if (running) return;
	    running = true;
	    if (false === options.dispatch) dispatch = false;
	    if (false === options.decodeURLComponents) decodeURLComponents = false;
	    if (false !== options.popstate) window.addEventListener('popstate', onpopstate, false);
	    if (false !== options.click) {
	      document.addEventListener(clickEvent, onclick, false);
	    }
	    if (true === options.hashbang) hashbang = true;
	    if (!dispatch) return;
	    var url = (hashbang && ~location.hash.indexOf('#!')) ? location.hash.substr(2) + location.search : location.pathname + location.search + location.hash;
	    page.replace(url, null, true, dispatch);
	  };

	  /**
	   * Unbind click and popstate event handlers.
	   *
	   * @api public
	   */

	  page.stop = function() {
	    if (!running) return;
	    page.current = '';
	    page.len = 0;
	    running = false;
	    document.removeEventListener(clickEvent, onclick, false);
	    window.removeEventListener('popstate', onpopstate, false);
	  };

	  /**
	   * Show `path` with optional `state` object.
	   *
	   * @param {String} path
	   * @param {Object} state
	   * @param {Boolean} dispatch
	   * @return {Context}
	   * @api public
	   */

	  page.show = function(path, state, dispatch, push) {
	    var ctx = new Context(path, state);
	    page.current = ctx.path;
	    if (false !== dispatch) page.dispatch(ctx);
	    if (false !== ctx.handled && false !== push) ctx.pushState();
	    return ctx;
	  };

	  /**
	   * Goes back in the history
	   * Back should always let the current route push state and then go back.
	   *
	   * @param {String} path - fallback path to go back if no more history exists, if undefined defaults to page.base
	   * @param {Object} [state]
	   * @api public
	   */

	  page.back = function(path, state) {
	    if (page.len > 0) {
	      // this may need more testing to see if all browsers
	      // wait for the next tick to go back in history
	      history.back();
	      page.len--;
	    } else if (path) {
	      setTimeout(function() {
	        page.show(path, state);
	      });
	    }else{
	      setTimeout(function() {
	        page.show(base, state);
	      });
	    }
	  };


	  /**
	   * Register route to redirect from one path to other
	   * or just redirect to another route
	   *
	   * @param {String} from - if param 'to' is undefined redirects to 'from'
	   * @param {String} [to]
	   * @api public
	   */
	  page.redirect = function(from, to) {
	    // Define route from a path to another
	    if ('string' === typeof from && 'string' === typeof to) {
	      page(from, function(e) {
	        setTimeout(function() {
	          page.replace(to);
	        }, 0);
	      });
	    }

	    // Wait for the push state and replace it with another
	    if ('string' === typeof from && 'undefined' === typeof to) {
	      setTimeout(function() {
	        page.replace(from);
	      }, 0);
	    }
	  };

	  /**
	   * Replace `path` with optional `state` object.
	   *
	   * @param {String} path
	   * @param {Object} state
	   * @return {Context}
	   * @api public
	   */


	  page.replace = function(path, state, init, dispatch) {
	    var ctx = new Context(path, state);
	    page.current = ctx.path;
	    ctx.init = init;
	    ctx.save(); // save before dispatching, which may redirect
	    if (false !== dispatch) page.dispatch(ctx);
	    return ctx;
	  };

	  /**
	   * Dispatch the given `ctx`.
	   *
	   * @param {Object} ctx
	   * @api private
	   */

	  page.dispatch = function(ctx) {
	    var prev = prevContext,
	      i = 0,
	      j = 0;

	    prevContext = ctx;

	    function nextExit() {
	      var fn = page.exits[j++];
	      if (!fn) return nextEnter();
	      fn(prev, nextExit);
	    }

	    function nextEnter() {
	      var fn = page.callbacks[i++];

	      if (ctx.path !== page.current) {
	        ctx.handled = false;
	        return;
	      }
	      if (!fn) return unhandled(ctx);
	      fn(ctx, nextEnter);
	    }

	    if (prev) {
	      nextExit();
	    } else {
	      nextEnter();
	    }
	  };

	  /**
	   * Unhandled `ctx`. When it's not the initial
	   * popstate then redirect. If you wish to handle
	   * 404s on your own use `page('*', callback)`.
	   *
	   * @param {Context} ctx
	   * @api private
	   */

	  function unhandled(ctx) {
	    if (ctx.handled) return;
	    var current;

	    if (hashbang) {
	      current = base + location.hash.replace('#!', '');
	    } else {
	      current = location.pathname + location.search;
	    }

	    if (current === ctx.canonicalPath) return;
	    page.stop();
	    ctx.handled = false;
	    location.href = ctx.canonicalPath;
	  }

	  /**
	   * Register an exit route on `path` with
	   * callback `fn()`, which will be called
	   * on the previous context when a new
	   * page is visited.
	   */
	  page.exit = function(path, fn) {
	    if (typeof path === 'function') {
	      return page.exit('*', path);
	    }

	    var route = new Route(path);
	    for (var i = 1; i < arguments.length; ++i) {
	      page.exits.push(route.middleware(arguments[i]));
	    }
	  };

	  /**
	   * Remove URL encoding from the given `str`.
	   * Accommodates whitespace in both x-www-form-urlencoded
	   * and regular percent-encoded form.
	   *
	   * @param {str} URL component to decode
	   */
	  function decodeURLEncodedURIComponent(val) {
	    if (typeof val !== 'string') { return val; }
	    return decodeURLComponents ? decodeURIComponent(val.replace(/\+/g, ' ')) : val;
	  }

	  /**
	   * Initialize a new "request" `Context`
	   * with the given `path` and optional initial `state`.
	   *
	   * @param {String} path
	   * @param {Object} state
	   * @api public
	   */

	  function Context(path, state) {
	    if ('/' === path[0] && 0 !== path.indexOf(base)) path = base + (hashbang ? '#!' : '') + path;
	    var i = path.indexOf('?');

	    this.canonicalPath = path;
	    this.path = path.replace(base, '') || '/';
	    if (hashbang) this.path = this.path.replace('#!', '') || '/';

	    this.title = document.title;
	    this.state = state || {};
	    this.state.path = path;
	    this.querystring = ~i ? decodeURLEncodedURIComponent(path.slice(i + 1)) : '';
	    this.pathname = decodeURLEncodedURIComponent(~i ? path.slice(0, i) : path);
	    this.params = {};

	    // fragment
	    this.hash = '';
	    if (!hashbang) {
	      if (!~this.path.indexOf('#')) return;
	      var parts = this.path.split('#');
	      this.path = parts[0];
	      this.hash = decodeURLEncodedURIComponent(parts[1]) || '';
	      this.querystring = this.querystring.split('#')[0];
	    }
	  }

	  /**
	   * Expose `Context`.
	   */

	  page.Context = Context;

	  /**
	   * Push state.
	   *
	   * @api private
	   */

	  Context.prototype.pushState = function() {
	    page.len++;
	    history.pushState(this.state, this.title, hashbang && this.path !== '/' ? '#!' + this.path : this.canonicalPath);
	  };

	  /**
	   * Save the context state.
	   *
	   * @api public
	   */

	  Context.prototype.save = function() {
	    history.replaceState(this.state, this.title, hashbang && this.path !== '/' ? '#!' + this.path : this.canonicalPath);
	  };

	  /**
	   * Initialize `Route` with the given HTTP `path`,
	   * and an array of `callbacks` and `options`.
	   *
	   * Options:
	   *
	   *   - `sensitive`    enable case-sensitive routes
	   *   - `strict`       enable strict matching for trailing slashes
	   *
	   * @param {String} path
	   * @param {Object} options.
	   * @api private
	   */

	  function Route(path, options) {
	    options = options || {};
	    this.path = (path === '*') ? '(.*)' : path;
	    this.method = 'GET';
	    this.regexp = pathtoRegexp(this.path,
	      this.keys = [],
	      options.sensitive,
	      options.strict);
	  }

	  /**
	   * Expose `Route`.
	   */

	  page.Route = Route;

	  /**
	   * Return route middleware with
	   * the given callback `fn()`.
	   *
	   * @param {Function} fn
	   * @return {Function}
	   * @api public
	   */

	  Route.prototype.middleware = function(fn) {
	    var self = this;
	    return function(ctx, next) {
	      if (self.match(ctx.path, ctx.params)) return fn(ctx, next);
	      next();
	    };
	  };

	  /**
	   * Check if this route matches `path`, if so
	   * populate `params`.
	   *
	   * @param {String} path
	   * @param {Object} params
	   * @return {Boolean}
	   * @api private
	   */

	  Route.prototype.match = function(path, params) {
	    var keys = this.keys,
	      qsIndex = path.indexOf('?'),
	      pathname = ~qsIndex ? path.slice(0, qsIndex) : path,
	      m = this.regexp.exec(decodeURIComponent(pathname));

	    if (!m) return false;

	    for (var i = 1, len = m.length; i < len; ++i) {
	      var key = keys[i - 1];
	      var val = decodeURLEncodedURIComponent(m[i]);
	      if (val !== undefined || !(hasOwnProperty.call(params, key.name))) {
	        params[key.name] = val;
	      }
	    }

	    return true;
	  };


	  /**
	   * Handle "populate" events.
	   */

	  var onpopstate = (function () {
	    var loaded = false;
	    if ('undefined' === typeof window) {
	      return;
	    }
	    if (document.readyState === 'complete') {
	      loaded = true;
	    } else {
	      window.addEventListener('load', function() {
	        setTimeout(function() {
	          loaded = true;
	        }, 0);
	      });
	    }
	    return function onpopstate(e) {
	      if (!loaded) return;
	      if (e.state) {
	        var path = e.state.path;
	        page.replace(path, e.state);
	      } else {
	        page.show(location.pathname + location.hash, undefined, undefined, false);
	      }
	    };
	  })();
	  /**
	   * Handle "click" events.
	   */

	  function onclick(e) {

	    if (1 !== which(e)) return;

	    if (e.metaKey || e.ctrlKey || e.shiftKey) return;
	    if (e.defaultPrevented) return;



	    // ensure link
	    var el = e.target;
	    while (el && 'A' !== el.nodeName) el = el.parentNode;
	    if (!el || 'A' !== el.nodeName) return;



	    // Ignore if tag has
	    // 1. "download" attribute
	    // 2. rel="external" attribute
	    if (el.hasAttribute('download') || el.getAttribute('rel') === 'external') return;

	    // ensure non-hash for the same path
	    var link = el.getAttribute('href');
	    if (!hashbang && el.pathname === location.pathname && (el.hash || '#' === link)) return;



	    // Check for mailto: in the href
	    if (link && link.indexOf('mailto:') > -1) return;

	    // check target
	    if (el.target) return;

	    // x-origin
	    if (!sameOrigin(el.href)) return;



	    // rebuild path
	    var path = el.pathname + el.search + (el.hash || '');

	    // strip leading "/[drive letter]:" on NW.js on Windows
	    if (typeof process !== 'undefined' && path.match(/^\/[a-zA-Z]:\//)) {
	      path = path.replace(/^\/[a-zA-Z]:\//, '/');
	    }

	    // same page
	    var orig = path;

	    if (path.indexOf(base) === 0) {
	      path = path.substr(base.length);
	    }

	    if (hashbang) path = path.replace('#!', '');

	    if (base && orig === path) return;

	    e.preventDefault();
	    page.show(orig);
	  }

	  /**
	   * Event button.
	   */

	  function which(e) {
	    e = e || window.event;
	    return null === e.which ? e.button : e.which;
	  }

	  /**
	   * Check if `href` is the same origin.
	   */

	  function sameOrigin(href) {
	    var origin = location.protocol + '//' + location.hostname;
	    if (location.port) origin += ':' + location.port;
	    return (href && (0 === href.indexOf(origin)));
	  }

	  page.sameOrigin = sameOrigin;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(46)))

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* Riot v2.2.2, @license MIT, (c) 2015 Muut Inc. + contributors */

	;(function(window, undefined) {
	  'use strict'
	  var riot = { version: 'v2.2.2', settings: {} }

	  // This globals 'const' helps code size reduction

	  // for typeof == '' comparisons
	  var T_STRING = 'string',
	      T_OBJECT = 'object',
	      T_UNDEF  = 'undefined'

	  // for IE8 and rest of the world
	  /* istanbul ignore next */
	  var isArray = Array.isArray || (function () {
	    var _ts = Object.prototype.toString
	    return function (v) { return _ts.call(v) === '[object Array]' }
	  })()

	  // Version# for IE 8-11, 0 for others
	  var ieVersion = (function (win) {
	    return (window && window.document || {}).documentMode | 0
	  })()

	riot.observable = function(el) {

	  el = el || {}

	  var callbacks = {},
	      _id = 0

	  el.on = function(events, fn) {
	    if (isFunction(fn)) {
	      if (typeof fn.id === T_UNDEF) fn._id = _id++

	      events.replace(/\S+/g, function(name, pos) {
	        (callbacks[name] = callbacks[name] || []).push(fn)
	        fn.typed = pos > 0
	      })
	    }
	    return el
	  }

	  el.off = function(events, fn) {
	    if (events == '*') callbacks = {}
	    else {
	      events.replace(/\S+/g, function(name) {
	        if (fn) {
	          var arr = callbacks[name]
	          for (var i = 0, cb; (cb = arr && arr[i]); ++i) {
	            if (cb._id == fn._id) arr.splice(i--, 1)
	          }
	        } else {
	          callbacks[name] = []
	        }
	      })
	    }
	    return el
	  }

	  // only single event supported
	  el.one = function(name, fn) {
	    function on() {
	      el.off(name, on)
	      fn.apply(el, arguments)
	    }
	    return el.on(name, on)
	  }

	  el.trigger = function(name) {
	    var args = [].slice.call(arguments, 1),
	        fns = callbacks[name] || []

	    for (var i = 0, fn; (fn = fns[i]); ++i) {
	      if (!fn.busy) {
	        fn.busy = 1
	        fn.apply(el, fn.typed ? [name].concat(args) : args)
	        if (fns[i] !== fn) { i-- }
	        fn.busy = 0
	      }
	    }

	    if (callbacks.all && name != 'all') {
	      el.trigger.apply(el, ['all', name].concat(args))
	    }

	    return el
	  }

	  return el

	}
	riot.mixin = (function() {
	  var mixins = {}

	  return function(name, mixin) {
	    if (!mixin) return mixins[name]
	    mixins[name] = mixin
	  }

	})()

	;(function(riot, evt, win) {

	  // browsers only
	  if (!win) return

	  var loc = win.location,
	      fns = riot.observable(),
	      started = false,
	      current

	  function hash() {
	    return loc.href.split('#')[1] || ''
	  }

	  function parser(path) {
	    return path.split('/')
	  }

	  function emit(path) {
	    if (path.type) path = hash()

	    if (path != current) {
	      fns.trigger.apply(null, ['H'].concat(parser(path)))
	      current = path
	    }
	  }

	  var r = riot.route = function(arg) {
	    // string
	    if (arg[0]) {
	      loc.hash = arg
	      emit(arg)

	    // function
	    } else {
	      fns.on('H', arg)
	    }
	  }

	  r.exec = function(fn) {
	    fn.apply(null, parser(hash()))
	  }

	  r.parser = function(fn) {
	    parser = fn
	  }

	  r.stop = function () {
	    if (!started) return
	    win.removeEventListener ? win.removeEventListener(evt, emit, false) : win.detachEvent('on' + evt, emit)
	    fns.off('*')
	    started = false
	  }

	  r.start = function () {
	    if (started) return
	    win.addEventListener ? win.addEventListener(evt, emit, false) : win.attachEvent('on' + evt, emit)
	    started = true
	  }

	  // autostart the router
	  r.start()

	})(riot, 'hashchange', window)
	/*

	//// How it works?


	Three ways:

	1. Expressions: tmpl('{ value }', data).
	   Returns the result of evaluated expression as a raw object.

	2. Templates: tmpl('Hi { name } { surname }', data).
	   Returns a string with evaluated expressions.

	3. Filters: tmpl('{ show: !done, highlight: active }', data).
	   Returns a space separated list of trueish keys (mainly
	   used for setting html classes), e.g. "show highlight".


	// Template examples

	tmpl('{ title || "Untitled" }', data)
	tmpl('Results are { results ? "ready" : "loading" }', data)
	tmpl('Today is { new Date() }', data)
	tmpl('{ message.length > 140 && "Message is too long" }', data)
	tmpl('This item got { Math.round(rating) } stars', data)
	tmpl('<h1>{ title }</h1>{ body }', data)


	// Falsy expressions in templates

	In templates (as opposed to single expressions) all falsy values
	except zero (undefined/null/false) will default to empty string:

	tmpl('{ undefined } - { false } - { null } - { 0 }', {})
	// will return: " - - - 0"

	*/


	var brackets = (function(orig) {

	  var cachedBrackets,
	      r,
	      b,
	      re = /[{}]/g

	  return function(x) {

	    // make sure we use the current setting
	    var s = riot.settings.brackets || orig

	    // recreate cached vars if needed
	    if (cachedBrackets !== s) {
	      cachedBrackets = s
	      b = s.split(' ')
	      r = b.map(function (e) { return e.replace(/(?=.)/g, '\\') })
	    }

	    // if regexp given, rewrite it with current brackets (only if differ from default)
	    return x instanceof RegExp ? (
	        s === orig ? x :
	        new RegExp(x.source.replace(re, function(b) { return r[~~(b === '}')] }), x.global ? 'g' : '')
	      ) :
	      // else, get specific bracket
	      b[x]
	  }
	})('{ }')


	var tmpl = (function() {

	  var cache = {},
	      reVars = /(['"\/]).*?[^\\]\1|\.\w*|\w*:|\b(?:(?:new|typeof|in|instanceof) |(?:this|true|false|null|undefined)\b|function *\()|([a-z_$]\w*)/gi
	              // [ 1               ][ 2  ][ 3 ][ 4                                                                                  ][ 5       ]
	              // find variable names:
	              // 1. skip quoted strings and regexps: "a b", 'a b', 'a \'b\'', /a b/
	              // 2. skip object properties: .name
	              // 3. skip object literals: name:
	              // 4. skip javascript keywords
	              // 5. match var name

	  // build a template (or get it from cache), render with data
	  return function(str, data) {
	    return str && (cache[str] = cache[str] || tmpl(str))(data)
	  }


	  // create a template instance

	  function tmpl(s, p) {

	    // default template string to {}
	    s = (s || (brackets(0) + brackets(1)))

	      // temporarily convert \{ and \} to a non-character
	      .replace(brackets(/\\{/g), '\uFFF0')
	      .replace(brackets(/\\}/g), '\uFFF1')

	    // split string to expression and non-expresion parts
	    p = split(s, extract(s, brackets(/{/), brackets(/}/)))

	    return new Function('d', 'return ' + (

	      // is it a single expression or a template? i.e. {x} or <b>{x}</b>
	      !p[0] && !p[2] && !p[3]

	        // if expression, evaluate it
	        ? expr(p[1])

	        // if template, evaluate all expressions in it
	        : '[' + p.map(function(s, i) {

	            // is it an expression or a string (every second part is an expression)
	          return i % 2

	              // evaluate the expressions
	              ? expr(s, true)

	              // process string parts of the template:
	              : '"' + s

	                  // preserve new lines
	                  .replace(/\n/g, '\\n')

	                  // escape quotes
	                  .replace(/"/g, '\\"')

	                + '"'

	        }).join(',') + '].join("")'
	      )

	      // bring escaped { and } back
	      .replace(/\uFFF0/g, brackets(0))
	      .replace(/\uFFF1/g, brackets(1))

	    + ';')

	  }


	  // parse { ... } expression

	  function expr(s, n) {
	    s = s

	      // convert new lines to spaces
	      .replace(/\n/g, ' ')

	      // trim whitespace, brackets, strip comments
	      .replace(brackets(/^[{ ]+|[ }]+$|\/\*.+?\*\//g), '')

	    // is it an object literal? i.e. { key : value }
	    return /^\s*[\w- "']+ *:/.test(s)

	      // if object literal, return trueish keys
	      // e.g.: { show: isOpen(), done: item.done } -> "show done"
	      ? '[' +

	          // extract key:val pairs, ignoring any nested objects
	          extract(s,

	              // name part: name:, "name":, 'name':, name :
	              /["' ]*[\w- ]+["' ]*:/,

	              // expression part: everything upto a comma followed by a name (see above) or end of line
	              /,(?=["' ]*[\w- ]+["' ]*:)|}|$/
	              ).map(function(pair) {

	                // get key, val parts
	                return pair.replace(/^[ "']*(.+?)[ "']*: *(.+?),? *$/, function(_, k, v) {

	                  // wrap all conditional parts to ignore errors
	                  return v.replace(/[^&|=!><]+/g, wrap) + '?"' + k + '":"",'

	                })

	              }).join('')

	        + '].join(" ").trim()'

	      // if js expression, evaluate as javascript
	      : wrap(s, n)

	  }


	  // execute js w/o breaking on errors or undefined vars

	  function wrap(s, nonull) {
	    s = s.trim()
	    return !s ? '' : '(function(v){try{v='

	        // prefix vars (name => data.name)
	        + (s.replace(reVars, function(s, _, v) { return v ? '(d.'+v+'===undefined?'+(typeof window == 'undefined' ? 'global.' : 'window.')+v+':d.'+v+')' : s })

	          // break the expression if its empty (resulting in undefined value)
	          || 'x')
	      + '}catch(e){'
	      + '}finally{return '

	        // default to empty string for falsy values except zero
	        + (nonull === true ? '!v&&v!==0?"":v' : 'v')

	      + '}}).call(d)'
	  }


	  // split string by an array of substrings

	  function split(str, substrings) {
	    var parts = []
	    substrings.map(function(sub, i) {

	      // push matched expression and part before it
	      i = str.indexOf(sub)
	      parts.push(str.slice(0, i), sub)
	      str = str.slice(i + sub.length)
	    })

	    // push the remaining part
	    return parts.concat(str)
	  }


	  // match strings between opening and closing regexp, skipping any inner/nested matches

	  function extract(str, open, close) {

	    var start,
	        level = 0,
	        matches = [],
	        re = new RegExp('('+open.source+')|('+close.source+')', 'g')

	    str.replace(re, function(_, open, close, pos) {

	      // if outer inner bracket, mark position
	      if (!level && open) start = pos

	      // in(de)crease bracket level
	      level += open ? 1 : -1

	      // if outer closing bracket, grab the match
	      if (!level && close != null) matches.push(str.slice(start, pos+close.length))

	    })

	    return matches
	  }

	})()

	// { key, i in items} -> { key, i, items }
	function loopKeys(expr) {
	  var b0 = brackets(0),
	      els = expr.slice(b0.length).match(/^\s*(\S+?)\s*(?:,\s*(\S+))?\s+in\s+(.+)$/)
	  return els ? { key: els[1], pos: els[2], val: b0 + els[3] } : { val: expr }
	}

	function mkitem(expr, key, val) {
	  var item = {}
	  item[expr.key] = key
	  if (expr.pos) item[expr.pos] = val
	  return item
	}


	/* Beware: heavy stuff */
	function _each(dom, parent, expr) {

	  remAttr(dom, 'each')

	  var tagName = getTagName(dom),
	      template = dom.outerHTML,
	      hasImpl = !!tagImpl[tagName],
	      impl = tagImpl[tagName] || {
	        tmpl: template
	      },
	      root = dom.parentNode,
	      placeholder = document.createComment('riot placeholder'),
	      tags = [],
	      child = getTag(dom),
	      checksum

	  root.insertBefore(placeholder, dom)

	  expr = loopKeys(expr)

	  // clean template code
	  parent
	    .one('premount', function () {
	      if (root.stub) root = parent.root
	      // remove the original DOM node
	      dom.parentNode.removeChild(dom)
	    })
	    .on('update', function () {
	      var items = tmpl(expr.val, parent)

	      // object loop. any changes cause full redraw
	      if (!isArray(items)) {

	        checksum = items ? JSON.stringify(items) : ''

	        items = !items ? [] :
	          Object.keys(items).map(function (key) {
	            return mkitem(expr, key, items[key])
	          })
	      }

	      var frag = document.createDocumentFragment(),
	          i = tags.length,
	          j = items.length

	      // unmount leftover items
	      while (i > j) {
	        tags[--i].unmount()
	        tags.splice(i, 1)
	      }

	      for (i = 0; i < j; ++i) {
	        var _item = !checksum && !!expr.key ? mkitem(expr, items[i], i) : items[i]

	        if (!tags[i]) {
	          // mount new
	          (tags[i] = new Tag(impl, {
	              parent: parent,
	              isLoop: true,
	              hasImpl: hasImpl,
	              root: hasImpl ? dom.cloneNode() : root,
	              item: _item
	            }, dom.innerHTML)
	          ).mount()

	          frag.appendChild(tags[i].root)
	        } else
	          tags[i].update(_item)

	        tags[i]._item = _item

	      }

	      root.insertBefore(frag, placeholder)

	      if (child) parent.tags[tagName] = tags

	    }).one('updated', function() {
	      var keys = Object.keys(parent)// only set new values
	      walk(root, function(node) {
	        // only set element node and not isLoop
	        if (node.nodeType == 1 && !node.isLoop && !node._looped) {
	          node._visited = false // reset _visited for loop node
	          node._looped = true // avoid set multiple each
	          setNamed(node, parent, keys)
	        }
	      })
	    })

	}


	function parseNamedElements(root, parent, childTags) {

	  walk(root, function(dom) {
	    if (dom.nodeType == 1) {
	      dom.isLoop = dom.isLoop || (dom.parentNode && dom.parentNode.isLoop || dom.getAttribute('each')) ? 1 : 0

	      // custom child tag
	      var child = getTag(dom)

	      if (child && !dom.isLoop) {
	        var tag = new Tag(child, { root: dom, parent: parent }, dom.innerHTML),
	            tagName = getTagName(dom),
	            ptag = parent,
	            cachedTag

	        while (!getTag(ptag.root)) {
	          if (!ptag.parent) break
	          ptag = ptag.parent
	        }

	        // fix for the parent attribute in the looped elements
	        tag.parent = ptag

	        cachedTag = ptag.tags[tagName]

	        // if there are multiple children tags having the same name
	        if (cachedTag) {
	          // if the parent tags property is not yet an array
	          // create it adding the first cached tag
	          if (!isArray(cachedTag))
	            ptag.tags[tagName] = [cachedTag]
	          // add the new nested tag to the array
	          ptag.tags[tagName].push(tag)
	        } else {
	          ptag.tags[tagName] = tag
	        }

	        // empty the child node once we got its template
	        // to avoid that its children get compiled multiple times
	        dom.innerHTML = ''
	        childTags.push(tag)
	      }

	      if (!dom.isLoop)
	        setNamed(dom, parent, [])
	    }

	  })

	}

	function parseExpressions(root, tag, expressions) {

	  function addExpr(dom, val, extra) {
	    if (val.indexOf(brackets(0)) >= 0) {
	      var expr = { dom: dom, expr: val }
	      expressions.push(extend(expr, extra))
	    }
	  }

	  walk(root, function(dom) {
	    var type = dom.nodeType

	    // text node
	    if (type == 3 && dom.parentNode.tagName != 'STYLE') addExpr(dom, dom.nodeValue)
	    if (type != 1) return

	    /* element */

	    // loop
	    var attr = dom.getAttribute('each')

	    if (attr) { _each(dom, tag, attr); return false }

	    // attribute expressions
	    each(dom.attributes, function(attr) {
	      var name = attr.name,
	        bool = name.split('__')[1]

	      addExpr(dom, attr.value, { attr: bool || name, bool: bool })
	      if (bool) { remAttr(dom, name); return false }

	    })

	    // skip custom tags
	    if (getTag(dom)) return false

	  })

	}
	function Tag(impl, conf, innerHTML) {

	  var self = riot.observable(this),
	      opts = inherit(conf.opts) || {},
	      dom = mkdom(impl.tmpl),
	      parent = conf.parent,
	      isLoop = conf.isLoop,
	      hasImpl = conf.hasImpl,
	      item = cleanUpData(conf.item),
	      expressions = [],
	      childTags = [],
	      root = conf.root,
	      fn = impl.fn,
	      tagName = root.tagName.toLowerCase(),
	      attr = {},
	      propsInSyncWithParent = [],
	      loopDom,
	      TAG_ATTRIBUTES = /([\w\-]+)\s?=\s?['"]([^'"]+)["']/gim


	  if (fn && root._tag) {
	    root._tag.unmount(true)
	  }

	  // not yet mounted
	  this.isMounted = false
	  root.isLoop = isLoop

	  if (impl.attrs) {
	    var attrs = impl.attrs.match(TAG_ATTRIBUTES)

	    each(attrs, function(a) {
	      var kv = a.split(/\s?=\s?/)
	      root.setAttribute(kv[0], kv[1].replace(/['"]/g, ''))
	    })

	  }

	  // keep a reference to the tag just created
	  // so we will be able to mount this tag multiple times
	  root._tag = this

	  // create a unique id to this tag
	  // it could be handy to use it also to improve the virtual dom rendering speed
	  this._id = fastAbs(~~(new Date().getTime() * Math.random()))

	  extend(this, { parent: parent, root: root, opts: opts, tags: {} }, item)

	  // grab attributes
	  each(root.attributes, function(el) {
	    var val = el.value
	    // remember attributes with expressions only
	    if (brackets(/\{.*\}/).test(val)) attr[el.name] = val
	  })

	  if (dom.innerHTML && !/select|select|optgroup|tbody|tr/.test(tagName))
	    // replace all the yield tags with the tag inner html
	    dom.innerHTML = replaceYield(dom.innerHTML, innerHTML)

	  // options
	  function updateOpts() {
	    var ctx = hasImpl && isLoop ? self : parent || self
	    // update opts from current DOM attributes
	    each(root.attributes, function(el) {
	      opts[el.name] = tmpl(el.value, ctx)
	    })
	    // recover those with expressions
	    each(Object.keys(attr), function(name) {
	      opts[name] = tmpl(attr[name], ctx)
	    })
	  }

	  function normalizeData(data) {
	    for (var key in item) {
	      if (typeof self[key] !== T_UNDEF)
	        self[key] = data[key]
	    }
	  }

	  function inheritFromParent () {
	    if (!self.parent || !isLoop) return
	    each(Object.keys(self.parent), function(k) {
	      // some properties must be always in sync with the parent tag
	      var mustSync = ~propsInSyncWithParent.indexOf(k)
	      if (typeof self[k] === T_UNDEF || mustSync) {
	        // track the property to keep in sync
	        // so we can keep it updated
	        if (!mustSync) propsInSyncWithParent.push(k)
	        self[k] = self.parent[k]
	      }
	    })
	  }

	  this.update = function(data) {
	    // make sure the data passed will not override
	    // the component core methods
	    data = cleanUpData(data)
	    // inherit properties from the parent
	    inheritFromParent()
	    // normalize the tag properties in case an item object was initially passed
	    if (typeof item === T_OBJECT || isArray(item)) {
	      normalizeData(data)
	      item = data
	    }
	    extend(self, data)
	    updateOpts()
	    self.trigger('update', data)
	    update(expressions, self)
	    self.trigger('updated')
	  }

	  this.mixin = function() {
	    each(arguments, function(mix) {
	      mix = typeof mix === T_STRING ? riot.mixin(mix) : mix
	      each(Object.keys(mix), function(key) {
	        // bind methods to self
	        if (key != 'init')
	          self[key] = isFunction(mix[key]) ? mix[key].bind(self) : mix[key]
	      })
	      // init method will be called automatically
	      if (mix.init) mix.init.bind(self)()
	    })
	  }

	  this.mount = function() {

	    updateOpts()

	    // initialiation
	    fn && fn.call(self, opts)

	    toggle(true)


	    // parse layout after init. fn may calculate args for nested custom tags
	    parseExpressions(dom, self, expressions)
	    if (!self.parent || hasImpl) parseExpressions(self.root, self, expressions) // top level before update, empty root

	    if (!self.parent || isLoop) self.update(item)

	    // internal use only, fixes #403
	    self.trigger('premount')

	    if (isLoop && !hasImpl) {
	      // update the root attribute for the looped elements
	      self.root = root = loopDom = dom.firstChild

	    } else {
	      while (dom.firstChild) root.appendChild(dom.firstChild)
	      if (root.stub) self.root = root = parent.root
	    }
	    // if it's not a child tag we can trigger its mount event
	    if (!self.parent || self.parent.isMounted) {
	      self.isMounted = true
	      self.trigger('mount')
	    }
	    // otherwise we need to wait that the parent event gets triggered
	    else self.parent.one('mount', function() {
	      // avoid to trigger the `mount` event for the tags
	      // not visible included in an if statement
	      if (!isInStub(self.root)) {
	        self.parent.isMounted = self.isMounted = true
	        self.trigger('mount')
	      }
	    })
	  }


	  this.unmount = function(keepRootTag) {
	    var el = loopDom || root,
	        p = el.parentNode

	    if (p) {

	      if (parent)
	        // remove this tag from the parent tags object
	        // if there are multiple nested tags with same name..
	        // remove this element form the array
	        if (isArray(parent.tags[tagName]))
	          each(parent.tags[tagName], function(tag, i) {
	            if (tag._id == self._id)
	              parent.tags[tagName].splice(i, 1)
	          })
	        else
	          // otherwise just delete the tag instance
	          parent.tags[tagName] = undefined
	      else
	        while (el.firstChild) el.removeChild(el.firstChild)

	      if (!keepRootTag)
	        p.removeChild(el)

	    }


	    self.trigger('unmount')
	    toggle()
	    self.off('*')
	    // somehow ie8 does not like `delete root._tag`
	    root._tag = null

	  }

	  function toggle(isMount) {

	    // mount/unmount children
	    each(childTags, function(child) { child[isMount ? 'mount' : 'unmount']() })

	    // listen/unlisten parent (events flow one way from parent to children)
	    if (parent) {
	      var evt = isMount ? 'on' : 'off'

	      // the loop tags will be always in sync with the parent automatically
	      if (isLoop)
	        parent[evt]('unmount', self.unmount)
	      else
	        parent[evt]('update', self.update)[evt]('unmount', self.unmount)
	    }
	  }

	  // named elements available for fn
	  parseNamedElements(dom, this, childTags)


	}

	function setEventHandler(name, handler, dom, tag) {

	  dom[name] = function(e) {

	    var item = tag._item,
	        ptag = tag.parent

	    if (!item)
	      while (ptag) {
	        item = ptag._item
	        ptag = item ? false : ptag.parent
	      }

	    // cross browser event fix
	    e = e || window.event

	    // ignore error on some browsers
	    try {
	      e.currentTarget = dom
	      if (!e.target) e.target = e.srcElement
	      if (!e.which) e.which = e.charCode || e.keyCode
	    } catch (ignored) { '' }

	    e.item = item

	    // prevent default behaviour (by default)
	    if (handler.call(tag, e) !== true && !/radio|check/.test(dom.type)) {
	      e.preventDefault && e.preventDefault()
	      e.returnValue = false
	    }

	    if (!e.preventUpdate) {
	      var el = item ? tag.parent : tag
	      el.update()
	    }

	  }

	}

	// used by if- attribute
	function insertTo(root, node, before) {
	  if (root) {
	    root.insertBefore(before, node)
	    root.removeChild(node)
	  }
	}

	function update(expressions, tag) {

	  each(expressions, function(expr, i) {

	    var dom = expr.dom,
	        attrName = expr.attr,
	        value = tmpl(expr.expr, tag),
	        parent = expr.dom.parentNode

	    if (value == null) value = ''

	    // leave out riot- prefixes from strings inside textarea
	    if (parent && parent.tagName == 'TEXTAREA') value = value.replace(/riot-/g, '')

	    // no change
	    if (expr.value === value) return
	    expr.value = value

	    // text node
	    if (!attrName) return dom.nodeValue = value.toString()

	    // remove original attribute
	    remAttr(dom, attrName)

	    // event handler
	    if (isFunction(value)) {
	      setEventHandler(attrName, value, dom, tag)

	    // if- conditional
	    } else if (attrName == 'if') {
	      var stub = expr.stub

	      // add to DOM
	      if (value) {
	        if (stub) {
	          insertTo(stub.parentNode, stub, dom)
	          dom.inStub = false
	          // avoid to trigger the mount event if the tags is not visible yet
	          // maybe we can optimize this avoiding to mount the tag at all
	          if (!isInStub(dom)) {
	            walk(dom, function(el) {
	              if (el._tag && !el._tag.isMounted) el._tag.isMounted = !!el._tag.trigger('mount')
	            })
	          }
	        }
	      // remove from DOM
	      } else {
	        stub = expr.stub = stub || document.createTextNode('')
	        insertTo(dom.parentNode, dom, stub)
	        dom.inStub = true
	      }
	    // show / hide
	    } else if (/^(show|hide)$/.test(attrName)) {
	      if (attrName == 'hide') value = !value
	      dom.style.display = value ? '' : 'none'

	    // field value
	    } else if (attrName == 'value') {
	      dom.value = value

	    // <img src="{ expr }">
	    } else if (attrName.slice(0, 5) == 'riot-' && attrName != 'riot-tag') {
	      attrName = attrName.slice(5)
	      value ? dom.setAttribute(attrName, value) : remAttr(dom, attrName)

	    } else {
	      if (expr.bool) {
	        dom[attrName] = value
	        if (!value) return
	        value = attrName
	      }

	      if (typeof value !== T_OBJECT) dom.setAttribute(attrName, value)

	    }

	  })

	}

	function each(els, fn) {
	  for (var i = 0, len = (els || []).length, el; i < len; i++) {
	    el = els[i]
	    // return false -> remove current item during loop
	    if (el != null && fn(el, i) === false) i--
	  }
	  return els
	}

	function isFunction(v) {
	  return typeof v === 'function' || false   // avoid IE problems
	}

	function remAttr(dom, name) {
	  dom.removeAttribute(name)
	}

	function fastAbs(nr) {
	  return (nr ^ (nr >> 31)) - (nr >> 31)
	}

	function getTag(dom) {
	  var tagName = dom.tagName.toLowerCase()
	  return tagImpl[dom.getAttribute(RIOT_TAG) || tagName]
	}

	function getTagName(dom) {
	  var child = getTag(dom),
	    namedTag = dom.getAttribute('name'),
	    tagName = namedTag && namedTag.indexOf(brackets(0)) < 0 ? namedTag : child ? child.name : dom.tagName.toLowerCase()

	  return tagName
	}

	function extend(src) {
	  var obj, args = arguments
	  for (var i = 1; i < args.length; ++i) {
	    if ((obj = args[i])) {
	      for (var key in obj) {      // eslint-disable-line guard-for-in
	        src[key] = obj[key]
	      }
	    }
	  }
	  return src
	}

	// with this function we avoid that the current Tag methods get overridden
	function cleanUpData(data) {
	  if (!(data instanceof Tag)) return data

	  var o = {},
	      blackList = ['update', 'root', 'mount', 'unmount', 'mixin', 'isMounted', 'isloop', 'tags', 'parent', 'opts']
	  for (var key in data) {
	    if (!~blackList.indexOf(key))
	      o[key] = data[key]
	  }
	  return o
	}

	function mkdom(template) {
	  var checkie = ieVersion && ieVersion < 10,
	      matches = /^\s*<([\w-]+)/.exec(template),
	      tagName = matches ? matches[1].toLowerCase() : '',
	      rootTag = (tagName === 'th' || tagName === 'td') ? 'tr' :
	                (tagName === 'tr' ? 'tbody' : 'div'),
	      el = mkEl(rootTag)

	  el.stub = true

	  if (checkie) {
	    if (tagName === 'optgroup')
	      optgroupInnerHTML(el, template)
	    else if (tagName === 'option')
	      optionInnerHTML(el, template)
	    else if (rootTag !== 'div')
	      tbodyInnerHTML(el, template, tagName)
	    else
	      checkie = 0
	  }
	  if (!checkie) el.innerHTML = template

	  return el
	}

	function walk(dom, fn) {
	  if (dom) {
	    if (fn(dom) === false) walk(dom.nextSibling, fn)
	    else {
	      dom = dom.firstChild

	      while (dom) {
	        walk(dom, fn)
	        dom = dom.nextSibling
	      }
	    }
	  }
	}

	function isInStub(dom) {
	  while (dom) {
	    if (dom.inStub) return true
	    dom = dom.parentNode
	  }
	  return false
	}

	function mkEl(name) {
	  return document.createElement(name)
	}

	function replaceYield (tmpl, innerHTML) {
	  return tmpl.replace(/<(yield)\/?>(<\/\1>)?/gim, innerHTML || '')
	}

	function $$(selector, ctx) {
	  return (ctx || document).querySelectorAll(selector)
	}

	function $(selector, ctx) {
	  return (ctx || document).querySelector(selector)
	}

	function inherit(parent) {
	  function Child() {}
	  Child.prototype = parent
	  return new Child()
	}

	function setNamed(dom, parent, keys) {
	  each(dom.attributes, function(attr) {
	    if (dom._visited) return
	    if (attr.name === 'id' || attr.name === 'name') {
	      dom._visited = true
	      var p, v = attr.value
	      if (~keys.indexOf(v)) return

	      p = parent[v]
	      if (!p)
	        parent[v] = dom
	      else
	        isArray(p) ? p.push(dom) : (parent[v] = [p, dom])
	    }
	  })
	}
	/**
	 *
	 * Hacks needed for the old internet explorer versions [lower than IE10]
	 *
	 */
	/* istanbul ignore next */
	function tbodyInnerHTML(el, html, tagName) {
	  var div = mkEl('div'),
	      loops = /td|th/.test(tagName) ? 3 : 2,
	      child

	  div.innerHTML = '<table>' + html + '</table>'
	  child = div.firstChild

	  while (loops--) child = child.firstChild

	  el.appendChild(child)

	}
	/* istanbul ignore next */
	function optionInnerHTML(el, html) {
	  var opt = mkEl('option'),
	      valRegx = /value=[\"'](.+?)[\"']/,
	      selRegx = /selected=[\"'](.+?)[\"']/,
	      eachRegx = /each=[\"'](.+?)[\"']/,
	      ifRegx = /if=[\"'](.+?)[\"']/,
	      innerRegx = />([^<]*)</,
	      valuesMatch = html.match(valRegx),
	      selectedMatch = html.match(selRegx),
	      innerValue = html.match(innerRegx),
	      eachMatch = html.match(eachRegx),
	      ifMatch = html.match(ifRegx)

	  if (innerValue) opt.innerHTML = innerValue[1]
	  else opt.innerHTML = html

	  if (valuesMatch) opt.value = valuesMatch[1]
	  if (selectedMatch) opt.setAttribute('riot-selected', selectedMatch[1])
	  if (eachMatch) opt.setAttribute('each', eachMatch[1])
	  if (ifMatch) opt.setAttribute('if', ifMatch[1])

	  el.appendChild(opt)
	}
	/* istanbul ignore next */
	function optgroupInnerHTML(el, html) {
	  var opt = mkEl('optgroup'),
	      labelRegx = /label=[\"'](.+?)[\"']/,
	      elementRegx = /^<([^>]*)>/,
	      tagRegx = /^<([^ \>]*)/,
	      labelMatch = html.match(labelRegx),
	      elementMatch = html.match(elementRegx),
	      tagMatch = html.match(tagRegx),
	      innerContent = html

	  if (elementMatch) {
	    var options = html.slice(elementMatch[1].length+2, -tagMatch[1].length-3).trim()
	    innerContent = options
	  }

	  if (labelMatch) opt.setAttribute('riot-label', labelMatch[1])

	  if (innerContent) {
	    var innerOpt = mkEl('div')

	    optionInnerHTML(innerOpt, innerContent)

	    opt.appendChild(innerOpt.firstChild)
	  }

	  el.appendChild(opt)
	}

	/*
	 Virtual dom is an array of custom tags on the document.
	 Updates and unmounts propagate downwards from parent to children.
	*/

	var virtualDom = [],
	    tagImpl = {},
	    styleNode

	var RIOT_TAG = 'riot-tag'

	function injectStyle(css) {

	  styleNode = styleNode || mkEl('style')

	  if (!document.head) return

	  if (styleNode.styleSheet)
	    styleNode.styleSheet.cssText += css
	  else
	    styleNode.innerHTML += css

	  if (!styleNode._rendered)
	    if (styleNode.styleSheet) {
	      document.body.appendChild(styleNode)
	    } else {
	      var rs = $('style[type=riot]')
	      if (rs) {
	        rs.parentNode.insertBefore(styleNode, rs)
	        rs.parentNode.removeChild(rs)
	      } else document.head.appendChild(styleNode)

	    }

	  styleNode._rendered = true

	}

	function mountTo(root, tagName, opts) {
	  var tag = tagImpl[tagName],
	      // cache the inner HTML to fix #855
	      innerHTML = root._innerHTML = root._innerHTML || root.innerHTML

	  // clear the inner html
	  root.innerHTML = ''

	  if (tag && root) tag = new Tag(tag, { root: root, opts: opts }, innerHTML)

	  if (tag && tag.mount) {
	    tag.mount()
	    virtualDom.push(tag)
	    return tag.on('unmount', function() {
	      virtualDom.splice(virtualDom.indexOf(tag), 1)
	    })
	  }

	}

	riot.tag = function(name, html, css, attrs, fn) {
	  if (isFunction(attrs)) {
	    fn = attrs
	    if (/^[\w\-]+\s?=/.test(css)) {
	      attrs = css
	      css = ''
	    } else attrs = ''
	  }
	  if (css) {
	    if (isFunction(css)) fn = css
	    else injectStyle(css)
	  }
	  tagImpl[name] = { name: name, tmpl: html, attrs: attrs, fn: fn }
	  return name
	}

	riot.mount = function(selector, tagName, opts) {

	  var els,
	      allTags,
	      tags = []

	  // helper functions

	  function addRiotTags(arr) {
	    var list = ''
	    each(arr, function (e) {
	      list += ', *[riot-tag="'+ e.trim() + '"]'
	    })
	    return list
	  }

	  function selectAllTags() {
	    var keys = Object.keys(tagImpl)
	    return keys + addRiotTags(keys)
	  }

	  function pushTags(root) {
	    if (root.tagName) {
	      if (tagName && !root.getAttribute(RIOT_TAG))
	        root.setAttribute(RIOT_TAG, tagName)

	      var tag = mountTo(root,
	        tagName || root.getAttribute(RIOT_TAG) || root.tagName.toLowerCase(), opts)

	      if (tag) tags.push(tag)
	    }
	    else if (root.length) {
	      each(root, pushTags)   // assume nodeList
	    }
	  }

	  // ----- mount code -----

	  if (typeof tagName === T_OBJECT) {
	    opts = tagName
	    tagName = 0
	  }

	  // crawl the DOM to find the tag
	  if (typeof selector === T_STRING) {
	    if (selector === '*')
	      // select all the tags registered
	      // and also the tags found with the riot-tag attribute set
	      selector = allTags = selectAllTags()
	    else
	      // or just the ones named like the selector
	      selector += addRiotTags(selector.split(','))

	    els = $$(selector)
	  }
	  else
	    // probably you have passed already a tag or a NodeList
	    els = selector

	  // select all the registered and mount them inside their root elements
	  if (tagName === '*') {
	    // get all custom tags
	    tagName = allTags || selectAllTags()
	    // if the root els it's just a single tag
	    if (els.tagName)
	      els = $$(tagName, els)
	    else {
	      // select all the children for all the different root elements
	      var nodeList = []
	      each(els, function (_el) {
	        nodeList.push($$(tagName, _el))
	      })
	      els = nodeList
	    }
	    // get rid of the tagName
	    tagName = 0
	  }

	  if (els.tagName)
	    pushTags(els)
	  else
	    each(els, pushTags)

	  return tags
	}

	// update everything
	riot.update = function() {
	  return each(virtualDom, function(tag) {
	    tag.update()
	  })
	}

	// @deprecated
	riot.mountTo = riot.mount


	  // share methods for other riot parts, e.g. compiler
	  riot.util = { brackets: brackets, tmpl: tmpl }

	  // support CommonJS, AMD & browser
	  /* istanbul ignore next */
	  if (typeof exports === T_OBJECT)
	    module.exports = riot
	  else if (true)
	    !(__WEBPACK_AMD_DEFINE_RESULT__ = function() { return window.riot = riot }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
	  else
	    window.riot = riot

	})(typeof window != 'undefined' ? window : undefined);


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	riot.tag('avatar', '<span class="avatar__image">{initials}</span> <a href="/profile/{opts.ref}" class="avatar__text">{opts.name}</a>', '.avatar__image { width: 2em; height: 2em; border-radius: 50%; background-color: pink; color:#fff; text-align:center; font: 0.75em/2em Lato,sans-serif; display:inline-block; /*padding: 0.5em 5px 0 0;*/ } .avatar__text { text-decoration:none; color:inherit; font: 0.75em/2em Lato,sans-serif; }', function(opts) {
			this.initials = (opts.name || "").split(" ").map(function (word) {
				return word[0]
			}).join("")
			console.log(this.intials)
		
	});

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	riot.tag('pagination', '<a>«</a> <a href=\'\'></a>', function(opts) {
			console.log(opts.page)
		
	});


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	/** @jsx m */
	__webpack_require__(31)
	var m = __webpack_require__(48)
	var colours = {}
	colours.COLOURS = {"Red": "#D0021B","Orange": "#EE802A","Yellow": "#F3F800","Green": "#7ED321","Brown": "#964827","Purple": "#8B00EF","Pink": "#FF6FE6","Blue": "#313CA3","Silver": "#F6F6F6","White": "#FFFFFF","Black": "#000000","Gold": "#DCAC2B"}
	colours.handleSelect = function (callback, value) {
		callback(value)
	}
	colours.view = function (vm) {
		var selected = vm.selected.reduce(function (dict, currentValue) {
			dict[currentValue] = true;
			return dict
		}, {})
		return Object.keys(colours.COLOURS).map(function (path) {
			var isSelected = !!selected[path] ? {tag: "span", attrs: {class:"sscolours__blockpallete--selected"}, children: [m.trust("&#xE60A;")]} : ""
			return {tag: "div", attrs: {class:"sscolours__block",onclick:colours.handleSelect.bind(null, vm.onclick, path)}, children: [{tag: "div", attrs: {class:"sscolours__blockpallete",style:{"background-color": colours.COLOURS[path]}}, children: [isSelected]}, {tag: "span", attrs: {class:"sscolours__blocktext"}, children: [path]}]}
		})
	}
	module.exports = colours




/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(18);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/detail.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/detail.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, ".ssdetail__wrapper{color:#666;background:#FEFEFE;border:1px solid #f9f9f9;overflow:auto;width:80%;margin:1em auto;padding:1em;box-sizing:border-box}.ssdetail__wrapper .entry_wrapper{overflow:hidden}.ssdetail__wrapper .related_entries{margin-top:2em}.ssdetail__wrapper .ssdetail__image{text-align:center}.ssdetail__wrapper .ssdetail__image>.image{width:100%;max-width:700px}@media only screen and (max-width: 960px){.ssdetail__wrapper{width:100%;}}.ssdetail__entry{*zoom:1;float:left;clear:none;text-align:inherit;width:65.66667%;margin-left:0%;margin-right:3%}.ssdetail__entry:before,.ssdetail__entry:after{content:'';display:table}.ssdetail__entry:after{clear:both}.ssdetail__entry:last-child{margin-right:0%}.ssdetail__info{*zoom:1;float:left;clear:none;text-align:inherit;width:31.33333%;margin-left:0%;margin-right:3%}.ssdetail__info:before,.ssdetail__info:after{content:'';display:table}.ssdetail__info:after{clear:both}.ssdetail__info:last-child{margin-right:0%}.ssdetail__entry,.ssdetail__info{}@media only screen and (max-width: 960px){.ssdetail__entry,.ssdetail__info{*zoom:1;float:left;clear:none;text-align:inherit;width:100%;margin-left:0%;margin-right:3%;}.ssdetail__entry:before,.ssdetail__entry:after,.ssdetail__info:before,.ssdetail__info:after{content:'';display:table}.ssdetail__entry:after,.ssdetail__info:after{clear:both}.ssdetail__entry:last-child,.ssdetail__info:last-child{margin-right:0%}}.ssdetail__info .owner{font-size:21px;color:#4A90E2;line-height:45px;margin-top:0}.ssdetail__info .description{font-family:OpenSans;font-size:14px;color:#878787;line-height:19px;margin-bottom:0}.ssdetail__info .tags_header{font-size:24px;color:#9B9B9B;line-height:33px}.ssdetail__info .tag{border:1px solid #E0E0E0;border-radius:4px;padding-right:0.3em;display:inline-block;margin:0.5em;text-decoration:none}.ssdetail__info .tag_icon,.ssdetail__info .colour{color:#6C6C6C;padding:0.2em}.ssdetail__info .tag_icon,.share_link{font-family:ssicons}.tag_icon,.tag_text{font-size:1em;vertical-align:middle}.ssdetail__info .colour{display:inline-block;height:2em;width:2em;cursor:pointer;margin:0;padding:0;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1)}.ssdetail__info .colour:first-of-type{border-radius:5px 0 0 5px}.ssdetail__info .colour:last-child{border-radius:0 5px 5px 0}.ssdetail__wrapper .entry_wraper{overflow:auto}.ssdetail__info .tag_wrapper{margin-top:1em}.colour_wrapper{position:relative;text-align:center;background-color:#fff;padding:0;padding-bottom:1em;box-shadow:0 0 2px rgba(0,0,0,0.1);margin:1.85em auto}.colour_wrapper:hover{z-index:3}.ssdetail__info .tag_text{vertical-align:middle;color:#959595;line-height:2em}.sssharebar__wrapper{text-align:center}.share_link{font-size:1.5em;color:#888;display:inline-block;padding:0.25em 0.5em;text-decoration:none;line-height:1.5em;transition:color 0.2s ease-out}.share_text{font-size:0.8em;margin:5px;vertical-align:middle;color:#888}.twitter_share:hover{color:#4099FF}.facebook_share:hover{color:#3b5999}.pinterest_share:hover{color:#C92228}.stats ul{padding:0;overflow:auto}.stats li{border-bottom:1px solid rgba(102,102,102,0.2);color:#666;font-size:1.2em;text-align:right;line-height:1.1em;padding:0.2em}.stats li:first-child{}.stats li:last-child{border-bottom:0}.stats .stats__stat{font-size:1.2em}.stats .stats__label{font-size:0.8em;display:inline-block;padding-left:0.6em}.favourite{text-align:center;display:block;max-width:300px;*zoom:1;width:auto;max-width:70%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;border:1px solid #dadada;padding:0.3em 0;border-radius:3px;cursor:pointer;transition:color 0.1s ease-out,background-color 0.1s ease-out}.favourite:before,.favourite:after{content:'';display:table}.favourite:after{clear:both}.favourite.favourite--added{color:#fff;background:#84df84;text-shadow:0 1px 1px rgba(0,0,0,0.2);border:none}.description{border-bottom:1px solid rgba(102,102,102,0.2);padding-bottom:1em}", ""]);

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	var fonts = {'trousers': '&#xE602;', 'top': '&#xE601;', 'skirt':'&#xE603;', 'dress': '&#xE604;', 'blazer': '&#xE605;', 'shorts': '&#xE608;', 'accessories': '&#xE606;', 'facebook': '&#xE60F', 'pinterest': '&#xE611', 'email': '&#xE60D', 'twitter': '&#xE60E', 'google': '&#xE610', success:'&#xE60A'}
	module.exports = fonts

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var countUp = __webpack_require__(49)

	riot.tag('raw', '<span></span>', function(opts) {
			this.root.innerHTML = opts.value
			this.on('update', function() {
				this.root.innerHTML = opts.value		
			})
		
	});
	riot.tag('time-ago', '<span>{value}</span>', function(opts) {
		this.timeSince = function(date) {
			console.log(date)
		    var seconds = Math.floor((new Date() - date) / 1000);
		    var interval = Math.floor(seconds / 31536000);

		    if (interval > 1) {
		        return interval + " years ago";
		    }
		    interval = Math.floor(seconds / 2592000);
		    if (interval > 1) {
		        return interval + " months ago";
		    }
		    interval = Math.floor(seconds / 86400);
		    if (interval > 1) {
		        return interval + " days ago";
		    }
		    interval = Math.floor(seconds / 3600);
		    if (interval > 1) {
		        return interval + " hours ago";
		    }
		    interval = Math.floor(seconds / 60);
		    if (interval > 1) {
		        return interval + " minutes ago";
		    }
		    return "just now";
	}.bind(this);
		this.value = this.timeSince(opts.date)
			this.on('update', function () {

			})
		
	});

	riot.tag('count', '<span></span>', function(opts) {
			this.on('mount', function () {
				var anim = new countUp(this.root, 0, opts.value);
				anim.start()
			})
		
	});
	riot.tag('message', '<div class="{\'message message--\' + opts.type + \' message--\' +opts.visibility}">{opts.message}</div>', function(opts) {
				this.duration = opts.duration || 5000
				var self = this
				this.on('update', function () {
					if (opts.visibility == 'visible') {
						self.timer = window.setTimeout(opts.dismiss, self.duration)
					} else {
						window.clearTimeout(self.timer)
					}	
				})
				__webpack_require__(39);
		
	});




/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(22);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/hint.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/hint.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, "/*! Hint.css - v1.3.4 - 2015-02-28\n* http://kushagragour.in/lab/hint/\n* Copyright (c) 2015 Kushagra Gour; Licensed MIT */.hint,[data-hint]{position:relative;display:inline-block}.hint:before,.hint:after,[data-hint]:before,[data-hint]:after{position:absolute;-webkit-transform:translate3d(0, 0, 0);-moz-transform:translate3d(0, 0, 0);transform:translate3d(0, 0, 0);visibility:hidden;opacity:0;z-index:1000000;pointer-events:none;-webkit-transition:0.3s ease;-moz-transition:0.3s ease;transition:0.3s ease;-webkit-transition-delay:0ms;-moz-transition-delay:0ms;transition-delay:0ms}.hint:hover:before,.hint:hover:after,.hint:focus:before,.hint:focus:after,[data-hint]:hover:before,[data-hint]:hover:after,[data-hint]:focus:before,[data-hint]:focus:after{visibility:visible;opacity:1}.hint:hover:before,.hint:hover:after,[data-hint]:hover:before,[data-hint]:hover:after{-webkit-transition-delay:100ms;-moz-transition-delay:100ms;transition-delay:100ms}.hint:before,[data-hint]:before{content:'';position:absolute;background:transparent;border:6px solid transparent;z-index:1000001}.hint:after,[data-hint]:after{content:attr(data-hint);background:#383838;color:#fff;padding:8px 10px;font-size:12px;line-height:12px;white-space:nowrap}.hint--top:before{border-top-color:#383838}.hint--bottom:before{border-bottom-color:#383838}.hint--left:before{border-left-color:#383838}.hint--right:before{border-right-color:#383838}.hint--top:before{margin-bottom:-12px}.hint--top:after{margin-left:-18px}.hint--top:before,.hint--top:after{bottom:100%;left:50%}.hint--top:hover:after,.hint--top:hover:before,.hint--top:focus:after,.hint--top:focus:before{-webkit-transform:translateY(-8px);-moz-transform:translateY(-8px);transform:translateY(-8px)}.hint--bottom:before{margin-top:-12px}.hint--bottom:after{margin-left:-18px}.hint--bottom:before,.hint--bottom:after{top:100%;left:50%}.hint--bottom:hover:after,.hint--bottom:hover:before,.hint--bottom:focus:after,.hint--bottom:focus:before{-webkit-transform:translateY(8px);-moz-transform:translateY(8px);transform:translateY(8px)}.hint--right:before{margin-left:-12px;margin-bottom:-6px}.hint--right:after{margin-bottom:-14px}.hint--right:before,.hint--right:after{left:100%;bottom:50%}.hint--right:hover:after,.hint--right:hover:before,.hint--right:focus:after,.hint--right:focus:before{-webkit-transform:translateX(8px);-moz-transform:translateX(8px);transform:translateX(8px)}.hint--left:before{margin-right:-12px;margin-bottom:-6px}.hint--left:after{margin-bottom:-14px}.hint--left:before,.hint--left:after{right:100%;bottom:50%}.hint--left:hover:after,.hint--left:hover:before,.hint--left:focus:after,.hint--left:focus:before{-webkit-transform:translateX(-8px);-moz-transform:translateX(-8px);transform:translateX(-8px)}.hint:after,[data-hint]:after{text-shadow:0 -1px 0 #000;box-shadow:4px 4px 8px rgba(0,0,0,0.3)}.hint--error:after{background-color:#b34e4d;text-shadow:0 -1px 0 #592726}.hint--error.hint--top:before{border-top-color:#b34e4d}.hint--error.hint--bottom:before{border-bottom-color:#b34e4d}.hint--error.hint--left:before{border-left-color:#b34e4d}.hint--error.hint--right:before{border-right-color:#b34e4d}.hint--warning:after{background-color:#c09854;text-shadow:0 -1px 0 #6c5328}.hint--warning.hint--top:before{border-top-color:#c09854}.hint--warning.hint--bottom:before{border-bottom-color:#c09854}.hint--warning.hint--left:before{border-left-color:#c09854}.hint--warning.hint--right:before{border-right-color:#c09854}.hint--info:after{background-color:#3986ac;text-shadow:0 -1px 0 #193b4d}.hint--info.hint--top:before{border-top-color:#3986ac}.hint--info.hint--bottom:before{border-bottom-color:#3986ac}.hint--info.hint--left:before{border-left-color:#3986ac}.hint--info.hint--right:before{border-right-color:#3986ac}.hint--success:after{background-color:#458746;text-shadow:0 -1px 0 #1a321a}.hint--success.hint--top:before{border-top-color:#458746}.hint--success.hint--bottom:before{border-bottom-color:#458746}.hint--success.hint--left:before{border-left-color:#458746}.hint--success.hint--right:before{border-right-color:#458746}.hint--always:after,.hint--always:before{opacity:1;visibility:visible}.hint--always.hint--top:after,.hint--always.hint--top:before{-webkit-transform:translateY(-8px);-moz-transform:translateY(-8px);transform:translateY(-8px)}.hint--always.hint--bottom:after,.hint--always.hint--bottom:before{-webkit-transform:translateY(8px);-moz-transform:translateY(8px);transform:translateY(8px)}.hint--always.hint--left:after,.hint--always.hint--left:before{-webkit-transform:translateX(-8px);-moz-transform:translateX(-8px);transform:translateX(-8px)}.hint--always.hint--right:after,.hint--always.hint--right:before{-webkit-transform:translateX(8px);-moz-transform:translateX(8px);transform:translateX(8px)}.hint--rounded:after{border-radius:4px}.hint--no-animate:before,.hint--no-animate:after{-webkit-transition-duration:0ms;-moz-transition-duration:0ms;transition-duration:0ms}.hint--bounce:before,.hint--bounce:after{-webkit-transition:opacity 0.3s ease,visibility 0.3s ease,-webkit-transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24);-moz-transition:opacity 0.3s ease,visibility 0.3s ease,-moz-transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24);transition:opacity 0.3s ease,visibility 0.3s ease,transform 0.3s cubic-bezier(0.71, 1.7, 0.77, 1.24)}", ""]);

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	riot.tag('flexgrid', '<div style="display:flex; flex-wrap:wrap"> <div style="flex: 1 0 auto; padding: 0.5em 0; display:inline-block" each="{item in items}"> <a href="/a/{item.Ref}"> <div riot-style="height: 250px; background-size:cover; background: transparent url(/static/images/ankara/{item.Image}_m.jpg) center center no-repeat"></div> </a> </div> </div>', function(opts) {
		this.on("update", function () {
			this.items = opts.items
			console.log(this.items)
		})
		
	});

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(25);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/login.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/login.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, ".sslogin__wrapper{width:80%;max-width:500px;background-color:#fff;border-radius:7px;padding:0.5em 1em 2em;margin:auto}.sslogin__wrapper .close{font-size:2em;font-weight:bold;text-align:right;cursor:pointer;margin:0}.sslogin__wrapper .not_ready{margin:1.5em 0 0.5em 0;cursor:pointer;text-decoration:underline;text-align:center;font-size:0.8em}.sslogin__wrapper .sslogin__intro_text{text-align:center;color:#888;margin:0 auto 2em}.sslogin__wrapper .srlogin__button{max-width:350px;margin:0.75em auto;text-decoration:none;outline:none;text-align:center;display:flex;font-size:0.9em;color:#fff;text-shadow:0 1px 1px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button .srlogin__provider__icon{display:inline-block;width:10%;font-family:ssicons;font-size:1.25em;font-weight:bold;margin:auto 0.5em}.sslogin__wrapper .srlogin__button .srlogin__provider__text{margin:auto}.sslogin__wrapper .srlogin__provider__icon{line-height:2.5em}.sslogin__wrapper .srlogin__button__facebook{background-color:#3b5999}.sslogin__wrapper .srlogin__button__facebook:hover{background-color:#2d4474;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button__twitter{background-color:#4099FF}.sslogin__wrapper .srlogin__button__twitter:hover{background-color:#0d7eff;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}.sslogin__wrapper .srlogin__button__google{background-color:#DD4B39}.sslogin__wrapper .srlogin__button__google:hover{background-color:#c23321;box-shadow:inset 0 1px 3px rgba(0,0,0,0.3)}", ""]);

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(27);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/main.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/main.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, "@font-face{font-family:\"Lato-Light\";src:url('/static/fonts/Lato-Light.eot');src:local('/static/fonts/Lato-Light');src:url('/static/fonts/Lato-Light.svg#Lato-Light') format('svg');src:url('/static/fonts/Lato-Light.woff') format('woff');}body{font-family:Lato-Light sans-serif;color:#222 !important}#content{margin:8em auto 5em 0}.ssentry__image__wrapper,.ssentry__details__wrapper{min-height:300px;text-align:left;display:inline;clear:none;width:auto;margin-left:0;margin-right:0;*zoom:1;float:left;clear:none;text-align:inherit;width:48.5%;margin-left:0%;margin-right:3%}.ssentry__image__wrapper:first-child,.ssentry__details__wrapper:first-child{margin-left:0}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:0}.ssentry__image__wrapper:before,.ssentry__image__wrapper:after,.ssentry__details__wrapper:before,.ssentry__details__wrapper:after{content:'';display:table}.ssentry__image__wrapper:after,.ssentry__details__wrapper:after{clear:both}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:0%}@media only screen and (max-width: 960px){.ssentry__image__wrapper,.ssentry__details__wrapper{display:block;clear:both;float:none;width:100%;margin-left:auto;margin-right:auto;}.ssentry__image__wrapper:first-child,.ssentry__details__wrapper:first-child{margin-left:auto}.ssentry__image__wrapper:last-child,.ssentry__details__wrapper:last-child{margin-right:auto}}.ssentry__image__wrapper{position:relative;background-color:#fefefe}.ssentry__image__wrapper:hover .ssimage__noimagetext{opacity:0.85}.ssentry__descriptioninput{border:none;outline:none;display:block;width:100%;font-size:2em;color:#666;text-align:center}.ssnewentry__wrapper{*zoom:1;width:auto;max-width:90%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;margin:1em auto}.ssnewentry__wrapper:before,.ssnewentry__wrapper:after{content:'';display:table}.ssnewentry__wrapper:after{clear:both}.sstags__stylepicker{text-align:center;color:#6f6f6f;background-color:#fcfcfc;padding:0.3em;font-weight:normal;font-size:1.1em}.sstags__stylepickerwrapper{background-color:#fefefe;box-shadow:0 1px 3px rgba(0,0,0,0.1);margin:1em 0 2em}.ssnewentry__submit{border:none;border-radius:7px;background-image:-o-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-moz-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-webkit-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:-ms-linear-gradient(-90deg, #8AE160 0%, #52AE26 100%);background-image:linear-gradient(-180deg, #8AE160 0%, #52AE26 100%);-moz-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.6);-webkit-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.6);box-shadow:0px 1px 2px 0px rgba(0,0,0,0.6);display:block;width:40%;margin:1em auto;font-family:OpenSans;font-size:1.3em;color:#FFFFFF;text-transform:uppercase;padding:0.5em;text-shadow:0px 1px 0px rgba(255,255,255,0.16)}.ssnewentry__submit:hover{background-image:-o-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-moz-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-webkit-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:-ms-linear-gradient(-90deg, #58B32C 0%, #52AE26 100%);background-image:linear-gradient(-180deg, #58B32C 0%, #52AE26 100%);-moz-box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82);-webkit-box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82);box-shadow:inset 0px 2px 2px 0px rgba(42,114,5,0.82)}.nav{*zoom:1;width:auto;max-width:30%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;margin-top:2em}.nav:before,.nav:after{content:'';display:table}.nav:after{clear:both}@media only screen and (max-width: 960px){.nav{*zoom:1;width:auto;max-width:60%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.nav:before,.nav:after{content:'';display:table}.nav:after{clear:both}}.nav .next_page,.nav .prev_page{display:block;width:50%;background-color:#d2d2d2;border-radius:50%;color:#f9f9f9;border:solid 1px #dfdfdf;width:3.5em;height:3.5em;font-size:1.75em;text-align:center;line-height:3.5em}.nav .next_page,.nav .prev_page{cursor:pointer}.nav .next_page{float:right}.nav .prev_page{float:left}.no_images_container{width:80%;margin:auto;text-align:center;min-height:300px;height:300px}.no_images_container .no_images_text{vertical-align:middle;display:inline-block;padding:2em;background-color:#fff;color:#657657;border-radius:5px;box-shadow:0 0 4px rgba(0,0,0,0.5)}.no_images_container:before{vertical-align:middle;content:\"\";height:100%;display:inline-block}", ""]);

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	__webpack_require__(41)
	var ajax = __webpack_require__(37)
	riot.tag('entry-image', '<div if="{loading}" class="ssimage__loadingimage"></div> <input class="ssimage__imageinput" type="file" onchange="{ onImageChange }"> <div if="{!!opts.image}"> <img if="{!!opts.image}" style="width:100%" riot-src="/static/images/ankara/{ opts.image }_m.jpg"}> <p class="ssimage__changephoto">change photo</p> </div> <div if="{!loading && !opts.image}" class="ssimage__noimage"> <p class="ssimage__noimagetext">+ add a photo</p> </div>', function(opts) {
			this.loading = false
			var self = this
			console.log(opts)
			this.onImageChange = function (e) {
				console.log(this)
				var file = e.target.files[0]
				var formData = new FormData

				    formData.append("file", file)

				console.log("changing: ", file)
				self.update({loading: true})
				ajax
					.post("/imageprocessor/entry")
					.attach("file", file)
					.end(function (error, response) {
						if (!error) {
							opts.onchange(response.body)
						}
						self.update({loading:false})
					})
			}
		
	});

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

		
	__webpack_require__(43)
	var fonts = __webpack_require__(19)
	__webpack_require__(20)
	riot.tag('tag', '<div key="{opts.text}" onclick="{opts.toggle}" class="{\'sstags__wrapper sstags__wrapper--\' + opts.gender.toLowerCase()}"> <span class="{\'sstags__icon \' + (opts.isselected ? \'sstags__icon--selected\' : \'\')}"><raw value="{!!opts.isselected ? FONTS[\'success\'] : FONTS[opts.text.toLowerCase()]}"></raw></span> <span class="sstags__text">{opts.text}</span> </div>', function(opts) {
		this.FONTS = fonts
		var self = this
		this.on('update', function () {


		})
		
	});

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);


	riot.tag('colours', '<div class="sscolours__block" onclick="{opts.toggle}" > <div class="sscolours__blockpallete" riot-style="background-color: {opts.background}"> <span if="{!!opts.isselected}" class="sscolours__blockpallete--selected"> <raw value="&#xE60A;"></raw></span> </div> <span class="sscolours__blocktext">{opts.label}</span> </div>', function(opts) {
			this.on('update', function() {


			})
		
	});

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(32);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/colours.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, "@font-face{font-family:\"ssicons\";src:url('/static/fonts/ss.eot');src:local('/static/fonts/ss');src:url('/static/fonts/ss.svg#ss') format('svg');src:url('/static/fonts/ss.woff') format('woff');}.sscolours__block{width:14%;margin:1%;display:inline-block;text-align:center}@media only screen and (max-width: 960px){.sscolours__block{width:20%;margin-left:2.5%;}}.sscolours__block .sscolours__blocktext{font-size:0.8em;font-weight:bold;color:#666;display:block}.sscolours__block .sscolours__blockpallete{margin:auto;box-shadow:inset 0 1px 3px rgba(0,0,0,0.2);width:35px;height:35px;border-radius:5px}@media only screen and (max-width: 960px){.sscolours__block .sscolours__blockpallete{width:50px;height:50px;}}.sscolours__block .sscolours__blockpallete--selected{color:#68BB0D !important;background-color:#fff;background-color:rgba(255,255,255,0.9);font-size:0.9em;padding:0.5em;border-radius:5px;line-height:35px;font-family:ssicons}@media only screen and (max-width: 960px){.sscolours__block .sscolours__blockpallete--selected{line-height:50px;}}", ""]);

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(13);

	var fonts = __webpack_require__(19)
	__webpack_require__(20)
	riot.tag('genders', '<gender each="{label, icon in defaultGenders}" label="{label}" onclick="{parent.opts.toggle.bind(null, label)}" } isselected="{parent.opts.selectedgender==label}" selectedicon="{parent.selectedIcon}" defaulticon = {parent.defaultGenders[label]}></gender>', function(opts) {
		this.selectedIcon = fonts.success
		this.defaultGenders = opts.defaultgenders
		console.log(this.defaultGenders, opts)

		this.selectedgender = opts.selectedgender
		
	});
	riot.tag('gender', '<div onclick="{opts.toggle.bind(null, label)}" class="sstags__wrapper sstags__wrapper--{label.toLowerCase()}"> <span class="{\'sstags__icon \'+ (opts.isselected ? \'sstags__icon--selected\' : \'\')}"> <raw value="{opts.isselected ? opts.selectedicon : opts.defaulticon}"></raw> </span> <span class="sstags__text">{opts.label}</span> </div>', function(opts) {
			
	});


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(35);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/profile.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, "#content{margin-top:4em}.profile_header__wrapper .profile_header{padding-top:60px;min-height:300px;background:transparent url(/static/images/hero.jpg) 0% 60% no-repeat;background-size:cover}.profile_header__wrapper .profile_header .profile_image{position:relative;left:20.6%;display:inline-block;height:150px;box-sizing:border-box;width:150px;border:5px solid #fcfcfc;vertical-align:middle;border-radius:50%;box-shadow:0px 2px 8px rgba(0,0,0,0.2);background:transparent url(/static/images/viccircle1.png) 0% 60% no-repeat;background-size:contain}@media only screen and (max-width: 960px){.profile_header__wrapper .profile_header .profile_image{position:static;left:0;margin:auto;*zoom:1;width:auto;max-width:150px;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.profile_header__wrapper .profile_header .profile_image:before,.profile_header__wrapper .profile_header .profile_image:after{content:'';display:table}.profile_header__wrapper .profile_header .profile_image:after{clear:both}}.profile_header__wrapper .profile_header .profile_personal{position:relative;left:29.42857%;text-shadow:1px 1px 2px rgba(0,0,0,0.5);display:inline-block;vertical-align:middle;color:#fff}@media only screen and (max-width: 960px){.profile_header__wrapper .profile_header .profile_personal{position:static;left:0;*zoom:1;width:auto;max-width:100%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;text-align:center;}.profile_header__wrapper .profile_header .profile_personal:before,.profile_header__wrapper .profile_header .profile_personal:after{content:'';display:table}.profile_header__wrapper .profile_header .profile_personal:after{clear:both}}.profile_header__wrapper .profile_header .display_name{font-size:2.5em}.profile_header__wrapper .profile_header .description{font-family:\"OpenSans\";font-size:1.1em}.profile_stats{background-color:#f9f9f9;*zoom:1;width:auto;max-width:80%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0}.profile_stats:before,.profile_stats:after{content:'';display:table}.profile_stats:after{clear:both}@media only screen and (max-width: 960px){.profile_stats{*zoom:1;width:auto;max-width:90%;float:none;display:block;margin-right:auto;margin-left:auto;padding-left:0;padding-right:0;}.profile_stats:before,.profile_stats:after{content:'';display:table}.profile_stats:after{clear:both}}.profile_stats .stat{box-sizing:border-box;*zoom:1;float:left;clear:none;text-align:inherit;width:25%;margin-left:0%;margin-right:0%;font-size:1.5em;line-height:3em;text-align:center;border-left:1px solid #e2e2e2;border-right:1px solid #e9e9e9;color:#7d7d7d;text-shadow:1px 1px #fafafa}.profile_stats .stat:before,.profile_stats .stat:after{content:'';display:table}.profile_stats .stat:after{clear:both}.profile_main{*zoom:1}.profile_main:before,.profile_main:after{content:'';display:table}.profile_main:after{clear:both}.profile_stats .uploads,.profile_stats .collections{color:#444;cursor:pointer;transition:border-bottom 0.3s ease-out;font-size:1.5em;line-height:1.5em}.profile_stats .uploads .stat__label,.profile_stats .collections .stat__label{display:inline-block;padding:0.5em 0}.profile_stats .uploads .stat__value,.profile_stats .collections .stat__value{font-size:0.9em;color:#fff;display:inline-block;text-shadow:0 1px 2px rgba(0,0,0,0.2);margin-left:1em;padding:0 0.15em;min-width:1.5em;border-radius:5px;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1)}.profile_stats .uploads .stat__value{background-color:#68AAF8}.profile_stats .collections .stat__value{background-color:#fd453f}.profile_stats.uploads .uploads{border-bottom:3px solid #68AAF8}.profile_stats.uploads .collections{border-bottom:0}.profile_stats.collections .collections{border-bottom:3px solid #fd453f}.profile_stats.collections .uploads{border-bottom:0}.pad{background-color:#fff;display:block;width:97.5%;margin:auto;padding:7px;border-radius:5px;box-shadow:0 0 7px rgba(0,0,0,0.25)}.profile__editor__header{color:#fff;display:flex;width:100%;min-height:350px;background:transparent url(/static/images/hero.jpg) 0% 60% no-repeat;background-size:cover}.profile__editor__header .header_contents{width:50%;padding:1em 0;margin:auto;background-color:rgba(0,0,0,0.4)}@media only screen and (max-width: 960px){.profile__editor__header .header_contents{width:80%;}}.profile__editor__header .header__avatar__wrapper{margin:0;font-size:3em;text-align:center}.profile__editor__header .header__profile__description{text-align:center;margin:auto;box-sizing:border-box}.profile__editor__body{padding:1em;margin:1em auto;width:60%;background-color:#fefefe;border-radius:7px;box-shadow:0 0 2px rgba(0,0,0,0.2)}.profile__editor__body .profile__body__wrapper{width:70%;margin:auto}@media only screen and (max-width: 960px){.profile__editor__body .profile__body__wrapper{width:100%;}}@media only screen and (max-width: 960px){.profile__editor__body{width:80%;}}.profile__input--isbrand{visibility:hidden;height:0;width:0}.profile__input{width:100%;font-weight:300;font-size:0.9em;padding:1em 0 0;display:block;background-color:transparent;border:none;outline:none;border-bottom:1px solid #888;box-sizing:border-box;transition:border-width 0.2s ease-out,max-height 0.1s ease-out}.profile__input--isbrand ~ .profile__input--brandname{max-height:0;opacity:0;padding:0}.profile__input--isbrand:checked ~ .profile__input--brandname{max-height:300px;opacity:1}.profile__input--brandname{margin-top:0.5em;margin-bottom:0}.profile__input:focus{border-bottom-width:2px;outline:none}.profile__done__button{border:none;color:#fff;background-color:#68D8AA;display:block;width:100px;margin:auto;padding:1em}.profile__input__label{font-weight:bold;font-size:0.9em}.switch__toggle__wrapper{cursor:pointer;display:block;position:relative;box-sizing:border-box;height:35px;width:70px;border:1px solid #898989;border-radius:17.5px;box-shadow:inset 0 1px 3px rgba(0,0,0,0.1)}.switch__toggle__wrapper .switch__toggle{transition:left 1s ease-in-out;top:-1px;display:block;width:35px;height:35px;border:1px solid #898989;border-radius:50%;box-sizing:border-box;position:relative}.switch__toggle__wrapper:after,.switch__toggle__wrapper:before{position:absolute;line-height:35px;color:#7c7c7c;padding:0 5px}.switch__toggle__wrapper:after{content:attr(on);left:100%;top:0}.switch__toggle__wrapper:before{font-weight:bolder;content:attr(off);right:100%}.profile__input--isbrand:checked ~ .switch__toggle__wrapper .switch__toggle{left:49%;background-color:#898989}.profile__input--isbrand:checked ~ .switch__toggle__wrapper .switch__toggle:after{font-weight:bolder}.profile__input--isbrand:checked ~ .switch__toggle__wrapper .switch__toggle:before{font-weight:normal}", ""]);

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isIE9 = memoize(function() {
			return /msie 9\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0;

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isIE9();

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function createStyleElement() {
		var styleElement = document.createElement("style");
		var head = getHeadElement();
		styleElement.type = "text/css";
		head.appendChild(styleElement);
		return styleElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement());
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else {
			styleElement = createStyleElement();
			update = applyToTag.bind(null, styleElement);
			remove = function () {
				styleElement.parentNode.removeChild(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	function replaceText(source, id, replacement) {
		var boundaries = ["/** >>" + id + " **/", "/** " + id + "<< **/"];
		var start = source.lastIndexOf(boundaries[0]);
		var wrappedReplacement = replacement
			? (boundaries[0] + replacement + boundaries[1])
			: "";
		if (source.lastIndexOf(boundaries[0]) >= 0) {
			var end = source.lastIndexOf(boundaries[1]) + boundaries[1].length;
			return source.slice(0, start) + wrappedReplacement + source.slice(end);
		} else {
			return source + wrappedReplacement;
		}
	}

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(styleElement.styleSheet.cssText, index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap && typeof btoa === "function") {
			try {
				css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(JSON.stringify(sourceMap)) + " */";
				css = "@import url(\"data:text/css;base64," + btoa(css) + "\")";
			} catch(e) {}
		}

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}


/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Module dependencies.
	 */

	var Emitter = __webpack_require__(50);
	var reduce = __webpack_require__(51);

	/**
	 * Root reference for iframes.
	 */

	var root = 'undefined' == typeof window
	  ? (this || self)
	  : window;

	/**
	 * Noop.
	 */

	function noop(){};

	/**
	 * Check if `obj` is a host object,
	 * we don't want to serialize these :)
	 *
	 * TODO: future proof, move to compoent land
	 *
	 * @param {Object} obj
	 * @return {Boolean}
	 * @api private
	 */

	function isHost(obj) {
	  var str = {}.toString.call(obj);

	  switch (str) {
	    case '[object File]':
	    case '[object Blob]':
	    case '[object FormData]':
	      return true;
	    default:
	      return false;
	  }
	}

	/**
	 * Determine XHR.
	 */

	request.getXHR = function () {
	  if (root.XMLHttpRequest
	      && (!root.location || 'file:' != root.location.protocol
	          || !root.ActiveXObject)) {
	    return new XMLHttpRequest;
	  } else {
	    try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch(e) {}
	  }
	  return false;
	};

	/**
	 * Removes leading and trailing whitespace, added to support IE.
	 *
	 * @param {String} s
	 * @return {String}
	 * @api private
	 */

	var trim = ''.trim
	  ? function(s) { return s.trim(); }
	  : function(s) { return s.replace(/(^\s*|\s*$)/g, ''); };

	/**
	 * Check if `obj` is an object.
	 *
	 * @param {Object} obj
	 * @return {Boolean}
	 * @api private
	 */

	function isObject(obj) {
	  return obj === Object(obj);
	}

	/**
	 * Serialize the given `obj`.
	 *
	 * @param {Object} obj
	 * @return {String}
	 * @api private
	 */

	function serialize(obj) {
	  if (!isObject(obj)) return obj;
	  var pairs = [];
	  for (var key in obj) {
	    if (null != obj[key]) {
	      pairs.push(encodeURIComponent(key)
	        + '=' + encodeURIComponent(obj[key]));
	    }
	  }
	  return pairs.join('&');
	}

	/**
	 * Expose serialization method.
	 */

	 request.serializeObject = serialize;

	 /**
	  * Parse the given x-www-form-urlencoded `str`.
	  *
	  * @param {String} str
	  * @return {Object}
	  * @api private
	  */

	function parseString(str) {
	  var obj = {};
	  var pairs = str.split('&');
	  var parts;
	  var pair;

	  for (var i = 0, len = pairs.length; i < len; ++i) {
	    pair = pairs[i];
	    parts = pair.split('=');
	    obj[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
	  }

	  return obj;
	}

	/**
	 * Expose parser.
	 */

	request.parseString = parseString;

	/**
	 * Default MIME type map.
	 *
	 *     superagent.types.xml = 'application/xml';
	 *
	 */

	request.types = {
	  html: 'text/html',
	  json: 'application/json',
	  xml: 'application/xml',
	  urlencoded: 'application/x-www-form-urlencoded',
	  'form': 'application/x-www-form-urlencoded',
	  'form-data': 'application/x-www-form-urlencoded'
	};

	/**
	 * Default serialization map.
	 *
	 *     superagent.serialize['application/xml'] = function(obj){
	 *       return 'generated xml here';
	 *     };
	 *
	 */

	 request.serialize = {
	   'application/x-www-form-urlencoded': serialize,
	   'application/json': JSON.stringify
	 };

	 /**
	  * Default parsers.
	  *
	  *     superagent.parse['application/xml'] = function(str){
	  *       return { object parsed from str };
	  *     };
	  *
	  */

	request.parse = {
	  'application/x-www-form-urlencoded': parseString,
	  'application/json': JSON.parse
	};

	/**
	 * Parse the given header `str` into
	 * an object containing the mapped fields.
	 *
	 * @param {String} str
	 * @return {Object}
	 * @api private
	 */

	function parseHeader(str) {
	  var lines = str.split(/\r?\n/);
	  var fields = {};
	  var index;
	  var line;
	  var field;
	  var val;

	  lines.pop(); // trailing CRLF

	  for (var i = 0, len = lines.length; i < len; ++i) {
	    line = lines[i];
	    index = line.indexOf(':');
	    field = line.slice(0, index).toLowerCase();
	    val = trim(line.slice(index + 1));
	    fields[field] = val;
	  }

	  return fields;
	}

	/**
	 * Return the mime type for the given `str`.
	 *
	 * @param {String} str
	 * @return {String}
	 * @api private
	 */

	function type(str){
	  return str.split(/ *; */).shift();
	};

	/**
	 * Return header field parameters.
	 *
	 * @param {String} str
	 * @return {Object}
	 * @api private
	 */

	function params(str){
	  return reduce(str.split(/ *; */), function(obj, str){
	    var parts = str.split(/ *= */)
	      , key = parts.shift()
	      , val = parts.shift();

	    if (key && val) obj[key] = val;
	    return obj;
	  }, {});
	};

	/**
	 * Initialize a new `Response` with the given `xhr`.
	 *
	 *  - set flags (.ok, .error, etc)
	 *  - parse header
	 *
	 * Examples:
	 *
	 *  Aliasing `superagent` as `request` is nice:
	 *
	 *      request = superagent;
	 *
	 *  We can use the promise-like API, or pass callbacks:
	 *
	 *      request.get('/').end(function(res){});
	 *      request.get('/', function(res){});
	 *
	 *  Sending data can be chained:
	 *
	 *      request
	 *        .post('/user')
	 *        .send({ name: 'tj' })
	 *        .end(function(res){});
	 *
	 *  Or passed to `.send()`:
	 *
	 *      request
	 *        .post('/user')
	 *        .send({ name: 'tj' }, function(res){});
	 *
	 *  Or passed to `.post()`:
	 *
	 *      request
	 *        .post('/user', { name: 'tj' })
	 *        .end(function(res){});
	 *
	 * Or further reduced to a single call for simple cases:
	 *
	 *      request
	 *        .post('/user', { name: 'tj' }, function(res){});
	 *
	 * @param {XMLHTTPRequest} xhr
	 * @param {Object} options
	 * @api private
	 */

	function Response(req, options) {
	  options = options || {};
	  this.req = req;
	  this.xhr = this.req.xhr;
	  // responseText is accessible only if responseType is '' or 'text' and on older browsers
	  this.text = ((this.req.method !='HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text')) || typeof this.xhr.responseType === 'undefined')
	     ? this.xhr.responseText
	     : null;
	  this.statusText = this.req.xhr.statusText;
	  this.setStatusProperties(this.xhr.status);
	  this.header = this.headers = parseHeader(this.xhr.getAllResponseHeaders());
	  // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
	  // getResponseHeader still works. so we get content-type even if getting
	  // other headers fails.
	  this.header['content-type'] = this.xhr.getResponseHeader('content-type');
	  this.setHeaderProperties(this.header);
	  this.body = this.req.method != 'HEAD'
	    ? this.parseBody(this.text ? this.text : this.xhr.response)
	    : null;
	}

	/**
	 * Get case-insensitive `field` value.
	 *
	 * @param {String} field
	 * @return {String}
	 * @api public
	 */

	Response.prototype.get = function(field){
	  return this.header[field.toLowerCase()];
	};

	/**
	 * Set header related properties:
	 *
	 *   - `.type` the content type without params
	 *
	 * A response of "Content-Type: text/plain; charset=utf-8"
	 * will provide you with a `.type` of "text/plain".
	 *
	 * @param {Object} header
	 * @api private
	 */

	Response.prototype.setHeaderProperties = function(header){
	  // content-type
	  var ct = this.header['content-type'] || '';
	  this.type = type(ct);

	  // params
	  var obj = params(ct);
	  for (var key in obj) this[key] = obj[key];
	};

	/**
	 * Parse the given body `str`.
	 *
	 * Used for auto-parsing of bodies. Parsers
	 * are defined on the `superagent.parse` object.
	 *
	 * @param {String} str
	 * @return {Mixed}
	 * @api private
	 */

	Response.prototype.parseBody = function(str){
	  var parse = request.parse[this.type];
	  return parse && str && (str.length || str instanceof Object)
	    ? parse(str)
	    : null;
	};

	/**
	 * Set flags such as `.ok` based on `status`.
	 *
	 * For example a 2xx response will give you a `.ok` of __true__
	 * whereas 5xx will be __false__ and `.error` will be __true__. The
	 * `.clientError` and `.serverError` are also available to be more
	 * specific, and `.statusType` is the class of error ranging from 1..5
	 * sometimes useful for mapping respond colors etc.
	 *
	 * "sugar" properties are also defined for common cases. Currently providing:
	 *
	 *   - .noContent
	 *   - .badRequest
	 *   - .unauthorized
	 *   - .notAcceptable
	 *   - .notFound
	 *
	 * @param {Number} status
	 * @api private
	 */

	Response.prototype.setStatusProperties = function(status){
	  // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
	  if (status === 1223) {
	    status = 204;
	  }

	  var type = status / 100 | 0;

	  // status / class
	  this.status = status;
	  this.statusType = type;

	  // basics
	  this.info = 1 == type;
	  this.ok = 2 == type;
	  this.clientError = 4 == type;
	  this.serverError = 5 == type;
	  this.error = (4 == type || 5 == type)
	    ? this.toError()
	    : false;

	  // sugar
	  this.accepted = 202 == status;
	  this.noContent = 204 == status;
	  this.badRequest = 400 == status;
	  this.unauthorized = 401 == status;
	  this.notAcceptable = 406 == status;
	  this.notFound = 404 == status;
	  this.forbidden = 403 == status;
	};

	/**
	 * Return an `Error` representative of this response.
	 *
	 * @return {Error}
	 * @api public
	 */

	Response.prototype.toError = function(){
	  var req = this.req;
	  var method = req.method;
	  var url = req.url;

	  var msg = 'cannot ' + method + ' ' + url + ' (' + this.status + ')';
	  var err = new Error(msg);
	  err.status = this.status;
	  err.method = method;
	  err.url = url;

	  return err;
	};

	/**
	 * Expose `Response`.
	 */

	request.Response = Response;

	/**
	 * Initialize a new `Request` with the given `method` and `url`.
	 *
	 * @param {String} method
	 * @param {String} url
	 * @api public
	 */

	function Request(method, url) {
	  var self = this;
	  Emitter.call(this);
	  this._query = this._query || [];
	  this.method = method;
	  this.url = url;
	  this.header = {};
	  this._header = {};
	  this.on('end', function(){
	    var err = null;
	    var res = null;

	    try {
	      res = new Response(self);
	    } catch(e) {
	      err = new Error('Parser is unable to parse the response');
	      err.parse = true;
	      err.original = e;
	      return self.callback(err);
	    }

	    self.emit('response', res);

	    if (err) {
	      return self.callback(err, res);
	    }

	    if (res.status >= 200 && res.status < 300) {
	      return self.callback(err, res);
	    }

	    var new_err = new Error(res.statusText || 'Unsuccessful HTTP response');
	    new_err.original = err;
	    new_err.response = res;
	    new_err.status = res.status;

	    self.callback(err || new_err, res);
	  });
	}

	/**
	 * Mixin `Emitter`.
	 */

	Emitter(Request.prototype);

	/**
	 * Allow for extension
	 */

	Request.prototype.use = function(fn) {
	  fn(this);
	  return this;
	}

	/**
	 * Set timeout to `ms`.
	 *
	 * @param {Number} ms
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.timeout = function(ms){
	  this._timeout = ms;
	  return this;
	};

	/**
	 * Clear previous timeout.
	 *
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.clearTimeout = function(){
	  this._timeout = 0;
	  clearTimeout(this._timer);
	  return this;
	};

	/**
	 * Abort the request, and clear potential timeout.
	 *
	 * @return {Request}
	 * @api public
	 */

	Request.prototype.abort = function(){
	  if (this.aborted) return;
	  this.aborted = true;
	  this.xhr.abort();
	  this.clearTimeout();
	  this.emit('abort');
	  return this;
	};

	/**
	 * Set header `field` to `val`, or multiple fields with one object.
	 *
	 * Examples:
	 *
	 *      req.get('/')
	 *        .set('Accept', 'application/json')
	 *        .set('X-API-Key', 'foobar')
	 *        .end(callback);
	 *
	 *      req.get('/')
	 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
	 *        .end(callback);
	 *
	 * @param {String|Object} field
	 * @param {String} val
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.set = function(field, val){
	  if (isObject(field)) {
	    for (var key in field) {
	      this.set(key, field[key]);
	    }
	    return this;
	  }
	  this._header[field.toLowerCase()] = val;
	  this.header[field] = val;
	  return this;
	};

	/**
	 * Remove header `field`.
	 *
	 * Example:
	 *
	 *      req.get('/')
	 *        .unset('User-Agent')
	 *        .end(callback);
	 *
	 * @param {String} field
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.unset = function(field){
	  delete this._header[field.toLowerCase()];
	  delete this.header[field];
	  return this;
	};

	/**
	 * Get case-insensitive header `field` value.
	 *
	 * @param {String} field
	 * @return {String}
	 * @api private
	 */

	Request.prototype.getHeader = function(field){
	  return this._header[field.toLowerCase()];
	};

	/**
	 * Set Content-Type to `type`, mapping values from `request.types`.
	 *
	 * Examples:
	 *
	 *      superagent.types.xml = 'application/xml';
	 *
	 *      request.post('/')
	 *        .type('xml')
	 *        .send(xmlstring)
	 *        .end(callback);
	 *
	 *      request.post('/')
	 *        .type('application/xml')
	 *        .send(xmlstring)
	 *        .end(callback);
	 *
	 * @param {String} type
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.type = function(type){
	  this.set('Content-Type', request.types[type] || type);
	  return this;
	};

	/**
	 * Set Accept to `type`, mapping values from `request.types`.
	 *
	 * Examples:
	 *
	 *      superagent.types.json = 'application/json';
	 *
	 *      request.get('/agent')
	 *        .accept('json')
	 *        .end(callback);
	 *
	 *      request.get('/agent')
	 *        .accept('application/json')
	 *        .end(callback);
	 *
	 * @param {String} accept
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.accept = function(type){
	  this.set('Accept', request.types[type] || type);
	  return this;
	};

	/**
	 * Set Authorization field value with `user` and `pass`.
	 *
	 * @param {String} user
	 * @param {String} pass
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.auth = function(user, pass){
	  var str = btoa(user + ':' + pass);
	  this.set('Authorization', 'Basic ' + str);
	  return this;
	};

	/**
	* Add query-string `val`.
	*
	* Examples:
	*
	*   request.get('/shoes')
	*     .query('size=10')
	*     .query({ color: 'blue' })
	*
	* @param {Object|String} val
	* @return {Request} for chaining
	* @api public
	*/

	Request.prototype.query = function(val){
	  if ('string' != typeof val) val = serialize(val);
	  if (val) this._query.push(val);
	  return this;
	};

	/**
	 * Write the field `name` and `val` for "multipart/form-data"
	 * request bodies.
	 *
	 * ``` js
	 * request.post('/upload')
	 *   .field('foo', 'bar')
	 *   .end(callback);
	 * ```
	 *
	 * @param {String} name
	 * @param {String|Blob|File} val
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.field = function(name, val){
	  if (!this._formData) this._formData = new root.FormData();
	  this._formData.append(name, val);
	  return this;
	};

	/**
	 * Queue the given `file` as an attachment to the specified `field`,
	 * with optional `filename`.
	 *
	 * ``` js
	 * request.post('/upload')
	 *   .attach(new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
	 *   .end(callback);
	 * ```
	 *
	 * @param {String} field
	 * @param {Blob|File} file
	 * @param {String} filename
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.attach = function(field, file, filename){
	  if (!this._formData) this._formData = new root.FormData();
	  this._formData.append(field, file, filename);
	  return this;
	};

	/**
	 * Send `data`, defaulting the `.type()` to "json" when
	 * an object is given.
	 *
	 * Examples:
	 *
	 *       // querystring
	 *       request.get('/search')
	 *         .end(callback)
	 *
	 *       // multiple data "writes"
	 *       request.get('/search')
	 *         .send({ search: 'query' })
	 *         .send({ range: '1..5' })
	 *         .send({ order: 'desc' })
	 *         .end(callback)
	 *
	 *       // manual json
	 *       request.post('/user')
	 *         .type('json')
	 *         .send('{"name":"tj"})
	 *         .end(callback)
	 *
	 *       // auto json
	 *       request.post('/user')
	 *         .send({ name: 'tj' })
	 *         .end(callback)
	 *
	 *       // manual x-www-form-urlencoded
	 *       request.post('/user')
	 *         .type('form')
	 *         .send('name=tj')
	 *         .end(callback)
	 *
	 *       // auto x-www-form-urlencoded
	 *       request.post('/user')
	 *         .type('form')
	 *         .send({ name: 'tj' })
	 *         .end(callback)
	 *
	 *       // defaults to x-www-form-urlencoded
	  *      request.post('/user')
	  *        .send('name=tobi')
	  *        .send('species=ferret')
	  *        .end(callback)
	 *
	 * @param {String|Object} data
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.send = function(data){
	  var obj = isObject(data);
	  var type = this.getHeader('Content-Type');

	  // merge
	  if (obj && isObject(this._data)) {
	    for (var key in data) {
	      this._data[key] = data[key];
	    }
	  } else if ('string' == typeof data) {
	    if (!type) this.type('form');
	    type = this.getHeader('Content-Type');
	    if ('application/x-www-form-urlencoded' == type) {
	      this._data = this._data
	        ? this._data + '&' + data
	        : data;
	    } else {
	      this._data = (this._data || '') + data;
	    }
	  } else {
	    this._data = data;
	  }

	  if (!obj || isHost(data)) return this;
	  if (!type) this.type('json');
	  return this;
	};

	/**
	 * Invoke the callback with `err` and `res`
	 * and handle arity check.
	 *
	 * @param {Error} err
	 * @param {Response} res
	 * @api private
	 */

	Request.prototype.callback = function(err, res){
	  var fn = this._callback;
	  this.clearTimeout();
	  fn(err, res);
	};

	/**
	 * Invoke callback with x-domain error.
	 *
	 * @api private
	 */

	Request.prototype.crossDomainError = function(){
	  var err = new Error('Origin is not allowed by Access-Control-Allow-Origin');
	  err.crossDomain = true;
	  this.callback(err);
	};

	/**
	 * Invoke callback with timeout error.
	 *
	 * @api private
	 */

	Request.prototype.timeoutError = function(){
	  var timeout = this._timeout;
	  var err = new Error('timeout of ' + timeout + 'ms exceeded');
	  err.timeout = timeout;
	  this.callback(err);
	};

	/**
	 * Enable transmission of cookies with x-domain requests.
	 *
	 * Note that for this to work the origin must not be
	 * using "Access-Control-Allow-Origin" with a wildcard,
	 * and also must set "Access-Control-Allow-Credentials"
	 * to "true".
	 *
	 * @api public
	 */

	Request.prototype.withCredentials = function(){
	  this._withCredentials = true;
	  return this;
	};

	/**
	 * Initiate request, invoking callback `fn(res)`
	 * with an instanceof `Response`.
	 *
	 * @param {Function} fn
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.end = function(fn){
	  var self = this;
	  var xhr = this.xhr = request.getXHR();
	  var query = this._query.join('&');
	  var timeout = this._timeout;
	  var data = this._formData || this._data;

	  // store callback
	  this._callback = fn || noop;

	  // state change
	  xhr.onreadystatechange = function(){
	    if (4 != xhr.readyState) return;

	    // In IE9, reads to any property (e.g. status) off of an aborted XHR will
	    // result in the error "Could not complete the operation due to error c00c023f"
	    var status;
	    try { status = xhr.status } catch(e) { status = 0; }

	    if (0 == status) {
	      if (self.timedout) return self.timeoutError();
	      if (self.aborted) return;
	      return self.crossDomainError();
	    }
	    self.emit('end');
	  };

	  // progress
	  var handleProgress = function(e){
	    if (e.total > 0) {
	      e.percent = e.loaded / e.total * 100;
	    }
	    self.emit('progress', e);
	  };
	  if (this.hasListeners('progress')) {
	    xhr.onprogress = handleProgress;
	  }
	  try {
	    if (xhr.upload && this.hasListeners('progress')) {
	      xhr.upload.onprogress = handleProgress;
	    }
	  } catch(e) {
	    // Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
	    // Reported here:
	    // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
	  }

	  // timeout
	  if (timeout && !this._timer) {
	    this._timer = setTimeout(function(){
	      self.timedout = true;
	      self.abort();
	    }, timeout);
	  }

	  // querystring
	  if (query) {
	    query = request.serializeObject(query);
	    this.url += ~this.url.indexOf('?')
	      ? '&' + query
	      : '?' + query;
	  }

	  // initiate request
	  xhr.open(this.method, this.url, true);

	  // CORS
	  if (this._withCredentials) xhr.withCredentials = true;

	  // body
	  if ('GET' != this.method && 'HEAD' != this.method && 'string' != typeof data && !isHost(data)) {
	    // serialize stuff
	    var serialize = request.serialize[this.getHeader('Content-Type')];
	    if (serialize) data = serialize(data);
	  }

	  // set header fields
	  for (var field in this.header) {
	    if (null == this.header[field]) continue;
	    xhr.setRequestHeader(field, this.header[field]);
	  }

	  // send stuff
	  this.emit('request', this);
	  xhr.send(data);
	  return this;
	};

	/**
	 * Expose `Request`.
	 */

	request.Request = Request;

	/**
	 * Issue a request:
	 *
	 * Examples:
	 *
	 *    request('GET', '/users').end(callback)
	 *    request('/users').end(callback)
	 *    request('/users', callback)
	 *
	 * @param {String} method
	 * @param {String|Function} url or callback
	 * @return {Request}
	 * @api public
	 */

	function request(method, url) {
	  // callback
	  if ('function' == typeof url) {
	    return new Request('GET', method).end(url);
	  }

	  // url first
	  if (1 == arguments.length) {
	    return new Request('GET', method);
	  }

	  return new Request(method, url);
	}

	/**
	 * GET `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} data or fn
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.get = function(url, data, fn){
	  var req = request('GET', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.query(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * HEAD `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} data or fn
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.head = function(url, data, fn){
	  var req = request('HEAD', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * DELETE `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.del = function(url, fn){
	  var req = request('DELETE', url);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * PATCH `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed} data
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.patch = function(url, data, fn){
	  var req = request('PATCH', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * POST `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed} data
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.post = function(url, data, fn){
	  var req = request('POST', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * PUT `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} data or fn
	 * @param {Function} fn
	 * @return {Request}
	 * @api public
	 */

	request.put = function(url, data, fn){
	  var req = request('PUT', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * Expose `request`.
	 */

	module.exports = request;


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	// Generated by CoffeeScript 1.6.3
	var linear_partition, min;

	min = function(arr) {
	  var computed, result, x, _i, _len;
	  for (_i = 0, _len = arr.length; _i < _len; _i++) {
	    x = arr[_i];
	    computed = x[0];
	    if (!result || computed < result.computed) {
	      result = {
	        value: x,
	        computed: computed
	      };
	    }
	  }
	  return result.value;
	};

	linear_partition = function(seq, k) {
	  var ans, i, j, m, n, solution, table, x, y, _i, _j, _k, _l;
	  n = seq.length;
	  if (k <= 0) {
	    return [];
	  }
	  if (k > n) {
	    return seq.map(function(x) {
	      return [x];
	    });
	  }
	  table = (function() {
	    var _i, _results;
	    _results = [];
	    for (y = _i = 0; 0 <= n ? _i < n : _i > n; y = 0 <= n ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _results1;
	        _results1 = [];
	        for (x = _j = 0; 0 <= k ? _j < k : _j > k; x = 0 <= k ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  solution = (function() {
	    var _i, _ref, _results;
	    _results = [];
	    for (y = _i = 0, _ref = n - 1; 0 <= _ref ? _i < _ref : _i > _ref; y = 0 <= _ref ? ++_i : --_i) {
	      _results.push((function() {
	        var _j, _ref1, _results1;
	        _results1 = [];
	        for (x = _j = 0, _ref1 = k - 1; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; x = 0 <= _ref1 ? ++_j : --_j) {
	          _results1.push(0);
	        }
	        return _results1;
	      })());
	    }
	    return _results;
	  })();
	  for (i = _i = 0; 0 <= n ? _i < n : _i > n; i = 0 <= n ? ++_i : --_i) {
	    table[i][0] = seq[i] + (i ? table[i - 1][0] : 0);
	  }
	  for (j = _j = 0; 0 <= k ? _j < k : _j > k; j = 0 <= k ? ++_j : --_j) {
	    table[0][j] = seq[0];
	  }
	  for (i = _k = 1; 1 <= n ? _k < n : _k > n; i = 1 <= n ? ++_k : --_k) {
	    for (j = _l = 1; 1 <= k ? _l < k : _l > k; j = 1 <= k ? ++_l : --_l) {
	      m = min((function() {
	        var _m, _results;
	        _results = [];
	        for (x = _m = 0; 0 <= i ? _m < i : _m > i; x = 0 <= i ? ++_m : --_m) {
	          _results.push([Math.max(table[x][j - 1], table[i][0] - table[x][0]), x]);
	        }
	        return _results;
	      })());
	      table[i][j] = m[0];
	      solution[i - 1][j - 1] = m[1];
	    }
	  }
	  n = n - 1;
	  k = k - 2;
	  ans = [];
	  while (k >= 0) {
	    ans = [
	      (function() {
	        var _m, _ref, _ref1, _results;
	        _results = [];
	        for (i = _m = _ref = solution[n - 1][k] + 1, _ref1 = n + 1; _ref <= _ref1 ? _m < _ref1 : _m > _ref1; i = _ref <= _ref1 ? ++_m : --_m) {
	          _results.push(seq[i]);
	        }
	        return _results;
	      })()
	    ].concat(ans);
	    n = solution[n - 1][k];
	    k = k - 1;
	  }
	  return [
	    (function() {
	      var _m, _ref, _results;
	      _results = [];
	      for (i = _m = 0, _ref = n + 1; 0 <= _ref ? _m < _ref : _m > _ref; i = 0 <= _ref ? ++_m : --_m) {
	        _results.push(seq[i]);
	      }
	      return _results;
	    })()
	  ].concat(ans);
	};

	module.exports = function(seq, k) {
	  if (k <= 0) {
	    return [];
	  }
	  while (k) {
	    try {
	      return linear_partition(seq, k--);
	    } catch (_error) {}
	  }
	};


/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(40);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/messages.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/messages.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, ".message{position:fixed;font-size:0.9em;padding:5px 0;width:30%;top:10%;left:35%;border:1px solid #dfdfdf;z-index:9;margin:auto;text-align:center;background-color:#fff;box-sizing:border-box;font-weight:bold;transition:max-height 0.1s ease-in-out}@media only screen and (max-width: 960px){.message{width:70%;left:15%;}}.message.message--visible{max-height:60px;opacity:1}.message.message--hidden{max-height:0;opacity:0}.message.message--error{color:#555;font-weight:bold;background-color:#fceeee;border-color:#f8d8d9}", ""]);

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(42);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/image.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, ".ssimage__noimagetext{transition:all 0.5s ease-in}.ssimage__wrapper{width:100%;position:relative;min-height:500px;background-color:#fefefe}.ssimage__wrapper:hover .ssimage__noimagetext{opacity:0.85}.ssimage__imageinput{z-index:3;position:absolute;top:0;left:0;position:absolute;display:block;width:100%;height:100%;opacity:0}.ssimage__changephoto{text-decoration:underline;cursor:pointer;text-align:center;color:#999}.ssimage__noimagetext{width:30%;position:absolute;transform-style:preserve-3d;top:50%;left:50%;transform:translate(-50%, -50%);font-size:1em;line-height:1em;text-align:center;height:auto;padding:1.2em;color:#fff;opacity:1;border-radius:24px;background-color:#67C400}.ssimage__loadingimage{height:500px;width:100%;background:transparent url(/static/images/loading.gif) center center no-repeat}", ""]);

/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(44);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(36)(content, {});
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		module.hot.accept("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/tags.scss", function() {
			var newContent = require("!!/Users/victor/node_modules/css-loader/index.js!/Users/victor/node_modules/sass-loader/index.js!/Users/victor/Code/golang/src/stylestation/preprocessing/SCSS/tags.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(45)();
	exports.push([module.id, ".sstags__wrapper--male,.sstags__wrapper--female{transition:all 0.1s ease-in;color:#989898}.sstags__wrapper--male .sstags__icon,.sstags__wrapper--female .sstags__icon{color:#989898}.sstags__wrapper{box-shadow:0 1px 2px rgba(0,0,0,0.2);*zoom:1;float:left;clear:none;text-align:inherit;width:28.57143%;margin-left:0%;margin-right:0%;padding:0.6em;cursor:pointer;margin:0.3em;font-size:0.85em;float:left;text-align:right;background-color:#fdfdfd;box-sizing:border-box}.sstags__wrapper:before,.sstags__wrapper:after{content:'';display:table}.sstags__wrapper:after{clear:both}@media only screen and (max-width: 960px){.sstags__wrapper{*zoom:1;float:left;clear:none;text-align:inherit;width:48.5%;margin-left:0%;margin-right:3%;margin-left:1%;}.sstags__wrapper:before,.sstags__wrapper:after{content:'';display:table}.sstags__wrapper:after{clear:both}.sstags__wrapper:last-child{margin-right:0%}}.sstags__wrapper:hover{box-shadow:inset 0 1px 2px rgba(0,0,0,0.2)}.sstags__wrapper:hover .sstags__icon{box-shadow:none}.sstags__wrapper--male{border-bottom:2px solid #458AD7}.sstags__wrapper--female{border-bottom:2px solid #EB6161}.sstags__icon{box-shadow:0 0 3px rgba(0,0,0,0.2)}.sstags__text{float:left;display:inline-block;margin-left:0.5em;line-height:2.25em}.sstags__icon--selected,.sstags__icon{font-family:ssicons;text-align:center;width:1.5em;display:inline-block;border-radius:50%;height:1.5em;font-size:1.5em;line-height:1.5em;float:left;background-color:#fff}.sstags__icon--selected{color:#68BB0D !important}", ""]);

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {
		var list = [];
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
		return list;
	}

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	// shim for using process in browser

	var process = module.exports = {};

	process.nextTick = (function () {
	    var canSetImmediate = typeof window !== 'undefined'
	    && window.setImmediate;
	    var canMutationObserver = typeof window !== 'undefined'
	    && window.MutationObserver;
	    var canPost = typeof window !== 'undefined'
	    && window.postMessage && window.addEventListener
	    ;

	    if (canSetImmediate) {
	        return function (f) { return window.setImmediate(f) };
	    }

	    var queue = [];

	    if (canMutationObserver) {
	        var hiddenDiv = document.createElement("div");
	        var observer = new MutationObserver(function () {
	            var queueList = queue.slice();
	            queue.length = 0;
	            queueList.forEach(function (fn) {
	                fn();
	            });
	        });

	        observer.observe(hiddenDiv, { attributes: true });

	        return function nextTick(fn) {
	            if (!queue.length) {
	                hiddenDiv.setAttribute('yes', 'no');
	            }
	            queue.push(fn);
	        };
	    }

	    if (canPost) {
	        window.addEventListener('message', function (ev) {
	            var source = ev.source;
	            if ((source === window || source === null) && ev.data === 'process-tick') {
	                ev.stopPropagation();
	                if (queue.length > 0) {
	                    var fn = queue.shift();
	                    fn();
	                }
	            }
	        }, true);

	        return function nextTick(fn) {
	            queue.push(fn);
	            window.postMessage('process-tick', '*');
	        };
	    }

	    return function nextTick(fn) {
	        setTimeout(fn, 0);
	    };
	})();

	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	// TODO(shtylman)
	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};


/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	var isArray = __webpack_require__(52);

	/**
	 * Expose `pathToRegexp`.
	 */
	module.exports = pathToRegexp;

	/**
	 * The main path matching regexp utility.
	 *
	 * @type {RegExp}
	 */
	var PATH_REGEXP = new RegExp([
	  // Match escaped characters that would otherwise appear in future matches.
	  // This allows the user to escape special characters that won't transform.
	  '(\\\\.)',
	  // Match Express-style parameters and un-named parameters with a prefix
	  // and optional suffixes. Matches appear as:
	  //
	  // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?"]
	  // "/route(\\d+)" => [undefined, undefined, undefined, "\d+", undefined]
	  '([\\/.])?(?:\\:(\\w+)(?:\\(((?:\\\\.|[^)])*)\\))?|\\(((?:\\\\.|[^)])*)\\))([+*?])?',
	  // Match regexp special characters that are always escaped.
	  '([.+*?=^!:${}()[\\]|\\/])'
	].join('|'), 'g');

	/**
	 * Escape the capturing group by escaping special characters and meaning.
	 *
	 * @param  {String} group
	 * @return {String}
	 */
	function escapeGroup (group) {
	  return group.replace(/([=!:$\/()])/g, '\\$1');
	}

	/**
	 * Attach the keys as a property of the regexp.
	 *
	 * @param  {RegExp} re
	 * @param  {Array}  keys
	 * @return {RegExp}
	 */
	function attachKeys (re, keys) {
	  re.keys = keys;
	  return re;
	}

	/**
	 * Get the flags for a regexp from the options.
	 *
	 * @param  {Object} options
	 * @return {String}
	 */
	function flags (options) {
	  return options.sensitive ? '' : 'i';
	}

	/**
	 * Pull out keys from a regexp.
	 *
	 * @param  {RegExp} path
	 * @param  {Array}  keys
	 * @return {RegExp}
	 */
	function regexpToRegexp (path, keys) {
	  // Use a negative lookahead to match only capturing groups.
	  var groups = path.source.match(/\((?!\?)/g);

	  if (groups) {
	    for (var i = 0; i < groups.length; i++) {
	      keys.push({
	        name:      i,
	        delimiter: null,
	        optional:  false,
	        repeat:    false
	      });
	    }
	  }

	  return attachKeys(path, keys);
	}

	/**
	 * Transform an array into a regexp.
	 *
	 * @param  {Array}  path
	 * @param  {Array}  keys
	 * @param  {Object} options
	 * @return {RegExp}
	 */
	function arrayToRegexp (path, keys, options) {
	  var parts = [];

	  for (var i = 0; i < path.length; i++) {
	    parts.push(pathToRegexp(path[i], keys, options).source);
	  }

	  var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options));
	  return attachKeys(regexp, keys);
	}

	/**
	 * Replace the specific tags with regexp strings.
	 *
	 * @param  {String} path
	 * @param  {Array}  keys
	 * @return {String}
	 */
	function replacePath (path, keys) {
	  var index = 0;

	  function replace (_, escaped, prefix, key, capture, group, suffix, escape) {
	    if (escaped) {
	      return escaped;
	    }

	    if (escape) {
	      return '\\' + escape;
	    }

	    var repeat   = suffix === '+' || suffix === '*';
	    var optional = suffix === '?' || suffix === '*';

	    keys.push({
	      name:      key || index++,
	      delimiter: prefix || '/',
	      optional:  optional,
	      repeat:    repeat
	    });

	    prefix = prefix ? ('\\' + prefix) : '';
	    capture = escapeGroup(capture || group || '[^' + (prefix || '\\/') + ']+?');

	    if (repeat) {
	      capture = capture + '(?:' + prefix + capture + ')*';
	    }

	    if (optional) {
	      return '(?:' + prefix + '(' + capture + '))?';
	    }

	    // Basic parameter support.
	    return prefix + '(' + capture + ')';
	  }

	  return path.replace(PATH_REGEXP, replace);
	}

	/**
	 * Normalize the given path string, returning a regular expression.
	 *
	 * An empty array can be passed in for the keys, which will hold the
	 * placeholder key descriptions. For example, using `/user/:id`, `keys` will
	 * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
	 *
	 * @param  {(String|RegExp|Array)} path
	 * @param  {Array}                 [keys]
	 * @param  {Object}                [options]
	 * @return {RegExp}
	 */
	function pathToRegexp (path, keys, options) {
	  keys = keys || [];

	  if (!isArray(keys)) {
	    options = keys;
	    keys = [];
	  } else if (!options) {
	    options = {};
	  }

	  if (path instanceof RegExp) {
	    return regexpToRegexp(path, keys, options);
	  }

	  if (isArray(path)) {
	    return arrayToRegexp(path, keys, options);
	  }

	  var strict = options.strict;
	  var end = options.end !== false;
	  var route = replacePath(path, keys);
	  var endsWithSlash = path.charAt(path.length - 1) === '/';

	  // In non-strict mode we allow a slash at the end of match. If the path to
	  // match already ends with a slash, we remove it for consistency. The slash
	  // is valid at the end of a path match, not in the middle. This is important
	  // in non-ending mode, where "/test/" shouldn't match "/test//route".
	  if (!strict) {
	    route = (endsWithSlash ? route.slice(0, -2) : route) + '(?:\\/(?=$))?';
	  }

	  if (end) {
	    route += '$';
	  } else {
	    // In non-ending mode, we need the capturing groups to match as much as
	    // possible by using a positive lookahead to the end or next path segment.
	    route += strict && endsWithSlash ? '' : '(?=\\/|$)';
	  }

	  return attachKeys(new RegExp('^' + route, flags(options)), keys);
	}


/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(module) {var m = (function app(window, undefined) {
		var OBJECT = "[object Object]", ARRAY = "[object Array]", STRING = "[object String]", FUNCTION = "function";
		var type = {}.toString;
		var parser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[.+?\])/g, attrParser = /\[(.+?)(?:=("|'|)(.*?)\2)?\]/;
		var voidElements = /^(AREA|BASE|BR|COL|COMMAND|EMBED|HR|IMG|INPUT|KEYGEN|LINK|META|PARAM|SOURCE|TRACK|WBR)$/;

		// caching commonly used variables
		var $document, $location, $requestAnimationFrame, $cancelAnimationFrame;

		// self invoking function needed because of the way mocks work
		function initialize(window){
			$document = window.document;
			$location = window.location;
			$cancelAnimationFrame = window.cancelAnimationFrame || window.clearTimeout;
			$requestAnimationFrame = window.requestAnimationFrame || window.setTimeout;
		}

		initialize(window);


		/**
		 * @typedef {String} Tag
		 * A string that looks like -> div.classname#id[param=one][param2=two]
		 * Which describes a DOM node
		 */

		/**
		 *
		 * @param {Tag} The DOM node tag
		 * @param {Object=[]} optional key-value pairs to be mapped to DOM attrs
		 * @param {...mNode=[]} Zero or more Mithril child nodes. Can be an array, or splat (optional)
		 *
		 */
		function m() {
			var args = [].slice.call(arguments);
			var hasAttrs = args[1] != null && type.call(args[1]) === OBJECT && !("tag" in args[1]) && !("subtree" in args[1]);
			var attrs = hasAttrs ? args[1] : {};
			var classAttrName = "class" in attrs ? "class" : "className";
			var cell = {tag: "div", attrs: {}};
			var match, classes = [];
			if (type.call(args[0]) != STRING) throw new Error("selector in m(selector, attrs, children) should be a string")
			while (match = parser.exec(args[0])) {
				if (match[1] === "" && match[2]) cell.tag = match[2];
				else if (match[1] === "#") cell.attrs.id = match[2];
				else if (match[1] === ".") classes.push(match[2]);
				else if (match[3][0] === "[") {
					var pair = attrParser.exec(match[3]);
					cell.attrs[pair[1]] = pair[3] || (pair[2] ? "" :true)
				}
			}
			if (classes.length > 0) cell.attrs[classAttrName] = classes.join(" ");


			var children = hasAttrs ? args[2] : args[1];
			if (type.call(children) === ARRAY) {
				cell.children = children
			}
			else {
				cell.children = hasAttrs ? args.slice(2) : args.slice(1)
			}

			for (var attrName in attrs) {
				if (attrName === classAttrName) {
					if (attrs[attrName] !== "") cell.attrs[attrName] = (cell.attrs[attrName] || "") + " " + attrs[attrName];
				}
				else cell.attrs[attrName] = attrs[attrName]
			}
			return cell
		}
		function build(parentElement, parentTag, parentCache, parentIndex, data, cached, shouldReattach, index, editable, namespace, configs) {
			//`build` is a recursive function that manages creation/diffing/removal of DOM elements based on comparison between `data` and `cached`
			//the diff algorithm can be summarized as this:
			//1 - compare `data` and `cached`
			//2 - if they are different, copy `data` to `cached` and update the DOM based on what the difference is
			//3 - recursively apply this algorithm for every array and for the children of every virtual element

			//the `cached` data structure is essentially the same as the previous redraw's `data` data structure, with a few additions:
			//- `cached` always has a property called `nodes`, which is a list of DOM elements that correspond to the data represented by the respective virtual element
			//- in order to support attaching `nodes` as a property of `cached`, `cached` is *always* a non-primitive object, i.e. if the data was a string, then cached is a String instance. If data was `null` or `undefined`, cached is `new String("")`
			//- `cached also has a `configContext` property, which is the state storage object exposed by config(element, isInitialized, context)
			//- when `cached` is an Object, it represents a virtual element; when it's an Array, it represents a list of elements; when it's a String, Number or Boolean, it represents a text node

			//`parentElement` is a DOM element used for W3C DOM API calls
			//`parentTag` is only used for handling a corner case for textarea values
			//`parentCache` is used to remove nodes in some multi-node cases
			//`parentIndex` and `index` are used to figure out the offset of nodes. They're artifacts from before arrays started being flattened and are likely refactorable
			//`data` and `cached` are, respectively, the new and old nodes being diffed
			//`shouldReattach` is a flag indicating whether a parent node was recreated (if so, and if this node is reused, then this node must reattach itself to the new parent)
			//`editable` is a flag that indicates whether an ancestor is contenteditable
			//`namespace` indicates the closest HTML namespace as it cascades down from an ancestor
			//`configs` is a list of config functions to run after the topmost `build` call finishes running

			//there's logic that relies on the assumption that null and undefined data are equivalent to empty strings
			//- this prevents lifecycle surprises from procedural helpers that mix implicit and explicit return statements (e.g. function foo() {if (cond) return m("div")}
			//- it simplifies diffing code
			//data.toString() is null if data is the return value of Console.log in Firefox
			if (data == null || data.toString() == null) data = "";
			if (data.subtree === "retain") return cached;
			var cachedType = type.call(cached), dataType = type.call(data);
			if (cached == null || cachedType !== dataType) {
				if (cached != null) {
					if (parentCache && parentCache.nodes) {
						var offset = index - parentIndex;
						var end = offset + (dataType === ARRAY ? data : cached.nodes).length;
						clear(parentCache.nodes.slice(offset, end), parentCache.slice(offset, end))
					}
					else if (cached.nodes) clear(cached.nodes, cached)
				}
				cached = new data.constructor;
				if (cached.tag) cached = {}; //if constructor creates a virtual dom element, use a blank object as the base cached node instead of copying the virtual el (#277)
				cached.nodes = []
			}

			if (dataType === ARRAY) {
				//recursively flatten array
				for (var i = 0, len = data.length; i < len; i++) {
					if (type.call(data[i]) === ARRAY) {
						data = data.concat.apply([], data);
						i-- //check current index again and flatten until there are no more nested arrays at that index
					}
				}
				
				var nodes = [], intact = cached.length === data.length, subArrayCount = 0;

				//keys algorithm: sort elements without recreating them if keys are present
				//1) create a map of all existing keys, and mark all for deletion
				//2) add new keys to map and mark them for addition
				//3) if key exists in new list, change action from deletion to a move
				//4) for each key, handle its corresponding action as marked in previous steps
				//5) copy unkeyed items into their respective gaps
				var DELETION = 1, INSERTION = 2 , MOVE = 3;
				var existing = {}, unkeyed = [], shouldMaintainIdentities = false;
				for (var i = 0; i < cached.length; i++) {
					if (cached[i] && cached[i].attrs && cached[i].attrs.key != null) {
						shouldMaintainIdentities = true;
						existing[cached[i].attrs.key] = {action: DELETION, index: i}
					}
				}
				if (shouldMaintainIdentities) {
					if (data.indexOf(null) > -1) data = data.filter(function(x) {return x != null})
					
					var keysDiffer = false
					if (data.length != cached.length) keysDiffer = true
					else for (var i = 0, cachedCell, dataCell; cachedCell = cached[i], dataCell = data[i]; i++) {
						if (cachedCell.attrs && dataCell.attrs && cachedCell.attrs.key != dataCell.attrs.key) {
							keysDiffer = true
							break
						}
					}
					
					if (keysDiffer) {
						for (var i = 0, len = data.length; i < len; i++) {
							if (data[i] && data[i].attrs) {
								if (data[i].attrs.key != null) {
									var key = data[i].attrs.key;
									if (!existing[key]) existing[key] = {action: INSERTION, index: i};
									else existing[key] = {
										action: MOVE,
										index: i,
										from: existing[key].index,
										element: cached.nodes[existing[key].index] || $document.createElement("div")
									}
								}
								else unkeyed.push({index: i, element: parentElement.childNodes[i] || $document.createElement("div")})
							}
						}
						var actions = []
						for (var prop in existing) actions.push(existing[prop])
						var changes = actions.sort(sortChanges);
						var newCached = new Array(cached.length)

						for (var i = 0, change; change = changes[i]; i++) {
							if (change.action === DELETION) {
								clear(cached[change.index].nodes, cached[change.index]);
								newCached.splice(change.index, 1)
							}
							if (change.action === INSERTION) {
								var dummy = $document.createElement("div");
								dummy.key = data[change.index].attrs.key;
								parentElement.insertBefore(dummy, parentElement.childNodes[change.index] || null);
								newCached.splice(change.index, 0, {attrs: {key: data[change.index].attrs.key}, nodes: [dummy]})
							}

							if (change.action === MOVE) {
								if (parentElement.childNodes[change.index] !== change.element && change.element !== null) {
									parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null)
								}
								newCached[change.index] = cached[change.from]
							}
						}
						for (var i = 0, len = unkeyed.length; i < len; i++) {
							var change = unkeyed[i];
							parentElement.insertBefore(change.element, parentElement.childNodes[change.index] || null);
							newCached[change.index] = cached[change.index]
						}
						cached = newCached;
						cached.nodes = new Array(parentElement.childNodes.length);
						for (var i = 0, child; child = parentElement.childNodes[i]; i++) cached.nodes[i] = child
					}
				}
				//end key algorithm

				for (var i = 0, cacheCount = 0, len = data.length; i < len; i++) {
					//diff each item in the array
					var item = build(parentElement, parentTag, cached, index, data[i], cached[cacheCount], shouldReattach, index + subArrayCount || subArrayCount, editable, namespace, configs);
					if (item === undefined) continue;
					if (!item.nodes.intact) intact = false;
					if (item.$trusted) {
						//fix offset of next element if item was a trusted string w/ more than one html element
						//the first clause in the regexp matches elements
						//the second clause (after the pipe) matches text nodes
						subArrayCount += (item.match(/<[^\/]|\>\s*[^<]/g) || []).length
					}
					else subArrayCount += type.call(item) === ARRAY ? item.length : 1;
					cached[cacheCount++] = item
				}
				if (!intact) {
					//diff the array itself
					
					//update the list of DOM nodes by collecting the nodes from each item
					for (var i = 0, len = data.length; i < len; i++) {
						if (cached[i] != null) nodes.push.apply(nodes, cached[i].nodes)
					}
					//remove items from the end of the array if the new array is shorter than the old one
					//if errors ever happen here, the issue is most likely a bug in the construction of the `cached` data structure somewhere earlier in the program
					for (var i = 0, node; node = cached.nodes[i]; i++) {
						if (node.parentNode != null && nodes.indexOf(node) < 0) clear([node], [cached[i]])
					}
					if (data.length < cached.length) cached.length = data.length;
					cached.nodes = nodes
				}
			}
			else if (data != null && dataType === OBJECT) {
				if (!data.attrs) data.attrs = {};
				if (!cached.attrs) cached.attrs = {};

				var dataAttrKeys = Object.keys(data.attrs)
				var hasKeys = dataAttrKeys.length > ("key" in data.attrs ? 1 : 0)
				//if an element is different enough from the one in cache, recreate it
				if (data.tag != cached.tag || dataAttrKeys.join() != Object.keys(cached.attrs).join() || data.attrs.id != cached.attrs.id) {
					if (cached.nodes.length) clear(cached.nodes);
					if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload()
				}
				if (type.call(data.tag) != STRING) return;

				var node, isNew = cached.nodes.length === 0;
				if (data.attrs.xmlns) namespace = data.attrs.xmlns;
				else if (data.tag === "svg") namespace = "http://www.w3.org/2000/svg";
				else if (data.tag === "math") namespace = "http://www.w3.org/1998/Math/MathML";
				if (isNew) {
					if (data.attrs.is) node = namespace === undefined ? $document.createElement(data.tag, data.attrs.is) : $document.createElementNS(namespace, data.tag, data.attrs.is);
					else node = namespace === undefined ? $document.createElement(data.tag) : $document.createElementNS(namespace, data.tag);
					cached = {
						tag: data.tag,
						//set attributes first, then create children
						attrs: hasKeys ? setAttributes(node, data.tag, data.attrs, {}, namespace) : data.attrs,
						children: data.children != null && data.children.length > 0 ?
							build(node, data.tag, undefined, undefined, data.children, cached.children, true, 0, data.attrs.contenteditable ? node : editable, namespace, configs) :
							data.children,
						nodes: [node]
					};
					if (cached.children && !cached.children.nodes) cached.children.nodes = [];
					//edge case: setting value on <select> doesn't work before children exist, so set it again after children have been created
					if (data.tag === "select" && data.attrs.value) setAttributes(node, data.tag, {value: data.attrs.value}, {}, namespace);
					parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				else {
					node = cached.nodes[0];
					if (hasKeys) setAttributes(node, data.tag, data.attrs, cached.attrs, namespace);
					cached.children = build(node, data.tag, undefined, undefined, data.children, cached.children, false, 0, data.attrs.contenteditable ? node : editable, namespace, configs);
					cached.nodes.intact = true;
					if (shouldReattach === true && node != null) parentElement.insertBefore(node, parentElement.childNodes[index] || null)
				}
				//schedule configs to be called. They are called after `build` finishes running
				if (typeof data.attrs["config"] === FUNCTION) {
					var context = cached.configContext = cached.configContext || {};

					// bind
					var callback = function(data, args) {
						return function() {
							return data.attrs["config"].apply(data, args)
						}
					};
					configs.push(callback(data, [node, !isNew, context, cached]))
				}
			}
			else if (typeof dataType != FUNCTION) {
				//handle text nodes
				var nodes;
				if (cached.nodes.length === 0) {
					if (data.$trusted) {
						nodes = injectHTML(parentElement, index, data)
					}
					else {
						nodes = [$document.createTextNode(data)];
						if (!parentElement.nodeName.match(voidElements)) parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null)
					}
					cached = "string number boolean".indexOf(typeof data) > -1 ? new data.constructor(data) : data;
					cached.nodes = nodes
				}
				else if (cached.valueOf() !== data.valueOf() || shouldReattach === true) {
					nodes = cached.nodes;
					if (!editable || editable !== $document.activeElement) {
						if (data.$trusted) {
							clear(nodes, cached);
							nodes = injectHTML(parentElement, index, data)
						}
						else {
							//corner case: replacing the nodeValue of a text node that is a child of a textarea/contenteditable doesn't work
							//we need to update the value property of the parent textarea or the innerHTML of the contenteditable element instead
							if (parentTag === "textarea") parentElement.value = data;
							else if (editable) editable.innerHTML = data;
							else {
								if (nodes[0].nodeType === 1 || nodes.length > 1) { //was a trusted string
									clear(cached.nodes, cached);
									nodes = [$document.createTextNode(data)]
								}
								parentElement.insertBefore(nodes[0], parentElement.childNodes[index] || null);
								nodes[0].nodeValue = data
							}
						}
					}
					cached = new data.constructor(data);
					cached.nodes = nodes
				}
				else cached.nodes.intact = true
			}

			return cached
		}
		function sortChanges(a, b) {return a.action - b.action || a.index - b.index}
		function setAttributes(node, tag, dataAttrs, cachedAttrs, namespace) {
			for (var attrName in dataAttrs) {
				var dataAttr = dataAttrs[attrName];
				var cachedAttr = cachedAttrs[attrName];
				if (!(attrName in cachedAttrs) || (cachedAttr !== dataAttr)) {
					cachedAttrs[attrName] = dataAttr;
					try {
						//`config` isn't a real attributes, so ignore it
						if (attrName === "config" || attrName == "key") continue;
						//hook event handlers to the auto-redrawing system
						else if (typeof dataAttr === FUNCTION && attrName.indexOf("on") === 0) {
							node[attrName] = autoredraw(dataAttr, node)
						}
						//handle `style: {...}`
						else if (attrName === "style" && dataAttr != null && type.call(dataAttr) === OBJECT) {
							for (var rule in dataAttr) {
								if (cachedAttr == null || cachedAttr[rule] !== dataAttr[rule]) node.style[rule] = dataAttr[rule]
							}
							for (var rule in cachedAttr) {
								if (!(rule in dataAttr)) node.style[rule] = ""
							}
						}
						//handle SVG
						else if (namespace != null) {
							if (attrName === "href") node.setAttributeNS("http://www.w3.org/1999/xlink", "href", dataAttr);
							else if (attrName === "className") node.setAttribute("class", dataAttr);
							else node.setAttribute(attrName, dataAttr)
						}
						//handle cases that are properties (but ignore cases where we should use setAttribute instead)
						//- list and form are typically used as strings, but are DOM element references in js
						//- when using CSS selectors (e.g. `m("[style='']")`), style is used as a string, but it's an object in js
						else if (attrName in node && !(attrName === "list" || attrName === "style" || attrName === "form" || attrName === "type")) {
							//#348 don't set the value if not needed otherwise cursor placement breaks in Chrome
							if (tag !== "input" || node[attrName] !== dataAttr) node[attrName] = dataAttr
						}
						else node.setAttribute(attrName, dataAttr)
					}
					catch (e) {
						//swallow IE's invalid argument errors to mimic HTML's fallback-to-doing-nothing-on-invalid-attributes behavior
						if (e.message.indexOf("Invalid argument") < 0) throw e
					}
				}
				//#348 dataAttr may not be a string, so use loose comparison (double equal) instead of strict (triple equal)
				else if (attrName === "value" && tag === "input" && node.value != dataAttr) {
					node.value = dataAttr
				}
			}
			return cachedAttrs
		}
		function clear(nodes, cached) {
			for (var i = nodes.length - 1; i > -1; i--) {
				if (nodes[i] && nodes[i].parentNode) {
					try {nodes[i].parentNode.removeChild(nodes[i])}
					catch (e) {} //ignore if this fails due to order of events (see http://stackoverflow.com/questions/21926083/failed-to-execute-removechild-on-node)
					cached = [].concat(cached);
					if (cached[i]) unload(cached[i])
				}
			}
			if (nodes.length != 0) nodes.length = 0
		}
		function unload(cached) {
			if (cached.configContext && typeof cached.configContext.onunload === FUNCTION) cached.configContext.onunload();
			if (cached.children) {
				if (type.call(cached.children) === ARRAY) {
					for (var i = 0, child; child = cached.children[i]; i++) unload(child)
				}
				else if (cached.children.tag) unload(cached.children)
			}
		}
		function injectHTML(parentElement, index, data) {
			var nextSibling = parentElement.childNodes[index];
			if (nextSibling) {
				var isElement = nextSibling.nodeType != 1;
				var placeholder = $document.createElement("span");
				if (isElement) {
					parentElement.insertBefore(placeholder, nextSibling || null);
					placeholder.insertAdjacentHTML("beforebegin", data);
					parentElement.removeChild(placeholder)
				}
				else nextSibling.insertAdjacentHTML("beforebegin", data)
			}
			else parentElement.insertAdjacentHTML("beforeend", data);
			var nodes = [];
			while (parentElement.childNodes[index] !== nextSibling) {
				nodes.push(parentElement.childNodes[index]);
				index++
			}
			return nodes
		}
		function autoredraw(callback, object) {
			return function(e) {
				e = e || event;
				m.redraw.strategy("diff");
				m.startComputation();
				try {return callback.call(object, e)}
				finally {
					endFirstComputation()
				}
			}
		}

		var html;
		var documentNode = {
			appendChild: function(node) {
				if (html === undefined) html = $document.createElement("html");
				if ($document.documentElement && $document.documentElement !== node) {
					$document.replaceChild(node, $document.documentElement)
				}
				else $document.appendChild(node);
				this.childNodes = $document.childNodes
			},
			insertBefore: function(node) {
				this.appendChild(node)
			},
			childNodes: []
		};
		var nodeCache = [], cellCache = {};
		m.render = function(root, cell, forceRecreation) {
			var configs = [];
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var id = getCellCacheKey(root);
			var isDocumentRoot = root === $document;
			var node = isDocumentRoot || root === $document.documentElement ? documentNode : root;
			if (isDocumentRoot && cell.tag != "html") cell = {tag: "html", attrs: {}, children: cell};
			if (cellCache[id] === undefined) clear(node.childNodes);
			if (forceRecreation === true) reset(root);
			cellCache[id] = build(node, null, undefined, undefined, cell, cellCache[id], false, 0, null, undefined, configs);
			for (var i = 0, len = configs.length; i < len; i++) configs[i]()
		};
		function getCellCacheKey(element) {
			var index = nodeCache.indexOf(element);
			return index < 0 ? nodeCache.push(element) - 1 : index
		}

		m.trust = function(value) {
			value = new String(value);
			value.$trusted = true;
			return value
		};

		function gettersetter(store) {
			var prop = function() {
				if (arguments.length) store = arguments[0];
				return store
			};

			prop.toJSON = function() {
				return store
			};

			return prop
		}

		m.prop = function (store) {
			//note: using non-strict equality check here because we're checking if store is null OR undefined
			if (((store != null && type.call(store) === OBJECT) || typeof store === FUNCTION) && typeof store.then === FUNCTION) {
				return propify(store)
			}

			return gettersetter(store)
		};

		var roots = [], modules = [], controllers = [], lastRedrawId = null, lastRedrawCallTime = 0, computePostRedrawHook = null, prevented = false, topModule;
		var FRAME_BUDGET = 16; //60 frames per second = 1 call per 16 ms
		m.module = function(root, module) {
			if (!root) throw new Error("Please ensure the DOM element exists before rendering a template into it.");
			var index = roots.indexOf(root);
			if (index < 0) index = roots.length;
			var isPrevented = false;
			if (controllers[index] && typeof controllers[index].onunload === FUNCTION) {
				var event = {
					preventDefault: function() {isPrevented = true}
				};
				controllers[index].onunload(event)
			}
			if (!isPrevented) {
				m.redraw.strategy("all");
				m.startComputation();
				roots[index] = root;
				var currentModule = topModule = module = module || {};
				var controller = new (module.controller || function() {});
				//controllers may call m.module recursively (via m.route redirects, for example)
				//this conditional ensures only the last recursive m.module call is applied
				if (currentModule === topModule) {
					controllers[index] = controller;
					modules[index] = module
				}
				endFirstComputation();
				return controllers[index]
			}
		};
		m.redraw = function(force) {
			//lastRedrawId is a positive number if a second redraw is requested before the next animation frame
			//lastRedrawID is null if it's the first redraw and not an event handler
			if (lastRedrawId && force !== true) {
				//when setTimeout: only reschedule redraw if time between now and previous redraw is bigger than a frame, otherwise keep currently scheduled timeout
				//when rAF: always reschedule redraw
				if (new Date - lastRedrawCallTime > FRAME_BUDGET || $requestAnimationFrame === window.requestAnimationFrame) {
					if (lastRedrawId > 0) $cancelAnimationFrame(lastRedrawId);
					lastRedrawId = $requestAnimationFrame(redraw, FRAME_BUDGET)
				}
			}
			else {
				redraw();
				lastRedrawId = $requestAnimationFrame(function() {lastRedrawId = null}, FRAME_BUDGET)
			}
		};
		m.redraw.strategy = m.prop();
		var blank = function() {return ""}
		function redraw() {
			var forceRedraw = m.redraw.strategy() === "all";
			for (var i = 0, root; root = roots[i]; i++) {
				if (controllers[i]) {
					m.render(root, modules[i].view ? modules[i].view(controllers[i]) : blank(), forceRedraw)
				}
			}
			//after rendering within a routed context, we need to scroll back to the top, and fetch the document title for history.pushState
			if (computePostRedrawHook) {
				computePostRedrawHook();
				computePostRedrawHook = null
			}
			lastRedrawId = null;
			lastRedrawCallTime = new Date;
			m.redraw.strategy("diff")
		}

		var pendingRequests = 0;
		m.startComputation = function() {pendingRequests++};
		m.endComputation = function() {
			pendingRequests = Math.max(pendingRequests - 1, 0);
			if (pendingRequests === 0) m.redraw()
		};
		var endFirstComputation = function() {
			if (m.redraw.strategy() == "none") {
				pendingRequests--
				m.redraw.strategy("diff")
			}
			else m.endComputation();
		}

		m.withAttr = function(prop, withAttrCallback) {
			return function(e) {
				e = e || event;
				var currentTarget = e.currentTarget || this;
				withAttrCallback(prop in currentTarget ? currentTarget[prop] : currentTarget.getAttribute(prop))
			}
		};

		//routing
		var modes = {pathname: "", hash: "#", search: "?"};
		var redirect = function() {}, routeParams, currentRoute;
		m.route = function() {
			//m.route()
			if (arguments.length === 0) return currentRoute;
			//m.route(el, defaultRoute, routes)
			else if (arguments.length === 3 && type.call(arguments[1]) === STRING) {
				var root = arguments[0], defaultRoute = arguments[1], router = arguments[2];
				redirect = function(source) {
					var path = currentRoute = normalizeRoute(source);
					if (!routeByValue(root, router, path)) {
						m.route(defaultRoute, true)
					}
				};
				var listener = m.route.mode === "hash" ? "onhashchange" : "onpopstate";
				window[listener] = function() {
					var path = $location[m.route.mode]
					if (m.route.mode === "pathname") path += $location.search
					if (currentRoute != normalizeRoute(path)) {
						redirect(path)
					}
				};
				computePostRedrawHook = setScroll;
				window[listener]()
			}
			//config: m.route
			else if (arguments[0].addEventListener) {
				var element = arguments[0];
				var isInitialized = arguments[1];
				var context = arguments[2];
				element.href = (m.route.mode !== 'pathname' ? $location.pathname : '') + modes[m.route.mode] + this.attrs.href;
				element.removeEventListener("click", routeUnobtrusive);
				element.addEventListener("click", routeUnobtrusive)
			}
			//m.route(route, params)
			else if (type.call(arguments[0]) === STRING) {
				var oldRoute = currentRoute;
				currentRoute = arguments[0];
				var args = arguments[1] || {}
				var queryIndex = currentRoute.indexOf("?")
				var params = queryIndex > -1 ? parseQueryString(currentRoute.slice(queryIndex + 1)) : {}
				for (var i in args) params[i] = args[i]
				var querystring = buildQueryString(params)
				var currentPath = queryIndex > -1 ? currentRoute.slice(0, queryIndex) : currentRoute
				if (querystring) currentRoute = currentPath + (currentPath.indexOf("?") === -1 ? "?" : "&") + querystring;

				var shouldReplaceHistoryEntry = (arguments.length === 3 ? arguments[2] : arguments[1]) === true || oldRoute === arguments[0];

				if (window.history.pushState) {
					computePostRedrawHook = function() {
						window.history[shouldReplaceHistoryEntry ? "replaceState" : "pushState"](null, $document.title, modes[m.route.mode] + currentRoute);
						setScroll()
					};
					redirect(modes[m.route.mode] + currentRoute)
				}
				else $location[m.route.mode] = currentRoute
			}
		};
		m.route.param = function(key) {
			if (!routeParams) throw new Error("You must call m.route(element, defaultRoute, routes) before calling m.route.param()")
			return routeParams[key]
		};
		m.route.mode = "search";
		function normalizeRoute(route) {
			return route.slice(modes[m.route.mode].length)
		}
		function routeByValue(root, router, path) {
			routeParams = {};

			var queryStart = path.indexOf("?");
			if (queryStart !== -1) {
				routeParams = parseQueryString(path.substr(queryStart + 1, path.length));
				path = path.substr(0, queryStart)
			}

			for (var route in router) {
				if (route === path) {
					m.module(root, router[route]);
					return true
				}

				var matcher = new RegExp("^" + route.replace(/:[^\/]+?\.{3}/g, "(.*?)").replace(/:[^\/]+/g, "([^\\/]+)") + "\/?$");

				if (matcher.test(path)) {
					path.replace(matcher, function() {
						var keys = route.match(/:[^\/]+/g) || [];
						var values = [].slice.call(arguments, 1, -2);
						for (var i = 0, len = keys.length; i < len; i++) routeParams[keys[i].replace(/:|\./g, "")] = decodeURIComponent(values[i])
						m.module(root, router[route])
					});
					return true
				}
			}
		}
		function routeUnobtrusive(e) {
			e = e || event;
			if (e.ctrlKey || e.metaKey || e.which === 2) return;
			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;
			var currentTarget = e.currentTarget || this;
			var args = m.route.mode === "pathname" && currentTarget.search ? parseQueryString(currentTarget.search.slice(1)) : {};
			m.route(currentTarget[m.route.mode].slice(modes[m.route.mode].length), args)
		}
		function setScroll() {
			if (m.route.mode != "hash" && $location.hash) $location.hash = $location.hash;
			else window.scrollTo(0, 0)
		}
		function buildQueryString(object, prefix) {
			var str = [];
			for(var prop in object) {
				var key = prefix ? prefix + "[" + prop + "]" : prop, value = object[prop];
				var valueType = type.call(value)
				var pair = value != null && (valueType === OBJECT) ?
					buildQueryString(value, key) :
					valueType === ARRAY ?
						value.map(function(item) {return encodeURIComponent(key + "[]") + "=" + encodeURIComponent(item)}).join("&") :
						encodeURIComponent(key) + "=" + encodeURIComponent(value)
				str.push(pair)
			}
			return str.join("&")
		}
		
		function parseQueryString(str) {
			var pairs = str.split("&"), params = {};
			for (var i = 0, len = pairs.length; i < len; i++) {
				var pair = pairs[i].split("=");
				params[decodeURIComponent(pair[0])] = pair[1] ? decodeURIComponent(pair[1]) : ""
			}
			return params
		}
		function reset(root) {
			var cacheKey = getCellCacheKey(root);
			clear(root.childNodes, cellCache[cacheKey]);
			cellCache[cacheKey] = undefined
		}

		m.deferred = function () {
			var deferred = new Deferred();
			deferred.promise = propify(deferred.promise);
			return deferred
		};
		function propify(promise) {
			var prop = m.prop();
			promise.then(prop);
			prop.then = function(resolve, reject) {
				return propify(promise.then(resolve, reject))
			};
			return prop
		}
		//Promiz.mithril.js | Zolmeister | MIT
		//a modified version of Promiz.js, which does not conform to Promises/A+ for two reasons:
		//1) `then` callbacks are called synchronously (because setTimeout is too slow, and the setImmediate polyfill is too big
		//2) throwing subclasses of Error cause the error to be bubbled up instead of triggering rejection (because the spec does not account for the important use case of default browser error handling, i.e. message w/ line number)
		function Deferred(successCallback, failureCallback) {
			var RESOLVING = 1, REJECTING = 2, RESOLVED = 3, REJECTED = 4;
			var self = this, state = 0, promiseValue = 0, next = [];

			self["promise"] = {};

			self["resolve"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = RESOLVING;

					fire()
				}
				return this
			};

			self["reject"] = function(value) {
				if (!state) {
					promiseValue = value;
					state = REJECTING;

					fire()
				}
				return this
			};

			self.promise["then"] = function(successCallback, failureCallback) {
				var deferred = new Deferred(successCallback, failureCallback);
				if (state === RESOLVED) {
					deferred.resolve(promiseValue)
				}
				else if (state === REJECTED) {
					deferred.reject(promiseValue)
				}
				else {
					next.push(deferred)
				}
				return deferred.promise
			};

			function finish(type) {
				state = type || REJECTED;
				next.map(function(deferred) {
					state === RESOLVED && deferred.resolve(promiseValue) || deferred.reject(promiseValue)
				})
			}

			function thennable(then, successCallback, failureCallback, notThennableCallback) {
				if (((promiseValue != null && type.call(promiseValue) === OBJECT) || typeof promiseValue === FUNCTION) && typeof then === FUNCTION) {
					try {
						// count protects against abuse calls from spec checker
						var count = 0;
						then.call(promiseValue, function(value) {
							if (count++) return;
							promiseValue = value;
							successCallback()
						}, function (value) {
							if (count++) return;
							promiseValue = value;
							failureCallback()
						})
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						failureCallback()
					}
				} else {
					notThennableCallback()
				}
			}

			function fire() {
				// check if it's a thenable
				var then;
				try {
					then = promiseValue && promiseValue.then
				}
				catch (e) {
					m.deferred.onerror(e);
					promiseValue = e;
					state = REJECTING;
					return fire()
				}
				thennable(then, function() {
					state = RESOLVING;
					fire()
				}, function() {
					state = REJECTING;
					fire()
				}, function() {
					try {
						if (state === RESOLVING && typeof successCallback === FUNCTION) {
							promiseValue = successCallback(promiseValue)
						}
						else if (state === REJECTING && typeof failureCallback === "function") {
							promiseValue = failureCallback(promiseValue);
							state = RESOLVING
						}
					}
					catch (e) {
						m.deferred.onerror(e);
						promiseValue = e;
						return finish()
					}

					if (promiseValue === self) {
						promiseValue = TypeError();
						finish()
					}
					else {
						thennable(then, function () {
							finish(RESOLVED)
						}, finish, function () {
							finish(state === RESOLVING && RESOLVED)
						})
					}
				})
			}
		}
		m.deferred.onerror = function(e) {
			if (type.call(e) === "[object Error]" && !e.constructor.toString().match(/ Error/)) throw e
		};

		m.sync = function(args) {
			var method = "resolve";
			function synchronizer(pos, resolved) {
				return function(value) {
					results[pos] = value;
					if (!resolved) method = "reject";
					if (--outstanding === 0) {
						deferred.promise(results);
						deferred[method](results)
					}
					return value
				}
			}

			var deferred = m.deferred();
			var outstanding = args.length;
			var results = new Array(outstanding);
			if (args.length > 0) {
				for (var i = 0; i < args.length; i++) {
					args[i].then(synchronizer(i, true), synchronizer(i, false))
				}
			}
			else deferred.resolve([]);

			return deferred.promise
		};
		function identity(value) {return value}

		function ajax(options) {
			if (options.dataType && options.dataType.toLowerCase() === "jsonp") {
				var callbackKey = "mithril_callback_" + new Date().getTime() + "_" + (Math.round(Math.random() * 1e16)).toString(36);
				var script = $document.createElement("script");

				window[callbackKey] = function(resp) {
					script.parentNode.removeChild(script);
					options.onload({
						type: "load",
						target: {
							responseText: resp
						}
					});
					window[callbackKey] = undefined
				};

				script.onerror = function(e) {
					script.parentNode.removeChild(script);

					options.onerror({
						type: "error",
						target: {
							status: 500,
							responseText: JSON.stringify({error: "Error making jsonp request"})
						}
					});
					window[callbackKey] = undefined;

					return false
				};

				script.onload = function(e) {
					return false
				};

				script.src = options.url
					+ (options.url.indexOf("?") > 0 ? "&" : "?")
					+ (options.callbackKey ? options.callbackKey : "callback")
					+ "=" + callbackKey
					+ "&" + buildQueryString(options.data || {});
				$document.body.appendChild(script)
			}
			else {
				var xhr = new window.XMLHttpRequest;
				xhr.open(options.method, options.url, true, options.user, options.password);
				xhr.onreadystatechange = function() {
					if (xhr.readyState === 4) {
						if (xhr.status >= 200 && xhr.status < 300) options.onload({type: "load", target: xhr});
						else options.onerror({type: "error", target: xhr})
					}
				};
				if (options.serialize === JSON.stringify && options.data && options.method !== "GET") {
					xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8")
				}
				if (options.deserialize === JSON.parse) {
					xhr.setRequestHeader("Accept", "application/json, text/*");
				}
				if (typeof options.config === FUNCTION) {
					var maybeXhr = options.config(xhr, options);
					if (maybeXhr != null) xhr = maybeXhr
				}

				var data = options.method === "GET" || !options.data ? "" : options.data
				if (data && (type.call(data) != STRING && data.constructor != window.FormData)) {
					throw "Request data should be either be a string or FormData. Check the `serialize` option in `m.request`";
				}
				xhr.send(data);
				return xhr
			}
		}
		function bindData(xhrOptions, data, serialize) {
			if (xhrOptions.method === "GET" && xhrOptions.dataType != "jsonp") {
				var prefix = xhrOptions.url.indexOf("?") < 0 ? "?" : "&";
				var querystring = buildQueryString(data);
				xhrOptions.url = xhrOptions.url + (querystring ? prefix + querystring : "")
			}
			else xhrOptions.data = serialize(data);
			return xhrOptions
		}
		function parameterizeUrl(url, data) {
			var tokens = url.match(/:[a-z]\w+/gi);
			if (tokens && data) {
				for (var i = 0; i < tokens.length; i++) {
					var key = tokens[i].slice(1);
					url = url.replace(tokens[i], data[key]);
					delete data[key]
				}
			}
			return url
		}

		m.request = function(xhrOptions) {
			if (xhrOptions.background !== true) m.startComputation();
			var deferred = m.deferred();
			var isJSONP = xhrOptions.dataType && xhrOptions.dataType.toLowerCase() === "jsonp";
			var serialize = xhrOptions.serialize = isJSONP ? identity : xhrOptions.serialize || JSON.stringify;
			var deserialize = xhrOptions.deserialize = isJSONP ? identity : xhrOptions.deserialize || JSON.parse;
			var extract = xhrOptions.extract || function(xhr) {
				return xhr.responseText.length === 0 && deserialize === JSON.parse ? null : xhr.responseText
			};
			xhrOptions.url = parameterizeUrl(xhrOptions.url, xhrOptions.data);
			xhrOptions = bindData(xhrOptions, xhrOptions.data, serialize);
			xhrOptions.onload = xhrOptions.onerror = function(e) {
				try {
					e = e || event;
					var unwrap = (e.type === "load" ? xhrOptions.unwrapSuccess : xhrOptions.unwrapError) || identity;
					var response = unwrap(deserialize(extract(e.target, xhrOptions)));
					if (e.type === "load") {
						if (type.call(response) === ARRAY && xhrOptions.type) {
							for (var i = 0; i < response.length; i++) response[i] = new xhrOptions.type(response[i])
						}
						else if (xhrOptions.type) response = new xhrOptions.type(response)
					}
					deferred[e.type === "load" ? "resolve" : "reject"](response)
				}
				catch (e) {
					m.deferred.onerror(e);
					deferred.reject(e)
				}
				if (xhrOptions.background !== true) m.endComputation()
			};
			ajax(xhrOptions);
			deferred.promise(xhrOptions.initialValue);
			return deferred.promise
		};

		//testing API
		m.deps = function(mock) {
			initialize(window = mock || window);
			return window;
		};
		//for internal testing only, do not use `m.deps.factory`
		m.deps.factory = app;

		return m
	})(typeof window != "undefined" ? window : {});

	if (typeof module != "undefined" && module !== null && module.exports) module.exports = m;
	else if (true) !(__WEBPACK_AMD_DEFINE_RESULT__ = function() {return m}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(53)(module)))

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	/*

	    countUp.js
	    by @hamedb89

	*/

	// target = id of html element or var of previously selected html element where counting occurs
	// startVal = the value you want to begin at
	// endVal = the value you want to arrive at
	// decimals = number of decimal places, default 0
	// duration = duration of animation in seconds, default 2
	// options = optional object of options (see below)

	module.exports = function(target, startVal, endVal, decimals, duration, options) {
	    // make sure requestAnimationFrame and cancelAnimationFrame are defined
	    // polyfill for browsers without native support
	    // by Opera engineer Erik Möller
	    var lastTime = 0;
	    var vendors = ['webkit', 'moz', 'ms', 'o'];
	    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
	        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
	        window.cancelAnimationFrame =
	          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	    }
	    if (!window.requestAnimationFrame) {
	        window.requestAnimationFrame = function(callback, element) {
	            var currTime = new Date().getTime();
	            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
	            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
	              timeToCall);
	            lastTime = currTime + timeToCall;
	            return id;
	        }
	    }
	    if (!window.cancelAnimationFrame) {
	        window.cancelAnimationFrame = function(id) {
	            clearTimeout(id);
	        }
	    }

	     // default options
	    this.options = options || {
	        useEasing : true, // toggle easing
	        useGrouping : true, // 1,000,000 vs 1000000
	        separator : ',', // character to use as a separator
	        decimal : '.', // character to use as a decimal
	    }
	    if (this.options.separator == '') this.options.useGrouping = false;
	    if (this.options.prefix == null) this.options.prefix = '';
	    if (this.options.suffix == null) this.options.suffix = '';

	    var self = this;

	    this.d = (typeof target === 'string') ? document.getElementById(target) : target;
	    this.startVal = Number(startVal);
	    this.endVal = Number(endVal);
	    this.countDown = (this.startVal > this.endVal) ? true : false;
	    this.startTime = null;
	    this.timestamp = null;
	    this.remaining = null;
	    this.frameVal = this.startVal;
	    this.rAF = null;
	    this.decimals = Math.max(0, decimals || 0);
	    this.dec = Math.pow(10, this.decimals);
	    this.duration = duration * 1000 || 2000;

	    this.version = function () { return '1.3.2' }

	    // Print value to target
	    this.printValue = function(value) {
	        var result = (!isNaN(value)) ? self.formatNumber(value) : '--';
	        if (self.d.tagName == 'INPUT') {
	            this.d.value = result;
	        }
	        else if (self.d.tagName == 'text') {
	            this.d.textContent = result;
	        }
	        else {
	            this.d.innerHTML = result;
	        }
	    }

	    // Robert Penner's easeOutExpo
	    this.easeOutExpo = function(t, b, c, d) {
	        return c * (-Math.pow(2, -10 * t / d) + 1) * 1024 / 1023 + b;
	    }
	    this.count = function(timestamp) {

	        if (self.startTime === null) self.startTime = timestamp;

	        self.timestamp = timestamp;

	        var progress = timestamp - self.startTime;
	        self.remaining = self.duration - progress;

	        // to ease or not to ease
	        if (self.options.useEasing) {
	            if (self.countDown) {
	                var i = self.easeOutExpo(progress, 0, self.startVal - self.endVal, self.duration);
	                self.frameVal = self.startVal - i;
	            } else {
	                self.frameVal = self.easeOutExpo(progress, self.startVal, self.endVal - self.startVal, self.duration);
	            }
	        } else {
	            if (self.countDown) {
	                var i = (self.startVal - self.endVal) * (progress / self.duration);
	                self.frameVal = self.startVal - i;
	            } else {
	                self.frameVal = self.startVal + (self.endVal - self.startVal) * (progress / self.duration);
	            }
	        }

	        // don't go past endVal since progress can exceed duration in the last frame
	        if (self.countDown) {
	            self.frameVal = (self.frameVal < self.endVal) ? self.endVal : self.frameVal;
	        } else {
	            self.frameVal = (self.frameVal > self.endVal) ? self.endVal : self.frameVal;
	        }

	        // decimal
	        self.frameVal = Math.round(self.frameVal*self.dec)/self.dec;

	        // format and print value
	        self.printValue(self.frameVal);

	        // whether to continue
	        if (progress < self.duration) {
	            self.rAF = requestAnimationFrame(self.count);
	        } else {
	            if (self.callback != null) self.callback();
	        }
	    }
	    this.start = function(callback) {
	        self.callback = callback;
	        // make sure values are valid
	        if (!isNaN(self.endVal) && !isNaN(self.startVal)) {
	            self.rAF = requestAnimationFrame(self.count);
	        } else {
	            console.log('countUp error: startVal or endVal is not a number');
	            self.printValue();
	        }
	        return false;
	    }
	    this.stop = function() {
	        cancelAnimationFrame(self.rAF);
	    }
	    this.reset = function() {
	        self.startTime = null;
	        self.startVal = startVal;
	        cancelAnimationFrame(self.rAF);
	        self.printValue(self.startVal);
	    }
	    this.resume = function() {
	        self.stop();
	        self.startTime = null;
	        self.duration = self.remaining;
	        self.startVal = self.frameVal;
	        requestAnimationFrame(self.count);
	    }
	    this.formatNumber = function(nStr) {
	        nStr = nStr.toFixed(self.decimals);
	        nStr += '';
	        var x, x1, x2, rgx;
	        x = nStr.split('.');
	        x1 = x[0];
	        x2 = x.length > 1 ? self.options.decimal + x[1] : '';
	        rgx = /(\d+)(\d{3})/;
	        if (self.options.useGrouping) {
	            while (rgx.test(x1)) {
	                x1 = x1.replace(rgx, '$1' + self.options.separator + '$2');
	            }
	        }
	        return self.options.prefix + x1 + x2 + self.options.suffix;
	    }

	    // format startVal on initialization
	    self.printValue(self.startVal);
	};

	// Example:
	// var numAnim = new countUp("SomeElementYouWantToAnimate", 0, 99.99, 2, 2.5);
	// numAnim.start();
	// with optional callback:
	// numAnim.start(someMethodToCallOnComplete);


/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	
	/**
	 * Expose `Emitter`.
	 */

	module.exports = Emitter;

	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */

	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};

	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */

	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}

	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks[event] = this._callbacks[event] || [])
	    .push(fn);
	  return this;
	};

	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.once = function(event, fn){
	  var self = this;
	  this._callbacks = this._callbacks || {};

	  function on() {
	    self.off(event, on);
	    fn.apply(this, arguments);
	  }

	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};

	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};

	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }

	  // specific event
	  var callbacks = this._callbacks[event];
	  if (!callbacks) return this;

	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks[event];
	    return this;
	  }

	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }
	  return this;
	};

	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */

	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};
	  var args = [].slice.call(arguments, 1)
	    , callbacks = this._callbacks[event];

	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }

	  return this;
	};

	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */

	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks[event] || [];
	};

	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */

	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};


/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	
	/**
	 * Reduce `arr` with `fn`.
	 *
	 * @param {Array} arr
	 * @param {Function} fn
	 * @param {Mixed} initial
	 *
	 * TODO: combatible error handling?
	 */

	module.exports = function(arr, fn, initial){  
	  var idx = 0;
	  var len = arr.length;
	  var curr = arguments.length == 3
	    ? initial
	    : arr[idx++];

	  while (idx < len) {
	    curr = fn.call(null, curr, arr[idx], ++idx, arr);
	  }
	  
	  return curr;
	};

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = Array.isArray || function (arr) {
	  return Object.prototype.toString.call(arr) == '[object Array]';
	};


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ }
/******/ ])