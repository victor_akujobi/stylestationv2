package main

import (
	"bytes"
	"code.google.com/p/gcfg"
	"fmt"
	gr "github.com/ftrvxmtrx/gravatar"
	mandrill "github.com/keighl/mandrill"
	"image"
	"image/jpeg"
	_ "image/png"
	"os"
	"regexp"
	// "strings"
	// "auth"
)

type ConfigFile struct {
	Dev     Config
	Staging Config
	Live    Config
}
type Config struct {
	Port                 string
	CookieString         string
	Hostname             string
	DatabasePort         string
	DatabaseUser         string
	DatabasePassword     string
	DatabaseHost         string
	FacebookClientID     string
	FacebookClientSecret string
	FacebookProfileUrl   string
	GoogleClientID       string
	GoogleClientSecret   string
	GoogleProfileUrl     string
	StylesDir            string
	ProfileDir           string
	AnkaraDir            string
	TwitterSecret        string
	TwitterKey           string
	GoogleSecret         string
	GoogleKey            string
	MandrillKey          string
}

var UsernameRegexp = regexp.MustCompile(`\A[a-z0-9\-_]{3,16}\z`)

func LoadConfig(cfgFile string) Config {
	// IMAGE_SIZES["medium"] = 400
	// IMAGE_SIZES["large"] = 800
	var configFile ConfigFile
	if err := gcfg.ReadFileInto(&configFile, cfgFile); err != nil {
		panic(err)
		panic("Config file not found, Stringer!!, Where the config file at? Huh String!!")
	}
	print(os.Getenv("MARTINI_ENV"))
	switch os.Getenv("MARTINI_ENV") {
	case "staging":
		return configFile.Staging
		break
	case "live":
		return configFile.Live
		break
	default:
		return configFile.Dev
		break
	}
	return configFile.Dev
}

// func IsValidUsername(username string) (success bool, message string) {
// 	var count int
// 	if !UsernameRegexp.MatchString(username) {
// 		success = false
// 		message = "Invalid Username"
// 		return
// 	}
// 	queryString := "SELECT COUNT(users.id) from users WHERE user_name = ?"
// 	DB.Debug().Raw(queryString, username).Row().Scan(&count)
// 	if count == 0 {
// 		fmt.Printf("\n User ID: %#v\n", count)
// 		success = true
// 		message = ""
// 		return
// 	}
// 	success = false
// 	message = "Username already in use"
// 	return
// }

// func IsValidPassword(password string) bool {
// 	return len(password) > 5
// }

// func IsValidEmail(email string) (success bool, message string) {
// 	var count int
// 	success = strings.Contains(email, "@") && strings.Contains(email, ".") && (strings.LastIndex(email, ".") > strings.LastIndex(email, "@"))
// 	if success {
// 		queryString := "SELECT COUNT(auths.email) from auths WHERE email = ?"
// 		DB.Debug().Raw(queryString, email).Row().Scan(&count)
// 		if count == 0 {
// 			fmt.Printf("\n User ID: %#v\n", count)
// 			success = true
// 			message = ""
// 			return
// 		}
// 		success = false
// 		message = "Email address already in use"
// 		return
// 	}
// 	success = false
// 	message = "Invalid email"
// 	return

// }

func SendWelcomeEmail(emailAddress string) {
	message := &mandrill.Message{}
	templateContent := map[string]string{"first_name": "Victor"}
	responses, err := mandrillClient.MessagesSendTemplate(message, "thank-you-for-signing-up", templateContent)
	if err != nil {
		fmt.Printf("\n\n\nResponse: %#v\n\nError: %#v\n\n\n", responses, err)
	}
}
func SetGravatar(username string, email string) (err error) {
	// emailHash := gr.EmailHash("marjorie.olat@gmail.com")
	emailHash := gr.EmailHash(email)
	var result []byte
	if result, err = gr.GetAvatar("http", emailHash, gr.DefaultIdentIcon, 32); err != nil {
		return err
	}
	// var cfg image.Config
	// var format string
	rawImage := bytes.NewReader(result)
	var decodedImage image.Image
	if decodedImage, _, err = image.Decode(rawImage); err != nil {
		return err
	}
	var outFile *os.File
	if outFile, err = os.Create(fmt.Sprintf("%s%s.jpg", config.ProfileDir, "marjorie_olat")); err != nil {
		return err
	}
	jpeg.Encode(outFile, decodedImage, nil)
	return nil
}
