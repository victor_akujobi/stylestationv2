package main

import (
	"fmt"
	"github.com/jmcvetta/neoism"
)

type CollectionQuery struct {
	ShowHidden bool
	OwnedBy    string
	Styles     []interface{}
	Colours    []interface{}
	Gender     []interface{}
	Brand      string
	Page       int64
}
type EntryImage struct {
	ImageHeight int32  `binding:"required"`
	ImageWidth  int32  `binding:"required"`
	Image       string `binding:"required"`
}
type Collection struct {
	Name        string
	Description string
	Count       int64
	Entries     []Ankara
	Images      []string
	AspectRatio []float64
	EntryImages []EntryImage
	Hidden      bool //Create as many hidden collections.
}
type Collections []Collection

func (this *Collection) SaveThumbnail() error {
	/* Save image here */
	// err = false
	return nil
}

func (this *Collections) PopulateEntries(skip string) {
	/* Enter entries */
}

func (this *Collection) Hide(hide bool) {

}

func (this *Collection) AddItem(item string) {

}

func (this *Collection) RemoveItem(item string) {

}

func FetchMatchingCollections(query CollectionQuery) []Collection {
	// collectionSubQuery := ""
	// favouritedBy := ""
	// ownedBy := ""
	params := neoism.Props{
		"queryColours": query.Colours,
		"queryStyles":  query.Styles,
		"queryGenders": query.Gender,
		"skip":         (query.Page - 1) * ENTRIES_PER_PAGE,
		"limit":        ENTRIES_PER_PAGE,
	}
	// if query.Collection != "" {
	// 	collectionSubQuery = `, (co: Collection {ref: {collectionRef}})<-[:IN_COLLECTION]-(a)`
	// 	params["collectionRef"] = query.Collection
	// } else if query.FavouritedBy != "" {
	// 	favouritedBy = `, (f:User {ref: {favouritedBy}})-[:FAVOURITED]->(a)`
	// 	params["favouritedBy"] = query.FavouritedBy
	// } else if query.Owner != "" {
	// 	ownedBy = `, (o: User {ref: {ownerRef}})-[:OWNS]->(a)`
	// 	params["ownerRef"] = query.Owner
	// }
	hiddenSubQuery := "MATCH (co: Collection {hidden: false})"
	if query.ShowHidden {
		hiddenSubQuery = "MATCH (co: Collection)"
	}
	stmt := hiddenSubQuery +
		`
		WITH co
		OPTIONAL MATCH (co)<-[l:IN_COLLECTION]-(a:Ankara)
		WITH co, l, a
		ORDER BY co.createdAt
		RETURN co.name AS name, co.description AS description, count(l) AS count, collect(a.image)[0..4] AS images, collect(toFloat(a.imageWidth)/toFloat(a.imageHeight)) as aspectRatio
	`

	// stmt := `
	// 	MATCH (s:Style)<-[is:IS]-(a:Ankara)-[coloured:COLOURED]->(c:Colour)` +
	// collectionSubQuery +
	// favouritedBy +
	// ownedBy +
	// `
	// WHERE c.name in {queryColours} AND s.name in {queryStyles} AND a.gender IN {queryGenders}
	// WITH distinct a as entry, collect(distinct s.name) as styles, collect(distinct c.name) as colours
	// MATCH (entry)<-[:OWNS]-(u:User)
	// WITH entry, u, styles, colours
	// ORDER BY toInt(entry.created) DESC
	// RETURN u.ref as ownerRef, u.name as ownerName, entry.ref as ref, entry.gender as gender, entry.image as image, entry.imageHeight as imageHeight, entry.imageWidth as imageWidth, toInt(entry.created) as createdAt, styles, colours SKIP {skip} LIMIT {limit}
	//   `
	res := []Collection{}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {

	}
	var height float64 = 100
	for index, _ := range res {
		for imageIndex, _ := range res[index].Images {
			aspectRatio := res[index].AspectRatio[imageIndex]
			entryImage := EntryImage{ImageHeight: int32(height), ImageWidth: int32(aspectRatio * height), Image: res[index].Images[imageIndex]}
			res[index].EntryImages = append(res[index].EntryImages, entryImage)
			fmt.Printf("\n Collections : %#v\n", res[index])
		}
	}
	return res
}

func GetEntriesForOwnerFromDB(userRef string, skip int64, count int64) []Ankara {
	stmt := `
		MATCH (s:Style)<-[is:IS]-(a:Ankara)<-[:OWNS]-(u:User{ref: {userRef}})
		WITH a, u, collect(s.name) as styles
		MATCH (a)-[coloured:COLOURED]->(c:Colour)
		WITH a, u, styles, collect(c.name) AS colours
		ORDER BY toInt(a.created) DESC
		RETURN a.ref as ref, u.ref as ownerRef, u.name as ownername, a.image as image, a.imageHeight as imageHeight, a.imageWidth as imageWidth, toInt(a.created) as createdAt, colours, styles SKIP {skip} LIMIT {limit}
    `
	// query params
	params := neoism.Props{
		"userRef": userRef,
		"skip":    skip,
		"limit":   ENTRIES_PER_PAGE,
	}

	// query results
	res := []Ankara{}
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\nOwner's Entries: %#v\n", res)
	// for k,v := range Ank
	return res
}

type UserEntries struct {
	Ref     string
	Entries []Ankara
	HasMore bool
}

func (this *UserEntries) PopulateEntries(skip int64, count int64) {
	/* We request an extra item so that we can tell if we have more to display */
	this.Entries = GetEntriesForOwnerFromDB(this.Ref, skip, count+1)

	/* Check if we have more to display. */
	/* This tells the UI whether to show the next buttons */
	this.HasMore = len(this.Entries) > int(count)

	/* If we have an extra item, we pop the last item.*/
	if this.HasMore {
		_, this.Entries = this.Entries[len(this.Entries)-1], this.Entries[:len(this.Entries)-1]
	}
}

type UserCollections struct {
	Ref         string
	Collections []Collection
	HasMore     bool
}

func (this *UserCollections) PopulateCollections(skip int64, count int64) {
	/* We request an extra item so that we can tell if we have more to display */
	this.Collections = GetCollectionsForOwnerFromDB(this.Ref, skip, count+1)

	/* Check if we have more to display. */
	/* This tells the UI whether to show the next buttons */
	this.HasMore = len(this.Collections) > int(count)

	/* If we have an extra item, we pop the last item.*/
	if this.HasMore {
		_, this.Collections = this.Collections[len(this.Collections)-1], this.Collections[:len(this.Collections)-1]
	}
}

func GetCollectionsForOwnerFromDB(userRef string, skip int64, count int64) []Collection {
	stmt := `
		MATCH (u:User {ref: ref})-[:OWNS_COLLECTION]->(c:Collection)
		WITH c
		OPTIONAL MATCH (c)<-[l:IN_COLLECTION]-(a:Ankara)
		RETURN c.name AS name, c.description AS description, count(l) AS count
    `
	// query params
	params := neoism.Props{
		"userRef": userRef,
		"skip":    skip,
		"limit":   count,
	}

	// query results
	res := []Collection{}
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\nOwner's Entries: %#v\n", res)
	// for k,v := range Ank
	return res
}
