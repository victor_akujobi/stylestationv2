package main

import (
	"fmt"
	"github.com/deckarep/golang-set"
	"github.com/jmcvetta/neoism"
	"github.com/jmcvetta/randutil"
	"strconv"
	"time"
)

func ConvertToInterface(array []string) []interface{} {
	typeInterface := make([]interface{}, len(array))
	for i := range array {
		typeInterface[i] = array[i]
	}
	return typeInterface
}
func init() {

}

var NUM_DASHBOARD_ITEMS = 1

type SearchQuery struct {
	Colours      []interface{} //`json:"colours"`
	Styles       []interface{} //`json:"styles"`
	Gender       []interface{}
	Collection   string
	Owner        string
	FavouritedBy string
	Page         int64
}

type RESTResource interface {
	GetOwnerId() interface{}
}
type Model interface {
	CanBeUpdatedBy(interface{}) bool
	CanBeCreatedBy(interface{}) bool
	CanBeDeletedBy(interface{}) bool
	GetOwnerId() int64
}

func isAdmin(string) bool {
	return false
}

func genericAuthorization(resource Model, userId int64) bool {
	return (resource.GetOwnerId() == userId) || false
}

type StyleStationUser struct {
	//Save
	Username    string
	Ref         string
	Provider    string
	AuthId      string
	DisplayName string
	Email       string
}

func (this *StyleStationUser) Save() {
	stmt := `
        MERGE (u:User {provider: {provider}, auth_id: {auth_id}}) -[:OWNS_PROFILE]-> (p:Profile)
        ON CREATE SET u.ref = {ref}, u.provider = {provider}, u.auth_id = {auth_id}, u.name= {display_name}, u.email = {email}, p.contactEmail = {email}
        RETURN u.ref, u.name
    `

	// query params
	params := neoism.Props{
		"ref":          this.Ref,
		"provider":     this.Provider,
		"auth_id":      this.AuthId,
		"display_name": this.DisplayName,
		"email":        this.Email,
	}
	// query results
	res := []struct {
		Ref  string `json:"u.ref"` // Column "n" gets automagically unmarshalled into field N
		Name string `json:"u.name"`
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\nRef: %#v\n", res)
	this.DisplayName = res[0].Name
	this.Ref = res[0].Ref
	// return res[0].Ref
}

/* END USER */

func EditProfile(userRef string) error {
	stmt := `
		MATCH (p:Profile)<-[:OWNS_PROFILE]-(u:User {ref: {userRef}})
		SET p.gender =  {gender},  p.description = {description},  p.profile_type = {profile_type}, p.url = {url}, p.last_edited = timestamp()
		RETURN p
    `
	// p.gender as gender, p.description as description, p.profile_type as profileType, p.url as url
	params := neoism.Props{
		"userRef":      userRef,
		"gender":       "Female",
		"profile_type": "Fashionista",
		"url":          "",
		"description":  "Really Interested in African Fashion",
	}

	// query results
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     nil,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	// fmt.Printf("\nEntries: %#v\n", res)
	// for k,v := range Ank
	return err
}

type Profile struct {
	Description  string
	Gender       string
	Type         string
	Url          string
	Views        int64
	Uploads      int64
	Collections  int64
	IsBrand      bool
	BrandName    string
	IsEditable   bool
	ContactEmail string
	PhoneNumber  string
}

type Ankaras []Ankara

// type DashboardItem struct {
// 	Count int64
// }

/* DASHBOARD */
type ProfileDashboard struct {
	Ref         string
	Uploads     int64
	Favourites  int64
	Collections int64
}

func PopulateDashboard(ref string) ProfileDashboard {
	dashboard := []ProfileDashboard{}
	stmt := `MATCH (u:User {ref: {ref}})
			WITH u
			OPTIONAL MATCH (u)-[c:OWNS_COLLECTION]->(Collection)
			WITH u, count(c) as collections
			OPTIONAL MATCH (u)-[o:OWNS]->(Ankara)
			WITH u, collections, count(o) AS uploads
			OPTIONAL MATCH(u)-[f:FAVOURITED]->(Ankara)
			RETURN collections, count(f) as favourites, uploads`
	params := neoism.Props{
		"ref": ref,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &dashboard,
	}
	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {

	}
	return dashboard[0]
}

// func (this *ProfileDashboard) GetDashboardUploads() {
// 	this.Uploads = GetUserUploads(this.Ref, NUM_DASHBOARD_ITEMS, 0)
// }

// func (this *ProfileDashboard) GetDashboardFavourites() {
// 	this.Favourites = GetUserFavourites(this.Ref, NUM_DASHBOARD_ITEMS, 0)
// }

// func (this *ProfileDashboard) GetDashboardCollections() {
// 	this.Collections = GetUserCollections(this.Ref, NUM_DASHBOARD_ITEMS, 0)
// }

func GetUserFavourites(ref string, count int, skip int) (favourites Ankaras) {
	stmt := `MATCH (u:User {ref: {ref}}), (a)<-[f:FAVOURITED]-(u), (a:Ankara)<-[:OWNS]-(owner:User), (a)-[:IS]->(s:Style), (a)-[:COLOURED]->(c:Colour)
	WITH u, a, owner, collect(DISTINCT s.name) AS styles, collect(DISTINCT c.name) AS colours, count(DISTINCT f) as favourites
	RETURN a.ref AS ref, owner.ref as ownerRef, owner.name AS ownerName, a.gender as gender, a.image AS image, a.views AS views, a.imageHeight AS imageHeight, a.imageWidth AS imageWidth, styles AS styles, colours AS colours, favourites AS favourites SKIP {skip} LIMIT {count}`

	params := neoism.Props{
		"ref":   ref,
		"skip":  skip,
		"count": count,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &favourites,
	}
	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {
	}
	return
}

func GetUserCollections(ref string, count int, skip int) (collections Collections) {
	stmt := `MATCH (u:User {ref: {ref}})-[:OWNS_COLLECTION]->(c:Collection)
		RETURN c.description as description, c.name as name, c.image as image
	`
	params := neoism.Props{
		"ref":   ref,
		"skip":  skip,
		"count": count,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &collections,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {
		fmt.Printf("\nCollections: %#v\n", collections)
	}
	return
}
func GetUserUploads(ref string, count int, skip int) (uploads Ankaras) {
	stmt := `MATCH (u:User {ref: {ref}}), (a:Ankara)<-[:OWNS]-(u), (a)-[:IS]->(s:Style), (a)-[:COLOURED]->(c:Colour)
	OPTIONAL MATCH (a)<-[f:FAVOURITED]-(u:User) 
	WITH u, a, collect(DISTINCT s.name) AS styles, collect(DISTINCT c.name) AS colours, count(DISTINCT f) as favourites
	RETURN a.ref AS ref, u.ref as ownerRef, u.name as ownerName, a.gender as gender, a.image AS image, a.views AS views, a.imageHeight AS imageHeight, a.imageWidth AS imageWidth, styles AS styles, colours AS colours, favourites AS favourites SKIP {skip} LIMIT {count}`

	params := neoism.Props{
		"ref":   ref,
		"skip":  skip,
		"count": count,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &uploads,
	}
	err := db.Cypher(&cq)

	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {
		fmt.Printf("Uploads: %v\n", uploads)
	}
	return
}

/* END DASHBOARD */

func (this *Profile) Save() {

}

func (this *Profile) FetchFromDB(username string) {
	// stmt := `MATCH (u:User) WHERE u.username = {username}`
}

type ProfileDisplay struct {
	Ref         string
	DisplayName string
	Profile
}

func (this *ProfileDisplay) IsValid() bool {
	return this.DisplayName != "" && this.ContactEmail != ""
}
func (this *ProfileDisplay) Update(username string) {
	stmt := `
		MATCH (u:User {ref: {ref}})
		MERGE (u)-[:OWNS_PROFILE]->(p:Profile)
		ON MATCH SET p.description = {description}, p.last_modified = timestamp(), u.name = {name}, p.url = {url}, p.phoneNumber = {phoneNumber}, p.contactEmail = {contactEmail}, p.isBrand = {isBrand}, p.brandName = {brandName}
        `
	params := neoism.Props{
		"ref":          username,
		"description":  this.Description,
		"name":         this.DisplayName,
		"url":          this.Url,
		"phoneNumber":  this.PhoneNumber,
		"contactEmail": this.ContactEmail,
		"isBrand":      this.IsBrand,
		"brandName":    this.BrandName,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     nil,
	}
	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {
	}

}
func FetchProfileFromDB(username string) (profile ProfileDisplay) {
	stmt := `MATCH (u:User {ref: {ref}})
		WITH u 
		OPTIONAL MATCH (u)-[:OWNS_PROFILE]->(p:Profile)
		OPTIONAL MATCH (u)-[own_collections:OWNS_COLLECTION]->(c:Collection)
		OPTIONAL MATCH (u)-[own_entries:OWNS]->(a:Ankara)
		RETURN u.ref as ref, u.name as displayName, count(c) as collections, count(a) as uploads, p.isBrand as isBrand, p.brandName as brandName, p.description as description, p.gender as gender, p.profile_type as type, p.url as url, p.contactEmail as contactEmail`
	params := neoism.Props{
		"ref": username,
	}
	profileDisplays := []ProfileDisplay{}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &profileDisplays,
	}
	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	} else {
		fmt.Printf("\nProfile: %#v\n", profileDisplays)
	}
	if len(profileDisplays) > 0 {
		profile = profileDisplays[0]
	}
	return profile
}

type Ankara struct {
	// Id     int64 //`json:"-"`
	Ref    string
	Gender string `binding:"required"`
	// Title       string //`binding:"required"`
	Description string   `binding:"required"`
	Colours     []string `binding:"required"`
	CreatedAt   int64
	ModifiedAt  time.Time
	DeletedAt   time.Time `json:"-"`
	Styles      []string  `binding:"required"`
	EntryImage
	Edit           bool `sql:"-"`
	Hide           bool
	IsInFavourites bool
	OwnerRef       string
	OwnerName      string
	Views          int32
	Favourites     int32
}

func (this *Ankara) IsInUsersFavourites(userRef string) bool {
	stmt := `MATCH (a:Ankara {ref:{ref}})<-[f:FAVOURITED]-(u:User {ref: {userRef}})
			RETURN a.ref`
	res := []struct {
		Ref string
	}{}
	params := neoism.Props{
		"ref":     this.Ref,
		"userRef": userRef,
	}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
		return false
	}
	fmt.Printf("\nOwner's Entries: %#v\n", res)
	return len(res) > 0
}
func GetSingleAnkara(ref string, trackHit bool) Ankara {
	var viewCountQuery string
	if trackHit {
		viewCountQuery = "SET a.views = coalesce(a.views, 0) + 1 "
	} else {
		viewCountQuery = ""
	}
	stmt := `MATCH (s:Style)<-[is:IS]-(a:Ankara {ref:{ref}})<-[:OWNS]-(u:User)
		WITH a, u, collect(s.name) as styles
		MATCH (a)-[coloured:COLOURED]->(c:Colour)
		WITH a, u, styles, collect(c.name) as colours
		OPTIONAL MATCH (a)<-[f:FAVOURITED]-(:User) 
		WITH a, u, styles, colours, count(f) as favourites ` +
		viewCountQuery +
		`RETURN a.ref as ref, u.ref as ownerRef, u.name as ownername, a.image as image, a.description as description,  toInt(a.created) as createdAt, a.gender as gender, a.imageHeight as imageHeight, a.imageWidth as imageWidth, a.views as views, colours, styles, favourites
    `
	params := neoism.Props{
		"ref": ref,
	}

	// query results
	res := []Ankara{}
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\n\n\n\nSINGLE: %#v\n\n\n", res[0])
	// for k,v := range Ank
	if len(res) > 0 {
		return res[0]
	} else {
		return Ankara{}
	}
}

func GetRelatedAnkara(ref string) []Ankara {
	stmt := `MATCH  (entry:Ankara {ref: {ref}})-[:IS|:COLOURED]->(attribute)<-[relationships:IS|:COLOURED]-(a:Ankara)<-[:OWNS]-(u:User)
	WHERE NOT entry = a
	RETURN distinct a.ref as ref, u.ref as ownerRef, u.name as ownername, a.image as image, a.imageHeight as imageHeight, a.imageWidth as imageWidth, a.created as created, collect(relationships), rand() as r
	ORDER BY length(collect(relationships)) DESC, r DESC LIMIT 5`
	params := neoism.Props{
		"ref": ref,
	}

	// query results
	res := []Ankara{}
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\nEntries: %#v\n", res)
	return res
}

func FetchMatchingAnkara(query SearchQuery) []Ankara {
	collectionSubQuery := ""
	favouritedBy := ""
	ownedBy := ""
	params := neoism.Props{
		"queryColours": query.Colours,
		"queryStyles":  query.Styles,
		"queryGenders": query.Gender,
		"skip":         (query.Page - 1) * ENTRIES_PER_PAGE,
		"limit":        ENTRIES_PER_PAGE,
	}
	if query.Collection != "" {
		collectionSubQuery = `, (co: Collection {ref: {collectionRef}})<-[:IN_COLLECTION]-(a)`
		params["collectionRef"] = query.Collection
	} else if query.FavouritedBy != "" {
		favouritedBy = `, (f:User {ref: {favouritedBy}})-[:FAVOURITED]->(a)`
		params["favouritedBy"] = query.FavouritedBy
	} else if query.Owner != "" {
		ownedBy = `, (o: User {ref: {ownerRef}})-[:OWNS]->(a)`
		params["ownerRef"] = query.Owner
	}
	stmt := `
		MATCH (s:Style)<-[is:IS]-(a:Ankara)-[coloured:COLOURED]->(c:Colour)` +
		collectionSubQuery +
		favouritedBy +
		ownedBy +
		`
		WHERE c.name in {queryColours} AND s.name in {queryStyles} AND a.gender IN {queryGenders}
		WITH distinct a as entry, collect(distinct s.name) as styles, collect(distinct c.name) as colours
		MATCH (entry)<-[:OWNS]-(u:User)
		WITH entry, u, styles, colours
		ORDER BY toInt(entry.created) DESC
		RETURN u.ref as ownerRef, u.name as ownerName, entry.ref as ref, entry.gender as gender, entry.image as image, entry.imageHeight as imageHeight, entry.imageWidth as imageWidth, toInt(entry.created) as createdAt, styles, colours SKIP {skip} LIMIT {limit}
    `
	fmt.Printf("\n\n\n%s\n\n\n", stmt)
	res := []Ankara{}
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	return res
}
func GetMultipleAnkaraFromDB(skip int64) []Ankara {
	stmt := `
		MATCH (s:Style)<-[is:IS]-(a:Ankara)<-[:OWNS]-(u:User)
		WITH a, u, collect(s.name) as styles
		MATCH (a)-[coloured:COLOURED]->(c:Colour)
		WITH a, u, styles, collect(c.name) AS colours
		ORDER BY toInt(a.created) DESC
		RETURN a.ref as ref, u.ref as ownerRef, u.name as ownername, a.image as image, a.imageHeight as imageHeight, a.imageWidth as imageWidth, toInt(a.created) as createdAt, colours, styles SKIP {skip} LIMIT {limit}
    `
	// query params
	params := neoism.Props{
		"skip":  skip,
		"limit": ENTRIES_PER_PAGE,
	}

	// query results
	res := []Ankara{}
	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	fmt.Printf("\nEntries: %#v\n", res)
	// for k,v := range Ank
	return res
}

func (this *Ankara) CanBeCreatedBy(userId interface{}) bool {
	return (userId.(string) != "")
}
func (this *Ankara) CanBeUpdatedBy(userId interface{}) bool {
	//get Owner!!
	return genericAuthorization(this, userId.(int64))
}

func (this *Ankara) CanBeDeletedBy(userId interface{}) bool {
	return genericAuthorization(this, userId.(int64))
}

func (this *Ankara) GetOwnerId() int64 {
	// return this.OwnerId
	return 1
}

func generateTimeStamp() int {
	currentTime := time.Now().UTC()
	creationTime, _ := strconv.Atoi(fmt.Sprintf("%d%02d%02d%02d%02d%02d", currentTime.Year(), currentTime.Month(), currentTime.Day(), currentTime.Hour(), currentTime.Minute(), currentTime.Second()))
	return creationTime
}
func (this *Ankara) Create(owner string) {
	colours := []string{}

	//Cast to interfaces!!
	coloursInterface := ConvertToInterface(this.Colours)
	styleInterface := ConvertToInterface(this.Styles)
	for colour := range mapset.NewSetFromSlice(coloursInterface).Intersect(COLOURS).Iter() {
		colours = append(colours, colour.(string))
	}
	styles := []string{}
	for style := range mapset.NewSetFromSlice(styleInterface).Intersect(STYLES).Iter() {
		styles = append(styles, style.(string))
	}
	this.Ref, _ = randutil.String(10, randutil.Alphabet)
	creationTime := generateTimeStamp()
	stmt := `
        MATCH (s:Style), (c:Colour), (o:User {ref: {userRef}})
        WHERE s.name IN {styles} AND c.name IN {colours}
        WITH c,s,o
        CREATE UNIQUE (s)<-[:IS]-(entry:Ankara {ref:{ref}, created:{createdAt}, imageHeight: {imageHeight}, imageWidth:{imageWidth}, description:{description}, gender:{gender}, image:{image}, view: 0})-[:COLOURED]->(c)
        CREATE UNIQUE (o)-[:OWNS]->(entry)
        RETURN entry
    `
	// query params
	// actor := "Tom Hanks"
	params := neoism.Props{
		"userRef":     owner,
		"ref":         this.Ref,
		"colours":     colours,
		"styles":      styles,
		"imageHeight": this.ImageHeight,
		"imageWidth":  this.ImageWidth,
		"gender":      this.Gender,
		"description": this.Description,
		"image":       this.Image,
		"createdAt":   creationTime,
	}

	// query results
	res := []struct {
		ankara neoism.Node
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
}

func (this *Ankara) Delete() bool {
	// Return if no reference is given
	if this.Ref == "" {
		return false
	}
	// Delete Ankara from DB
	stmt := `MATCH (a:Ankara {ref:{ref}})-[f]-(b) DELETE f,a`
	params := neoism.Props{
		"ref": this.Ref,
	}
	res := []struct {
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	return err == nil
}
func (this *Ankara) Update(editedAnkara *Ankara) {
	colours := []string{}
	coloursInterface := ConvertToInterface(editedAnkara.Colours)
	styleInterface := ConvertToInterface(editedAnkara.Styles)
	for colour := range mapset.NewSetFromSlice(coloursInterface).Intersect(COLOURS).Iter() {
		colours = append(colours, colour.(string))
	}
	styles := []string{}
	for style := range mapset.NewSetFromSlice(styleInterface).Intersect(STYLES).Iter() {
		styles = append(styles, style.(string))
	}
	fmt.Printf("\n\nColours: %#v\n\n", styles)
	stmt := `
        MERGE (a:Ankara {ref: {ref}})
        ON MATCH SET a.modified = timestamp(), a.imageHeight = {imageHeight}, a.imageWidth = {imageWidth}, a.description = {description}, a.gender = {gender}, a.image = {image}
        WITH a
        MATCH (a)-[old:IS|:COLOURED]-(property) 
        DELETE old
        WITH a
        MATCH (style:Style)
        WHERE style.name IN {styles}
        MERGE (a)-[:IS]->(style)
		WITH a 
		MATCH (colour:Colour)
        WHERE colour.name IN {colours}
        MERGE (a)-[:COLOURED]->(colour)
    `
	params := neoism.Props{
		"ref":         this.Ref,
		"colours":     colours,
		"styles":      styles,
		"imageHeight": editedAnkara.ImageHeight,
		"imageWidth":  editedAnkara.ImageWidth,
		"gender":      editedAnkara.Gender,
		"description": editedAnkara.Description,
		"image":       editedAnkara.Image,
	}

	// query results
	res := []struct {
		ankara neoism.Node
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	// pnicErr(err)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
}

/* Stats */
type Stats struct {
	Id         int64
	Ref        string
	Date       time.Time
	Views      int32
	Favourites int32
}

/* End Stats */

/* Favourites */
type Favourite struct {
	Id int64
	// Item Ankara
	AnkaraId  int64
	OwnerId   int64
	CreatedAt time.Time
}

func (this *Favourite) CanBeCreatedBy(userId interface{}) bool {
	return (userId.(string) != "")
}

func (this *Favourite) CanBeUpdatedBy(userId interface{}) bool {
	return genericAuthorization(this, userId.(int64))
}

func (this *Favourite) CanBeDeletedBy(userId interface{}) bool {
	return genericAuthorization(this, userId.(int64))
}

func AddFavouriteToDB(ankaraRef string, userRef string) error {
	stmt := `
		MATCH (a:Ankara {ref: {ankaraRef}}), (u:User {ref: {userRef}}) 
        MERGE (a)<-[f:FAVOURITED]-(u)
        ON CREATE SET f.created = timestamp()
        RETURN f
    `
	// query params
	params := neoism.Props{
		"ankaraRef": ankaraRef,
		"userRef":   userRef,
	}

	// query results
	res := []struct {
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	return err
}

func DeleteFavouriteFromDB(ankaraRef string, userRef string) error {
	stmt := `
		MATCH (a:Ankara {ref: {ankaraRef}}) <-[f:FAVOURITED]- (u:User {ref: {userRef}}) 
        DELETE f
    `
	// query params
	params := neoism.Props{
		"ankaraRef": ankaraRef,
		"userRef":   userRef,
	}

	// query results
	res := []struct {
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	// execute query

	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	return err
}
func (this *Favourite) GetOwnerId() int64 {
	return this.OwnerId
}
func (this *Favourite) Save() {
	// DB.Save(this)
}

/* End Favourites */

func CreateUserCollection(user string, name string, isHidden bool) bool {
	stmt := `
	MATCH (u:User {ref: {user}})
	WITH u CREATE (c:Collection {ref: {ref}, name: {name}, hidden: {hidden}, createdAt: {createdAt}})
	MERGE (u)-[:OWNS_COLLECTION]->(c)
	`
	collectionId, _ := randutil.String(10, randutil.Alphabet)
	params := neoism.Props{
		"name":      name,
		"user":      user,
		"ref":       collectionId,
		"hidden":    isHidden,
		"createdAt": generateTimeStamp(),
	}

	// query results
	res := []struct {
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}

	return err == nil
}
func CreateUserCollectionItem(user string, collection string, entry string) bool {
	stmt := `
	MATCH (a: Ankara {ref: {entry}}), (c:Collection {ref: {collection}})<-[:OWNS_COLLECTION]-(u:User {ref: {user}})
	MERGE (a)-[:IN_COLLECTION]->(c)
	`
	params := neoism.Props{
		"entry":      entry,
		"collection": collection,
		"user":       user,
	}

	// query results
	res := []struct {
	}{}

	// construct query
	cq := neoism.CypherQuery{
		Statement:  stmt,
		Parameters: params,
		Result:     &res,
	}
	err := db.Cypher(&cq)
	if err != nil {
		fmt.Printf("\nError: %v\n", err)
	}
	return err == nil
}

type Tag struct {
	AnkaraId int64 `json:"-"`
	Id       int64 `json:"-"`
	Name     string
}
