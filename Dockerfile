FROM scratch

MAINTAINER Victor Akujobi, <victor@stylestation.co>

ADD ./stylestation /
ADD ./conf.gcfg /

EXPOSE 3000

ENTRYPOINT  ["/stylestation"]
